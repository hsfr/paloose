#!/bin/bash

echo "================ (no cache) concurrent=10" >> /Users/hsfr/Library/Logs/siege/siege.log
siege --log "/Users/hsfr/Library/Logs/siege/siege.log" --reps=5 --delay=0.5 --concurrent=1 http://tux/hv7/catalogue.html
siege --log "/Users/hsfr/Library/Logs/siege/siege.log" --reps=5 --delay=0.5 --concurrent=4 http://tux/hv7/catalogue.html
siege --log "/Users/hsfr/Library/Logs/siege/siege.log" --reps=5 --delay=0.5 --concurrent=8 http://tux/hv7/catalogue.html
siege --log "/Users/hsfr/Library/Logs/siege/siege.log" --reps=5 --delay=0.5 --concurrent=12 http://tux/hv7/catalogue.html
siege --log "/Users/hsfr/Library/Logs/siege/siege.log" --reps=5 --delay=0.5 --concurrent=16 http://tux/hv7/catalogue.html
siege --log "/Users/hsfr/Library/Logs/siege/siege.log" --reps=5 --delay=0.5 --concurrent=20 http://tux/hv7/catalogue.html
siege --log "/Users/hsfr/Library/Logs/siege/siege.log" --reps=5 --delay=0.5 --concurrent=24 http://tux/hv7/catalogue.html

#
