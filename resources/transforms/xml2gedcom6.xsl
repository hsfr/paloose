<?xml version="1.0"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--
   
   WARNING - Work in progress - this is a stub to be completed later !!!!!!!
   
   Author:
   Name  : Hugh Field-Richards
   Email : hsfr@hsfr.org.uk
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   Date                   Who    Changes
   ==========================================================================
   
   July 2008              HSFR   Created
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   All copyright in this software is the property of Hugh Field-Richards.
   The software is published under the terms of the LGPL License a copy of
   which has been included with this distribution in the LICENSE file.
   
-->

<xsl:stylesheet
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:paloose="http://www.paloose.org/schemas/Paloose/1.0"
   version="1.0">

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="/">
      <xsl:apply-templates mode="processDoc"/>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="gedcom" mode="processDoc">
      <xsl:element name="gedcom">
         <xsl:apply-templates mode="processHeader" />
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="HEAD" mode="processHeader">
      <xsl:element name="HeaderRec">
         <xsl:element name="FileCreation">
            <xsl:attribute name="date">
               <xsl:value-of select="//HEAD/DATE/@data"/>
            </xsl:attribute>
         </xsl:element>
         <xsl:apply-templates mode="processDoc" />
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="SOUR" mode="processHeader">
      <xsl:element name="Product">
         <xsl:element name="Name">
            <xsl:value-of select="//HEAD/SOUR/@data"/>
         </xsl:element>
         <xsl:apply-templates mode="processSource" />
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="VERS" mode="processSource">
      <xsl:element name="Version">
         <xsl:value-of select="//HEAD/VERS/@data"/>
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="CORP" mode="processSource">
      <xsl:element name="Version">
         <xsl:value-of select="//HEAD/VERS/@data"/>
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="//SOUR/DATE" mode="processProduct">
      <xsl:element name="Version">
         <xsl:value-of select="//HEAD/VERS/@data"/>
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Pass through all other tags
   -->
   
   <xsl:template match="node()|@*" mode="processDoc" >
      <xsl:copy>
         <xsl:apply-templates select="@*" mode="processDoc" />
         <xsl:apply-templates mode="processDoc" />
      </xsl:copy>
   </xsl:template>
   
</xsl:stylesheet>
