<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--
   
   This stylesheet is part of a two part process that merges a Paloose Forms
   document into a final document. It is heavily based (= plagiarised) on JXForms
   that was used in old versions of Cocoon and credit must be given to the main
   authors below. I have changed many things in it so beware!
   
   JXForms Authors:
      Ivelin Ivanov, ivelin@apache.org, May 2002
      Konstantin Piroumian <kpiroumian@protek.com>, September 2002
      Simon Price <price@bristol.ac.uk>, September 2002
   
   Modified for Paloose forms:
      Hugh Field-Richards, <hsfr@hsfr.org.uk>
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   Date                   Who    Changes
   ==========================================================================
   
   14th February 2007     HSFR   Heavily modified for Paloose
   
-->

<xsl:stylesheet
   version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:pf="http://www.paloose.org/schemas/Forms/1.0">
   
   <xsl:output encoding="UTF-8"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="/">
      <xsl:apply-templates/>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Process the form into an HTML table framework. It does not translate the individual
      pforms items yet, that is done by pforms-html.xsl. It also processes the errors that
      may have occurred and modify the form to show them.
   -->
   
   <xsl:template match="pf:form">
      <xsl:element name="pf:form">
         <xsl:attribute name="method">post</xsl:attribute>
         <xsl:attribute name="action">
            <xsl:value-of select="concat( @flow, '.kont?__session=', @session )"/>
         </xsl:attribute>
         <xsl:copy-of select="@*"/>
         
         <xsl:element name="table">
            <xsl:attribute name="width">100%</xsl:attribute>
            <xsl:attribute name="align">left</xsl:attribute>
            <xsl:attribute name="border">0</xsl:attribute>
            
            <!-- Heading of complete form -->
            <xsl:element name="tr">
               <xsl:element name="td">
                  <xsl:attribute name="align">left</xsl:attribute>
                  <xsl:attribute name="colspan">3</xsl:attribute>
                  <xsl:attribute name="class">samplesGroupRow</xsl:attribute>
                  <xsl:value-of select="pf:label"/>
               </xsl:element> <!-- td -->
            </xsl:element> <!-- tr -->
            
            <!-- Say how many errors there and what they are. I will need to sort out locale in this sometime -->
            <xsl:if test="count( //pf:violations/pf:violation ) > 0">
               <xsl:element name="tr">
                  <xsl:element name="td">
                     <xsl:attribute name="align">left</xsl:attribute>
                     <xsl:attribute name="colspan">3</xsl:attribute>
                     <!-- Take global class from first violations element -->
                     <xsl:attribute name="class">violationsBlock</xsl:attribute>
                     <xsl:choose>
                        <!-- A simple nicety to cope with good Engish :-) -->
                        <xsl:when test="count( //pf:violations/pf:violation ) = 1">
                           <xsl:element name="p">
                              <xsl:text>* There is </xsl:text>
                              <xsl:element name="b">
                                 <xsl:value-of select="count(//pf:violation)"/>
                              </xsl:element> <!-- b -->
                              <xsl:text> error. Please fix and submit the form again.</xsl:text>
                           </xsl:element> <!-- p -->
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:element name="p">
                              <xsl:text>* There are </xsl:text>
                              <xsl:element name="b">
                                 <xsl:value-of select="count(//pf:violation)"/>
                              </xsl:element> <!-- b -->
                              <xsl:text> errors. Please fix and submit the form again.</xsl:text>
                           </xsl:element> <!-- p -->
                        </xsl:otherwise>
                     </xsl:choose>
                     
                  </xsl:element> <!-- td -->
               </xsl:element> <!-- tr -->
            </xsl:if>
            
            <!-- Process each of the elements in turn exccept for the submit -->
            <xsl:for-each select="*[name() != 'pf:submit']">
               <xsl:choose>
                  <xsl:when test="name() = 'error'"/>
                  <xsl:when test="name() = 'pf:label'"/>
                  <xsl:when test="pf:*">
                     <xsl:apply-templates select="."/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:copy-of select="."/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:for-each>
            
            <!-- Finally put in the submit buttons -->
            <xsl:element name="tr">
               <xsl:element name="td">
                  <xsl:attribute name="align">center</xsl:attribute>
                  <xsl:attribute name="colspan">3</xsl:attribute>
                  <xsl:for-each select="*[name() = 'pf:submit']">
                     <xsl:copy-of select="."/>
                     <xsl:text>
                     </xsl:text>
                  </xsl:for-each>
               </xsl:element> <!-- td -->
            </xsl:element> <!-- tr -->
            
         </xsl:element> <!-- table -->
      </xsl:element> <!-- form -->
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      We expand group here as it is a structural part of the form rather
      than a field of the form.
   -->
   
   <xsl:template match="pf:group">
      <xsl:element name="tr">
         <xsl:attribute name="width">100%</xsl:attribute>
         <xsl:element name="td">
            <xsl:attribute name="width">100%</xsl:attribute>
            <xsl:attribute name="colspan">2</xsl:attribute>
            <xsl:attribute name="border">0</xsl:attribute>
            <xsl:element name="table">
               <xsl:attribute name="class">group</xsl:attribute>
               <xsl:attribute name="border">0</xsl:attribute>
               <xsl:element name="tr">
                  <xsl:element name="td">
                     <xsl:attribute name="align">left</xsl:attribute>
                     <xsl:attribute name="colspan">2</xsl:attribute>
                     <xsl:attribute name="border">1</xsl:attribute>
                     <xsl:value-of select="pf:label"/>
                  </xsl:element> <!-- td -->
               </xsl:element> <!-- tr -->
               <xsl:apply-templates select="*"/>
            </xsl:element> <!-- table -->
         </xsl:element> <!-- td -->
      </xsl:element> <!-- tr -->
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="pf:output[@form]">
      <br clear="all"/>
      <br/>
      <font size="-1">
         <code>
            <xsl:value-of select="pf:label"/> : <xsl:copy-of select="."/>
         </code>
      </font>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="pf:label"/>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Process all other PForm elements
   -->
   
   <xsl:template match="pf:*">
      <xsl:element name="tr">
         <xsl:call-template name="pf.outputLabel"/>
         <xsl:call-template name="pf.outputFormElement"/>
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template name="pf.outputLabel">
      <xsl:element name="td">
         <xsl:attribute name="align">left</xsl:attribute>
         <xsl:attribute name="valign">center</xsl:attribute>
         <xsl:element name="p">
            <xsl:attribute name="class">label</xsl:attribute>
            <xsl:value-of select="pf:label"/>
         </xsl:element> <!-- p -->
      </xsl:element> <!-- td -->
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template name="pf.outputFormElement">
      <xsl:element name="td">
         <xsl:attribute name="align">left</xsl:attribute>
         <xsl:element name="table">
            <xsl:attribute name="class">plaintable</xsl:attribute>
            <xsl:element name="tr">
               <xsl:element name="td">
                  <xsl:attribute name="align">left</xsl:attribute>
                  <xsl:copy-of select="."/>
               </xsl:element>
               <xsl:if test="pf:violations/pf:violation">
                  <xsl:element name="td">
                     <xsl:attribute name="align">left</xsl:attribute>
                     <xsl:attribute name="width">100%</xsl:attribute>
                     <xsl:attribute name="class">violationsField</xsl:attribute>
                     <xsl:for-each select="pf:violations/pf:violation">
                        <xsl:text>* </xsl:text>
                        <xsl:value-of select="."/>
                        <xsl:element name="br"/>
                     </xsl:for-each>
                  </xsl:element>
               </xsl:if>
            </xsl:element>
         </xsl:element>
         <xsl:if test="pf:help">
            <xsl:element name="div">
               <xsl:attribute name="class">help</xsl:attribute>
               <xsl:value-of select="pf:help"/>
            </xsl:element>
            <xsl:element name="br"/>
         </xsl:if>
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Pass through anything not eaten by the above
   -->
   
   <xsl:template match="@*|node()" priority="-1">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>
   
</xsl:stylesheet>
