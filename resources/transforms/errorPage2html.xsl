<?xml version="1.0"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--
	
	Error Page to HTML Transform
	============================
	
	This tranform takes a standard HTML error page (either the user's or the default
	one) and inserts the extra error message stuff.
	
	Author:
   	Name  : Hugh Field-Richards
   	Email : hsfr@hsfr.org.uk
	
	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	
	Date                   Who    Changes
	==========================================================================
	
	7th June 2005          HSFR   Created
	
	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	
  LICENSE:
  
  This software is free software; you can redistribute it
  and/or modify it under the terms of the GNU Lesser General
  Public License as published by the Free Software Foundation;
  either version 2.1 of the License, or (at your option) any
  later version.
  
  This software is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	
	<xsl:param name="errorCode"/>
	<xsl:param name="errorMessage"/>
	<xsl:param name="errorContext"/>
	<xsl:param name="styleSheet"/>
	
	<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
	<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
	
	<xsl:template match="error-code">
		<xsl:value-of select="$errorCode"/>
	</xsl:template>
	
	<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
	
	<xsl:template match="link[ @rel = 'stylesheet']">
		<xsl:element name="link">
			<xsl:attribute name="href">
				<xsl:choose>
					<xsl:when test="@href"><xsl:value-of select="$styleSheet"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="@href"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="rel">stylesheet</xsl:attribute>
			<xsl:attribute name="type">text/css</xsl:attribute>
		</xsl:element>
	</xsl:template>
	
	<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
	
	<xsl:template match="error-message">
		<xsl:value-of select="$errorMessage"/>	
	</xsl:template>
	
	<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
	
	<xsl:template match="error-context">
		<xsl:value-of select="$errorContext"/>	
	</xsl:template>
	
	<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
	<!-- 
		Soak up any remaining elements not processed by the above
	-->
	
	<xsl:template match="node()|@*" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>

