<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--
	
	RELAX NG Schema to XHTML Pretty Printer
	
	This XSLT style sheet pretty prints a RELAX NG schema. It has been tested
	on the current Xalan processor and renders to most browsers (Safari, IE, Mozilla,
	Netscape etc). It was inspired by the DSD-1 Schema pretty printer written by Nils
	Klarlund for the original DSD-1, and mine for the DSD-2 Schema language - the
	best (unknown) schema language there is.
	
	Schematron inserts are also fully supported.
	
	Author:
   	Name  : Hugh Field-Richards.
   	Email : hsfr@hsfr.org.uk
	
	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	
	Date                   Who    Changes
	==========================================================================

   9th February 2005      HSFR   Created
	15th February 2005     HSFR   Issued
	8th March 2006         HSFR   Corrected nsName problem (not present)
	17th November 2009     HSFR   Major cleaning up
	18th September 2011    HSFR   Added raw display of XML
	
   LICENSE:
   
   This software is free software; you can redistribute it
   and/or modify it under the terms of the GNU Lesser General
   Public License as published by the Free Software Foundation;
   either version 2.1 of the License, or (at your option) any
   later version.
   
   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.
   
   You should have received a copy of the GNU Lesser General Public
   License along with this software; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
	
-->
    
<!DOCTYPE xsl:stylesheet [
    <!ENTITY lt       "&#38;#60;" >     <!-- less-than sign -->
    <!ENTITY gt       "&#62;" >         <!-- greater-than sign -->
    <!ENTITY nbsp     "&#160;" >        <!-- non breaking space -->
    <!ENTITY dash     "&#x2010;" >      <!-- hyphen -->
    <!ENTITY ndash    "&#x2013;" >      <!-- en dash -->
    <!ENTITY mdash    "&#x2014;" >      <!-- em dash -->
    <!ENTITY ldquo    "&#x201c;" >      <!-- Left Double Quote -->
    <!ENTITY rdquo    "&#x201d;" >      <!-- Right Double Quote -->
    <!ENTITY lsquo    "&#x2018;" >      <!-- Left Single Quote -->
    <!ENTITY rsquo    "&#x2019;" >      <!-- Right Single Quote -->
    <!ENTITY bull     "&#x2022;" >      <!-- round bullet, filled -->
    <!ENTITY nbsp     "&#160;" >        <!-- no break space -->
    <!ENTITY rarr     "&#x2192;" >      <!-- right arrow -->
    <!ENTITY copy     "&#169;" >        <!-- copyright -->
] >
    
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://relaxng.org/ns/structure/1.0" xmlns:rng="http://relaxng.org/ns/structure/1.0"
   xmlns:a="http://relaxng.org/ns/annotation/1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xhtml="http://www.w3.org/1999/xhtml"
   xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:php="http://php.net/xsl" exclude-result-prefixes="php" version="1.0" xml:lang="en" id="rng-html">

   <xsl:output version="1.0" encoding="UTF-8"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Parameters brought in from sitemap
   -->

   <xsl:param name="stylesDir"/>
   <xsl:param name="scriptsDir"/>
   <xsl:param name="jQueryScriptsDir"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Global variables
   -->

   <xsl:variable name="gCSSFilesDir" select="$stylesDir"/>
   <xsl:variable name="gCSSFiles" select="'prettySchematron.css,prettyRNG.css'"/>

   <xsl:variable name="gScriptFilesDir" select="$scriptsDir"/>
   <xsl:variable name="gScriptFiles" select="'rng.js'"/>

   <xsl:variable name="gJQueryScriptFilesDir" select="$jQueryScriptsDir"/>
   <xsl:variable name="gJQueryScriptFiles" select="'jquery.min.js;jquery-ui-1.8.16.custom.min.js'"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!-- 
		Change this file if you change the way Schemtron is dealt with
	-->

   <xsl:include href="schematron2xhtml.xsl"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->

   <xsl:template match="/">
      <xsl:element name="html">
         <xsl:call-template name="output.header"/>
         <xsl:call-template name="output.body"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->

   <xsl:template name="output.header">
      <xsl:element name="head">
         <xsl:call-template name="buildHeader.htmlHead">
            <xsl:with-param name="inTitle">
               <xsl:choose>
                  <xsl:when test="//dc:title">
                     <xsl:value-of select="//dc:title"/>
                  </xsl:when>
                  <xsl:otherwise>Untitled</xsl:otherwise>
                  <!-- Necessary for Safari -->
               </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="inStylesList" select="$gCSSFiles"/>
            <xsl:with-param name="inStylesDir" select="$gCSSFilesDir"/>
            <xsl:with-param name="inScriptsList" select="$gScriptFiles"/>
            <xsl:with-param name="inScriptsDir" select="$gScriptFilesDir"/>
            <xsl:with-param name="inJQueryScriptsList" select="$gJQueryScriptFiles"/>
            <xsl:with-param name="inJQueryScriptsDir" select="$gJQueryScriptFilesDir"/>
         </xsl:call-template>

      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
     param inTitle the title of the page
     param inStylesList the list of style files to output (file-1,file-2,file-3, ...)
   -->

   <xsl:template name="buildHeader.htmlHead">
      <xsl:param name="inTitle"/>
      <xsl:param name="inStylesList"/>
      <xsl:param name="inStylesDir"/>
      <xsl:param name="inScriptsList"/>
      <xsl:param name="inScriptsDir"/>
      <xsl:param name="inJQueryScriptsList"/>
      <xsl:param name="inJQueryScriptsDir"/>

      <xsl:element name="meta">
         <xsl:attribute name="name">DC.creator</xsl:attribute>
         <xsl:attribute name="content">Hugh Field-Richards</xsl:attribute>
      </xsl:element>

      <xsl:element name="meta">
         <xsl:attribute name="name">DC.language</xsl:attribute>
         <xsl:attribute name="scheme">RFC1766</xsl:attribute>
         <xsl:attribute name="content">en</xsl:attribute>
      </xsl:element>

      <xsl:element name="meta">
         <xsl:attribute name="name">DC.Title</xsl:attribute>
         <xsl:attribute name="lang">en</xsl:attribute>
         <xsl:attribute name="content">
            <xsl:value-of select="$inTitle"/>
         </xsl:attribute>
      </xsl:element>

      <xsl:element name="title">
         <xsl:value-of select="$inTitle"/>
      </xsl:element>

      <xsl:call-template name="buildHeader.outputScriptsList">
         <xsl:with-param name="inScriptsList" select="$inJQueryScriptsList"/>
         <xsl:with-param name="inScriptsDir" select="$inJQueryScriptsDir"/>
      </xsl:call-template>

      <xsl:call-template name="buildHeader.outputScriptsList">
         <xsl:with-param name="inScriptsList" select="$inScriptsList"/>
         <xsl:with-param name="inScriptsDir" select="$inScriptsDir"/>
      </xsl:call-template>

      <xsl:call-template name="buildHeader.outputStylesList">
         <xsl:with-param name="inStylesList" select="$inStylesList"/>
         <xsl:with-param name="inStylesDir" select="$inStylesDir"/>
      </xsl:call-template>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
    	Nothing should normally need to be changed below this line. All the specific
    	meta-data stuff and styles are done above.
    -->

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output a list of comma separated style files as
      
      <link href="file-1" ... />
      <link href="file-2" ... />
      <link href="file-3" ... />
      
      param inStylesList the list of style files to output (file-1,file-2,file-3, ...)
   -->

   <xsl:template name="buildHeader.outputStylesList">
      <xsl:param name="inStylesList"/>
      <xsl:param name="inStylesDir"/>

      <xsl:choose>
         <xsl:when test="contains($inStylesList, ',')">
            <xsl:variable name="firstStyle" select="substring-before($inStylesList, ',')"/>
            <xsl:variable name="remainderStyles" select="substring-after($inStylesList, ',')"/>
            <xsl:call-template name="buildHeader.outputStyle">
               <xsl:with-param name="inStyle" select="$firstStyle"/>
               <xsl:with-param name="inStylesDir" select="$inStylesDir"/>
            </xsl:call-template>
            <xsl:call-template name="buildHeader.outputStylesList">
               <xsl:with-param name="inStylesList" select="$remainderStyles"/>
               <xsl:with-param name="inStylesDir" select="$inStylesDir"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="buildHeader.outputStyle">
               <xsl:with-param name="inStyle" select="$inStylesList"/>
               <xsl:with-param name="inStylesDir" select="$inStylesDir"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output a single style files as <link href="inStyle" ... />
      
      param inStyle the styles file name
   -->

   <xsl:template name="buildHeader.outputStyle">
      <xsl:param name="inStyle"/>
      <xsl:param name="inStylesDir"/>
      <xsl:element name="link">
         <xsl:attribute name="href">
            <xsl:value-of select="concat($inStylesDir, $inStyle)"/>
         </xsl:attribute>
         <xsl:attribute name="rel">stylesheet</xsl:attribute>
         <xsl:attribute name="type">text/css</xsl:attribute>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template name="buildHeader.outputScriptsList">
      <xsl:param name="inScriptsList"/>
      <xsl:param name="inScriptsDir"/>

      <xsl:choose>
         <xsl:when test="contains($inScriptsList, ';')">
            <xsl:variable name="firstScript" select="substring-before($inScriptsList, ';')"/>
            <xsl:variable name="remainderScripts" select="substring-after($inScriptsList, ';')"/>
            <xsl:call-template name="buildHeader.outputScript">
               <xsl:with-param name="inScript" select="$firstScript"/>
               <xsl:with-param name="inScriptsDir" select="$inScriptsDir"/>
            </xsl:call-template>
            <xsl:call-template name="buildHeader.outputScriptsList">
               <xsl:with-param name="inScriptsList" select="$remainderScripts"/>
               <xsl:with-param name="inScriptsDir" select="$inScriptsDir"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="buildHeader.outputScript">
               <xsl:with-param name="inScript" select="$inScriptsList"/>
               <xsl:with-param name="inScriptsDir" select="$inScriptsDir"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output a single script files as <script type="text/javascript" src="js/prototype.js" ... />
   -->

   <xsl:template name="buildHeader.outputScript">
      <xsl:param name="inScript"/>
      <xsl:param name="inScriptsDir"/>

      <xsl:element name="script">
         <xsl:attribute name="src">
            <xsl:value-of select="concat($inScriptsDir, $inScript)"/>
         </xsl:attribute>
         <xsl:attribute name="type">text/javascript</xsl:attribute>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->

   <xsl:template name="output.body">
      <xsl:element name="body">
         <xsl:call-template name="mainDescription"/>
         <xsl:if test="//*[local-name() = 'anyName']">
            <xsl:element name="a">
               <xsl:attribute name="name">
                  <xsl:text>__warning</xsl:text>
               </xsl:attribute>
               <xsl:element name="div">
                  <xsl:attribute name="class">anyWarning</xsl:attribute>
                  <xsl:element name="p">
                     <xsl:text>WARNING! This schema uses ANY type elements. This is dangerous: it
                      allows anything to appear within the elements. The cost of this generality is the dismantling of the structure. Because
                      any element is allowed it is impossible to validate formally the structure of the document. See Goldfarb (The SGML
                      Handbook): "An element type that has an ANY content specification is completely unstructured."</xsl:text>>
                  </xsl:element>
               </xsl:element>
            </xsl:element>
         </xsl:if>
         <xsl:apply-templates select="//rng:grammar/*" mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!--  
    	Process the meta-data. Change this dependent on local conditions.
    -->

   <xsl:template name="mainDescription">
      <xsl:element name="div">
         <xsl:attribute name="class">mainDescription</xsl:attribute>
         <xsl:apply-templates select="//a:documentation" mode="header"/>
      </xsl:element>
   </xsl:template>

   <xsl:template match="dc:title" mode="header">
      <xsl:element name="div">
         <xsl:attribute name="id">title</xsl:attribute>
         <xsl:value-of select="//dc:title"/>
      </xsl:element>
   </xsl:template>

   <xsl:template match="dc:creator" mode="header">
      <xsl:element name="div">
         <xsl:attribute name="id">creator</xsl:attribute>
         <xsl:text>Author: </xsl:text>
         <xsl:value-of select="//dc:creator"/>
      </xsl:element>
   </xsl:template>

   <xsl:template match="dc:date" mode="header">
      <xsl:element name="div">
         <xsl:attribute name="id">date</xsl:attribute>
         <xsl:text>Date: </xsl:text>
         <xsl:value-of select="//dc:date"/>
      </xsl:element>
   </xsl:template>

   <xsl:template match="dc:language" mode="header"/>

   <!-- Make sure that we eat all the Dublin core and XHTML when processing the header
		and grammar. How I hate this language :-( -->
   <xsl:template match="*[namespace-uri() = 'http://www.w3.org/1999/xhtml']" mode="header" priority="2"/>

   <xsl:template match="*[namespace-uri() = 'http://purl.org/dc/elements/1.1/']" mode="grammar" priority="2"/>

   <xsl:template match="a:documentation" mode="grammar"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* GRAMMAR *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
    	grammarContent ::= start
    		| define
    		| <div> grammarContent* </div>
    		...
    -->

   <xsl:template match="rng:div" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
    		| <include href="anyURI"> includeContent* </include>
    
    	includeContent ::= start
    		| define
    		| <div> includeContent* </div>
    -->

   <xsl:template match="rng:include" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">primaryTitle</xsl:attribute>
         <xsl:element name="p">
            <xsl:text>Included file: </xsl:text>
            <xsl:element name="a">
               <xsl:attribute name="href">
                  <xsl:value-of select="@href"/>
               </xsl:attribute>
               <xsl:value-of select="@href"/>
            </xsl:element>
         </xsl:element>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
    	start ::= <start [combine="method"]> pattern </start>
    	define ::= <define name="NCName" [combine="method"]> pattern+ </define>
    -->

   <xsl:template match="rng:start | rng:define" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">
            <xsl:text>primaryRule</xsl:text>
         </xsl:attribute>
         <xsl:element name="div">
            <xsl:attribute name="class">primaryTitle</xsl:attribute>
            <xsl:element name="div">
               <xsl:attribute name="class">primaryTitle-left</xsl:attribute>
               <xsl:variable name="thisName" select="@name"/>
               <xsl:choose>
                  <xsl:when test="local-name() = 'start'">
                     <xsl:text>Schema Root</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:element name="a">
                        <xsl:attribute name="name">
                           <xsl:value-of select="./@name"/>
                        </xsl:attribute>
                        <xsl:value-of select="./@name"/>
                     </xsl:element>
                     <xsl:if test="count(//*[local-name() = 'ref'][@name = $thisName]) = 0">
                        <xsl:text> &#x2014; Warning! not used within this schema file.</xsl:text>
                     </xsl:if>
                  </xsl:otherwise>
               </xsl:choose>
               <xsl:if test="@combine">
                  <xsl:text>  (Combine: </xsl:text>
                  <xsl:value-of select="@combine"/>
                  <xsl:text>)</xsl:text>
               </xsl:if>
            </xsl:element>

            <xsl:element name="div">
               <xsl:attribute name="class">primaryTitle-right</xsl:attribute>
               <xsl:element name="div">
                  <xsl:attribute name="class">switchButton rawButton</xsl:attribute>
               </xsl:element>
            </xsl:element>
         </xsl:element>

         <xsl:element name="div">
            <xsl:attribute name="class">indentedBlock</xsl:attribute>
            <xsl:call-template name="render-element-name"/>
            <xsl:apply-templates mode="grammar"/>
         </xsl:element>

         <!-- Put in code block (raw) hide initially -->
         <xsl:element name="div">
            <xsl:attribute name="class">rawXML</xsl:attribute>
            <xsl:element name="pre">
               <xsl:apply-templates select="." mode="cdata">
                  <xsl:with-param name="inSpaces"/>
               </xsl:apply-templates>
            </xsl:element>
         </xsl:element>

      </xsl:element>


   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      The following outputs the raw XML 
   -->

   <xsl:template match="node()" mode="cdata">
      <xsl:param name="inSpaces" select="''"/>

      <xsl:variable name="spaces" select="$inSpaces"/>

      <xsl:choose>
         <xsl:when test="count(*) > 0">
            <xsl:value-of select="$spaces"/>
            <xsl:text>&lt;</xsl:text>
            <xsl:value-of select="name()"/>
            <xsl:apply-templates select="@*" mode="cdata"/>
            <xsl:text>&gt;&#10;</xsl:text>
            <xsl:apply-templates select="node()" mode="cdata">
               <xsl:with-param name="inSpaces" select="concat($spaces, '   ')"/>
            </xsl:apply-templates>
            <xsl:value-of select="$spaces"/>
            <xsl:text>&lt;/</xsl:text>
            <xsl:value-of select="name()"/>
            <xsl:text>&gt;&#10;</xsl:text>
         </xsl:when>
         <xsl:when test="text()">
            <xsl:variable name="attribs">
               <xsl:apply-templates select="@*" mode="cdata"/>
            </xsl:variable>
            <xsl:value-of select="concat($spaces, '&lt;', name(), $attribs, '&gt;')"/>
            <xsl:value-of select="concat(normalize-space(.), '&lt;/', name(), '&gt;&#10;')"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$spaces"/>
            <xsl:text>&lt;</xsl:text>
            <xsl:value-of select="name()"/>
            <xsl:apply-templates select="@* | node()" mode="cdata"/>
            <xsl:text>/&gt;&#10;</xsl:text>
         </xsl:otherwise>
      </xsl:choose>

   </xsl:template>

   <xsl:template match="@*" mode="cdata">
      <xsl:text> </xsl:text>
      <xsl:value-of select="name()"/>
      <xsl:text>="</xsl:text>
      <xsl:value-of select="."/>
      <xsl:text>"</xsl:text>
      <xsl:apply-templates select="@* | node()" mode="cdata"/>
   </xsl:template>

   <xsl:template match="text()" mode="cdata">
      <xsl:apply-templates select="@* | node()" mode="cdata"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      pattern ::= <element name="QName"> pattern+ </element>
      | <element> nameClass pattern+ </element>
      | <attribute name="QName"> [pattern] </attribute>
	    	| <attribute> nameClass [pattern] </attribute>
    -->

   <xsl:template match="rng:element | rng:attribute" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <!-- <xsl:if test="not( ./@name )">
  				<xsl:apply-templates mode="grammar"/>
  			</xsl:if> -->
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	    	| <group> pattern+ </group>
	    	| <interleave> pattern+ </interleave>
	    	| <choice> pattern+ </choice>
	    	| <optional> pattern+ </optional>
	    	| <zeroOrMore> pattern+ </zeroOrMore>
	    	| <oneOrMore> pattern+ </oneOrMore>
	    	| <list> pattern+ </list>
	    	| <mixed> pattern+ </mixed>
    -->

   <xsl:template match="rng:interleave | rng:choice | rng:optional | rng:zeroOrMore | rng:oneOrMore | rng:list | rng:mixed" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	    	| <ref name="NCName"/>
	    	| <parentRef name="NCName"/>
    -->

   <xsl:template match="rng:ref | rng:parentRef" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
    		| <text/>
    -->

   <xsl:template match="rng:text" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:text>Any text</xsl:text>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	    	| <empty/>
	    	| <notAllowed/>
    -->

   <xsl:template match="rng:empty | rng:notAllowed" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	    	| <value [type="NCName"]> string </value>
    -->

   <xsl:template match="rng:value" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:element name="span">
            <xsl:attribute name="class">ruleTag</xsl:attribute>
            <xsl:value-of select="local-name(.)"/>
         </xsl:element>
         <xsl:text> = "</xsl:text>
         <xsl:element name="span">
            <xsl:attribute name="class">enumerations</xsl:attribute>
            <xsl:value-of select="."/>
         </xsl:element>
         <xsl:text>"</xsl:text>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
    		| <data type="NCName"> param* [exceptPattern] </data>
    -->

   <xsl:template match="rng:data" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	    	| <externalRef href="anyURI"/>
   -->

   <xsl:template match="rng:externalRef" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	    	| <grammar> grammarContent* </grammar>
    -->

   <xsl:template match="rng:grammar" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
    	nameClass ::= <name> QName </name>
	    	| <anyName> [exceptNameClass] </anyName>
	    	| <nsName> [exceptNameClass] </nsName>
	    	| <choice> nameClass+ </choice>
    	
    	exceptNameClass ::= <except> nameClass+ </except>	
    -->

   <xsl:template match="rng:name" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:text>  :  </xsl:text>
         <xsl:element name="span">
            <xsl:attribute name="class">QName</xsl:attribute>
            <xsl:value-of select="."/>
         </xsl:element>
      </xsl:element>
   </xsl:template>

   <xsl:template match="rng:anyName" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <xsl:template match="rng:nsName" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/> "<span class="string"><xsl:value-of select="@ns"/></span>" </xsl:element>
   </xsl:template>

   <xsl:template match="rng:choice" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <xsl:template match="rng:exceptNameClass" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
	    	param ::= <param name="NCName"> string </param>
    -->

   <xsl:template match="rng:param" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/> "<span class="string"><xsl:value-of select="."/></span>" </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
    		exceptPattern ::= <except> pattern+ </except>
    -->

   <xsl:template match="rng:except" mode="grammar">
      <xsl:element name="div">
         <xsl:attribute name="class">indentedBlock</xsl:attribute>
         <xsl:call-template name="render-element-name"/>
         <xsl:apply-templates mode="grammar"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template name="render-element-name">
      <xsl:element name="span">
         <xsl:attribute name="class">ruleTag</xsl:attribute>
         <xsl:value-of select="local-name(.)"/>
      </xsl:element>
      <xsl:choose>
         <xsl:when test="local-name(.) = 'ref'">
            <xsl:text>  &#x2192;  </xsl:text>
            <xsl:call-template name="render-reference">
               <xsl:with-param name="inRef" select="@name"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="@name">
            <xsl:text>  :  </xsl:text>
            <xsl:element name="span">
               <xsl:attribute name="class">ruleName</xsl:attribute>
               <xsl:value-of select="@name"/>
            </xsl:element>
         </xsl:when>
         <xsl:when test="@type">
            <xsl:text>  :  </xsl:text>
            <xsl:element name="span">
               <xsl:attribute name="class">ruleType</xsl:attribute>
               <xsl:value-of select="@type"/>
            </xsl:element>
         </xsl:when>
      </xsl:choose>
      <xsl:if test="local-name() = 'anyName'">
         <xsl:text>  </xsl:text>
         <span class="warning">WARNING! <a href="#__warning">See note at start of schema!</a></span>
      </xsl:if>

   </xsl:template>

   <xsl:template name="render-reference">
      <xsl:param name="inRef"/>
      <xsl:choose>
         <!-- First case is easy - an in document link -->
         <xsl:when test="//rng:define[@name = $inRef]">
            <xsl:element name="span">
               <xsl:attribute name="class">ruleRef</xsl:attribute>
               <xsl:element name="a">
                  <xsl:attribute name="href">
                     <xsl:value-of select="concat('#', $inRef)"/>
                  </xsl:attribute>
                  <xsl:value-of select="$inRef"/>
               </xsl:element>
            </xsl:element>
         </xsl:when>
         <xsl:otherwise>
            <xsl:element name="span">
               <xsl:attribute name="class">ruleRef</xsl:attribute>
               <xsl:value-of select="$inRef"/>
            </xsl:element>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- SCHEMATRON *-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="*[namespace-uri() = 'http://www.ascc.net/xml/schematron']" mode="grammar">
      <xsl:call-template name="render-schematron"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
    	Pass through any that aren't eaten by the above. This is for things
    	like the XHTML documentation
    -->

   <xsl:template match="@* | node()" mode="grammar">
      <xsl:copy>
         <xsl:apply-templates select="@* | node()" mode="grammar"/>
      </xsl:copy>
   </xsl:template>

</xsl:stylesheet>
