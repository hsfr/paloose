<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--
   
   Schematron to XHTML Pretty Printer
   
   This Schematron pretty printer is designed to output the Schematron extras 
   within other schema pretty printers. It has been tested with a XML Schema and
   RELAX NG pretty printer.
   
   Author:
      Name  : Hugh Field-Richards.
      Email : hsfr@hsfr.org.uk
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   Date                   Who    Changes
   ==========================================================================
   
   9th February 2005      HSFR   Created
   15th February 2005     HSFR   Issued
   30th October 2009      HSFR   Changed in line CSS to external CSS file
    
   LICENSE:
   
   This software is free software; you can redistribute it
   and/or modify it under the terms of the GNU Lesser General
   Public License as published by the Free Software Foundation;
   either version 2.1 of the License, or (at your option) any
   later version.
   
   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   Lesser General Public License for more details.
   
   You should have received a copy of the GNU Lesser General Public
   License along with this software; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
	
-->
    
<!DOCTYPE xsl:stylesheet [
   <!ENTITY lt       "&#38;#60;" >     <!-- less-than sign -->
   <!ENTITY gt       "&#62;" >         <!-- greater-than sign -->
   <!ENTITY dash     "&#x2010;" >      <!-- hyphen -->
   <!ENTITY ndash    "&#x2013;" >      <!-- en dash -->
   <!ENTITY mdash    "&#x2014;" >      <!-- em dash -->
   <!ENTITY ldquo    "&#x201c;" >      <!-- Left Double Quote -->
   <!ENTITY rdquo    "&#x201d;" >      <!-- Right Double Quote -->
   <!ENTITY lsquo    "&#x2018;" >      <!-- Left Single Quote -->
   <!ENTITY rsquo    "&#x2019;" >      <!-- Right Single Quote -->
   <!ENTITY bull     "&#x2022;" >      <!-- round bullet, filled -->
   <!ENTITY nbsp     "&#160;" >        <!-- no break space -->
   <!ENTITY rarr     "&#x2192;" >      <!-- right arrow -->
   <!ENTITY copy     "&#169;" >        <!-- copyright -->

	<!-- This is to aid checking - do not touch in normal operation -->
	<!ENTITY box1 "border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
	<!ENTITY box "">

] >

<xsl:stylesheet
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:sch="http://www.ascc.net/xml/schematron"
	version="1.0"  xml:lang="en" id="xsd-html">
	
	<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
	<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- SCHEMATRON *-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
	<!--
	-->

	<xsl:template name="render-schematron">
    	<xsl:element name="div">
    		<xsl:attribute name="class">schematronBlock</xsl:attribute>
    		<div class="schematronTitle">Schematron</div>
    		<div>
    			<xsl:attribute name="class">indentedBlock</xsl:attribute>
    			<xsl:apply-templates select="." mode="schematron"/>
    		</div>
    	</xsl:element>
    </xsl:template>

    <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

    <xsl:template match="*[local-name() = 'ns']" mode="schematron" >
    	<xsl:value-of select="concat( 'Declare schematron namespace : ', @prefix, ' = ', @uri )"/>
    </xsl:template>

    <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

    <xsl:template match="*[local-name() = 'phase']" mode="schematron">
    	<xsl:value-of select="concat( 'Active phase pattern ', @id, ' defined as ', sch:active/@pattern )"/> 
    </xsl:template>

    <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

    <xsl:template match="*[local-name() = 'pattern']" mode="schematron">
    	<xsl:element name="div">
    		<xsl:attribute name="class">schematronPattern</xsl:attribute>
    		<span class="schematronPatternName"><xsl:value-of select="@name"/></span>
    		<xsl:element name="div">
    			<xsl:attribute name="class">schematronRules</xsl:attribute>
    			<xsl:apply-templates mode="schematron"/>
    		</xsl:element>
    	</xsl:element>
    </xsl:template>

    <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

    <xsl:template match="*[local-name() = 'rule']" mode="schematron">
  		<xsl:text>Context : &ldquo;</xsl:text>
    	<span class="schematronXPath"><xsl:value-of select="@context"/></span>
    	<xsl:text>&rdquo;</xsl:text>
    	<xsl:element name="div">
    		<xsl:attribute name="class">schematronAsserts</xsl:attribute>
    		<xsl:apply-templates mode="schematron"/>
    	</xsl:element>
    </xsl:template>

    <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

    <xsl:template match="*[local-name() = 'assert']" mode="schematron">
    	<xsl:element name="div">
    		<xsl:attribute name="class">schematronAsserts</xsl:attribute>
			<xsl:attribute name="style">&box;</xsl:attribute>
    		<xsl:text>if not condition &ldquo;</xsl:text>
    		<span class="schematronXPath"><xsl:value-of select="@test"/></span>
    		<xsl:text>&rdquo;</xsl:text>
    		<xsl:element name="div">
    			<xsl:attribute name="class">schematronAsserts</xsl:attribute>
    			<xsl:text>then output: </xsl:text><span class="schematronText"><xsl:value-of select="."/></span>
    		</xsl:element>
    	</xsl:element>
    </xsl:template>

    <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

    <xsl:template match="*[local-name() = 'report']" mode="schematron">
    	<xsl:element name="div">
    		<xsl:attribute name="class">schematronAsserts</xsl:attribute>
    		<xsl:text>If condition &ldquo;</xsl:text>
    		<span class="schematronXPath"><xsl:value-of select="@test"/></span>
    		<xsl:text>&rdquo;</xsl:text>
    		<xsl:element name="div">
    			<xsl:attribute name="class">schematronAsserts</xsl:attribute>
    			<xsl:text>then output: </xsl:text><span class="schematronText"><xsl:value-of select="."/></span>
    		</xsl:element>
    	</xsl:element>
    </xsl:template>

</xsl:stylesheet>
