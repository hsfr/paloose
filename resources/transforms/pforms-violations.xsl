<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--
   
   This stylesheet is part of a three part process that merges a Paloose Forms
   document into a final document. This (first part) is an extension to the
   Cocoon-like process.
   
   Author:
      Name  : Hugh Field-Richards
      Email : hsfr@hsfr.org.uk
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   Date                   Who    Changes
   ==========================================================================
   
   28th February 2007     HSFR   Created
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   LICENSE:
   
   This software is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or (at
   your option) any later version.
   
   This library is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
   details.
   
   COPYRIGHT:
   
   Copyright 2006, 2007 Hugh Field-Richards.
   
-->

<xsl:stylesheet
   version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:pf="http://www.paloose.org/schemas/Forms/1.0">

   <xsl:output encoding="UTF-8"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Parameters brought in from sitemap
   -->

   <xsl:param name="formViolations"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Global variables
   -->

   <xsl:variable name="gFormViolations" select="$formViolations"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="/">
      <xsl:apply-templates/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      This is template really disgusting. The form violations come in as an XML string, for
      example:
      
         <violation-list>
            <username>Must have a username</username>
         </violation-list>
      
      At present I have not found a good way to bring a parameter into the XSL as
      a DOMElement. Everything I have tried has failed — so I am going for a slightly
      more crude approach and treating it as a simple string from which I can extract
      the necessary information. As soon as I find a better way I will change it.
      
      Typical XML In
      ==========================================================================
         <pf:violations class="error"/>
      ==========================================================================
      
      Typical XML Out (assuming form violation structure above)
      ==========================================================================
         <pf:violations class="error">
            <pf:violation>Must have a username</pf:violation>
         </pf:violations>
      ==========================================================================
      
      This is then processed by the next stage of the pforms process in pforms-default.
      
   -->
      
   <xsl:template match="pf:violations">
      <xsl:element name="pf:violations">
         <xsl:attribute name="class">violations</xsl:attribute>
         <xsl:copy-of select="@*"/>
         <xsl:variable name="ref"><xsl:value-of select="parent::*/@ref"/></xsl:variable>
         <!-- Make up string search delimiters -->
         <xsl:variable name="startDel">
            <xsl:value-of select="concat( '&lt;', $ref, '&gt;' )"/>
         </xsl:variable>
         <xsl:variable name="endDel">
            <xsl:value-of select="concat( '&lt;/', $ref, '&gt;' )"/>
         </xsl:variable>
         <xsl:variable name="violationString">
            <xsl:value-of select="substring-before( substring-after( $gFormViolations, $startDel ), $endDel )"/>
         </xsl:variable>
         <xsl:if test="string-length( $violationString ) > 0">
            <xsl:element name="pf:violation">
               <xsl:value-of select="$violationString"/>
            </xsl:element>
         </xsl:if>
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Pass through anything not eaten by the above
   -->

   <xsl:template match="@*|node()" priority="-1">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()"/>
      </xsl:copy>
   </xsl:template>

</xsl:stylesheet>
