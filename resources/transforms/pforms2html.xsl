<?xml version="1.0" encoding="UTF-8" ?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--
   
   This stylesheet is part of a two part process that merges a Paloose Forms
   document into a final document. It converts Paloose form tags into HTML. This
   stylesheet is applied at the end of a transformation process after laying
   out the form tags on the page is complete. At this stage form tags 
   are rendered in device specific format.
   
   Different widgets are broken into templates to allow customization in
   importing stylesheets.
   
   It is heavily based (= plagiarised) on JXForms that was used in old versions
   of Cocoon and credit must be given to the main authors below. I have changed
   many things in it so beware!
   
   JXForms Authors:
      Ivelin Ivanov, ivelin@apache.org, June 2002
      Andrew Timberlake <andrew@timberlake.co.za>, June 2002
      Michael Ratliff, mratliff@collegenet.com <mratliff@collegenet.com>, May 2002
      Torsten Curdt, tcurdt@dff.st, March 2002
      Simon Price <price@bristol.ac.uk>, September 2002
      Konstantin Piroumian <kpiroumian@protek.com>, September 2002
      Robert Ellis Parrott <parrott@fas.harvard.edu>, October 2002
   
   Modified for Paloose forms:
      Hugh Field-Richards, <hsfr@hsfr.org.uk>
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   Date                   Who    Changes
   ==========================================================================
   
   14th February 2007     HSFR   Heavily modified for Paloose
-->

<xsl:stylesheet
   version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:pf="http://www.paloose.org/schemas/Forms/1.0">

   <xsl:output encoding="UTF-8"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   
   <xsl:template match="/">
      <xsl:apply-templates/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*- FORM STRUCTURE *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="pf:form">
      <xsl:element name="form">
         <!-- Copy all the attributes with no change -->
         <xsl:copy-of select="@*"/>
         <xsl:element name="input">
            <xsl:attribute name="type">hidden</xsl:attribute>
            <xsl:attribute name="name">paloose-kont</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="//@continuation"/></xsl:attribute>
         </xsl:element>
         <xsl:apply-templates/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- OUTPUT *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Simple output field for displaying uneditable data.
   -->
   
   <xsl:template match="pf:output">
      <xsl:element name="span">
         <xsl:attribute name="class">outputField</xsl:attribute>
         <xsl:apply-templates select="pf:value"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- INPUT -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Ordinary input fields. Note that the ref attribute is assigned to html:name attribute,
      which is how it is linked to the model.
   -->
   
   <xsl:template match="pf:input">
      <xsl:element name="input">
         <xsl:attribute name="name"><xsl:value-of select="@ref"/></xsl:attribute>
         <xsl:attribute name="type">text</xsl:attribute>
         <xsl:attribute name="value"><xsl:value-of select="pf:value/text()"/></xsl:attribute>
         <!-- if there is an associated hint -->
         <xsl:apply-templates select="pf:hint"/>
         <!-- copy all attributes from the original markup, except for "ref" -->
         <xsl:copy-of select="@*[not(name()='ref')]"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- TEXT AREA -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      A simple text area to enter several lines of text
   -->
   
   <xsl:template match="pf:textarea">
      <xsl:element name="textarea">
         <xsl:attribute name="name"><xsl:value-of select="@ref"/></xsl:attribute>
         <!-- if there is an associated hint -->
         <xsl:apply-templates select="pf:hint"/>
         <!-- copy all attributes from the original markup, except for "ref" -->
         <xsl:copy-of select="@*[not(name()='ref')]"/>
         <xsl:value-of select="pf:value/text()"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- SECRET *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Text fields that need to have their contents hidden, passwords etc.
   -->
   
   <xsl:template match="pf:secret">
      <xsl:element name="input">
         <xsl:attribute name="name"><xsl:value-of select="@ref"/></xsl:attribute>
         <xsl:attribute name="value"><xsl:value-of select="pf:value/text()"/></xsl:attribute>
         <xsl:attribute name="type">password</xsl:attribute>
         <!-- if there is an associated hint -->
         <xsl:apply-templates select="pf:hint"/>
         <!-- copy all attributes from the original markup, except for "ref" -->
         <xsl:copy-of select="@*[not(name()='ref')]"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- HIDDEN *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Fields hidden on the form to pass data back to the server.
   -->
   
   <xsl:template match="pf:hidden">
      <xsl:element name="input">
         <xsl:attribute name="name"><xsl:value-of select="@ref"/></xsl:attribute>
         <xsl:attribute name="value"><xsl:value-of select="pf:value/text()"/></xsl:attribute>
         <xsl:attribute name="type">hidden</xsl:attribute>
         <xsl:copy-of select="@*[not(name()='ref')]"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- SELECT *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Select is based on the W3C XForms select element. For example we start with
      a typical multi-choice (not select1)
      
      Typical XML In:
      ========================================================================      
         <pf:select appearance="full" ref="roles">
            <pf:value>{flow:roles}</pf:value>
            <pf:label>Administration roles</pf:label>
            <pf:choices>
               <pf:item>
                  <pf:label>Manage Users<pf:/label>
                  <pf:value>manageUsers<pf:/value>
               <pf:/item>
               <pf:item checked="true">
                  <pf:label>Edit Pages<pf:/label>
                  <pf:value>editPage<pf:/value>
               <pf:/item>
            </pf:choices>
         </pf:select>
      ========================================================================      
       
      XML Out:
      ========================================================================      
         <input name="roles" type="checkbox" value="manageUsers" />Manage Users<br/>
         <input name="roles" type="checkbox" value="editPage" checked="true" />Edit Pages<br/>
      ========================================================================      
      
      Setting which of the check boxes should be checked is a little trickier. The information
      comes in as a comma separated list in the value, see the {flow:roles} above.
   -->
   
   <xsl:template match="pf:select[@appearance='full']">
      <xsl:variable name="ref" select="@ref"/>
      <xsl:variable name="checkedBoxList" select="pf:value"/>
      
      <xsl:for-each select="pf:choices/pf:item">
         <xsl:element name="input">
            <xsl:attribute name="name"><xsl:value-of select="concat( $ref, '[]' )"/></xsl:attribute>
            <xsl:attribute name="type">checkbox</xsl:attribute>
            <xsl:attribute name="value"><xsl:value-of select="pf:value"/></xsl:attribute>
            
            <xsl:variable name="isChecked">
               <xsl:choose>
                  <xsl:when test="string-length( $checkedBoxList ) = 0">
                     <!-- Only use checked if the checkedBoxList is null -->
                     <xsl:choose>
                        <xsl:when test="@checked = 'true'">true</xsl:when>
                        <xsl:otherwise>false</xsl:otherwise>
                     </xsl:choose>
                  </xsl:when>
                  
                  <xsl:otherwise>
                     <!-- Only use checked if the checkedBoxList is null -->
                     <xsl:variable name="checked">
                        <xsl:call-template name="wholeWordContains">
                           <xsl:with-param name="inList" select="$checkedBoxList"/>
                           <xsl:with-param name="inValue" select="pf:value"/>
                        </xsl:call-template>
                     </xsl:variable>
                     
                     <xsl:choose>
                        <xsl:when test="string-length( $checked ) > 0">true</xsl:when>
                        <xsl:otherwise>false</xsl:otherwise>
                     </xsl:choose>
                  </xsl:otherwise>
                  
               </xsl:choose>
            </xsl:variable>
            
            <xsl:if test="$isChecked='true'">
               <xsl:attribute name="checked">true</xsl:attribute>
            </xsl:if>
            
            <xsl:copy-of select="@*[ not(name()='ref') ]"/>
         </xsl:element>
         <xsl:value-of select="pf:label"/>
         <xsl:element name="br"/>
      </xsl:for-each>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      The above example set to compact will give
      
      Typical XML In:
      ========================================================================      
         <pf:select appearance="compact" ref="roles">
            <pf:value>{flow:roles}</pf:value>
            <pf:label>Administration roles</pf:label>
            <pf:choices>
               <pf:item>
                  <pf:label>Manage Users<pf:/label>
                  <pf:value>manageUsers<pf:/value>
               <pf:/item>
               <pf:item checked="true">
                  <pf:label>Edit Pages<pf:/label>
                  <pf:value>editPage<pf:/value>
               <pf:/item>
            </pf:choices>
         </pf:select>
      ========================================================================      
       
      XML Out:
      ========================================================================      
         <select name="roles[]" size="4" multiple="true">
            <option value="editPage">Edit Pages</option>
            <option value="manageUsers">Add/update/delete Users</option>
         </select>
      ========================================================================      
   -->

   <xsl:template match="pf:select | pf:select[@appearance='compact']">
      <xsl:variable name="ref" select="@ref"/>
      <xsl:variable name="checkedBoxList" select="pf:value"/>
      <xsl:element name="select">
         <xsl:attribute name="name"><xsl:value-of select="concat( $ref, '[]' )"/></xsl:attribute>
         <xsl:attribute name="size"><xsl:value-of select="count( pf:choices/pf:item )"/></xsl:attribute>
         <xsl:copy-of select="@*[not(name()='ref' or name()='appearance' )]"/>
         <xsl:attribute name="multiple">true</xsl:attribute>
         
         <xsl:for-each select="pf:choices/pf:item">
            <xsl:element name="option">
               <xsl:attribute name="value">
                  <xsl:value-of select="pf:value"/>
               </xsl:attribute>
               <xsl:variable name="isChecked">
                  <xsl:choose>
                     <xsl:when test="string-length( $checkedBoxList ) = 0">
                        <!-- Only use checked if the checkedBoxList is null -->
                        <xsl:choose>
                           <xsl:when test="@checked = 'true'">true</xsl:when>
                           <xsl:otherwise>false</xsl:otherwise>
                        </xsl:choose>
                     </xsl:when>
                     <xsl:otherwise>
                        <!-- Only use checked if the checkedBoxList is null -->
                        <xsl:variable name="checked">
                           <xsl:call-template name="wholeWordContains">
                              <xsl:with-param name="inList" select="$checkedBoxList"/>
                              <xsl:with-param name="inValue" select="pf:value"/>
                           </xsl:call-template>
                        </xsl:variable>
                        <xsl:choose>
                           <xsl:when test="string-length( $checked ) > 0">true</xsl:when>
                           <xsl:otherwise>false</xsl:otherwise>
                        </xsl:choose>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:variable>
               <xsl:if test="$isChecked='true'">
                  <xsl:attribute name="selected">true</xsl:attribute>
               </xsl:if>
               <xsl:copy-of select="@*[ not(name()='ref') ]"/>
               <xsl:value-of select="pf:label"/>
            </xsl:element>
         </xsl:for-each>
      </xsl:element>
      
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- SUBMIT *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Typical XML In:
      ========================================================================      
         <pf:submit class="button" id="next">
            <pf:label>Next</pf:label>
            <pf:hint>Go to optional details entry</pf:hint>
         </pf:submit>
      ========================================================================      
      
      XML Out:
      ========================================================================      
         <input
            name="paloose-action-next"
            type="submit"
            value="Next"
            class="button"
            title="Go to optional details entry" />
      ========================================================================      
   -->
   
   <xsl:template match="pf:submit">
      <xsl:choose>
         <xsl:when test="@src">
            <xsl:element name="input">
               <xsl:attribute name="name">
                  <xsl:value-of select="concat( 'paloose-action-', @id )"/>
               </xsl:attribute>
               <xsl:attribute name="type">image</xsl:attribute>
               <xsl:attribute name="value">
                  <xsl:value-of select="pf:label/text()"/>
               </xsl:attribute>
               <!-- input name="paloose-action-{@id}" type="image" value="{pf:label/text()}" -->
               <xsl:copy-of select="@*[not(name()='id')]"/>
               <xsl:apply-templates select="pf:hint"/>
            </xsl:element>
         </xsl:when>
         <xsl:otherwise>
            <xsl:element name="input">
               <xsl:attribute name="name">
                  <xsl:value-of select="concat( 'paloose-action-', @id )"/>
               </xsl:attribute>
               <xsl:attribute name="type">submit</xsl:attribute>
               <xsl:attribute name="value">
                  <xsl:value-of select="pf:label/text()"/>
               </xsl:attribute>
               <!-- input name="paloose-action-{@id}" type="submit" value="{pf:label/text()}" -->
               <xsl:copy-of select="@*[not(name()='id')]"/>
               <xsl:apply-templates select="pf:hint"/>
            </xsl:element>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Hints resolve to a title for the form element which are seen when the mouse hovers
      on that element
   -->
   
   <xsl:template match="pf:hint">
      <xsl:attribute name="title">
         <xsl:value-of select="."/>
      </xsl:attribute>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      values provide a simple string to use as an initial value for fields.
   -->
   
   <xsl:template match="pf:value">
      <xsl:choose>
         <xsl:when test="@ref"><xsl:value-of select="@ref"/></xsl:when>
         <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Copy everything else if not eaten by the above
   -->
   
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()" />
      </xsl:copy>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      This rather horrid affair checks to see whether a token appears in a comma separated
      list ... ah, the joys of recursion.
   -->

   <xsl:template name="wholeWordContains">
      <xsl:param name="inList"/>
      <xsl:param name="inValue"/>
      <xsl:choose>
         <xsl:when test="contains( $inList, ',' )">
            <!-- A list not a single value -->
            <xsl:variable name="token" select="substring-before( $inList, ',')"/>
            <xsl:variable name="remainder" select="substring-after( $inList, ',')"/>
            <xsl:choose>
               <xsl:when test="$token = $inValue">
                  <!-- Found something so return value -->
                  <xsl:value-of select="$token"/>
               </xsl:when>
               <xsl:otherwise>
                  <!-- Nothing found so go down a level in he recursion -->
                  <xsl:call-template name="wholeWordContains">
                     <xsl:with-param name="inList" select="$remainder"/>
                     <xsl:with-param name="inValue" select="$inValue"/>
                  </xsl:call-template>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:when>
         <xsl:otherwise>
            <!-- End of trawl through list, so check final token in list -->
            <xsl:if test="$inList = $inValue">
               <xsl:value-of select="$inValue"/>
            </xsl:if>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   
   
</xsl:stylesheet>
