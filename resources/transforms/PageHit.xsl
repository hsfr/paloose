<?xml version="1.0"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--
   
   i18n Transformer
   ================
   
   Author:
   Name  : Hugh Field-Richards
   Email : hsfr@hsfr.org.uk
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   Date                   Who    Changes
   ==========================================================================
   
   30th May 2006          HSFR   Created
   
   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   All copyright in this software is the property of Hugh Field-Richards.
   The software is published under the terms of the LGPL License a copy of
   which has been included with this distribution in the LICENSE file.
   
-->

<xsl:stylesheet
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:paloose="http://www.paloose.org/schemas/Paloose/1.0"
   version="1.0">

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Passed in parameters
   -->
   
   <xsl:param name="locale"/>
   <xsl:param name="pageHits"/>
   
   <xsl:template match="/">
      <xsl:apply-templates mode="processDoc"/>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xsl:template match="paloose:page-hit" mode="processDoc">
      <xsl:element name="paloose:page-hit">
         <xsl:value-of select="$pageHits"/>
      </xsl:element>
   </xsl:template>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Pass through all other tags
   -->
   
   <xsl:template match="node()|@*" mode="processDoc" >
      <xsl:copy>
         <xsl:apply-templates select="@*" mode="processDoc" />
         <xsl:apply-templates mode="processDoc" />
      </xsl:copy>
   </xsl:template>
   
</xsl:stylesheet>
