<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * This library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package paloose
 * @subpackage generation/transforming/serialization
 * @author   Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2008 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
// Use whichever is needed
require_once( PALOOSE_LIB_DIRECTORY . "/generation/GeneratorPipeElement.php" );
/* require_once( PALOOSE_LIB_DIRECTORY . "/generation/TransformerPipeElement.php" ); */
/* require_once( PALOOSE_LIB_DIRECTORY . "/generation/SerializerPipeElement.php" ); */

// One or more exception handlers for this class as required
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/XXXException.php" );

// Include any more of your PHP classes here

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class is the instance class that performs the actual pipeline
 * functionality.
 *
 * @package paloose
 * @subpackage generation/transforming/serialization
 */

// Use whichever is needed
class XXXGenerator extends GeneratorPipeElement implements PipeElementInterface
/* class XXXTransformer extends TransformerPipeElement implements PipeElementInterface */
/* class XXXSerializer extends SerializerPipeElement implements PipeElementInterface */
{

    /** Logger instance for this class */
   private $gLogger;      // Could be removed if not using logging

	/** Example user global to hold first parameter - not needed if declared and used locally to run method */
	private $gParam_a;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of XXXGenerator/XXXTransformer/XXXSerializer. A
    * generator is used as an example here but they all use the same format.
    * 
    * The type and src input parameter are taken from generator tag attributes
    * of the same name.
    *
    * Assume that the instance has been called by the following typical
    * component declaration
    *  
    *        <map:generators default="file">
    *           <map:generator name="xxx-gen" src="resource://lib/generation/XXXGenerator"/>
    *        </map:generators>
    *
    *     and pipeline use
    *
    *        <map:generate type="xxx-gen" src="context://path/to/user/content">
    *           <map:parameter name="param-a" value="param-a-value"/>
    *        </map:generate>
    *
    * The type attribute defines which generator in the component decalration to use. The
    * src attribute is a user defined value. In the FileGenerator it is the file to use. In the
    * DirectoryGenerator it defines the directory to interrogate. Basically it defines where the
    * generator gets its input form.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element within the generator tag
    * @param string $inSrc the src attribute within the generator tag
    * @param _FileTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if any problems exist in the parent
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      
      // Always needed 
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      
      // Could be removed if not using logging
      $this->gLogger =& LoggerManager::getLogger( __CLASS__ );
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
	 * The method is used when the pipeline component is run. The parameters
	 * of the method contain all the necessary details of the current run
	 * environment.
	 *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the input pipeline DOM from the previous stage (NULL for a generator).
    * @return DOMDocument The document DOM generated by this method (ignored when component is a serializer).
    * @throws RunException if an error occurs
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      global $gEnvironment;         
 		global $gSitemapStack;         // Needed when this component is a serializer

      // For accessing the various modules: global, params, request-param, flow, session
 		global $gModules;

      // For accessing the configuration variables. For example $gConfiguration[ 'palooseNamespace' ]
		global $gConfiguration;

      // If the src attribute contains pseudo variables etc it is important to run this. The $gSrc
      // was set by the parent when its constructor was called from the src attribute
      $expandedSrcAttribute = StringResolver::expandPseudoProtocols(
               StringResolver::expandString( $inVariableStack, $this->gSrc )
            );
      // At this point the src tag will be a full path+file name in the example above. For
      // example:
      //              /full/path/to/user/content

      // Any parameters in the pipeline use of this generator are in the array variable
      // $this->gParameters. Several ways to extract it can be used. First read it into a
      // local array $parameterList and expand the variables:

      $parameterList = array();
      if ( sizeof( $this->gParameters->getParameterList() ) > 0  ) {
         $parameterList = $this->gParameters->getParameterList();
         foreach ( $parameterList as $name => $value ) {
            $expandedValue = StringResolver::expandString( $inVariableStack, $value );
            $expandedValue = StringResolver::expandPseudoProtocols( $expandedValue );
            $parameterList[ $name ] = $expandedValue;
         }
      }
      
      // Or extract a single parameter into the class global $this->gParam_a
 	   if ( $this->gParameters->getParameter( "param-a" ) != NULL ) {
    		$this->gParam_a = $this->gParameters->getParameter( "param-a" );
      }
      // Obviously use this as
      $param_a = $parameterList[ 'param-a' ];

      try {
         // Declare a document into which to put the XL
         $documentDOM = new DomDocument();
         
         // User code goes here
         
         // When the class is a serializer the following is used 
         $gSitemapStack->peek()->getOutputStream()->out( $dom->saveXML() );
		   $gSitemapStack->peek()->getOutputStream()->clearBuffer();


         return $documentDOM;
         // Define as many exception catch statements as required
      } catch ( XXXException $e ) {
        throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
      // Insurance - not really needed
      return NULL;
  }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this XXXGenerator.
    *
    * @return string the representation of the element as a string
    *
    */

   public function toString()
   {
      // Change this if any special output is needed. Otherwise leave alone.
      return parent::toString( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component. Note that the class
 * name must start with a "_" character and be extactly the same as the instance
 * class above.
 *
 * There will be only
 * one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage generation/transforming/serialization
 */
 
class _XXXGenerator extends Generator {

   /** Logger instance for this class */
   private $gLogger;
   
  	/** declare any component level paraters required here. These are set
  	 * by the component declaration parameter. They are access by the instance
  	 * using the component parameter $inComponent (see constr method above. For
  	 * example within the run methos above you could say
  	 * 
  	 *    param_b = $inComponent->gParam-b;
  	 */
	public $gParam_b;
	
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param inType the type of this generator ( "file", etc. ) confusing declared as the 'name' attribute
    * @param inSrc the src attribute (or package/class required in this case)
    * @param inParameterNode The parameters for this generator
    */
    
   public function __construct( $inType, $inSrc, DOMNode $inParameterNode )
   {
      parent::__construct( $inType, $inSrc, $inParameterNode );
      $this->gLogger =& LoggerManager::getLogger( __CLASS__ );
      $this->gPackageName = "XXXGenerator"; // or XXXTransformer or XXXSerializer
      
      // Extract the parameters into the global variables. For example
 		$this->gParam_b = "";
		if ( strlen( $this->gParameters[ 'param-b' ] ) > 0 ) $this->gParam_b = $this->gParameters[ 'param-b' ];

      // Any other initialisation stuff goes here
   }

}
?>
