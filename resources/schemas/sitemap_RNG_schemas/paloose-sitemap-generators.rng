<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--

RNG Schema for Paloose Sitemaps

Name  : Hugh Field-Richards.
Email : hsfr@hsfr.org.uk

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

Copyright (c) 2006 - 2011  Hugh Field-Richards

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

For a copy of the GNU General Public License write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-->

<grammar xmlns="http://relaxng.org/ns/structure/1.0" xmlns:xhtml="http://www.w3.org/1999/xhtml"
   xmlns:a="http://relaxng.org/ns/annotation/1.0" xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:map="http://apache.org/cocoon/sitemap/1.0"
   datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
   
   <a:documentation>
      <dc:title>RELAX NG Schema for Paloose Sitemaps</dc:title>
      <dc:creator>Hugh Field-Richards</dc:creator>
      <dc:date>2011-08-03T12:15</dc:date>
      <dc:language>en</dc:language>
   </a:documentation>
   
   <xhtml:p>This is the RELAX NG schema for the Paloose sitemaps. They are very similar to Cocoon sitemaps, but with enough
      differences to make it worth formalising separately. Any bug reports, help, suggestions, new task schemas etc to me, Hugh
      Field-Richards (hsfr@hsfr.org.uk).</xhtml:p>
   
   <xhtml:p>The documentation is designed to be processed by the rng2xhtml.xsl pretty-printing processor available from the
      author.</xhtml:p>
   
   <sch:ns prefix="map" uri="http://apache.org/cocoon/sitemap/1.0"/>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xhtml:h2>Generators</xhtml:h2>
   <xhtml:p>A Generator generates XML content as DOM objects and initializes the pipeline processing.</xhtml:p>
   
   <define name="components.generators">
      <element name="map:generators">
         <attribute name="default">
            <ref name="data.componentName"/>
         </attribute>

         <zeroOrMore>
            <ref name="generators.generator"/>
         </zeroOrMore>
      </element>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <define name="generators.generator">
      
      <sch:pattern name="Check the generator names are unique">
         <sch:rule context="map:generator">
            <sch:assert test="count(//map:generator[@name = current()/@name]) = 1">There should be unique generator
               names</sch:assert>
         </sch:rule>
      </sch:pattern>
      
      <element name="map:generator">
         <ref name="component.attribute.cachable"/>
         <ref name="component.attributes.common"/>
         <ref name="FileGenerator.contents"/>
         <ref name="PXTemplateGenerator.contents"/>
         <ref name="DirectoryGenerator.contents"/>
         <ref name="TidyGenerator.contents"/>
         <ref name="GedComGenerator.contents"/>
      </element>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <define name="FileGenerator.contents">
      <interleave>
         <optional>
            <ref name="component.use-request-parameters"/>
         </optional>
      </interleave>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <define name="PXTemplateGenerator.contents">
      <interleave>
         <optional>
            <ref name="component.use-request-parameters"/>
         </optional>
      </interleave>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <define name="DirectoryGenerator.contents">
      <interleave>
         <optional>
            <ref name="component.use-request-parameters"/>
         </optional>
      </interleave>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <define name="TidyGenerator.contents">
      
      <sch:pattern name="Tidy parameters elements only allowed within TidyGenerator">
         <sch:rule context="//map:generator/map:parameter[ @name='char-encoding' ]">
            <sch:report test="parent::*[ not( contains( @src, 'TidyGenerator' ) ) ]">char-encoding parameter only allowed within
               TidyGenerator</sch:report>
         </sch:rule>
         <sch:rule context="//map:generator/map:parameter[ @name='clean' ]">
            <sch:report test="parent::*[ not( contains( @src, 'TidyGenerator' ) ) ]">clean parameter only allowed within
               TidyGenerator</sch:report>
         </sch:rule>
         <sch:rule context="//map:generator/map:parameter[ @name='word-2000' ]">
            <sch:report test="parent::*[ not( contains( @src, 'TidyGenerator' ) ) ]">word-2000 parameter only allowed within
               TidyGenerator</sch:report>
         </sch:rule>
      </sch:pattern>
      
      <interleave>
         <optional>
            <ref name="component.use-request-parameters"/>
         </optional>
         <zeroOrMore>
            <ref name="common.element.parameter"/>
         </zeroOrMore>
      </interleave>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <define name="GedComGenerator.contents">
      
      <sch:pattern name="GedCom parameters only allowed within GedComGenerator">
         <sch:rule context="//map:generator/map:parameter[ @name='generateXMLFile' ]">
            <sch:report test="parent::*[ not( contains( @src, 'GedComGenerator' ) ) ]">generateXMLFile parameter only allowed
               within GedComGenerator</sch:report>
         </sch:rule>
         <sch:rule context="//map:generator/map:parameter[ @name='generateDOM' ]">
            <sch:report test="parent::*[ not( contains( @src, 'GedComGenerator' ) ) ]">generateDOM parameter only allowed within
               GedComGenerator</sch:report>
         </sch:rule>
         <sch:rule context="//map:generator/map:parameter[ @name='useXMLFile' ]">
            <sch:report test="parent::*[ not( contains( @src, 'GedComGenerator' ) ) ]">useXMLFile parameter only allowed within
               GedComGenerator</sch:report>
         </sch:rule>
      </sch:pattern>
      
      <interleave>
         <optional>
            <ref name="component.use-request-parameters"/>
         </optional>
         <zeroOrMore>
            <ref name="common.element.parameter"/>
         </zeroOrMore>
      </interleave>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xhtml:h2>Generator Pipeline Instance</xhtml:h2>
   
   <xhtml:p>Generators are instanced within a pipeline. However there are certain constraints on where the generator can occur.
      Primarily it must always be the first element within a pipeline (it makes no sense to be anywhere else).</xhtml:p>
   
   <define name="generator.instance">
      <element name="map:generate">
         <optional>
            <ref name="components.attribute.label"/>
         </optional>
         <optional>
            <ref name="components.attribute.type"/>
         </optional>
         <ref name="components.attribute.src"/>
         <zeroOrMore>
            <ref name="common.element.parameter"/>
         </zeroOrMore>
      </element>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Aggregation -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
   <map:aggregate element="root" label="aggr-content">
   <map:part src="cocoon:/menus.xml" element="menus" strip-root="true"/>
   <map:part src="cocoon:/home.xml" element="content" strip-root="true"/>
   <map:part src="cocoon:/footer.xml" element="content" strip-root="true"/>
   </map:aggregate>
   -->
   
   <xhtml:h1>Aggregation</xhtml:h1>
   
   <define name="aggregate.instance">
      <element name="map:aggregate">
         <ref name="components.attribute.element"/>
         <optional>
            <ref name="components.attribute.label"/>
         </optional>
         <oneOrMore>
            <ref name="aggregate.part"/>
         </oneOrMore>
      </element>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <define name="aggregate.part">
      <element name="map:part">
         <ref name="components.attribute.element"/>
         <optional>
            <ref name="components.attribute.strip-root"/>
         </optional>
         <ref name="components.attribute.src"/>
      </element>
   </define>
   
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   
   <xhtml:h1>Copyright</xhtml:h1>
   
   <xhtml:div class="copyrightPanel">
      <xhtml:p class="normalPara">Copyright (c) 2006 &#x2013; 2009 Hugh Field-Richards</xhtml:p>
      <xhtml:p class="normalPara">This program is free software; you can redistribute it and/or modify it under the terms of the
         GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your
         option) any later version.</xhtml:p>
      <xhtml:p class="normalPara">This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
         without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
         License for more details.</xhtml:p>
      <xhtml:p class="normalPara">For a copy of the GNU General Public License write to the Free Software Foundation, Inc., 51
         Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. </xhtml:p>
   </xhtml:div>
   
</grammar>
