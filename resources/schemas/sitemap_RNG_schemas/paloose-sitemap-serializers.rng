<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--

	RNG Schema for Paloose Sitemaps

	Name  : Hugh Field-Richards.
	Email : hsfr@hsfr.org.uk

	-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

	Copyright (c) 2006 - 2011  Hugh Field-Richards

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	For a copy of the GNU General Public License write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-->

<grammar xmlns="http://relaxng.org/ns/structure/1.0" xmlns:xhtml="http://www.w3.org/1999/xhtml"
   xmlns:a="http://relaxng.org/ns/annotation/1.0" xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:sch="http://www.ascc.net/xml/schematron" xmlns:map="http://apache.org/cocoon/sitemap/1.0"
   datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">

   <a:documentation>
      <dc:title>RELAX NG Schema for Paloose Sitemaps</dc:title>
      <dc:creator>Hugh Field-Richards</dc:creator>
      <dc:date>2011-08-03T12:15</dc:date>
      <dc:language>en</dc:language>
   </a:documentation>

   <xhtml:p>This is the RELAX NG schema for the Paloose sitemaps. They are very similar to Cocoon sitemaps, but with enough
      differences to make it worth formalising separately. Any bug reports, help, suggestions, new task schemas etc to me, Hugh
      Field-Richards (hsfr@hsfr.org.uk).</xhtml:p>

   <xhtml:p>The documentation is designed to be processed by the rng2xhtml.xsl pretty-printing processor available from the
      author.</xhtml:p>

   <sch:ns prefix="map" uri="http://apache.org/cocoon/sitemap/1.0"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <define name="components.serializers">
      <element name="map:serializers">
         <ref name="components.attribute.default"/>
         <zeroOrMore>
            <ref name="serializers.serializer"/>
         </zeroOrMore>
      </element>
   </define>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <define name="serializers.serializer">

      <sch:pattern name="Check the serializer names are unique">
         <sch:rule context="map:serializer">
            <sch:assert test="count(//map:serializer[@name = current()/@name]) = 1">There should be unique serializer
               names</sch:assert>
         </sch:rule>
      </sch:pattern>

      <element name="map:serializer">
         <ref name="component.attributes.common"/>
         <optional>
            <ref name="component.attribute.cachable"/>
         </optional>
         <attribute name="mime-type"/>
         <ref name="serializers.contents"/>
      </element>
   </define>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <define name="serializers.contents">

      <sch:pattern name="doctype-public elements only allowed within HTMLSerializer">
         <sch:rule context="//map:serializer/map:property[ @name = 'doctype-public' ]">
            <sch:report test="parent::*[ contains( @src, 'TextSerializer' ) ]">doctype-public not allowed in TextSerializer</sch:report>
         </sch:rule>
         
         <sch:rule context="//map:serializer/map:property[ @name = 'doctype-system' ]">
            <sch:report test="parent::*[ contains( @src, 'TextSerializer' ) ]">doctype-public not allowed in TextSerializer</sch:report>
         </sch:rule>
      	
      	<sch:rule context="//map:serializer/map:property[ @name = 'omit-xml-declaration' ]">
      		<sch:report test="parent::*[ contains( @src, 'TextSerializer' ) ]">omit-xml-declaration not allowed in TextSerializer</sch:report>
      	</sch:rule>
      	
      	<sch:rule context="//map:serializer/map:property[ @name = 'cdata-encode' ]">
      		<sch:report test="parent::*[ contains( @src, 'TextSerializer' ) ]">cdata-encode not allowed in TextSerializer</sch:report>
      	</sch:rule>
      </sch:pattern>

      <zeroOrMore>
         <ref name="HTMLSerializer.property"/>
      </zeroOrMore>
   </define>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <define name="HTMLSerializer.property">
      <element name="map:property">
         <attribute name="name">
            <ref name="HTMLSerializer.property.values"/>
         </attribute>
         <attribute name="value"/>
      </element>
   </define>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <define name="HTMLSerializer.property.values" combine="choice">
      <choice>
         <value>doctype-public</value>
         <value>doctype-system</value>
         <value>HTMLSerializer.encoding</value>
         <value>omitXMLdeclaration</value>
      	<value>encoding</value>
      	<value>cdata-encode</value>
      </choice>
   </define>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <define name="serializer.instance">
      <element name="map:serialize">
         <optional>
            <attribute name="type"/>
         </optional>
      </element>
   </define>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xhtml:h1>Copyright</xhtml:h1>

   <xhtml:div class="copyrightPanel">
      <xhtml:p class="normalPara">Copyright (c) 2006 &#x2013; 2009 Hugh Field-Richards</xhtml:p>
      <xhtml:p class="normalPara">This program is free software; you can redistribute it and/or modify it under the terms of the
         GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your
         option) any later version.</xhtml:p>
      <xhtml:p class="normalPara">This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
         without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
         License for more details.</xhtml:p>
      <xhtml:p class="normalPara">For a copy of the GNU General Public License write to the Free Software Foundation, Inc., 51
         Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. </xhtml:p>
   </xhtml:div>

</grammar>
