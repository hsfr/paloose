<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
/*

	Paloose Entry Point for site requests
	=====================================
	
	All this does is load the primary Paloose file and class.
	It requires that there is a rewrite rule set up in
	the local .htaccess file. The entry in this file should
	be similar to

		RewriteEngine On
		RewriteRule (.+)\.html paloose.php?url=$1.html [L,qsappend]

   Author:
   	Name  : Hugh Field-Richards.
   	Email : hsfr@hsfr.org.uk

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

	This software is published under the terms of the LGPL License
	a copy of which has been included with this distribution in the LICENSE file.
             
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Version -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
             
   @since 1.5.2b1
   
   Note that there several variables within this file ({ant:name.server} etc.)
   which are resolved at build time.
   
*/

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// Change the PALOOSE_DIRECTORY address here to indicate where you have put
// the main Paloose folder. It can be relative to your application site folder.
define( 'PALOOSE_DIRECTORY', '../paloose' );
define( 'PALOOSE_LIB_DIRECTORY', PALOOSE_DIRECTORY . '/lib' );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// Where the main log4php logger configuration file is kept. Do not use
// pseudo protocols here. Relative paths are from the Paloose server directory.
// Note that all sites logging comes here.
define( 'PALOOSE_LOG4PHP_DIRECTORY', '{pcms:deploy.folder.log4php}' );
define( 'PALOOSE_LOGGER_CONFIGURATION_FILE', 'configs/log4php.xml' );
define( 'TIME_ZONE', 'Europe/London' );   // 1.5.2b1

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// Where the paloose caches are held. Does not include the images cache for
// the directory. 
define( 'PALOOSE_CACHE_DIR', 'system/resources/cache' );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// On the PCMS system there is a different sitemap depending on the type of request.
define( 'PALOOSE_ROOT_SITEMAP', 'sitemap.xmap' );
define( 'PALOOSE_SITE_ROOT_DIRECTORY', '../{change to reflect location of site}' );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// The error page to give back for User errors (errors in sitemap etc). They
// are not run time errors (missing page (404) ). If you override these to your
// area make sure that you use an absolute directory path - you can use the
// 'context' pseudo-protocol here.
define( 'PALOOSE_USER_EXCEPTION_PAGE', 'context://system/resources/errorHandling/userError.xml' );
define( 'PALOOSE_USER_EXCEPTION_TRANSFORM', 'context://system/resources/transforms/error2xhtml.xsl' );
// Be careful of the path here - although it is relative it looks as an absolute one. It is in
// fact relative to the Apache root document directory. Must always have a leading path
// separator.
define( 'PALOOSE_USER_EXCEPTION_STYLE', '/paloose/resources/styles/userError.css' );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// The file that contains the gallery information in each gallery directory
define( 'PALOOSE_GALLERY_INDEX', 'gallery.xml' );

// Define if PATH on server is not set to pick up correct ImageMagick binaries
//define( 'IMAGEMAGICK_BIN', '/usr/local/bin/' );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// The error page to give back for internal programming errors. These should
// not need to be overridden.
define( 'PALOOSE_INTERNAL_EXCEPTION_PAGE', 'context://system/resources/errorHandling/userError.xml' );
define( 'PALOOSE_INTERNAL_EXCEPTION_TRANSFORM', 'context://system/resources/transforms/error2xhtml.xsl' );
// Be careful of the path here - although it is relative it looks as an absolute one. It is in
// fact relative to the Apache root document directory. Must always have a leading path
// separator.
define( 'PALOOSE_INTERNAL_EXCEPTION_STYLE', '/paloose/resources/styles/userError.css' );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// Change this with caution - should not be changed in 99.9% of cases
define( 'PALOOSE_SITEMAP_NAMESPACE', 'http://apache.org/cocoon/sitemap/1.0' );
define( 'PALOOSE_SOURCE_NAMESPACE', 'http://apache.org/cocoon/source/1.0' );
define( 'PALOOSE_DIR_NAMESPACE', 'http://apache.org/cocoon/directory/2.0' );
define( 'PALOOSE_SQL_QUERY_NAMESPACE', 'http://apache.org/cocoon/SQL/2.0' );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// Change these with caution - should only be changed for different translation
// mechanism. Requires understanding of Paloose internals :-)
define( 'PALOOSE_I18N_NAMESPACE', 'http://apache.org/cocoon/i18n/2.1' );
define( 'PALOOSE_PAGE_HIT_TRANSFORM', 'resource://resources/transforms/PageHit.xsl' );
define( 'PALOOSE_NAMESPACE', 'http://www.paloose.org/schemas/Paloose/1.0' );

define( 'PALOOSE_SESSIONS_REQUIRED', 'yes' );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
// Definitely do not change anything below this line!
ini_set( 'track_errors', '1' );
date_default_timezone_set( defined( 'TIME_ZONE' ) ? TIME_ZONE : "GMT");   // 1.5.2b1
require_once( PALOOSE_DIRECTORY . '/lib/Paloose.php' );
$paloose = new Paloose();
$paloose->run( $_SERVER[ 'REQUEST_URI' ], $_SERVER[ 'QUERY_STRING' ] );
unset( $paloose );

?>