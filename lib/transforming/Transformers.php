<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/Transformer.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>Transformers</i> class maintains a list of transformer components.
 *
 * @package paloose
 * @subpackage transforming
 */

class Transformers {

   /** Default tranformer use when not specified in pipe */
   private $gDefault;
   
   /** Array of transformers (each entry a class: Transformer) and indexed by name */
   private $gTransformers;

   /** The DOM object for this class */
   private $gDOM;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>Transformers</i> class.
    */
    
   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gTransformers = array();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the transformer components.
    *
    * The class takes the transformers list in the components declaration of the
    * site map and parses them. That is all the enclosed tags within
    *
    * <pre>   &lt;map:transformers xmlns:map="http://apache.org/cocoon/sitemap/1.0" default="xslt">
    *       &lt;map:transformer name="xslt" src="resource://lib/transforming/TRAXTransformer" />
    *       ...
    *    &lt;/map:transformers></pre>
    *
    * @param DOMNode $inNode the DOM that contains the contents of this pipe element.
    * @throws UserException if duplicate transformer already exists
    * @throws UserException if component does not have <i>name</i> and <i>src</i> attributes
    * @throws UserException if component constructor throws UserException
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $this->gDOM = new DOMDocument;
      $this->gDOM->appendChild( $this->gDOM->importNode( $inNode, 1 ) );
   
      // Set the namespace for the sitemap. Let us hope people use it.
      $xpath = new domxpath( $this->gDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $component = $xpath->query( "//m:transformer" );
      // Scan around each transformer and set up the list in $gTransformers. Exit false if problem
      foreach ( $component as $node ) { 
         $name = $node->getAttribute( "name" );
         $src = $node->getAttribute( "src" );
         $isCachable = Utilities::normalizeBoolean( $node->getAttribute( "cachable" ) );
         $this->gLogger->debug( "Adding transformer name = '". $name . "' using src '" . $src . "'" );
         if ( !strlen( $name ) or !strlen( $src ) ) {
            throw new UserException(
               "Sitemap component transformer must have name and src attributes defined",
               PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR,
               $this->gDOM );
         }

         // The source may have path details attached so strip the path off to input the correct class.
         if ( strstr( $src, Environment::getPathSeparator() ) == 1 ) {
            $packageFilename = $src;
         } else if ( !strstr( $src, ":" . Environment::getPathSeparator() ) ) {
            // Relative file name so process
            $packageFilename = PALOOSE_LIB_DIRECTORY . Environment::getPathSeparator() . $src;
         } else {
            // Relative file name with pseudo protocol
            $packageFilename = StringResolver::expandPseudoProtocols( $src );
         }
         
         // The source may have path details attached (for example "../lib/FileTransformer"). We need to
         // strip the path off to input the correct class.
         $package = Utilities::stripFileName( $packageFilename );
         // make sure pseudo protocols are expanded first (should be done above - this s just insurance)
         $path = Utilities::stripPath( StringResolver::expandPseudoProtocols( $packageFilename ) );
         $this->gLogger->debug( "Package='". $package . "' : path='" . $path . "'" );
         
         // We have something to add, so first of all check for duplicate names
         if ( array_key_exists( $name, $this->gTransformers ) ) {
            $this->gLogger->debug( "Overriding transformer: '". $name . "'" );
         } else {
            $this->gLogger->debug( "Adding transformer: '". $name . "'" );
         }
         // Get the necessary for forming a specific transformer
         require_once( $path . $package. ".php" );
         $transformerName = "_" . $package;
         $transformer = new $transformerName( $name, $path.$package, $node, $isCachable );
         // Check that the transformer thinks it is valid
         try {
            $transformer->isValid();
            $this->gTransformers[ $name ] = $transformer;
         } catch( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $this->gDOM );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set the default transformer. Will cause exception if not found in list.
    *
    * @param string $inDefault the default transformer name.
    * @throws UserException if cannot find transformer
    */

   public function setDefault( $inDefault )
   {
      if ( !( array_key_exists( $inDefault, $this->gTransformers ) ) ) {
           throw new UserException(
              "Default transformer '". $inDefault . "' not found",
              PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR,
              $this->gDOM );
      }
      $this->gDefault = $inDefault;
      $this->gLogger->debug( "Set default transformer: '". $inDefault . "'" );
      return true;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the default transformer
    *
    * @retval string the default value
    */
    
   public function getDefault()
   {
      return $this->gDefault;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the named transformer.
    *
    * @param string $inType the default transformer type.
    * @retval Transformer the transformer instance.
    * @throws UserException if cannot find transformer.
    */

   public function getTransformer( $inType )
   {
      if ( !( array_key_exists( $inType, $this->gTransformers ) ) ) {
           throw new UserException( "Transformer '". $inType . "' not found", PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR );
      } else {
         return $this->gTransformers[ $inType ];
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets class name of the named transformer.
    *
    * @param string $inType the default transformer type.
    * @retval string the transformer package/class name.
    * @throws UserException if cannot find transformer.
    */

   public function getTransformerClassName( $inType )
   {
      if ( !( array_key_exists( $inType, $this->gTransformers ) ) ) {
           throw new UserException( "Transformer '". $inType . "' not found", PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR );
      } else {
         return $this->gTransformers[ $inType ]->getPackageName();
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this class.
    *
    * @retval string the representation of the class as a string
    */

   public function toString()
   {
      $mess = "  <transformers default='{$this->gDefault}'>\n";
      foreach ( $this->gTransformers as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "  </transformers>\n";
      return $mess;
   }

}

?>