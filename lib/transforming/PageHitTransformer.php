<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>PageHitTransformer</i> provides a very simple mechanism to see how many
 * times a particular page has been accessed.
 * 
 * Individual pages can be targeted rather
 * than a complete site. It is also possible to ignore certain IP numbers.
 *
 * The root sitemap needs to have the <i>PageHitTransformer</i> declared as a component:
 *
 * <pre>   &lt;map:components&gt;
 *      &lt;map:transformers default="xslt"&gt;
 *         &lt;map:transformer name="pageHit" src="resource://lib/transforming/PageHitTransformer"&gt;
 *             &lt;map:parameter name="file" value="context://logs/PageHit.cnt"/&gt;
 *             &lt;map:parameter name="ignore" value="127.0.0.1"/&gt;
 *         &lt;/map:transformer&gt;</pre>
 *
 * where the parameters are
 *
 * <ul>
 *   <li><i>file</i> : the actual counter file where the record is kept.</li>
 *   <li><i>ignore</i> : a comma separated list of IP numbers to ignore.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */

class PageHitTransformer extends TransformerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   /** The file to store counter data in. Must be writable (set to 777 with CHMOD). */
   private $gCounterFileName;

   //IP addresses to ignore. Useful if your IP is static, to ignore your own hits. 
   //IPs in standard form. Separate each address with a comma. 
   private $gIgnoreIP;

   /** Number of retries to lock file before failure */
   const RETRIES = 10;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>PageHitTransformer</i> pipeline element.
    * 
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _TRAXTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if no <i>src</i> attribute
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );

      $expandedString = StringResolver::expandPseudoProtocols( $inComponent->gCounterFileName );
      $this->gCounterFileName = $expandedString;
      $this->gIgnoreIP = $inComponent->gIgnoreIP;

      // See whether there are any local overrides.
      if ( strlen( $this->gParameters->getParameter( 'file' ) ) > 0 )
         $this->gCounterFileName = $this->gParameters->getParameter( 'file' );
      if ( strlen( $this->gParameters->getParameter( 'ignore' ) ) > 0 )
         $this->gIgnoreIP = $this->gParameters->getParameter( 'ignore' );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the inputted DOM document.
    * 
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string. In this case it contains the directory to display.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws InternalException if problem sending page hits parameter to transformer
    * @throws InternalException if problem setting parameter '$name' with value ' $value'
    * @throws UserException if problem setting page hit counter (file permissions etc)
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $PageHitTransformFile = Environment::$configuration[ 'pageHitTransform' ];

      // Translate the string and expand the various inline strings, {1} etc
      $expandedString = StringResolver::expandString( $inVariableStack, $PageHitTransformFile );
      $pageHitTransformFile = StringResolver::expandPseudoProtocols( $expandedString );
      $this->gLogger->debug( "PageHitTransformFile: '$pageHitTransformFile'" );

      // Translate the string and expand the various inline strings, {1} etc
      $expandedString = StringResolver::expandString( $inVariableStack, $this->gCounterFileName );
      $this->gCounterFileName = StringResolver::expandPseudoProtocols( $expandedString );

      // Load the transform
      $xsl = new DomDocument(); 
      $xsl->load( $pageHitTransformFile ); 
      // Create the processor and import the stylesheet
      $proc = new XsltProcessor(); 
      $xsl = $proc->importStylesheet( $xsl );

      // If the use-request-params is set then add to the parameter list
      if ( $this->gComponent->getUseRequestParametersFlag() === true ) {
         $requestParameterModule = Environment::$modules[ 'request-param' ];
         $params = $requestParameterModule->getParams();
         foreach ( $params as $name => $value ) {
            $this->gParameters->setParameter( $name, $value );
         }
      }

      if ( !empty( $this->gParameters->getParameterList() ) ) {
         $parameterList = $this->gParameters->getParameterList();
         foreach ( $parameterList as $name => $value ) {
            $expandedValue = StringResolver::expandString( $inVariableStack, $value );
            $expandedValue = StringResolver::expandPseudoProtocols( $expandedValue );
            if ( $proc->setParameter( '', $name, $expandedValue ) === false ) {
               throw new InternalException( "Problem setting parameter '$name' with value ' $value'", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
            }
         }
      }
      
      try {
         $hitCounter = $this->processHit( $inURL );
         if ( $proc->setParameter( '', "pageHits", $hitCounter ) === false ) {
            throw new InternalException( "Problem sending page hits parameter to transformer", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
         }
      } catch ( UserException $e ) {
          throw new UserException( "PageHit counter problem: \n" . $e->getMessage(), PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, $inDOM );
      } catch ( InternalException $e ) {
          throw new InternalException( $e->getMessage(), $e->getCode(), NULL );
      }
      return $proc->transformToDoc( $inDOM );
   }


   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process hit counter
    *
    * @param string $inPageName the name of the page.
    * @retval int the transformed DOM.
    * throws InternalError if problem
    */

   private function processHit( $inPageName )
   {
      // Open the counter file for reading and writing
      $this->gLogger->debug( "Opening file: {$this->gCounterFileName} for {$inPageName}" );
      $counterFile = NULL;
      if ( !file_exists( $this->gCounterFileName ) ) {
         $counterFile = fopen( $this->gCounterFileName, "x+" );
      } else {
         $counterFile = fopen( $this->gCounterFileName, "r+" );
      }
      if ( !isset( $counterFile ) ) {
         $this->gMess = "Could not open PageHit counter file: '" . $this->gCounterFileName . "'";
          throw new InternalError( "Could not open PageHit counter file: '" . $this->gCounterFileName . "'", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL ); //Other details filled in calling method
      }

      // read in the page values and look out for current page
      $count = 0;
      $hitCounter = 0;
      $found = false;
      while ( $line = fgets( $counterFile, 4096 ) ) {
         $this->gLogger->debug( "Read line '$line' from counter file" );
         $data = explode( ':', $line );
         if ( $data[ 0 ] == $inPageName ) {
            $found = true;
            $hitCounter = (int)$data[ 1 ] + 1;
            $count += strlen( $data[ 0 ] ) + 1;
            $this->gLogger->debug( "Found page to update: '$inPageName' : [$count][$hitCounter]" );
            break;
         }
         $count += strlen( $line );
      }
      
      $ignoreIP = false;
      if ( isset( $this->gIgnoreIP ) ) {
         $ignoreIPList = explode( ',', $this->gIgnoreIP );
         $this->gLogger->debug( "Ignoring hits from " . $this->gIgnoreIP );
         foreach( $ignoreIPList as $ip ) {
            if ( $_SERVER[ 'REMOTE_ADDR' ] == trim( $ip ) ) {
               $ignoreIP = true;
               break;
            }
         }
      }
  
      // only increment if not ignored ip
      if ( !$ignoreIP ) {
         $this->gLogger->debug( "Good IP to add: " . $_SERVER[ 'REMOTE_ADDR' ] );

         $retriesCount = 0;
         $finished = false;
         while ( $retriesCount < self::RETRIES && !$finished ) {
            if ( flock( $counterFile, LOCK_EX ) ) { 
               // only increment if unique
               if ( !$found ) {
                  $this->gLogger->debug( "Page not found" );
                  $count = '0000000000000001';
                  fwrite( $counterFile, ( ( filesize( $this->gCounterFileName ) == 0 ) ? '' : "\r\n" ).$inPageName.':'.$count.':'.time());
                  $hitCounter = (int)$count;
               } else {
                  // page was found, so update its count
                  $this->gLogger->debug( "Page found, writing to " );
                  fseek( $counterFile, $count );
                  fwrite( $counterFile, str_pad( $hitCounter, 16, '0', STR_PAD_LEFT ) );
               }
               flock( $counterFile, LOCK_UN );
               fclose( $counterFile );
               $finished = true;
            }
            $retriesCount ++;
         }
         $this->gLogger->debug( "Finished update" );
         if ( $retriesCount >= self::RETRIES && !$finished ) {
            throw new InternalException(
               "Cannot write page hit data (no lock after $retriesCount tries - permissions on directory/files?) : [{$this->gCounterFileName}]",
               PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
         }
      }
      $this->gLogger->debug( "Hit count: " . $hitCounter );
      return $hitCounter;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_PageHitTransformer</i> class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * The root sitemap needs to have the <i>PageHitTransformer</i> declared as a component:
 *
 * <pre>   &lt;map:components&gt;
 *      &lt;map:transformers default="xslt"&gt;
 *         &lt;map:transformer name="pageHit" src="resource://lib/transforming/PageHitTransformer"&gt;
 *             &lt;map:parameter name="file" value="context://logs/PageHit.cnt"/&gt;
 *             &lt;map:parameter name="ignore" value="127.0.0.1"/&gt;
 *         &lt;/map:transformer&gt;</pre>
 *
 * where the parameters are
 *
 * <ul>
 *   <li><i>file</i> : the actual counter file where the record is kept.</li>
 *   <li><i>ignore</i> : a comma separated list of IP numbers to ignore.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _PageHitTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
    /** Page hit counter */
   public $gCounterFileName;
   
   /** IP addresses to ignore. Useful if your IP is static, to ignore your own hits. 
    * IPs in standard form. Separate each address with a comma. */
   public $gIgnoreIP;

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
       $this->gLogger = Logger::getLogger( __CLASS__ );
       
      $this->gPackageName = "PageHitTransformer";
      $this->gCounterFileName = "";
      $this->gIgnoreIP = "";

      if ( array_key_exists( 'file', $this->gParameters ) ) {
         $this->gCounterFileName = $this->gParameters[ 'file' ];
      }
      if ( array_key_exists( 'ignore', $this->gParameters ) ) {
         $this->gIgnoreIP = $this->gParameters[ 'ignore' ];
      }
      
      $this->gLogger->debug( "Constructed PageHitTransformer component: [file:" . $this->gCounterFileName . "]" );
      $this->gLogger->debug( "   [ignore:" . $this->gIgnoreIP . "]" );
   }

}
?>
