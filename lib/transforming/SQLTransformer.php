<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2008 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>SQLTransformer</i> uses a given XSLT transformer to query a database
 * and return the result as a scrap of XML within the document.
 * A typical use of SQLTransformer would be
 *
 *      <t:verbatim> 
 *   &lt;map:transformers default="xslt">
 *      &lt;map:transformer name="mysql" src="resource://lib/transforming/SQLTransformer">
 *         &lt;map:parameter name="type" value="mysql"/>
 *         &lt;map:parameter name="host" value="localhost:3306"/>
 *         &lt;map:parameter name="user" value="root"/>
 *      &lt;/map:transformer>
 *      &lt;map:transformer name="xslt" src="resource://lib/transforming/TRAXTransformer">
 *         &lt;map:use-request-parameters>true&lt;/map:use-request-parameters>
 *      &lt;/map:transformer>
 *   &lt;/map:transformers>
 *
 *   ...
 *
 *   &lt;map:pipeline>
 *
 *      &lt;map:match pattern="**.html">
 *         &lt;map:generate src="context://content/{1}.xml" label="xml-content"/>
 *         &lt;map:transform type="mysql" label="sql-transform">
 *            &lt;map:parameter name="show-nr-of-rows" value="true"/>
 *         &lt;/map:transform>
 *         ...
 *      &lt;/map:match></t:verbatim>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class SQLTransformer extends TransformerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   /** Host where the database resides */
   public $gHost;
   
    /** Logger instance for this class */
   public $gUser;
   
    /** Logger instance for this class */
   public $gPassword;
   
    /** show number of row */   
   private $gShowNumberOfRows;

   /** Selects the database type */
   public $gDatabaseType;

   /** Database type. */
   const  DBX_GENERIC = 0;
   const  DBX_MYSQL = 1;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>SQLTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _SQLTransformer $inComponent the associated component instance (stores parameters etc)
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
        $this->gLogger = Logger::getLogger( __CLASS__ );
       
        // These are the fixed parameters
        $parameter = $this->gParameters->getParameter( 'host' );
        if ( strlen( $parameter ) > 0 ) {
         $this->gHost = $parameter;
      } else {
         $this->gHost = StringResolver::expandPseudoProtocols( $inComponent->gHost );
      }

        $parameter = $this->gParameters->getParameter( 'user' );
        if ( strlen( $parameter ) > 0 ) {
         $this->gUser = $parameter;
      } else {
         $this->gUser = StringResolver::expandPseudoProtocols( $inComponent->gUser );
      }

       $parameter = $this->gParameters->getParameter( 'password' );
        if ( strlen( $parameter ) > 0 ) {
         $this->gPassword = $parameter;
      } else {
         $this->gPassword = StringResolver::expandPseudoProtocols( $inComponent->gPassword );
      }

       $parameter = $this->gParameters->getParameter( 'show-nr-of-rows' );
        if ( strlen( $parameter ) > 0 ) {
         $this->gShowNumberOfRows = Utilities::normalizeBoolean( $parameter );
      } else {
         $this->gShowNumberOfRows = false;
      }

      /** Selects the database type */
      $this->gDatabaseType = $inComponent->gDatabaseType;

       $this->gLogger->debug( "Constructed SQLTransformer instance: [type:" . $this->gType . "][host:" . $this->gHost . "][user:" . $this->gUser . "][password:" . $this->gPassword . "][show-nr-of-rows:" . $this->gShowNumberOfRows . "]" );
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the document.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws RunException if problem quaerying the document.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Running SQLTransformer with namespace [" . Environment::$configuration[ 'sqlNamespace' ] . "]" );

      // The inputted DOM has a set of <sql:xxx> tags that must be extracted and processed
      // and then replaced with the success/failure code.
      $documentDOM = $inDOM;
      $rootNode = $documentDOM->documentElement;
      $this->gTranslatedDOM = new DOMDocument( '1.0', 'UTF-8' );
      $elementNS = $rootNode->namespaceURI;
      $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
      $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
      $this->gTranslatedDOM->appendChild( $newElement );
      try {
         $this->processNode( $inVariableStack, $rootNode, $newElement, Environment::$configuration[ 'sqlNamespace' ] );
         $this->gTranslatedDOM->formatOutput = true;
         return $this->gTranslatedDOM;
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process a single node.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document (reference)
    * @param string $inSQLNamespace the namespace for all sql query tags
    * @retval DOMDocument the replacement DOM that represents the result.
    * @throws RunException if problem quaerying the document.
    */

   private function processNode( VariableStack $inVariableStack, DOMNode $inParentNode, DOMNode &$inNewParentNode, $inSQLNamespace )
   {
      // Create a new node and update it with the old nodes attributes
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {
         // $this->gLogger->debug( "Processing parent node: <" . $inParentNode->localName . ">" );
         // At this point we have a list of attributes that may need translating to expand variables
         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = StringResolver::expandString( $inVariableStack, $attribute->name );
               $attribValue = StringResolver::expandString( $inVariableStack, $attribute->value );
               $inNewParentNode->setAttribute( $attribName, $attribValue );
               $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         }
      }

      // We have transferred the current node at this point - now we must traverse the children of the
      // old documents current node.
      $children = $inParentNode->childNodes;

      // Walk this list of children and check for each one being a i18n namespace candidate
      try {
         foreach ( $children as $child ) {
            if ( $child->nodeType == XML_ELEMENT_NODE ) {
               $this->gLogger->debug( "Processing: <" . $child->namespaceURI . ":" . $child->localName . ">" . " [" . $inSQLNamespace . "]"  );
            }
            if ( $child->localName == "execute-query" && $child->namespaceURI == $inSQLNamespace ) {
               $this->gLogger->debug( "Found node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
               // Now process the child node - calls this recursively
               // $newElementNode = $this->gTranslatedDOM->createElementNS( $inSQLNamespace, "rowset" );
               $this->processNode( $inVariableStack, $child, $inNewParentNode, $inSQLNamespace );
            } else if ( $child->localName == "query" && $child->namespaceURI == $inSQLNamespace ) {
               $query = $child->nodeValue;
               
               $this->gLogger->debug( "Variable stack: '" . $inVariableStack->toString() . "'" );
               $this->gLogger->debug( "Raw query: '" . $query . "'" );
               $query = StringResolver::expandPseudoProtocols( StringResolver::expandString( $inVariableStack, $query ) );
               $this->gLogger->debug( "   new query: '" . $query . "'" );
               
               $this->gLogger->debug( "Found node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
               $this->gLogger->debug( "   query string: " . $query );
              
               // Now the hard part to submit the query string to the database. The errors need trapping at all stages
               // of the connection to the database. I could put in a separate Exception but I hope the RunException
               // will be enough. It means nesting them which may be a problem.
               try {
                  $queryName = $child->getAttribute( 'name' );
                  $databaseName = $child->getAttribute( 'database' );
                  if ( strlen( $databaseName ) == 0 ) {
                     throw new RunException( "No SQL database specified for query: ", PalooseException::SQL_ERROR_TYPE, NULL );
                  }
                  $replacementDOM = $this->queryMySQLDatabase( $this->gHost, $this->gUser, $this->gPassword, $databaseName, $queryName, $query, $inSQLNamespace );
                  
                  // Utilities::outputStringVerbatim( $replacementDOM->saveXML() );
                  // $replacementDom has the results which we need to put in in place of the query element
                  // Get root element of the replacement 
                  $replacementNode = $replacementDOM->documentElement;
                  $newNode = $this->gTranslatedDOM->importNode( $replacementNode, true );
                  // $this->gLogger->debug( "Adding : " . $replacementNode->localName . " to " . $inNewParentNode->localName );
                  $inNewParentNode->appendChild( $newNode );
               } catch ( RunException $e ) {
                  // Form up error DOM fragment
                  $this->gLogger->debug( "mySQL error: " . $e->getMessage() );
                  $this->gLogger->debug( "  [host:" . $this->gHost . "][user:" . $this->gUser . "][password:" . $this->gPassword . "]" );
                  $this->gLogger->debug( "  [catalogue:" . $databaseName . "][query:" . $query . "]" );
                  $replacementDOM = $this->formatErrorFragment( $query, $e->getMessage(), $inSQLNamespace );
                  $replacementRoot = $replacementDOM->documentElement;
                  $newElementNode = $this->gTranslatedDOM->importNode( $replacementRoot, true );
                  $inNewParentNode->appendChild( $newElementNode );
               }
            } else { 
               // Pass all others through
               if ( $child->nodeType == XML_ELEMENT_NODE ) {
                 // $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
                 $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
                 // Now process the child node - calls this recursively
                 $this->processNode( $inVariableStack, $child, $newElementNode, $inSQLNamespace );
                 $inNewParentNode->appendChild( $newElementNode );
               } else if ( $child->nodeType == XML_TEXT_NODE ) {
                  // $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
                  $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
                  $inNewParentNode->appendChild( $newTextNode );
               }
            }
         }
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
   }
  
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Query the MySQL Database.
    *
    * @param string $inHost the name of the host where the database server runs
    * @param string $inUser the user name for the database
    * @param string $inPassword the associated password
    * @param string $inDatabaseName name of database to query
    * @param string $inQueryName name of this query
    * @param string $inQuery the query string
    * @param string $inSQLNamespace the SQL namespace
    * @retval DOMDocument the DOM representing the answer to the query
    * @throws RunException if problem quaerying the document.
    */
  
   private function queryMySQLDatabase( $inHost, $inUser, $inPassword, $inDatabaseName, $inQueryName, $inQuery, $inSQLNamespace )
   {
      $link = mysql_connect( $inHost, $inUser, $inPassword );
      if ( !$link ) {
         throw new RunException( "SQL connect error: " . mysql_error(), PalooseException::SQL_ERROR_TYPE, NULL );
      }
       $db_selected = mysql_select_db( $inDatabaseName, $link );
      if ( !$db_selected ) {
         mysql_close( $link );
         throw new RunException( "SQL cannot use database [" . $inDatabaseName . "] :" . mysql_error(), PalooseException::SQL_ERROR_TYPE, NULL );
      }
      $this->gLogger->debug( "SQL connect to [$inDatabaseName] with query [$inQuery]:[$inQueryName]"  );
      $result = mysql_query( $inQuery );
      if ( !$result ) {
         mysql_close( $link );
         throw new RunException( " SQL query error: " . mysql_error(), PalooseException::SQL_ERROR_TYPE, NULL );
      }

      $dom = new DOMDocument();
      
      if ( $result === true ) {
         // Result of an INSERT etc
         $root = $dom->createElementNS( $inSQLNamespace, 'result', "true" );
         $rootNode = $dom->appendChild( $root );
         $this->gLogger->debug( "Returning operation true ..."  );
         $rootNode->setAttribute( "name", $inQueryName );
      } else {
         // Result of a query to get data
         $root = $dom->createElementNS( $inSQLNamespace, 'row-set' );
         $rootNode = $dom->appendChild( $root );
         $numOfRows = 0;
         $this->gLogger->debug( "Checking results ..."  );
         while ( $row = mysql_fetch_assoc( $result ) ) {
            $numOfRows++;
            $rowElement = $dom->createElementNS( $inSQLNamespace, 'row' );
            $rowNode = $root->appendChild( $rowElement );
            foreach ( $row as $name => $value  ) {
               // $value should contain a UTF-8 string here - but still check to make sure
               $encoding = mb_detect_encoding( $value, "auto" );
               if ( $encoding == "UTF-8" ) {
                  if ( Utilities::is_utf8( $value ) ) {
                     $normalisedValue = $value;
                  } else {
                     $normalisedValue = utf8_encode( $value );
                  }
               } else if ( $encoding == "ASCII" ) {
                  $normalisedValue = $value;
               }
               $rowElement->appendChild( $dom->createElementNS( $inSQLNamespace, $name, $normalisedValue ) );     
            }
         }
         $rootNode->setAttribute( "nrofrows", $numOfRows );
         $rootNode->setAttribute( "name", $inQueryName );
      }

      mysql_close( $link );
      return $dom;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Form up the error XML.
    *
    * This is a divergence from Cocoon. I cannot find any details of a suitable error format for
    * SQL error reporting. I can easily change this as the situation changes.
    *
    * &lt;sql-namespace:sql-error>
    *    &lt;sql-namespace:message>a message about what happened&lt;/sql-namespace:message>
    * &lt;/sql-namespace:sql-error>
    *
    * @param string $inQuery query message
    * @param string $inMessage error message to output
    * @param string $inSQLNamespace the SQL namespace
    * @retval DOMDocument the error DOM fragment
    */
  
   private function formatErrorFragment( $inQuery, $inMessage, $inSQLNamespace )
   {

      
      $dom = new DOMDocument();
      $root = $dom->createElementNS( $inSQLNamespace, 'sql-error' );
      $dom->appendChild( $root );
      $root->appendChild( $dom->createElementNS( $inSQLNamespace, 'host', $this->gHost ) );
      $root->appendChild( $dom->createElementNS( $inSQLNamespace, 'user', $this->gUser  ) );
      $root->appendChild( $dom->createElementNS( $inSQLNamespace, 'password', $this->gPassword  ) );
      $root->appendChild( $dom->createElementNS( $inSQLNamespace, 'message', $inMessage . " => query: " . $inQuery ) );
      return $dom;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_SQLTransformer</i> holds the information for the component.
 *
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _SQLTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
    /** Host where the database resides */
   public $gHost;
   
    /** User used to access database */
   public $gUser;
   
    /** Password used to access database */
   public $gPassword;
   
   /** Selects the database type */
   public $gDatabaseType;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "SQLTransformer";
      
      $this->gHost = "";
      if ( strlen( $this->gParameters[ 'host' ] ) > 0 ) $this->gHost = $this->gParameters[ 'host' ];

      $this->gUser = "";
      if ( strlen( $this->gParameters[ 'user' ] ) > 0 ) $this->gUser = $this->gParameters[ 'user' ];

      $this->gPassword = "";
      if ( strlen( $this->gParameters[ 'password' ] ) > 0 ) $this->gPassword = $this->gParameters[ 'password' ];

      $this->gDatabaseType = SQLTransformer::DBX_GENERIC;
      if ( strlen( $this->gParameters[ 'type' ] ) > 0 ) {
         $type = $this->gParameters[ 'type' ];
         if ( $type == 'mysql' ) {
            $this->gDatabaseType = SQLTransformer::DBX_MYSQL;
         }
      }
      
      $this->gLogger->debug( "Constructed _SQLTransformer component: [host:" . $this->gHost . "][user:" . $this->gUser . "][password:" . $this->gPassword . "]" );
   }

}
?>