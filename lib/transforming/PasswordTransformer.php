<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/authentication/EncryptBF.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>PasswordTransformer</i> takes a username and password in plain text
 * and encodes the password.
 * 
 * Typical XML in:
 * <pre> ====================================================================== 
 *    &lt;authentication usename="hsfr" password="mypassword" /&gt;
 * ======================================================================</pre>
 *
 * Typical XML out:
 * <pre> ====================================================================== 
 *    &lt;authentication usename="hsfr" password="$2y$10$w0xPLJdk7FEN3HjadW6PmuKyD1z8Dg8KF9ADZtYYt7CavXKEqe.Li" /&gt;
 * ======================================================================</pre>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class PasswordTransformer extends TransformerPipeElement implements PipeElementInterface
{

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>PasswordTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _PasswordTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if no <i>src</i> attribute
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
       try {
          parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
       } catch ( UserException $e ) {
          throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
       }
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the input DOM (in $inDOM) and returns a transformed DOM.
    *
    * Typical XML in:
    * <pre> ====================================================================== 
    *    &lt;authentication username="hsfr" password="mypassword" /&gt;
    * ======================================================================</pre>
    *
    * Typical XML out:
    * <pre> ====================================================================== 
    *    &lt;authentication username="hsfr" password="$2y$10$w0xPLJdk7FEN3HjadW6PmuKyD1z8Dg8KF9ADZtYYt7CavXKEqe.Li" /&gt;
    * ======================================================================</pre>
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the document DOM through the pipeline to transform.
    * @retval DOMDocument the transformed DOM.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $rootNode = $inDOM->documentElement;
      $this->gTranslatedDOM = new DOMDocument();
      $elementNS = $rootNode->namespaceURI;
      $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
      $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
      $this->gTranslatedDOM->appendChild( $newElement );
      $this->processNode( $inVariableStack, $rootNode, $newElement );
      return $this->gTranslatedDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document
    */

   private function processNode( VariableStack $inVariableStack, DOMNode $inParentNode, DOMNode &$inNewParentNode )
   {
      // Create a new node and update it with the old nodes attributes
      $autheticateTagFound = false;
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {
         $encodedAttribs = array();
         if ( $inParentNode->hasAttributes() and $inParentNode->localName == "authentication" ) {
            $autheticateTagFound = true;
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = $attribute->name;
               $attribValue = $attribute->value;
               $this->gLogger->debug( "   Found attribute '$attribName' = '$attribValue'" );
               $encodedAttribs[ $attribName ] = $attribValue;
            }
         } else if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = StringResolver::expandString( $inVariableStack, $attribute->name );
               $attribValue = StringResolver::expandString( $inVariableStack, $attribute->value );
               $inNewParentNode->setAttribute( $attribName, $attribValue );
               $this->gLogger->debug( "   Found attribute '$attribName' = '$attribValue'" );
            }
         }
      }
      
      // At this point we have a list of attributes for the name node check for username and password
      // and generate encrypted password. If there is no password given then return a empty string.
      if ( $autheticateTagFound === true ) {
         if ( $encodedAttribs[ "password" ] != "" ) {
            $encodedPassword = EncryptBF::encrypt( $encodedAttribs[ "username" ], $encodedAttribs[ "password" ] );
         } else {
            $encodedPassword = "";
         }
         $this->gLogger->debug( "   Encoded password: $encodedPassword" );
         $encodedAttribs[ "password" ] = $encodedPassword;
      }
      
      foreach ( $encodedAttribs as $name => $value ) {
         $inNewParentNode->setAttribute( $name, $value );
      }
 
      // We have transferred the current node at this point - now we must traverse the children of the
      // old documents current node.
      $children = $inParentNode->childNodes;

      foreach ( $children as $child ) {
         $this->gLogger->debug( "Processing child node: <" . $child->nodeName . ">" );
         if ( $child->nodeType == XML_ELEMENT_NODE ) {
            // Just an ordinary element not in to be translated so make a new element and append it.
            $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
            $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
            // Now process the child node - calls this recursively
            $this->processNode( $inVariableStack, $child, $newElementNode );
            $inNewParentNode->appendChild( $newElementNode );
         } else if ( $child->nodeType == XML_TEXT_NODE ) {
            // Straight copy by appending to child list of new parent node
            $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
            $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
            $inNewParentNode->appendChild( $newTextNode );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_PasswordTransformer</i> class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * <pre>   &lt;map:components>
 *      &lt;map:transformers default="xslt">
 *          &lt;map:transformer name="password"/>
 *      &lt;/map:transformers>
 *   &lt;/map:components></pre>
 *
 * The attributes are similar to those used in Cocoon: 
 * 
 * <ul>
 *   <li><i>name</i> : the name of the transformer used within the pipelines.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _PasswordTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "PasswordTransformer";
   }

}
?>
