<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>LogTransformer</i> provides a means to output parts of the pipeline DOM
 * to a log file.
 * 
 * Typical use:
 *
 * <pre>   &lt;map:match pattern="news-rss.xml">
 *      &lt;map:generate src="context://content/newsArticles.xml"/>
 *      &lt;map:transform type="log">
 *         &lt;map:parameter name="logfile" value="context://logs/logfile-aggr.log"/>
 *         &lt;map:parameter name="append" value="yes"/>
 *         &lt;map:parameter name="filter" value="//page:content"/>
 *      &lt;/map:transform>
 *      &lt;map:transform src="context://resources/transforms/xml2rss.xsl"/>
 *      &lt;map:serialize/>
 *   &lt;/map:match></pre>
 *
 * and the following parameters are used as defaults: 
 * <ul>
 *   <li><i>logfile</i> : the name of the logfile to use (making this not thread safe).</li>
 *   <li><i>append</i> : whether the logging information should be appended or written afresh.</li>
 *   <li><i>filter</i> : which part of the DOM whould be output to the log file (XPath expression).</li>
 * </ul>
 *
 *
 * @package paloose
 * @subpackage transforming
 */
 
class LogTransformer extends TransformerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

    /** Append flag */   
   private $gAppend;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>LogTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _LogTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if no <i>src</i> attribute
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );

      if ( $this->gParameters->getParameter( "logfile" ) == NULL ) {
         throw new UserException( "Must have logfile attribute",
            PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR,
            $inDOM );
      } else {
         $filename = StringResolver::expandPseudoProtocols( $this->gParameters->getParameter( "logfile" ) );
         $this->gParameters->setParameter( "logfile", $filename );
      }
      if ( $this->gParameters->getParameter( "append" ) == NULL ) {
         $this->gParameters->setParameter( "append", "yes" );
      }
      $this->gAppend = Utilities::normalizeBoolean( $this->gParameters->getParameter( "append" ) );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the document.
    *
    * The LogTransformer processes the input DOM. If there are no errors returns the inputted
    * DOM and the necessary extracts written to an external file. All errors are caught by exception..
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws RunException if problem.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $fileName = $this->gParameters->getParameter( "logfile" );
      $this->gLogger->debug( "Filename: $fileName" );
      if ( $this->gAppend == 0 ) {
         $this->gLogger->debug( "Opening log for overwrite: $fileName" );
         if ( !$fileHandle = fopen( $fileName, "w" ) ) {
              throw new RunException( "Cannot open log file for overwrite: $fileName", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
         }
      } else {
         $this->gLogger->debug( "Opening log for append: $fileName" );
         if ( !$fileHandle = fopen( $fileName, "a" ) ) {
              throw new RunException( "Cannot open log file for append: $fileName", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
         }
      }

      $filterXPath = $this->gParameters->getParameter( "filter" );
      $xpath = new domxpath( $inDOM );
      if ( $filterXPath == NULL ) {
          $filterXPath = "/*";  // Complete document
      }
      $this->gLogger->debug( "Filter: $filterXPath" );
      $logNode = Utilities::getXPathListNode( 0, $xpath, $filterXPath );
      $logString = $inDOM->saveXML( $logNode );
      // Utilities::outputStringVerbatim( $logString );
      /* if ( fwrite( $fileHandle, "\n==============================================================================\n" ) === false ) {
           throw new RunException( "Cannot write to log file: $fileName", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
      } */
      if ( fwrite( $fileHandle, $logString ) === false ) {
           throw new RunException( "Cannot write to log file: $fileName", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
      }
      if ( !fclose( $fileHandle ) ) {
           throw new RunException( "Cannot close log file: $fileName", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
      }
      return $inDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_LogTransformer</i> holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * <pre>   &lt;map:components>
 *      &lt;map:transformers default="xslt">
 *          &lt;map:transformer name="log" src="resource://lib/transforming/LogTransformer"/>
 *      &lt;/map:transformers>
 *   &lt;/map:components></pre>
 *
 * where: 
 * <ul>
 *   <li><i>name</i> : the name of the transformer used within the pipelines.</li>
 *   <li><i>src</i> : the package to be used. In Paloose this is the filename of the PHP file (this file)
 *       without the php extension.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _LogTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "LogTransformer";
   }

}
?>
