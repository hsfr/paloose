<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/authentication/EncryptBF.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>BlowfishVerifyTransformer</i> takes a username and password in plain text
 * and encodes the password.
 * 
 * Typical XML in:
 * <pre> ====================================================================== 
 *    &lt;authentication usename="hsfr" password="mypassword" /&gt;
 * ======================================================================</pre>
 *
 * Typical XML out:
 * <pre> ====================================================================== 
 *    &lt;authentication usename="hsfr" password="$2y$10$w0xPLJdk7FEN3HjadW6PmuKyD1z8Dg8KF9ADZtYYt7CavXKEqe.Li" /&gt;
 * ======================================================================</pre>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class BlowfishVerifyTransformer extends TransformerPipeElement implements PipeElementInterface
{

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>BlowfishVerifyTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _BlowfishVerifyTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if no <i>src</i> attribute
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
       try {
          parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
       } catch ( UserException $e ) {
          throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
       }
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the input DOM (in $inDOM) and returns a transformed DOM.
    *
    * Typical XML in:
    * <pre> ====================================================================== 
    *     &lt;authentication username="hsfr" password="xgyiowrtn">
    *        &lt;users>
    *           &lt;user>
    *              &lt;username>hsfr&lt;/username>
    *              &lt;password>$2y$10$Ba4Gqbd8YwNRRWCjcwI7fem40vfbNlDoAoSxjkNesMRWXI9fYjSVO&lt;/password>
    *              &lt;data>
    *                 &lt;roles>displayPages,editPages,addDeletePages,displayUsers,editUsers,addDeleteUsers&lt;/roles>
    *                 &lt;fullname>Hugh Field-Richards&lt;/fullname>
    *                 &lt;email>hsfr@hsfr.org.uk&lt;/email>
    *                 &lt;telephone>01234567&lt;/telephone>
    *                 &lt;comments>super-admin&lt;/comments>
    *              &lt;/data>
    *           &lt;/user>
    *        &lt;/users>
    *    &lt;/authentication>
    * ======================================================================</pre>
    *
    * Typical XML out if verified:
    * <pre> ======================================================================
    *     &lt;authentication verify="true">
    *        &lt;users>
    *           &lt;user>
    *              &lt;username>hsfr&lt;/username>
    *              &lt;password>$2y$10$Ba4Gqbd8YwNRRWCjcwI7fem40vfbNlDoAoSxjkNesMRWXI9fYjSVO&lt;/password>
    *           &lt;/user>
    *        &lt;/users>
    *    &lt;/authentication>
    * ======================================================================</pre>
    *
    * XML out if not verified:
    * <pre> ======================================================================
    *     &lt;authentication verify="false"/>
    * ======================================================================</pre>
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the document DOM through the pipeline to transform.
    * @retval DOMDocument the transformed DOM.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $documentDOM = $inDOM;
      $rootNode = $documentDOM->documentElement;
      
      $this->gTranslatedDOM = new DOMDocument();
      $elementNS = $rootNode->namespaceURI;
      $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
      $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );

      $username = "";
      $password = "";
      $encryptedPassword = "";

      $xpath = new DOMXpath( $inDOM );
      $nodes = $xpath->query( "/authentication/@username" );
      if ( !is_null( $nodes ) ) {
         $username = $nodes[0]->textContent;
      }
      $this->gLogger->debug( "User name: '" . $username . "'" );
      
      $nodes = $xpath->query( "/authentication/@password" );
      if ( !is_null( $nodes ) ) {
         $password = $nodes[0]->textContent;
      }
      $this->gLogger->debug( "Password: '" . $password . "'" );
      
      $nodes = $xpath->query( "//password" );
      if ( !is_null( $nodes ) ) {
         $encryptedPassword = $nodes[0]->textContent;
      }
      $this->gLogger->debug( "Encrypted password: '" . $encryptedPassword . "'" );

      // If the password is empty then assume user ok (only happens in system development).
      // Should be removed in really secure system!
      
      $emptyPassword = ( strlen($password) == 0 and $encryptedPassword == 0 );
      if ( EncryptBF::verify( $username, $password, $encryptedPassword ) or $emptyPassword ) {
         $this->gLogger->debug( "User verified" );
         $xmlString = "<authentication verify='true'>
   <users>
      <user>
         <username>$username</username>
         <password>$encryptedPassword</password>
      </user>
   </users>
</authentication>";
      } else {
         $this->gLogger->debug( "User not verified" );
         $xmlString = "<authentication verify='false'/>";
      }

      $this->gTranslatedDOM = new DOMDocument();
      $this->gTranslatedDOM->loadXML( $xmlString );
      $this->gLogger->debug( "Returned XML:\n" . $this->gTranslatedDOM->saveXML() );
   
      return $this->gTranslatedDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_BlowfishVerifyTransformer</i> class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * <pre>   &lt;map:components>
 *      &lt;map:transformers default="xslt">
 *          &lt;map:transformer name="password"/>
 *      &lt;/map:transformers>
 *   &lt;/map:components></pre>
 *
 * The attributes are similar to those used in Cocoon: 
 * 
 * <ul>
 *   <li><i>name</i> : the name of the transformer used within the pipelines.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _BlowfishVerifyTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "BlowfishVerifyTransformer";
   }

}
?>
