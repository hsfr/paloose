<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/gallery/GalleryIndex.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>GalleryTransformer</i> maintains a simple picture gallery.
 *
 * It is used in the pipeline as follows
 *
 * <pre>   &lt;map:match pattern="*.xml">
 *        &lt;map:generate src="context://content/{1}.xml"/>
 *        &lt;map:transform type="gallery"/>
 *        &lt;map:transform src="context://resources/transforms/gallery2html.xsl"/>
 *        &lt;map:serialize/>
 *    &lt;/map:match></pre>
 *
 * You can also add a root attribute to override the component declaration.
 * Note that underlying this is an XSL gallery transformer, gallery2html.xsl. This is to process the output of the 
 * transformer into HTML. Wherever a gallery is to be used have the following
 * 
 * <pre>   &lt;paloose:gallery xmlns:paloose="http://www.paloose.org/schemas/Paloose/1.0"/></pre>
 *
 * After being precessed by the <i>GalleryTransformer</i> we might get the following, for example from the Guinness Park Farm site
 *
 * <pre>   &lt;paloose:gallery xmlns:paloose="http://www.paloose.org/schemas/Paloose/1.0">
 *     &lt;paloose:name>Photos&lt;/paloose:name>
 *     &lt;paloose:dir>&lt;/paloose:dir>
 *     &lt;paloose:breadcrumb>
 *        &lt;paloose:name src="">GPF&lt;/paloose:name>
 *     &lt;/paloose:breadcrumb>
 *     &lt;paloose:description>
 *        &lt;t:p>
 *           We will add galleries as we get them. If you have any to put up please email them to
 *           &lt;link:link type="email" ref="xxx@xxx.xxx"/>. Any format is acceptable, but
 *           please do not process them before you send them. If they are too many or too big
 *           then let us have them on CDROM.&lt;/t:p>
 *     &lt;/paloose:description>
 *     &lt;paloose:folders>
 *        &lt;paloose:folder src="general">General Views around the farm (lessons etc)&lt;/paloose:folder>
 *        &lt;paloose:folder src="RDA">Some of our RDA activities&lt;/paloose:folder>
 *        &lt;paloose:folder src="racing">Our racing successes!&lt;/paloose:folder>
 *        &lt;paloose:folder src="horses">Some of our horses&lt;/paloose:folder>
 *     &lt;/paloose:folders>
 *     &lt;paloose:images type="multi"/>
 *   &lt;/paloose:gallery></pre>
 *
 * Image information is displayed in a similar fashion to the above as:
 *
 * <pre>   &lt;paloose:gallery&gt;
 *      &lt;paloose:name&gt;Farm Views&lt;/paloose:name&gt;
 *      &lt;paloose:dir&gt;general/farm&lt;/paloose:dir&gt;
 *     &lt;paloose:breadcrumb&gt;
 *        &lt;paloose:name src=""&gt;GPF&lt;/paloose:name&gt;
 *        &lt;paloose:name src="general"&gt;General&lt;/paloose:name&gt;
 *        &lt;paloose:name src="general/farm"&gt;The Farm&lt;/paloose:name&gt;
 *     &lt;/paloose:breadcrumb&gt;
 *     &lt;paloose:description&gt;
 *        &lt;t:p&gt;Some selected views around the farm. As you can see the countryside
 *           around here is beautiful with even a hack for an hour staying close to the
 *        farm or its immediate surrounds.&lt;/t:p&gt;
 *     &lt;/paloose:description&gt;
 *     &lt;paloose:folders/&gt;
 *     &lt;paloose:images type="multi"&gt;
 *        &lt;paloose:image img="752d02b392a887821236a05a85280f7f.jpg"
 *           thumb="752d02b392a887821236a05a85280f7f-th.jpg" name="gpf-1.jpg"&gt;&lt;/paloose:image&gt;
 *        &lt;paloose:image img="b77f713a4467e1654739a2a87dcfa767.jpg"
 *           thumb="b77f713a4467e1654739a2a87dcfa767-th.jpg" name="gpf-2.jpg"&gt;&lt;/paloose:image&gt;
 *        &lt;paloose:image img="a6387faadcda0c4ea77c43794a79a1af.jpg"
 *           thumb="a6387faadcda0c4ea77c43794a79a1af-th.jpg" name="gpf-3.jpg"&gt;&lt;/paloose:image&gt;
 *        &lt;paloose:image img="f42af50ff4d47343ac3afc0a918cf5b2.jpg"
 *           thumb="f42af50ff4d47343ac3afc0a918cf5b2-th.jpg" name="gpf-4.jpg"&gt;&lt;/paloose:image&gt;
 *     &lt;/paloose:images&gt;
 *   &lt;/paloose:gallery&gt;</pre>
 *
 * Note that the folder in this case is null. It is perfectly possible to mix folders and images.
 *
 * For an example try {@link http://www.guinnessparkfarm.co.uk/gpf-1/gallery.html}.
 *
 * @package paloose
 * @subpackage transforming
 */
 
class GalleryTransformer extends TransformerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   /** The root directory where to start */
   private $gRootDirectory;

   /** The request image for a simple image display */
   private $gRequestedImage;

   /** The gallery directory relative to the root directory */
   private $gGalleryDirectory;

   /** The name of the next image (original file name) */
   private $gNextImage;

   /** The name of the previous image (original file name) */
   private $gPreviousImage;

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>GalleryTransformer</i> pipeline element.
    * 
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _TRAXTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if no <i>src</i> attribute
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
       $this->gLogger = Logger::getLogger( __CLASS__ );

      if ( !is_null( $this->gParameters->getParameter( 'root' ) ) && strlen( $this->gParameters->getParameter( 'root' ) ) > 0 ) {
         $this->gRootDirectory = $this->gParameters->getParameter( 'root' );
      } else {
         $this->gRootDirectory = StringResolver::expandPseudoProtocols( $inComponent->gRootDirectory );
      }
      if ( strrpos( $this->gRootDirectory, Environment::getPathSeparator() ) < strlen( $this->gRootDirectory ) - 1 ) {
         // Add trailing separator to root directory if necessary
         $this->gRootDirectory = $this->gRootDirectory . Environment::getPathSeparator();
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the inputted DOM document.
    * 
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string. In this case it contains the directory to display.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gDOM = $inDOM;

      // First thing is to analyse the Query string.
      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $galleryDirectory = $requestParameterModule->get( 'src' );
      $this->gGalleryDirectory = $galleryDirectory;

      $this->gRequestedImage = $requestParameterModule->get( 'name' );
      $this->gNextImage = $requestParameterModule->get( 'next' );
      $this->gPrevImage = $requestParameterModule->get( 'prev' );

      $fullGalleryDirectory = $this->gRootDirectory . $galleryDirectory;
      if ( strlen( $galleryDirectory ) > 0 and strrpos( $fullGalleryDirectory, Environment::getPathSeparator() ) < strlen( $fullGalleryDirectory ) ) {
         $fullGalleryDirectory = $fullGalleryDirectory . Environment::getPathSeparator();
      }

      $rootNode = $inDOM->documentElement;
      $this->gTranslatedDOM = new DOMDocument();
      $elementNS = $rootNode->namespaceURI;
      $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
      $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
      $this->gTranslatedDOM->appendChild( $newElement );

      // No image so treat as a gallery request
      $this->gLogger->debug( "Building Gallery from directory: '" . $fullGalleryDirectory . "'" );
      $this->gLogger->debug( "Displaying single image: '" . $this->gRequestedImage . "'" );
      $this->gLogger->debug( "    directory: '" . $fullGalleryDirectory . "'" );
      $this->gLogger->debug( "    src: '" . $this->gGalleryDirectory . "'" );
      $this->gLogger->debug( "    next: '" . $this->gNextImage . "'" );
      $this->gLogger->debug( "    prev: '" . $this->gPrevImage . "'" );
      
      // Walk the DOM tree and extract the gallery nodes and replace them.
      $this->processNode( $fullGalleryDirectory, $rootNode, $newElement );
      return $this->gTranslatedDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process single element node.
    *
    * This recursive function traverses the DOM tree and processes the gallery
    * elements.
    *
    * @param string $inGalleryDirectory the directory to process (changes at each level)
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document
    */

   private function processNode( $inGalleryDirectory, DOMNode $inParentNode, DOMNode $inNewParentNode )
   {

     
      // Create a new node and update it with the old nodes attributes
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {

         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = $attribute->name;
               $attribValue = $attribute->value;
               $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         }

         // Copy over the attributes
         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = $attribute->name;
               $attribValue = $attribute->value;
               $inNewParentNode->setAttribute( $attribName, $attribValue );
               $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         } else {
            // No attributes
         }
      }

      // We have transfered the current node at this point - now we must traverse the children of the
      // old documment's current node.
      $children = $inParentNode->childNodes;

      // Walk this list of children and check for each one being a i18n namespace candidate
      foreach ( $children as $child ) {
         $this->gLogger->debug( "Processing child node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
         if ( $child->nodeType == XML_ELEMENT_NODE ) {
         
            if ( $child->namespaceURI == Environment::$configuration[ 'palooseNamespace' ] and $child->localName == "gallery" ) {
               $this->gLogger->debug( "Processing gallery child node: <" . $child->nodeName . "> [$inGalleryDirectory]" );

               // The paloose gallery element has one attribute
               // root - the root of the gallery. This will override whatever is in the sitemap.
               
               $rootDir = $child->getAttribute( "root" );

               if ( strlen( $rootDir ) > 0 ) {
                  $rootDir = StringResolver::expandPseudoProtocols( $rootDir );
                  if ( strlen( $rootDir ) > 0 and strrpos( $rootDir, Environment::getPathSeparator() ) < strlen( $rootDir ) ) {
                     $rootDir = $rootDir . Environment::getPathSeparator();
                  }
                  $fullGalleryDirectory = $rootDir . $this->gGalleryDirectory;
               } else {
                  $fullGalleryDirectory = $inGalleryDirectory;
               }
               
               // $fullGalleryDirectory now has the root and path from which to start.
               
               $this->gLogger->debug( "Building Gallery from directory: '" . $fullGalleryDirectory . "'" );
               $nodeName = $child->localName;
               $fullGalleryDirectory = StringResolver::expandPseudoProtocols( $fullGalleryDirectory );

               try {
                  if ( strlen( $this->gRequestedImage ) == 0 ) {
                     $this->processGallery( $fullGalleryDirectory, $child, $inNewParentNode );
                  } else {
                     $this->processImage( $fullGalleryDirectory, $child, $inNewParentNode );
                  }
               } catch ( UserException $e ) {
                  throw new UserException( $e->getmessage(), $e->getCode(), $e->getDOMScrap() );
               }

               // We do not call processNode recursively since we eat the paloose elements 
          } else {
              // Just an ordinary element not in to be translated so make a new element and append it.
              $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
              $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
              // Now process the child node - calls this recursively
              $this->processNode( $inGalleryDirectory, $child, $newElementNode );
              $inNewParentNode->appendChild( $newElementNode );
              }
         } else if ( $child->nodeType == XML_TEXT_NODE ) {
            // Straight copy by appending to child list of new parent node
            $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
            $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
            $inNewParentNode->appendChild( $newTextNode );
         }
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the gallery element.
    *
    * @param string $inGalleryDirectory where the current gallery is.
    * @param DOMNode $inChildNode the node we are processing.
    * @param DOMNode $inNewParentNode the parent node where we will add the child based on $inChildNode.
    */

   private function processGallery( $inGalleryDirectory, DOMNode $inChildNode, DOMNode $inNewParentNode )
   {

      $this->gLogger->debug( "Processing gallery: '" . $inGalleryDirectory . "' : " . $inChildNode->nodeName );

      //Read the gallery index file
      $galleryIndexName = $inGalleryDirectory . Environment::$configuration[ 'galleryIndex' ];
      if ( !file_exists( $galleryIndexName ) ) {
         throw new UserException( "Cannot find gallery index file: $galleryIndexName", 0, NULL );
      }
      $this->gLogger->debug( "Found gallery index file: '" . $galleryIndexName . "'" );

      $galleryIndexDOM = new DOMDocument();
      if ( !$galleryIndexDOM->load( $galleryIndexName ) ) {
         throw new UserException( $e->getMessage(), $e->getCode(), NULL );
      }
      try {
         $galleryIndex = new GalleryIndex( $galleryIndexDOM,
                                 $inGalleryDirectory,
                                 $this->gComponent );
          //StringResolver::expandPseudoProtocols( $this->gComponent->gCacheDirectory ) );
      } catch ( UserException $e ) {
         throw new UserException( $e->getmessage(), $e->getCode(), $e->getDOMScrap() );
      }

      $palooseNS = $inChildNode->namespaceURI;
      $paloosePrefix = $inChildNode->prefix;
      $this->gLogger->debug( "Building new fragment with xmlns:" . $paloosePrefix . "='" . $palooseNS . "'" );
      
      $galleryRoot = $this->gTranslatedDOM->createElementNS( $palooseNS, $inChildNode->nodeName );
      $inNewParentNode->appendChild( $galleryRoot );
      
      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":name" );
      $textNode = $this->gTranslatedDOM->createTextNode( $galleryIndex->getGalleryName() );
      $galleryElement->appendChild( $textNode );
      $galleryRoot->appendChild( $galleryElement );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":dir" );
      $textNode = $this->gTranslatedDOM->createTextNode( $this->gGalleryDirectory );
      $galleryElement->appendChild( $textNode );
      $galleryRoot->appendChild( $galleryElement );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":breadcrumb" );
      $breadcrumbArray = $this->generateBreadcrumbArray( $this->gRootDirectory, $this->gGalleryDirectory );
      $this->gLogger->debug( "this->gRootDirectory: '" . $this->gRootDirectory . "'" );
      $this->gLogger->debug( "this->gGalleryDirectory: '" . $this->gGalleryDirectory . "'" );
      if ( !empty( $breadcrumbArray ) ) {
         foreach ( $breadcrumbArray as $key => $crumb ) {
            $nameElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":name" );

            $attrNode = $this->gTranslatedDOM->createAttribute( "src" );
            $textNode = $this->gTranslatedDOM->createTextNode( $key );
            $attrNode->appendChild( $textNode );
            $nameElement->appendChild( $attrNode );
            
            $textNode = $this->gTranslatedDOM->createTextNode( $crumb );
            $nameElement->appendChild( $textNode );
            $galleryElement->appendChild( $nameElement );
         }
      }
      $galleryRoot->appendChild( $galleryElement );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":description" );
      
      if ( $galleryIndex->getGalleryDescription() != NULL ) {
         $descriptionNodeList = $galleryIndex->getGalleryDescription()->documentElement->childNodes;
         if ( $descriptionNodeList != NULL and $descriptionNodeList->length > 0 ) {
            foreach( $descriptionNodeList as $node ) {
               $galleryElement->appendChild( $this->gTranslatedDOM->importNode( $node, 1 ) );
            }
         }
      }
      $galleryRoot->appendChild( $galleryElement );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":folders" );
      $foldersList = $galleryIndex->getGalleryFolderList();
      if ( !empty( $foldersList ) ) {
         foreach ( $foldersList as $folder ) {
            $folderElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":folder" );
   
            $attrNode = $this->gTranslatedDOM->createAttribute( "src" );
            $textNode = $this->gTranslatedDOM->createTextNode( $folder->getRelativeFolderName() );
            $attrNode->appendChild( $textNode );
            $folderElement->appendChild( $attrNode );
   
            // $textNode = $this->gTranslatedDOM->createTextNode( $folder->getDescription() );
            // $folderElement->appendChild( $textNode );
            // $galleryElement->appendChild( $folderElement );
            $descriptionNode = $folder->getDescription();
            if ( $descriptionNode != NULL ) {
                  $descNode = $this->gTranslatedDOM->importNode( $descriptionNode, 1 );
                  $folderElement->appendChild( $descNode );
                  $galleryElement->appendChild( $folderElement );
            } else {
               $textNode = $this->gTranslatedDOM->createTextNode( "" );
               $folderElement->appendChild( $textNode );
               $galleryElement->appendChild( $folderElement );
            }
         }
      }
      $galleryRoot->appendChild( $galleryElement );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":images" );
      
      // Displaying a set of thumbnails not a single image
      $attrNode = $this->gTranslatedDOM->createAttribute( "type" );
      $textNode = $this->gTranslatedDOM->createTextNode( "multi" );
      $attrNode->appendChild( $textNode );
      $galleryElement->appendChild( $attrNode );
   
      $imagesList = $galleryIndex->getGalleryImageList();
      if ( !empty( $imagesList ) ) {
         foreach ( $imagesList as $image ) {
            $imageElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":image" );
   
            $attrNode = $this->gTranslatedDOM->createAttribute( "img" );
            $textNode = $this->gTranslatedDOM->createTextNode( $image->getCachedImageName() );
            $attrNode->appendChild( $textNode );
            $imageElement->appendChild( $attrNode );
   
            $attrNode = $this->gTranslatedDOM->createAttribute( "thumb" );
            $textNode = $this->gTranslatedDOM->createTextNode( $image->getCachedThumbnailName() );
            $attrNode->appendChild( $textNode );
            $imageElement->appendChild( $attrNode );
   
            $attrNode = $this->gTranslatedDOM->createAttribute( "name" );
            $textNode = $this->gTranslatedDOM->createTextNode( $image->getOriginalFileName() );
            $attrNode->appendChild( $textNode );
            $imageElement->appendChild( $attrNode );
   
            $attrNode = $this->gTranslatedDOM->createAttribute( "piclens" );
            $textNode = $this->gTranslatedDOM->createTextNode( $image->getPicLensId() );
            $attrNode->appendChild( $textNode );
            $imageElement->appendChild( $attrNode );
   
            // $textNode = $this->gTranslatedDOM->createTextNode( $image->getDescription() );
            // $imageElement->appendChild( $textNode );
            // $galleryElement->appendChild( $imageElement );
            $descriptionNode = $image->getDescription();
            if ( $descriptionNode != NULL ) {
                  $descNode = $this->gTranslatedDOM->importNode( $descriptionNode, 1 );
                  $imageElement->appendChild( $descNode );
                  $galleryElement->appendChild( $imageElement );
            } else {
               $textNode = $this->gTranslatedDOM->createTextNode( "" );
               $imageElement->appendChild( $textNode );
               $galleryElement->appendChild( $imageElement );
            }
         }
      }
      $galleryRoot->appendChild( $galleryElement );

   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param string $inGalleryDirectory where the current gallery is.
    * @param DOMNode $inChildNode the node we are processing.
    * @param DOMNode $inNewParentNode the parent node where we will add the child based on $inChildNode.
    */

   private function processImage( $inGalleryDirectory, DOMNode $inChildNode, DOMNode $inNewParentNode )
   {

      $this->gLogger->debug( "Processing image: '" . $inGalleryDirectory . "' : " . $inChildNode->nodeName );

      //Read the gallery index file (need it for the next and previous)
      $galleryIndexName = $inGalleryDirectory . Environment::$configuration[ 'galleryIndex' ];
      if ( !file_exists( $galleryIndexName ) ) {
         throw new UserException( "Cannot find gallery index file: $galleryIndexName", 0, NULL );
      }
      $this->gLogger->debug( "Found gallery index file: '" . $galleryIndexName . "'" );

      $galleryIndexDOM = new DOMDocument();
      if ( !$galleryIndexDOM->load( $galleryIndexName ) ) {
         throw new UserException( $e->getMessage(), $e->getCode(), NULL );
      }
      try {
         $galleryIndex = new GalleryIndex( $galleryIndexDOM,
            $inGalleryDirectory,
            $this->gComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), NULL );
      }
 
      $this->gLogger->debug( "Building new fragment" );
      $palooseNS = $inChildNode->namespaceURI;
      $paloosePrefix = $inChildNode->prefix;
      
      $galleryRoot = $this->gTranslatedDOM->createElementNS( $palooseNS, $inChildNode->nodeName );
      $inNewParentNode->appendChild( $galleryRoot );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":name" );
      $textNode = $this->gTranslatedDOM->createTextNode( $galleryIndex->getGalleryName() );
      $galleryElement->appendChild( $textNode );
      $galleryRoot->appendChild( $galleryElement );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":dir" );
      $textNode = $this->gTranslatedDOM->createTextNode( $this->gGalleryDirectory );
      $galleryElement->appendChild( $textNode );
      $galleryRoot->appendChild( $galleryElement );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":breadcrumb" );
      $breadcrumbArray = $this->generateBreadcrumbArray( $this->gRootDirectory, $this->gGalleryDirectory );
      if ( !empty( $breadcrumbArray ) ) {
         foreach ( $breadcrumbArray as $key => $crumb ) {
            $nameElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":name" );

            $attrNode = $this->gTranslatedDOM->createAttribute( "src" );
            $textNode = $this->gTranslatedDOM->createTextNode( $key );
            $attrNode->appendChild( $textNode );
            $nameElement->appendChild( $attrNode );

            
            $textNode = $this->gTranslatedDOM->createTextNode( $crumb );
            $nameElement->appendChild( $textNode );
            $galleryElement->appendChild( $nameElement );
         }
      }
      $galleryRoot->appendChild( $galleryElement );

      $galleryElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":images" );
      //Displaying a single image not a set of thumbnails
      $attrNode = $this->gTranslatedDOM->createAttribute( "type" );
      $textNode = $this->gTranslatedDOM->createTextNode( "single" );
      $attrNode->appendChild( $textNode );
      $galleryElement->appendChild( $attrNode );
      //Displaying a single image not a set of thumbnails
      $attrNode = $this->gTranslatedDOM->createAttribute( "name" );
      $textNode = $this->gTranslatedDOM->createTextNode( $this->gRequestedImage );
      $attrNode->appendChild( $textNode );
      $galleryElement->appendChild( $attrNode );
   
      $imagesList = $galleryIndex->getGalleryImageList();
      if ( !empty( $imagesList ) ) {
         foreach ( $imagesList as $image ) {
            $imageElement = $this->gTranslatedDOM->createElementNS( $palooseNS, $paloosePrefix. ":image" );

            $attrNode = $this->gTranslatedDOM->createAttribute( "img" );
            $textNode = $this->gTranslatedDOM->createTextNode( $image->getCachedImageName() );
            $attrNode->appendChild( $textNode );
            $imageElement->appendChild( $attrNode );
   
            $attrNode = $this->gTranslatedDOM->createAttribute( "thumb" );
            $textNode = $this->gTranslatedDOM->createTextNode( $image->getCachedThumbnailName() );
            $attrNode->appendChild( $textNode );
            $imageElement->appendChild( $attrNode );
   
            $attrNode = $this->gTranslatedDOM->createAttribute( "name" );
            $textNode = $this->gTranslatedDOM->createTextNode( $image->getOriginalFileName() );
            $attrNode->appendChild( $textNode );
            $imageElement->appendChild( $attrNode );
   
            $attrNode = $this->gTranslatedDOM->createAttribute( "piclens" );
            $textNode = $this->gTranslatedDOM->createTextNode( $image->getPicLensId() );
            $attrNode->appendChild( $textNode );
            $imageElement->appendChild( $attrNode );
   
            // $textNode = $this->gTranslatedDOM->createTextNode( $image->getDescription() );
            // $imageElement->appendChild( $textNode );
            // $galleryElement->appendChild( $imageElement );
            $descriptionNode = $image->getDescription();
            if ( $descriptionNode != NULL ) {
                 $descNode = $this->gTranslatedDOM->importNode( $descriptionNode, 1 );
                 $imageElement->appendChild( $descNode );
                 $galleryElement->appendChild( $imageElement );
            } else {
               $textNode = $this->gTranslatedDOM->createTextNode( "" );
               $imageElement->appendChild( $textNode );
               $galleryElement->appendChild( $imageElement );
            }
         }
      }
      $galleryRoot->appendChild( $galleryElement );

   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return the path information in the form of a breadcrumb trail.
    *
    * It returns an array of the form
    * 
    * <pre>   breadcrumb name       relative dir
    *    ""                =>  "/Library/Apache2/htdocs/gpf-1/gallery/"
    *    "horses"          =>  "/Library/Apache2/htdocs/gpf-1/gallery/horses"
    *    "horses/megan"    =>  "/Library/Apache2/htdocs/gpf-1/gallery/horses/megan"</pre>
    *
    * Note that the first always has a null key (is this safe in PHP ?)
     *
    * @param string $inRoot the top level path of the gallery
    * @param string $inPath the path of the current gallery
    * @retval array of breadcrumb strings (null if nothing - should not happen :-) )
    */

   public function generateBreadcrumbArray( $inRoot, $inPath )
   {

      
      $this->gLogger->debug( "Root gallery path: '$inRoot'" );
      $this->gLogger->debug( "Current gallery path: '$inPath'" );

      // Set up an array with the set of directories that we need to inspect
      $levelPath = "";
      $breadCrumbPathArray[ "" ] = $inRoot;
      if ( strlen( $inPath ) > 0 ) {
         $pathArray = explode( Environment::getPathSeparator(), $inPath );
         foreach ( $pathArray as $path ) {
            $levelPath = Utilities::addTrailingSeparator( $levelPath ) . $path;
            $breadCrumbPathArray[ $levelPath ] = Utilities::addTrailingSeparator( $inRoot ) . $levelPath;
         }
      }

      // At this point $breadCrumbPathArray has the following typical structure
      //
      // $key             $value
      // ""               "/Library/Apache2/htdocs/gpf-1/gallery/"
      // "horses"         "/Library/Apache2/htdocs/gpf-1/gallery/horses"
      // "horses/megan"   "/Library/Apache2/htdocs/gpf-1/gallery/horses/megan"

      $breadcrumbArray = NULL;
      // Go through each path and pick up the gallery.xml entry for the breadcrumb
      $i = 0;
      foreach ( $breadCrumbPathArray as $key => $value ) {
         $this->gLogger->debug( "   $i : '$key'='$value'" );
         $i++;
         $value = Utilities::addTrailingSeparator( $value );
         $galleryIndexName = $value . Environment::$configuration[ 'galleryIndex' ];
         // We know the index exists as we would not have got this far :-)
         $galleryIndexDOM = new DOMDocument();
         $galleryIndexDOM->load( $galleryIndexName );
         
         // Extract the parts to be used from the inDOM
         $xpath = new domxpath( $galleryIndexDOM );
         $xpath->registerNamespace( "p", Environment::$configuration[ 'palooseNamespace' ] );  // 1.1.1
         $breadcrumbArray[ $key ] = Utilities::getXPathListStringItem( 0, $xpath, "//p:breadcrumb" );
         $this->gLogger->debug( "   breadcrumb: " . $breadcrumbArray[ $key ] );
      }

      return $breadcrumbArray;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_GalleryTransformer</i> class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * <pre>   &lt;map:components>
 *    &lt;map:transformers default="xslt">
 *       &lt;map:transformer name="gallery" src="resource://lib/transforming/GalleryTransformer">
 *          &lt;map:parameter name="root" value="context://gallery/"/>
 *          &lt;map:parameter name="image-cache" value="context://resources/images/cache/"/>
 *          &lt;map:parameter name="max-thumbnail-width" value="150"/>
 *          &lt;map:parameter name="max-thumbnail-height" value="150"/>
 *          &lt;map:parameter name="resize" value="1"/>
 *          &lt;map:parameter name="max-width" value="600"/>
 *          &lt;map:parameter name="max-height" value="600"/>
 *       &lt;/map:transformer>
 *    &lt;/map:transformers></pre>
 *
 * where
 * <ul>
 *   <li><i>root</i> : the root of the gallery (a folder that holds the entire gallery).
 *      This can be overridden by the pipeline component, or by an explicit declaration in the XML content.</li>
 *   <li><i>root</i> : the root of the gallery (a folder that holds the entire gallery). This can be overridden
 *      by the pipeline component, or by an explicit declaration in the XML content (see below).</li>
 *   <li><i>image-cache</i> : the cahe where the scaled image and the thumbnails are kept. In this case in the
 *      "resources/images/cache/" folder within the root of the site.</li>
 *   <li><i>max-thumbnail-width, max-thumbnail-height</i> : the size, in pixels, of the thumbnnail image.</li>
 *   <li><i>max-width, max-height</i> : the maximum size, in pixels, of the displayed image. If either of the 
 *      dimensions of the original image exceed these values then a suitable scaling is applied to the cached image.</li>
 *   <li><i>resize</i> : should the original image be scaled to the max values (1), or left as it is (0).</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _GalleryTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
    /** Root directory of gallery */
   public $gRootDirectory;
   
    /** Root where the image cahe is kept */
   public $gCacheDirectory;
   
    /** Max width of image thumbnail */
   public $gMaxThumbnailWidth;
   
    /** Max height of image thumbnail */
   public $gMaxThumbnailHeight;
   
    /** Resize? 1 for yes, 0 for no */
   public $gResizeImage;
   
    /** Max width of image */
   public $gMaxWidth;
   
    /** Max height of image */
   public $gMaxHeight;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the component from the sitemap declaration.
    *
    * <pre>
    *    &lt;map:transformer name="gallery" src="resource://lib/transforming/GalleryTransformer">
    *        &lt;map:parameter name="root" value="context://gallery/"/>
    *        &lt;map:parameter name="image-cache" value="context://resources/images/cache/"/>
    *        &lt;map:parameter name="max-thumbnail-width" value="100"/>
    *        &lt;map:parameter name="max-thumbnail-height" value="100"/>
    *        &lt;map:parameter name="resize" value="1"/>
    *        &lt;map:parameter name="max-width" value="600"/>
    *        &lt;map:parameter name="max-height" value="600"/>
    *    &lt;/map:transformer></pre>
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
       $this->gLogger = Logger::getLogger( __CLASS__ );
       
      $this->gPackageName = "GalleryTransformer";
      
      $this->gRootDirectory = "";
      if ( strlen( $this->gParameters[ 'root' ] ) > 0 ) $this->gRootDirectory = $this->gParameters[ 'root' ];

      $this->gMaxThumbnailWidth = "";
      if ( strlen( $this->gParameters[ 'max-thumbnail-width' ] ) > 0 ) $this->gMaxThumbnailWidth = $this->gParameters[ 'max-thumbnail-width' ];

      $this->gMaxThumbnailHeight = "";
      if ( strlen( $this->gParameters[ 'max-thumbnail-height' ] ) > 0 ) $this->gMaxThumbnailHeight = $this->gParameters[ 'max-thumbnail-height' ];

      $this->gResizeImage = "";
      if ( strlen( $this->gParameters[ 'resize' ] ) > 0 ) $this->gResizeImage = $this->gParameters[ 'resize' ];

      $this->gMaxWidth = "";
      if ( strlen( $this->gParameters[ 'max-width' ] ) > 0 ) $this->gMaxWidth = $this->gParameters[ 'max-width' ];

      $this->gMaxHeight = "";
      if ( strlen( $this->gParameters[ 'max-height' ] ) > 0 ) $this->gMaxHeight = $this->gParameters[ 'max-height' ];

      $this->gCacheDirectory = "";
      if ( strlen( $this->gParameters[ 'image-cache' ] ) > 0 )
         $this->gCacheDirectory = StringResolver::expandPseudoProtocols( $this->gParameters[ 'image-cache' ] );
      $this->gLogger->debug( "Constructed GalleryTransformer component: [root:" . $this->gRootDirectory . "][cache:" . $this->gCacheDirectory . "]" );
   }

 }
?>