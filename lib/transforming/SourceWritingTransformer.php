<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>SourceWritingTransformer</i> diverts XML from the pipeline and
 * puts it into an external file.
 *
 * It is similar to the Cocoon equivalent.
 *
 * @package paloose
 * @subpackage transforming
 */

class SourceWritingTransformer extends TransformerPipeElement implements PipeElementInterface
{
   
   /** Logger instance for this class */
   private $gLogger;
   
   const WRITE_SOURCE_ACTION_NEW = 1;
   const WRITE_SOURCE_ACTION_OVERWRITTEN = 2;
   const WRITE_SOURCE_ACTION_NONE = 3;
   
   const WRITE_SOURCE_BEHAVIOUR_WRITE = 1;
   const WRITE_SOURCE_BEHAVIOUR_INSERT = 2;
   const WRITE_SOURCE_BEHAVIOUR_DELETE = 3;
   
   const WRITE_SOURCE_EXECUTION_SUCCESS = 1;
   const WRITE_SOURCE_EXECUTION_FAILURE = 2;
   
   const WRITE_SOURCE_SERIALIZER_XML = 1;
   
   /** True if external file can be created */
   private $gCreateFlag = false;
   
   /** If overwriting is enabled */
   private $gOverwriteFlag = true;
   
   const IMPORT_SUBTREE = true;
   
   const INITIAL = 0;
   const APPENDING_NODE = 1;
   const REPLACING_NODE = 2;
   const DELETING_NODE = 3;
   const NO_ACTION = 4;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Construct an instance of the <i>SourceWritingTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _SourceWritingTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if no <i>src</i> attribute
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
    */
   
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Process the document.
    *
    * The XSL Transformer processes the input DOM. If there are no errors returns the transformed
    * DOM. All errors are caught by exception..
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws UserException if cannot find the XSL transformation file.
    * @throws InternalException if problem setting parameters.
    */
   
   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      // The inputted DOM has a set of <source:xxx> tags that must be extracted and processed
      // and then replaced with the success/failure code.
      $documentDOM = $inDOM;
      $rootNode = $documentDOM->documentElement;
      
      $this->gTranslatedDOM = new DOMDocument();
      $elementNS = $rootNode->namespaceURI;
      $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
      $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
      
      $this->gTranslatedDOM->appendChild( $newElement );
      try {
         $this->processNode( $inVariableStack, null, $rootNode, $newElement );
         return $this->gTranslatedDOM;
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
      
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Process a single node.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param DOMDocument $inCatalogueDOM the document that holds catalogue which has the translation scraps,
    *  null if there is no catalogue file.
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document (reference)
    */
   
   private function processNode( VariableStack $inVariableStack, $inCatalogueDOM, DOMNode $inParentNode, DOMNode &$inNewParentNode )
   {
      // Create a new node and update it with the old nodes attributes
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {
         // At this point we have a list of attributes that may need translating to expand variables
         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = StringResolver::expandString( $inVariableStack, $attribute->name );
               $attribValue = StringResolver::expandString( $inVariableStack, $attribute->value );
               $inNewParentNode->setAttribute( $attribName, $attribValue );
               // $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         }
      }
      
      // We have transferred the current node at this point - now we must traverse the children of the
      // old document's current node.
      $children = $inParentNode->childNodes;
      
      // Walk this list of children
      try {
         foreach ( $children as $child ) {
            if ( $child->nodeName == "source:write" ) {
               $this->gLogger->debug( "Found node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
               $replacementDOM = $this->processWrite( $child );
               // Replace source:write with result
               $replacementRoot = $replacementDOM->documentElement;
               // Utilities::outputStringVerbatim( $replacementDOM->saveXML() );
               $newElementNode = $this->gTranslatedDOM->importNode( $replacementRoot, true );
               $inNewParentNode->appendChild( $newElementNode );
            } else if ( $child->nodeName == "source:insert" ) {
               $this->gLogger->debug( "Found node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
               
               if ( $child->hasAttributes() ) {
                  foreach ( $child->attributes as $attribute ) {
                     $attribName = StringResolver::expandString( $inVariableStack, $attribute->name );
                     $attribValue = StringResolver::expandString( $inVariableStack, $attribute->value );
                     $inNewParentNode->setAttribute( $attribName, $attribValue );
                     $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
                  }
               }
               
               $replacementDOM = $this->processInsert( $child );
               // Replace source:insert with result
               $replacementRoot = $replacementDOM->documentElement;
               // Utilities::outputStringVerbatim( $replacementDOM->saveXML() );
               $newElementNode = $this->gTranslatedDOM->importNode( $replacementRoot, true );
               $inNewParentNode->appendChild( $newElementNode );
            } else if ( $child->nodeName == "source:delete" ) {
               $this->gLogger->debug( "Found node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
               $replacementDOM = $this->processDelete( $child );
               // Replace source:delete with result
               $replacementRoot = $replacementDOM->documentElement;
               // Utilities::outputStringVerbatim( $replacementDOM->saveXML() );
               $newElementNode = $this->gTranslatedDOM->importNode( $replacementRoot, true );
               $inNewParentNode->appendChild( $newElementNode );
            } else {               // Pass all others through
               if ( $child->nodeType == XML_ELEMENT_NODE ) {
                  // $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
                  $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
                  // Now process the child node - calls this recursively
                  $this->processNode( $inVariableStack, $inCatalogueDOM, $child, $newElementNode );
                  $inNewParentNode->appendChild( $newElementNode );
               } else if ( $child->nodeType == XML_TEXT_NODE ) {
                  // $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
                  $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
                  $inNewParentNode->appendChild( $newTextNode );
               }
            }
         }
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Process Write.
    *
    *   <source:write [create="true|false"]>
    *       [<source:path/>]
    *       <source:source/>
    *       <source:fragment/>
    *   </source:write>
    *
    * where
    *    create attribute    defines whether to create the file first if it does not exist
    *    source:source       is the file name of the file to be written. It can have
    *                        pseudo variables such as “context://”.
    *    source:path         is an optional XPath for defining the root structure of the
    *                        file with which the fragment is placed.
    *    source:fragment     is the fragment of XML that will be written.
    *
    * For example the following structure:
    *
    *    <source:write create="true">
    *      <source:source>context://test.xml</source:source>
    *      <source:path>/root/AAA</source:path>
    *      <source:fragment>
    *         <BBB>
    *            <CCC name="bob"/>
    *         </BBB>
    *      </source:fragment>
    *    </source:write>
    *
    *  will write the following file (added indentation for clarity)
    *
    *    <?xml version="1.0"?>
    *    <root>
    *       <AAA>
    *         <BBB>
    *            <CCC name="bob"/>
    *         </BBB>
    *       </AAA>
    *    </root>
    *
    * If <source:path> is omitted it is important that the XML within
    * <source:fragment> has only a single node, which will become the
    * root node of the written document.
    *
    * @param DOMNode $inSourceNode the source node to process
    * @retval DOMNode the replacement node (contains error/success information)
    */
   
   private function processWrite( DOMNode $inSourceNode )
   {
      // Turn fragment into DOM
      $sourceDom = new DOMDocument();
      $sourceDom->appendChild( $sourceDom->importNode( $inSourceNode, 1 ) );
      $this->gLogger->debug( "   source input:\n" . $sourceDom->saveXML() );

      // Extract the file etc to use
      $xpath = new DOMXpath( $sourceDom );
      $xpath->registerNamespace( "source", Environment::$configuration[ 'sourceNamespace' ] );
      $filename = Utilities::getXPathListStringItem( 0, $xpath, "//source:source" );
      $filename = StringResolver::expandPseudoProtocols( $filename );
      $childXpath = Utilities::getXPathListStringItem( 0, $xpath, "//source:path" );
      $this->gLogger->debug( "   source file: '" . $filename . "'" );
      $this->gLogger->debug( "   insert as child of: '" . $childXpath . "'" );
      
      $this->gCreateFlag = Utilities::normalizeBoolean( Utilities::getXPathListStringItem( 0, $xpath, "//@create" ) );
      
      // Check to see whether we have to create file first
      if ( !file_exists( $filename ) and !$this->gCreateFlag ) {
         throw new RunException( "No file to receive source write fragment (try create attrib?)",
                                PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
      }
      
      // Now get the fragment to process
      $fragmentDOM = new DOMDocument();
      $fragmentDOM->appendChild( $fragmentDOM->importNode( Utilities::getXPathListNode( 0, $xpath, "//source:fragment/*" ), 1 ) );
      
      $documentDOM = new DOMDocument();
      try {
         if ( strlen( $childXpath ) > 0 ) {
            $pathXML = $this->xpathToXML( $childXpath );
            $documentDOM->loadXML( $pathXML );
            $sourceXpath = new DOMXpath( $documentDOM );
            $this->gLogger->debug( "   insert into: '" . $documentDOM->saveXML() . "'" );
            
            // Get the node from the source DOM of which we will add the fragment node as a child
            $parentNode = Utilities::getXPathListNode( 0, $sourceXpath, $childXpath );
            if ( $parentNode == NULL ) {
               $this->gLogger->debug( "   parent node not found: '" . $documentDOM->saveXML() . "'" );
            }
            
            // Make a copy of the fragment node and import it into the source DOM
            $documentDOM->importNode( $fragmentDOM->documentElement, true );
            $parentNode->appendChild( $documentDOM->importNode( $fragmentDOM->documentElement, true ) );
            
            // At this point $documentDOM is the new document to write back
            if ( fopen( $filename, "wb" ) === false ) {
               throw new RunException( "Cannot write source write fragment (directory permissions?)",
                                      PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
            }
            // Utilities::outputStringVerbatim( $documentDOM->saveXML() );
         } else {
            // No root so let's hope it's a correctly formed fragment
            $nodeList = $xpath->query( "//source:fragment/*" );
            if ( $nodeList->length == 1 ) {
               $documentDOM->loadXML( $fragmentDOM->saveXML() );
            } else {
               throw new RunException( "Fragment must have one root element if no path declared",
                                      PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
            }
         }
         
         // Write back into the file
         if ( !$documentDOM->save( $filename ) ) {
            $errorMessage = "Could not save document to file '$filename': $php_errormsg";
            $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                     self::WRITE_SOURCE_BEHAVIOUR_WRITE,
                                                     self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                     self::WRITE_SOURCE_SERIALIZER_XML,
                                                     $filename,
                                                     $errorMessage );
            $this->gLogger->fatal( "Error: $errorMessage" );
         } else {
            $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                     self::WRITE_SOURCE_BEHAVIOUR_WRITE,
                                                     self::WRITE_SOURCE_EXECUTION_SUCCESS,
                                                     self::WRITE_SOURCE_SERIALIZER_XML,
                                                     $filename,
                                                     "File written ok" );
            $this->gLogger->debug( "Wrote back file ok" );
         }
         
      } catch ( Exception $e ) {
         $errorMessage = "Problem processing source document in write-source: $e->getMessage()";
         $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                  self::WRITE_SOURCE_BEHAVIOUR_WRITE,
                                                  self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                  self::WRITE_SOURCE_SERIALIZER_XML,
                                                  $filename,
                                                  $errorMessage );
         $this->gLogger->fatal( "Error: $errorMessage" );
      }
      return $resultDOM;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Process Insert.
    *
    *    <pre><source:insert [create="true|false"]>
    *       <source:source/>
    *       <source:path/>
    *       <source:fragment/>
    *       [<source:replace/>]
    *    </source:insert></pre>
    *
    *  where
    *
    *      create attribute   defines whether to create the file first if it does not exist.
    *      source:source      is the file name of the file to be written. It can have
    *                         pseudo variables such as ‘context://’.
    *      source:path        is an optional XPath for defining the root structure of the
    *                         file with which the fragment is placed.
    *      source:fragment    is the fragment of XML that will be written.
    *      source:replace     an optional XPath to select the node that is to be replaced
    *                         by the XML fragment.
    *
    * Note that we do not check for duplication. It is assumed that that has
    * been done already. Higher levels should know what the criterea for
    * duplication are rather than here.
    *
    * @param DOMNode $inSourceNode the source node to process (just the source tag and its children)
    * @retval DOMNode the replacement node (contains error/success information)
    */
   
   private function processInsert( DOMNode $inSourceNode )
   {
      // Turn fragment into DOM
      $sourceDom = new DOMDocument();
      $sourceDom->appendChild( $sourceDom->importNode( $inSourceNode, 1 ) );
      $this->gLogger->debug( "   source input:\n" . $sourceDom->saveXML() );
      
      // Extract the file etc to use
      $sourceXpath = new DOMXpath( $sourceDom );
      $sourceXpath->registerNamespace( "source", Environment::$configuration[ 'sourceNamespace' ] );
      
      $filename = Utilities::getXPathListStringItem( 0, $sourceXpath, "//source:source" );
      $filename = StringResolver::expandPseudoProtocols( $filename );
      $emptyFilename = ( strlen( $filename ) == 0 );
      
      $pathXpath = Utilities::getXPathListStringItem( 0, $sourceXpath, "//source:path" );
      $emptyPath = ( strlen( $pathXpath ) == 0 );
      
      $replaceXpath = Utilities::getXPathListStringItem( 0, $sourceXpath, "//source:replace" );
      $emptyReplace = ( strlen( $replaceXpath ) == 0 );
      
      $reinsertXpath = Utilities::getXPathListStringItem( 0, $sourceXpath, "//source:reinsert" );
      $emptyReinsert = ( strlen( $reinsertXpath ) == 0 );
      
      $this->gCreateFlag = Utilities::normalizeBoolean( Utilities::getXPathListStringItem( 0, $sourceXpath, "/source:insert/@create" ) );
      $this->gOverwriteFlag = Utilities::normalizeBoolean( Utilities::getXPathListStringItem( 0, $sourceXpath, "/source:insert/@overwrite" ) );
      
      $fragmentNode = Utilities::getXPathListNode( 0, $sourceXpath, "//source:fragment/*" );
      $emptyFragment = ( $fragmentNode == NULL );
      
      $fragmentDOM = new DOMDocument();
      if ( !$emptyFragment ) {
         $fragmentDOM->importNode( Utilities::getXPathListNode( 0, $sourceXpath, "//source:fragment/*" ), 1 );
      }
      
      $this->gLogger->debug( "   source file: '$filename'" );
      $this->gLogger->debug( "   parent path: '$pathXpath'" );
      $this->gLogger->debug( "   replace path: '$replaceXpath'" );
      $this->gLogger->debug( "   create file: '$this->gCreateFlag'" );
      $this->gLogger->debug( "   overwrite file: '$this->gOverwriteFlag'" );
      if ( !$emptyFragment )  {
         $this->gLogger->debug( "   fragment:\n'" . $fragmentDOM->saveXML() );
      } else {
         $this->gLogger->debug( "   no fragment" );
      }
      
      // First of all check that we have the necessary obligatory items. We could check for
      // fragment here but it is allowed to be null for deletion.
      if ( $emptyFilename ) {
         $errorMessage = "Must have <source:source> defined in <source:insert>";
         $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                  self::WRITE_SOURCE_BEHAVIOUR_INSERT,
                                                  self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                  self::WRITE_SOURCE_SERIALIZER_XML,
                                                  $filename,
                                                  $errorMessage );
         $this->gLogger->fatal( "Error: $errorMessage" );
         return $resultDOM;
      }
      
      if ( $emptyPath ) {
         $errorMessage = "Must have <source:path> defined in <source:insert>";
         $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                  self::WRITE_SOURCE_BEHAVIOUR_INSERT,
                                                  self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                  self::WRITE_SOURCE_SERIALIZER_XML,
                                                  $filename,
                                                  $errorMessage );
         $this->gLogger->fatal( "Error: $errorMessage" );
         return $resultDOM;
      }
      
      // Check to see whether we have to create file first
      if ( !file_exists( $filename ) and !$this->gCreateFlag ) {
         throw new RunException( "No file to receive source write fragment (try @create attrib in source:insert?)",
                                PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, NULL );
      }
      
      // Now - concentrate - this is complicated. The cases here reflect the documentation values. We assume that
      // both the source and path are set here because of the error checking above. Slightly long-winded way
      // of coding it but necessary for clarity.
      
      // The filename and path are checked above so are always there.
      $case = $this::INITIAL;
      if ( $emptyReplace && !$emptyFragment ) {
         //  if 'replace' is not specified, the 'fragment' is appended as a child of 'path'.
         $case = $this::APPENDING_NODE;
      } else if ( !$emptyReplace && !$emptyFragment && $this->gOverwriteFlag ) {
         // if 'replace' is specified and it exists and 'overwrite' is true, your 'fragment' is
         // inserted in 'path', before 'replace' and then 'replace' is deleted.
         $case = $this::REPLACING_NODE;
      } else if ( !$emptyReplace && $emptyFragment && $this->gOverwriteFlag ) {
         // if 'replace' is specified and there is no fragment then 'replace' is deleted.
         $case = $this::DELETING_NODE;
      } else if ( !$emptyReplace && !$this->gOverwriteFlag ) {
         // if 'replace' is specified and 'overwrite' is false, no action occurs.
         $case = $this::NO_ACTION;
      } else if ( !$emptyReplace && !$emptyFragment ) {
         // if 'replace' is specified and 'overwrite' is either true or |false, 'fragment'
         // is appended as a child of 'path'.
         $case = $this::APPENDING_NODE;
      } else if ( $emptyReinsert ) {
         // $ Future expansion!
         $case = $this::NO_ACTION;
      }
      $this->gLogger->debug( "   case: $case" );
      
      if ( !file_exists( $filename ) ) {
         throw new RunException( "File '$filename' does not exist", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
      }
      
      // Read the source file that we will be processing with source:insert
      $documentDOM = new DOMDocument();
      if ( !$documentDOM->load( $filename ) ) {
         throw new RunException( "Could not load source document from file '$filename'", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
      }
      $this->gLogger->debug( "   document file:\n" . $documentDOM->saveXML() );
      
      // A few convenience flags
      $documentXPath = new DOMXpath( $documentDOM );
      $documentXPath->registerNamespace( "source", Environment::$configuration[ 'sourceNamespace' ] );
      $writeFileFlag = false;
      
      // We have all that we need now for updating the source document
      try {
         
         switch ( $case ) {
            case $this::APPENDING_NODE :
               // Used to add a new node after current node defined by path.
               // Get the fragment into a new node and form new fragment DOM
               $fragmentNode = Utilities::getXPathListNode( 0, $sourceXpath, "//source:fragment/*" );
               if ( $fragmentNode == NULL ) {
                  throw new Exception( "Cannot find fragment node in source" );
               }
               $fragmentDOM->appendChild( $fragmentDOM->importNode( $fragmentNode, self::IMPORT_SUBTREE ) );
               $this->gLogger->debug( "fragmentDOM:\n" . $fragmentDOM->saveXML() );
               
               // Get the node from the document DOM where we will add the fragment node as a child
               $parentNode = Utilities::getXPathListNode( 0, $documentXPath, $pathXpath );
               if ( $parentNode == NULL ) {
                  throw new Exception( "Cannot find parent of node to append as child in '$pathXpath'" );
               }
               
               // Import the fragment root into the source document and append it to the parent node
               $parentNode->appendChild( $documentDOM->importNode( $fragmentDOM->documentElement, self::IMPORT_SUBTREE ) );
               $this->gLogger->debug( "fragmentDOM:\n" . $documentDOM->saveXML() );
               $writeFileFlag = true;
               break;
               
            case $this::REPLACING_NODE:
               // Used to replace a chunk with the fragment. Extract the replace Xpath.
               $nodeToReplace = Utilities::getXPathListNode( 0, $documentXPath, $replaceXpath );
               $this->gLogger->debug( "   replacing node: '" . $nodeToReplace->getNodePath() . "'" );
               if ( $nodeToReplace == NULL ) {
                  throw new Exception( "Cannot find node to replace: $replaceXpath" );
               }
               
               // Get the node from the document DOM where we will add the fragment node as a child
               $parentNode = Utilities::getXPathListNode( 0, $documentXPath, $pathXpath );
               $this->gLogger->debug( "   parent node: '" . $parentNode->getNodePath() . "'" );
               if ( $parentNode == NULL ) {
                  throw new Exception( "Cannot find parent of node to replace in '$pathXpath'" );
               }
               
               $fragmentNode = Utilities::getXPathListNode( 0, $sourceXpath, "//source:fragment/*" );
               $this->gLogger->debug( "   fragment node: '" . $fragmentNode->getNodePath() . "'" );
               if ( $fragmentNode != NULL ) {
                  $fragmentDOM->appendChild( $fragmentDOM->importNode( $fragmentNode, self::IMPORT_SUBTREE ) );
                  $this->gLogger->debug( "fragmentDOM:\n" . $fragmentDOM->saveXML() );
               }
               
               if ( !$parentNode->removeChild( $nodeToReplace ) ) {
                  throw new RunException( "Could not remove child: " . $nodeToReplace->getNodePath(), PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
               }
               
               if ( !$emptyFragment ) {
                  $this->gLogger->debug( "   replacing node: '" . $nodeToReplace->getNodePath() . "' in '$pathXpath'" );
                  $this->gLogger->debug( "   removed child: '$replaceXpath'" );
                  $parentNode->appendChild( $documentDOM->importNode( $fragmentDOM->documentElement, self::IMPORT_SUBTREE ) );
               }
               $writeFileFlag = true;
               break;
               
            case $this::DELETING_NODE:
               // Used to delete a chunk. Extract the delete Xpath.
               $nodeToDelete = Utilities::getXPathListNode( 0, $documentXPath, $replaceXpath );
               $this->gLogger->debug( "   deleting node: '" . $nodeToDelete->getNodePath() . "'" );
               if ( $nodeToDelete == NULL ) {
                  throw new Exception( "Cannot find node to delete: $pathXpath" );
               }
               
               // Get the node from the document DOM where we will delete the child
               $parentNode = Utilities::getXPathListNode( 0, $documentXPath, $pathXpath );
               $this->gLogger->debug( "   parent node: '" . $parentNode->getNodePath() . "'" );
               if ( $parentNode == NULL ) {
                  throw new Exception( "Cannot find parent of node to delete in '$pathXpath'" );
               }
               
               if ( !$parentNode->removeChild( $nodeToDelete ) ) {
                  throw new RunException( "Could not remove child: " . $nodeToReplace->getNodePath(), PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
               }
               
               $writeFileFlag = true;
               break;
               
            case $this::NO_ACTION:
               //    3. if 'replace' is specified and it exists and 'overwrite' is false, no action occurs.
               $this->gLogger->debug( "   <source:replace> is specified and it exists and '@overwrite' is false" );
               $writeFileFlag = false;
               break;
               
         } // switch
         
         // At this point $documentDOM is the new document to write back
         // Utilities::outputStringVerbatim( $documentDOM->saveXML() );
         
         if ( $writeFileFlag ) {
            // Write back into the file
            if ( !$documentDOM->save( $filename ) ) {
                 $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                        self::WRITE_SOURCE_BEHAVIOUR_INSERT,
                                                        self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                        self::WRITE_SOURCE_SERIALIZER_XML,
                                                        $filename,
                                                        "Could not save document to file '$filename': " . $php_errormsg );
               $this->gLogger->debug( "Error: " . $php_errormsg );
            } else {
               $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                        self::WRITE_SOURCE_BEHAVIOUR_INSERT,
                                                        self::WRITE_SOURCE_EXECUTION_SUCCESS,
                                                        self::WRITE_SOURCE_SERIALIZER_XML,
                                                        $filename,
                                                        "File written ok" );
               $this->gLogger->debug( "Wrote back file ok" );
            }
         } else {
            $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                     self::WRITE_SOURCE_BEHAVIOUR_INSERT,
                                                     self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                     self::WRITE_SOURCE_SERIALIZER_XML,
                                                     $filename,
                                                     "Did not write to '$filename': " );
            $this->gLogger->debug( "Did not write to '$filename'" );
         }
      } catch ( Exception $e ) {
         $errorMessage = "Problem processing source document in write-source: $e->getMessage() ";
         $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                  self::WRITE_SOURCE_BEHAVIOUR_INSERT,
                                                  self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                  self::WRITE_SOURCE_SERIALIZER_XML,
                                                  $filename,
                                                  $errorMessage );
         $this->gLogger->debug( "Error: $errorMessage" );
      }
      return $resultDOM;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Process delete.
    *
    *   <pre>   <source:delete>
    *       <source:source/>
    *    </source:delete></pre>
    *
    * @param DOMNode $inSourceNode the source node to process
    * @retval DOMNode the replacement node (contains error/success information)
    *
    */
   
   private function processDelete( DOMNode $inSourceNode )
   {
      // Turn fragment into DOM
      $sourceDom = new DOMDocument();
      $sourceDom->appendChild( $sourceDom->importNode( $inSourceNode, 1 ) );
      // Utilities::outputStringVerbatim( $sourceDom->saveXML() );
      $this->gLogger->debug( "   delete: inputted DOM : '" . Utilities::outputStringVerbatim( $sourceDom->saveXML() ) . "'" );
      
      // Extract the file etc to use
      $xpath = new DOMXpath( $sourceDom );
      $xpath->registerNamespace( "source", Environment::$configuration[ 'sourceNamespace' ] );
      
      $filename = StringResolver::expandPseudoProtocols( Utilities::getXPathListStringItem( 0, $xpath, "//source:source" ) );
      $childXpath = Utilities::getXPathListStringItem( 0, $xpath, "//source:path" );
      $replaceXpath = Utilities::getXPathListStringItem( 0, $xpath, "//source:replace" );
      $this->gLogger->debug( "   source file: '" . $filename . "'" );
      $this->gLogger->debug( "   childXPath: '" . $childXpath . "'" );
      $this->gLogger->debug( "   replaceXPath: '" . $replaceXpath . "'" );
      
      try {
         if ( !file_exists( $filename ) ) {
            $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                     self::WRITE_SOURCE_BEHAVIOUR_DELETE,
                                                     self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                     self::WRITE_SOURCE_SERIALIZER_XML,
                                                     $filename,
                                                     "File does not exist" );
            $this->gLogger->debug( "Could not delete file: '$filename'" );
         } else {
            // Delete the file
            if ( !unlink( $filename ) ) {
               $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                        self::WRITE_SOURCE_BEHAVIOUR_DELETE,
                                                        self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                        self::WRITE_SOURCE_SERIALIZER_XML,
                                                        $filename,
                                                        "Could not delete file '$filename': " . $php_errormsg );
               $this->gLogger->fatal( "Error: " . $php_errormsg );
            } else {
               $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                        self::WRITE_SOURCE_BEHAVIOUR_DELETE,
                                                        self::WRITE_SOURCE_EXECUTION_SUCCESS,
                                                        self::WRITE_SOURCE_SERIALIZER_XML,
                                                        $filename,
                                                        "File delete ok" );
               $this->gLogger->debug( "Wrote back file ok" );
            }
         }
      } catch ( Exception $e ) {
         $errorMessage = "Problem processing source document in write-source: $e->getMessage() ";
         $resultDOM = $this->formatResultFragment( self::WRITE_SOURCE_ACTION_NEW,
                                                  self::WRITE_SOURCE_BEHAVIOUR_DELETE,
                                                  self::WRITE_SOURCE_EXECUTION_FAILURE,
                                                  self::WRITE_SOURCE_SERIALIZER_XML,
                                                  $filename,
                                                  $errorMessage );
         $this->gLogger->fatal( "Error: $errorMessage" );
      }
      return $resultDOM;
   }
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Form up the result XML.
    *
    * This is identical to the Cocoon structure
    *
    * <sourceResult>
    *    <action>new|overwritten|none</action>
    *    <behaviour>write|insert<behaviour>
    *    <execution>success|failure</execution>
    *    <serializer>xml</serializer>
    *    <source>source:specific/path/to/context/doc/editable/my.xml</source>
    *    <message>a message about what happened</message>
    * </sourceResult>
    *
    * @retval DOMDocument the result DOM
    */
   
   private function formatResultFragment( $inAction, $inBehaviour, $inExecution, $inSerializer, $inSource, $inMessage )
   {
      $ns = Environment::$configuration[ 'sourceNamespace' ];
      
      $dom = new DOMDocument();
      $root = $dom->createElementNS( $ns, 'sourceResult' );
      $dom->appendChild( $root );
      switch ( $inAction ) {
         case self::WRITE_SOURCE_ACTION_NEW : $action = 'new'; break;
         case self::WRITE_SOURCE_ACTION_OVERWRITTEN : $action = 'overwritten'; break;
         case self::WRITE_SOURCE_ACTION_NONE : $action = 'none'; break;
         default: $action = "";
      }
      $root->appendChild( $dom->createElementNS( $ns, 'action', $action ) );
      
      switch ( $inBehaviour ) {
         case self::WRITE_SOURCE_BEHAVIOUR_WRITE : $behaviour = 'write'; break;
         case self::WRITE_SOURCE_BEHAVIOUR_INSERT : $behaviour = 'insert'; break;
         default: $behaviour = "";
      }
      $root->appendChild( $dom->createElementNS( $ns, 'behaviour', $behaviour ) );
      
      switch ( $inExecution ) {
         case self::WRITE_SOURCE_EXECUTION_SUCCESS : $execution = 'success'; break;
         case self::WRITE_SOURCE_EXECUTION_FAILURE : $execution = 'failure'; break;
         default: $execution = "";
      }
      $root->appendChild( $dom->createElementNS( $ns, 'execution', $execution ) );
      
      switch ( $inSerializer ) {
         case self::WRITE_SOURCE_SERIALIZER_XML : $serializer = 'xml'; break;
         default: $serializer = "";
      }
      $root->appendChild( $dom->createElementNS( $ns, 'serializer', $serializer ) );
      $root->appendChild( $dom->createElementNS( $ns, 'source', $inSource ) );
      $root->appendChild( $dom->createElementNS( $ns, 'message', $inMessage ) );
      return $dom;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Translate an XPath expression to XML.
    *
    * Typical XPath in
    *
    *   "/root/child/grandchild"
    *
    * Giving
    *
    *    "<root><child><grandchild/></child></root>"
    *
    * @param string an XPath expression
    * @retval string the representation of the element as a string
    *
    */
   
   private function xpathToXML( $inXpathExpr )
   {
      $firstTag = "";
      $remainder = "";
      preg_match( "%/([^/]+)(.*)%", $inXpathExpr, $matches );
      if ( isset( $matches[ 1 ] ) ) { $firstTag = $matches[ 1 ]; }
      if ( isset( $matches[ 2 ] ) ) { $remainder = $matches[ 2 ]; }
      if ( strlen( $remainder ) > 0 ) {
         return "<$firstTag>" . $this->xpathToXML( $remainder ) . "</$firstTag>";
      } else {
         return "<$firstTag/>";
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
   
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }
   
}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>_SourceWritingTransformer</i> holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * <pre>   &lt;map:components>
 *      &lt;map:transformers default="xslt">
 *          &lt;map:transformer name="write-source" src="resource://lib/transforming/SourceWritingTransformer" />
 *          ...
 *      &lt;/map:transformers>
 *   &lt;/map:components></pre>
 *
 * The attributes are similar to those used in Cocoon:
 *
 * <ul>
 *   <li><i>name</i> : the name of the transformer used within the pipelines.</li>
 *
 *   <li><i>src</i> : the package to be used. In Paloose this is the filename of the PHP file (this file)
 *       without the php extension.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */

class _SourceWritingTransformer extends Transformer {
   
   /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */
   
   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "SourceWritingTransformer";
   }
   
}
?>
