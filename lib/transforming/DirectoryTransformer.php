<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2017 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>DirectoryTransformer</i> expands an embedded XML element to include
 * the contents of a folder (similar to DirectoryGenerator except this is a trsnsformer).
 *
 * @package paloose
 * @subpackage transforming
 */
 
class DirectoryTransformer extends TransformerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   /** The depth that the scan should be done */
   private $gDepth = 1;

   /** The directory where we started */
   private $gStartDir = 1;

   /** Sets the format for the date attribute of each node as described in the documentation.
   If unset, the default format for the current locale will be used.*/
   private $gDateFormat = ""; 
   
   /** Reverses the sort order */
   private $gReverse = false;

   /** Specifies the directories and files that should be included. Regular expression. */
   private $gInclude = "/.*/";

   /** Specifies the directories and files that should be excluded. Also a regular expression. */
   private $gExclude = "";

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>DirectoryTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _DirectoryTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if no <i>src</i> attribute
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
       try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the document.
    *
    * The XSL Transformer processes the input DOM. If there are no errors returns the transformed
    * DOM. All errors are caught by exception..
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws UserException if cannot find the XSL transformation file.
    * @throws InternalException if problem setting parameters.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $documentDOM = $inDOM;
      $rootNode = $documentDOM->documentElement;
      $this->gTranslatedDOM = new DOMDocument();
      $elementNS = $rootNode->namespaceURI;
      $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
      $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
      $this->gTranslatedDOM->appendChild( $newElement );
      try {
         $this->processNode( $inVariableStack, $rootNode, $newElement );
         $this->gTranslatedDOM->formatOutput = true;
         // Utilities::outputStringVerbatim( $this->gTranslatedDOM->saveXML() );
         return $this->gTranslatedDOM;
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document
    */

   private function processNode( VariableStack $inVariableStack, DOMNode $inParentNode, DOMNode &$inNewParentNode )
   {
      // Create a new node and update it with the old nodes attributes
      $folderTagFound = false;
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {
         $encodedAttribs = array();
         // $this->gLogger->debug( "Found tag: " . $inParentNode->nodeName );
         if ( $inParentNode->hasAttributes() and $inParentNode->nodeName == "pcms:folder" ) {
            $folderTagFound = true;
            $this->gLogger->debug( "Found folder tag" );
            /* foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = $attribute->name;
               $attribValue = $attribute->value;
               $this->gLogger->debug( "   Found folder element attribute '$attribName' = '$attribValue'" );
               $encodedAttribs[ $attribName ] = $attribValue;
            } */
         } else if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = StringResolver::expandString( $inVariableStack, $attribute->name );
               $attribValue = StringResolver::expandString( $inVariableStack, $attribute->value );
               $inNewParentNode->setAttribute( $attribName, $attribValue );
            }
         }
      }
            
      // We have to replace inParentNode with directory listing.
      if ( $folderTagFound === true ) {
	      $inParentNode = $this->processFolderData( $inParentNode );
      }

      // We have transferred the current node at this point - now we must traverse the children of the
      // old documents current node.
      $children = $inParentNode->childNodes;

      foreach ( $children as $child ) {
         // $this->gLogger->debug( "Processing child node: <" . $child->nodeName . ">" );
         if ( $child->nodeType == XML_ELEMENT_NODE ) {
            // Just an ordinary element not in to be translated so make a new element and append it.
            // $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
            $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
            // Now process the child node - calls this recursively
            $this->processNode( $inVariableStack, $child, $newElementNode );
            $inNewParentNode->appendChild( $newElementNode );
         } else if ( $child->nodeType == XML_TEXT_NODE ) {
            // Straight copy by appending to child list of new parent node
            // $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
            $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
            $inNewParentNode->appendChild( $newTextNode );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process directory information (identical to DirectoryGenerator but a transformer).
    *
    *   <pre>   <pcms:folder src="" [depth=""] [dateFormat=""] [include=""] [exclude=""] [reverse=""]/> </pre>
    *
    * @param DOMNode $inSourceNode the source node to process
    * @retval DOMNode the replacement node (contains error/success information)
    *
    */
 
   private function processFolderData( DOMNode $inSourceNode )
   {
      // Turn fragment into DOM
      $dom = new DOMDocument();
	  $rootElement = $dom->appendChild( $dom->createElementNS( Environment::$configuration[ 'dirNamespace' ], 'dir:listing' ) );
      // Utilities::outputStringVerbatim( $dom->saveXML() );

        if ( $inSourceNode->hasAttributes() ) {
            foreach ( $inSourceNode->attributes as $attribute ) {
               $attribName = $attribute->name;
               $attribValue = $attribute->value;
               $rootElement->setAttribute( $attribName, $attribValue );
               $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         }
               
         $this->gStartDir = $inSourceNode->getAttribute( "src" );
         // Assume
         if ( $inSourceNode->hasAttribute(  "depth"  ) ) {
            $this->gDepth = $inSourceNode->getAttribute( "depth" );
         } else {
		    $this->gDepth = 1;
         }
         if ( $inSourceNode->hasAttribute(  "dateFormat"  ) ) {
            $this->gDateFormat = $inSourceNode->getAttribute( "dateFormat" );
         }
         if ( $inSourceNode->hasAttribute(  "include"  ) ) {
            $this->gInclude = $inSourceNode->getAttribute( "include" );
         }
         if ( $inSourceNode->hasAttribute(  "exclude"  ) ) {
            $this->gExclude = $inSourceNode->getAttribute( "exclude" );
         }
         if ( $inSourceNode->hasAttribute(  "reverse"  ) ) {
            $this->gReverse = $inSourceNode->getAttribute( "reverse" );
         }
          
		$directoryDOM = $this->scanDirectory( $dom, $rootElement, $this->gStartDir, $this->gDepth );

      // Utilities::outputStringVerbatim( $directoryDOM->saveXML() );

     return $directoryDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Scan directory at this level.
    *
    * @param DOMNode $inRootElement the node under which to add the results
    * @retval DOMNode the replacement node (contains error/success information)
    */

   private function scanDirectory( $inDOMDocument, $inRootElement, $inStartDirectory, $inDepth )
   {
      $this->gLogger->debug( "Scanning directory: '{$inStartDirectory}'" );
      // Utilities::outputStringVerbatim( $inDOMDocument->saveXML() );

	  $numberFound = 0;
      $inDepth--;
      $element = NULL;
      
      // Set up directory node in the DOM
      $dirFileListing = scandir( $inStartDirectory, ( $this->gReverse === false ) ? 0 : 1 );
      foreach( $dirFileListing as $key => $value ) {
         $fullName = "$inStartDirectory/$value";
         // $this->gLogger->debug( "Found: '$fullName'" );
         if ( $value == '.'
               || $value == '..'
               || ( strlen( $this->gInclude ) > 0 && preg_match( $this->gInclude, $value ) == 0 )
               || ( strlen( $this->gExclude ) > 0 && preg_match( $this->gExclude, $value ) != 0 ) ) {
            // Do nothing
         } else if ( is_dir( $fullName ) ) {
            $this->gLogger->debug( "Found directory: '$fullName'" );
            $element = $inRootElement->appendChild( $inDOMDocument->createElementNS( Environment::$configuration[ 'dirNamespace' ], 'dir:directory' ) );
            $element->setAttribute( "name" , $fullName );
            $element->setAttribute( "lastmodified" , $this->getLastModified( $fullName ) );
            $element->setAttribute( "size" , filesize( $fullName ) );
            if ( $inDepth > 0 ) {
               $scanString = $this->scanDirectory( $inDOMDocument, $element, $fullName, $inDepth );
            } else {
               $scanString = "";
            }
         } else {
            $this->gLogger->debug( "Found file: '$fullName'" );
            $numberFound++;
            $element = $inRootElement->appendChild( $inDOMDocument->createElementNS( Environment::$configuration[ 'dirNamespace' ], 'dir:file' ) );
            $element->setAttribute( "name" , $value );
            $element->setAttribute( "lastmodified" , $this->getLastModified( $fullName ) );
            $element->setAttribute( "size" , filesize( $fullName ) );
         }
      }
      // Utilities::outputStringVerbatim( $inDOMDocument->saveXML() );
      if ( $numberFound == 0 ) {
		   $element = $inParentElement->appendChild( $inDOMDocument->createElementNS( Environment::$configuration[ 'dirNamespace' ], 'dir:empty' ) );
	   }
      return $inDOMDocument;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get last modified time for this dir/file.
    *
    * @param string $inDirectory the directory to retrieve the time of.
    * @retval the formatted time.
    */

   private function getLastModified( $inDirectory )
   {
      return date( $this->gDateFormat, filemtime( $inDirectory ));
   }
   

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_DirectoryTransformer</i> holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * <pre>   &lt;map:components>
 *      &lt;map:transformers default="xslt">
 *          &lt;map:transformer name="write-source" src="resource://lib/transforming/DirectoryTransformer" />
 *          ...
 *      &lt;/map:transformers>
 *   &lt;/map:components></pre>
 *
 * The attributes are similar to those used in Cocoon: 
 * 
 * <ul>
 *   <li><i>name</i> : the name of the transformer used within the pipelines.</li>
 * 
 *   <li><i>src</i> : the package to be used. In Paloose this is the filename of the PHP file (this file)
 *       without the php extension.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _DirectoryTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "DirectoryTransformer";
   }

}
?>