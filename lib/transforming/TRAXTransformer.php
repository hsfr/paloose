<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/caching/CacheFile.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>TRAXTransformer</i> uses a given XSLT transformer to transform a DOM.
 * 
 * It takes an inputted DOM
 * from the pipeline, transforms it and return it into the pipeline as a new DOM.
 * All errors are caught by exception.
 * 
 * Typical use:
 *
 * <pre>   &lt;map:match pattern="news-rss.xml">
 *      &lt;map:generate src="context://content/newsArticles.xml"/>
 *      &lt;map:transform src="context://resources/transforms/xml2rss.xsl"/>
 *      &lt;map:serialize/>
 *   &lt;/map:match></pre>
 *
 * The attributes are similar to those used in Cocoon: 
 * 
 * <ul>
 *   <li><i>src</i> : the file name of the transformation code, an XSL file. Variables are allowed here.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class TRAXTransformer extends TransformerPipeElement implements PipeElementInterface
{

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>TRAXTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _TRAXTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if no <i>src</i> attribute
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );
       
      $this->gLogger->debug( "Constructing cachable transformer: [{$this->gComponent->gIsCachable}]" );
      $this->gLogger->debug( "src : '" . $this->gSrc . "'" );
      if ( strlen( $inSrc ) == 0 ) {
         throw new UserException( "Must have a src attribute present", PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR, $inDOM );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the document.
    *
    * The XSL Transformer processes the input DOM. If there are no errors it returns the transformed
    * DOM. All errors are caught by exception.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws UserException if cannot find the XSL transformation file.
    * @throws InternalException if problem setting parameters.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      parent::run( $inVariableStack, $inURL, $inQueryString, $inDOM );
      $documentDOM = $inDOM;
      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Running TRAXTransformer with raw '" . $this->gSrc . "'" );
      $fullXSLFileName = StringResolver::expandString( $inVariableStack, $this->gSrc );
      $this->gLogger->debug( "Result after substitution: '$fullXSLFileName'" );
      $fullXSLFileName = StringResolver::expandPseudoProtocols( $fullXSLFileName );
      $this->gLogger->debug( "Result after pseudo protocol expansion: '$fullXSLFileName'" );

      $validDOM = false;
      $validXSLFile = false;
      $validParameters = false;

      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $isCachable = $this->gComponent->gIsCachable;
      $this->gLogger->debug( "Global cachable: [{$this->gComponent->gIsCachable}]" );
      $useReqParams = Utilities::normalizeBoolean( $this->gComponent->getUseRequestParametersFlag() );
      $this->gLogger->debug( "User request parameters: [$useReqParams]" );
      
      // Check to see if we need to override global cachable flag
      $localCachable = $requestParameterModule->get( CacheFile::CACHABLE_ATTRIBUTE_NAME );
      $this->gLogger->debug( "Local cachable: [$localCachable]" );
      if ( $useReqParams && $localCachable != NULL ) {
         $this->gLogger->debug( "Using request parameter value" );
         $localCachable = Utilities::normalizeBoolean( $localCachable );
         $isCachable = $localCachable;
      }
      $this->gLogger->debug( "Cachable transformer: [$isCachable]" );

      $fullFileName = "";

      if ( $isCachable ) {
         $this->gCacheFile = new CacheFile( Environment::$configuration[ 'cacheDir' ] );

         // We have to check all the inputs are valid. First the input DOM.
         $fullFileName = $documentDOM->documentElement->getAttribute( "__file" );
         $validDOM = ( $documentDOM->documentElement->getAttribute( "__status" ) == CacheFile::CACHE_VALID );
         $this->gLogger->debug( "Input DOM: [$fullFileName] : " . ( ( $validDOM ) ? "valid" : "invalid" ) );
         // Make up a cache key from both file names
         $cacheKey = CacheFile::getHash( $fullFileName . $fullXSLFileName );
         $this->gLogger->debug( "cache key: $cacheKey" );
         
         // Now the XSL file
         $oldXSLTimeStamp = ( $isCachable ) ? $this->gCacheFile->getTimeStamp( $cacheKey ) : 0;
         $this->gLogger->debug( "Time stamp of old XSL file [$fullXSLFileName] : $oldXSLTimeStamp" );
         $XSLTimeStamp = filemtime( $fullXSLFileName );
         $this->gLogger->debug( "Time stamp of current file: $XSLTimeStamp" );
         $validXSLFile = ( $oldXSLTimeStamp == $XSLTimeStamp );
         $this->gLogger->debug( "XSL File [$fullXSLFileName] : " . ( ( $validXSLFile ) ? "valid" : "invalid" ) );
   
         // Finally the parameters
         $oldParametersHash = ( $isCachable ) ? $this->gCacheFile->getParameterHash( $cacheKey ) : 0;
         $newParameterHash = CacheFile::getHash( $this->gParameters->toString() );
         $this->gLogger->debug( "Old Parameter hash : $oldParametersHash" );
         $this->gLogger->debug( "New Parameter hash : $newParameterHash" );
         $validParameters = ( $oldParametersHash == $newParameterHash );
         $this->gLogger->debug( "Parameters [{$this->gParameters->toString()}] : " . ( ( $validParameters ) ? "valid" : "invalid" ) );
      }

      if ( $validDOM && $validXSLFile && $validParameters && $isCachable ) {
         $this->gLogger->debug( "Reading DOM from cache" );
         $documentDOM = $this->gCacheFile->getCacheDOM( $cacheKey );
         $documentDOM->documentElement->setAttribute( "__status", CacheFile::CACHE_VALID );
      } else {
          // Invalid (somewhere) or not cachable so run normally
         $this->gLogger->debug( "Transforming DOM from pipeline" );
         $xslScript = new DomDocument(); 
         $xslProcessor = new XSLTProcessor();
         $xslProcessor->registerPHPFunctions();

         try {
            if ( !file_exists( $fullXSLFileName ) ) {
                 throw new UserException(
                    "Cannot find XSL file '$fullXSLFileName'",
                    PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
            }
           // Now check for well-formed etc (no check for valid as that will take too long)
            $check = new XML_check();
            if ( !$check->check_url( $fullXSLFileName ) ) {
               throw new RunException(
                  "Invalid transform file '$fullXSLFileName'" .
                  "\n" . $check->get_full_error(),
                  PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
            }
            if ( !$xslScript->load( $fullXSLFileName ) ) {
               throw new RunException(
                  "Could not load transform file '$fullXSLFileName'" .
                  "\n" . $check->get_full_error(),
                  PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
            }
            $xslProcessor->importStylesheet( $xslScript );
            if ( !empty( $this->gParameters->getParameterList() ) ) {
               $parameterList = $this->gParameters->getParameterList();
               foreach ( $parameterList as $name => $value ) {
                  $expandedValue = StringResolver::expandPseudoProtocols( StringResolver::expandString( $inVariableStack, $value ) );
                  // Put into globals module to make available for XML expansion in widgets etc.
                  $globalsModule = Environment::$modules[ 'global' ];
//                  echo "[name: $name][expandedValue: $expandedValue]<br/>";
                  $globalsModule->add( $name, $value );
                  $this->gLogger->debug( "Setting parameter [$name] = [$expandedValue]" );
                  if ( $xslProcessor->setParameter( '', $name, $expandedValue ) === false ) {
                     throw new InternalException( "Problem setting parameter '$name' with value ' $value'",
                        PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
                  }
               }
            }
  
            $this->gLogger->debug( $documentDOM->saveXML() );
            $documentDOM = $xslProcessor->transformToDoc( $documentDOM );

            if ( $documentDOM->documentElement ) {
               $documentDOM->documentElement->setAttribute( "__status", CacheFile::CACHE_INVALID );
            }
            // Finally update the cache if required
            if ( $isCachable ) {
               $this->gLogger->debug( "Cachable file so updating" );
               $this->gCacheFile->updateDOM2Cache( $cacheKey, $XSLTimeStamp, $documentDOM, $newParameterHash );
            }
         } catch ( RunException $e ) {
              throw new RunException( $e->getMessage(), $e->getCode() );
         }
      }
      if ( $documentDOM->documentElement ) {
         $documentDOM->documentElement->setAttribute( "__file", $fullFileName );
      }
      // $this->gLogger->debug( $documentDOM->saveXML() );
      return $documentDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_TRAXTransformer</i> holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * <pre>   &lt;map:components>
 *      &lt;map:transformers default="xslt">
 *          &lt;map:transformer name="xslt" src="resource://lib/transforming/TRAXTransformer">
 *              &lt;map:use-request-parameters>true&lt;/map:use-request-parameters>
 *          &lt;/map:transformer>
 *      &lt;/map:transformers>
 *   &lt;/map:components></pre>
 *
 * The attributes are similar to those used in Cocoon: 
 * 
 * <ul>
 *   <li><i>name</i> : the name of the transformer used within the pipelines.</li>
 * 
 *   <li><i>src</i> : the package to be used. In Paloose this is the filename of the PHP file (this file)
 *       without the php extension.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _TRAXTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "TRAXTransformer";
   }

}
?>
