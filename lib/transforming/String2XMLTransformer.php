<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2017 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The String2XMLTransformer.
 *
 * Convert a string within a root element into XML.
 *      
 * @package paloose
 * @subpackage transforming
 */
 
class String2XMLTransformer extends TransformerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   /** Specifies the enclosing element for the text */
   private $gRoot = "";

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>String2XMLTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _String2XMLTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if parameters do not have correct attributes present
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gLogger->debug( "Constructed String2XMLTransformer instance" );

      if ( $this->gParameters->getParameter( "root-node" ) == NULL ) {
         throw new UserException( "Must have root node attribute attribute", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, $inDOM );
      } else {
         $this->gRoot = $this->gParameters->getParameter( "root-node" );
      }
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the document.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws RunException if problem.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Running String2XMLTransformer" );

      $documentDOM = $inDOM;
      $rootNode = $documentDOM->documentElement;
      $this->gTranslatedDOM = new DOMDocument();
      $elementNS = $rootNode->namespaceURI;
      $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
      $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
      $this->gTranslatedDOM->appendChild( $newElement );
      try {
         $this->processNode( $inVariableStack, $rootNode, $newElement );
         // Utilities::outputStringVerbatim( $this->gTranslatedDOM->saveXML() );
         return $this->gTranslatedDOM;
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process a single node.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document (reference)
    * @throws RunException if problem.
    */

   private function processNode( VariableStack $inVariableStack, DOMNode $inParentNode, DOMNode &$inNewParentNode )
   {
     // Create a new node and update it with the old nodes attributes
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {
         // At this point we have a list of attributes that may need translating to expand variables
         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = StringResolver::expandString( $inVariableStack, $attribute->name );
               $attribValue = StringResolver::expandPseudoProtocols( StringResolver::expandString( $inVariableStack, $attribute->value ) );
               $inNewParentNode->setAttribute( $attribName, $attribValue );
               $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         }
      }

      // We have transferred the current node at this point - now we must traverse the children of the
      // old documents current node.
      $children = $inParentNode->childNodes;

      foreach ( $children as $child ) {
         $this->gLogger->debug( "Processing child node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
         if ( $child->nodeType == XML_ELEMENT_NODE ) {
           // Just an ordinary element not in to be translated so make a new element and append it.
           $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
           $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
           // Now process the child node - calls this recursively
           $this->processNode( $inVariableStack, $child, $newElementNode );
           $inNewParentNode->appendChild( $newElementNode );
         } else if ( $child->nodeType == XML_TEXT_NODE ) {
            if ( $child->parentNode->nodeName == $this->gRoot ) {
               $fragment = $this->gTranslatedDOM->createDocumentFragment();
               $fragment->appendXML( $child->textContent );
               $inNewParentNode->appendChild( $fragment );
            } else {
               $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
               $inNewParentNode->appendChild( $newTextNode );
            }
         }
      }
   }
  
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_String2XMLTransformer</i> holds the information for the component.
 *
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _String2XMLTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gGenericType = PipeElement::TRANSFORMER;
        $this->gPackageName = "String2XMLTransformer";
      
        $this->gLogger->debug( "Constructed _String2XMLTransformer component" );
   }

}
?>