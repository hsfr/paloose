<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The FilterTransformer allows the restriction of the number of tags passed through to the
 * next stage of the pipeline. This is particular relevant to SQL results which are
 * returned as a set of rows. A typical use of FilterTransformer would be </t:p>
 *
 *     <pre>     &lt;map:transformers default="xslt">
 *         &lt;map:transformer name="xslt" src="resource://lib/transforming/TRAXTransformer">
 *            &lt;map:use-request-parameters>true&lt;/map:use-request-parameters>
 *         &lt;/map:transformer>
 *         &lt;map:transformer name="mysql" src="resource://lib/transforming/SQLTransformer">
 *            &lt;map:parameter name="type" value="mysql"/>
 *            &lt;map:parameter name="host" value="localhost:3306"/>
 *            &lt;map:parameter name="user" value="root"/>
 *         &lt;/map:transformer>
 *         &lt;map:transformer name="filter" src="resource://lib/transforming/FilterTransformer"/>
 *      &lt;/map:transformers>
 *
 *      ...
 *
 *      &lt;map:pipeline>
 *
 *         &lt;map:match pattern="**.html">
 *            &lt;map:generate src="context://content/{1}.xml" label="xml-content"/>
 *            &lt;map:transform type="mysql" label="sql-transform">
 *               &lt;map:parameter name="show-nr-of-rows" value="true"/>
 *               &lt;map:parameter name="composer" value="Bach"/>
 *            &lt;/map:transform>
 *            &lt;map:transform type="filter">
 *               &lt;map:parameter name="element-name" value="http://apache.org/cocoon/SQL/2.0:row"/>
 *               &lt;map:parameter name="count" value="2"/>
 *               &lt;map:parameter name="blocknr" value="3"/>
 *            &lt;/map:transform>
 *            ...
 *         &lt;/map:match></pre>
 *
 *  where
 *
 *      <ul>
 *         <li><emph>element-name</emph> &#x2014; the name of the tag which will be restricted.
 *         It can either be a tag with or without a namespace. However the dec;aration must match what is in the document.</li>
 *         <li><emph>count</emph> &#x2014; the size of the blocks.</li>
 *         <li><emph>blocknr</emph> &#x2014; the block number that is required.</li>
 *      </ul>
 *      
 * @package paloose
 * @subpackage transforming
 */
 
class FilterTransformer extends TransformerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

    /** Element name to filter */
   public $gElementName;
   
    /** Element NS */
   public $gElementNS;
   
    /** Count of block size */
   public $gCount;
   
    /** Which block to output */
   public $gBlockNum;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>FilterTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _FilterTransformer $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if parameters do not have correct attributes present
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
        $this->gLogger = Logger::getLogger( __CLASS__ );
       
        // These are the fixed parameters
        $this->gElementName = $this->gParameters->getParameter( 'element-name' );
      if ( strlen( $this->gElementName ) == 0 ) {
           throw new UserException( "Must have an element-name attribute present", PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR, $inDOM );
        }

       // These are the fixed parameters
        $this->gCount = $this->gParameters->getParameter( 'count' );
      if ( strlen( $this->gCount ) == 0 ) {
           throw new UserException( "Must have a count attribute present", PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR, $inDOM );
        }

       // These are the fixed parameters
        $this->gBlockNum = $this->gParameters->getParameter( 'blocknr' );
      if ( strlen( $this->gBlockNum ) == 0 ) {
           throw new UserException( "Must have an blocknr attribute present", PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR, $inDOM );
        }
        
        $colonPos = strrpos( $this->gElementName, ":" );
        if ( $colonPos == 0 ) {
           $this->gElementNS = "";
        } else {
           $this->gElementNS = substr( $this->gElementName, 0, $colonPos );
           $this->gElementName = substr( $this->gElementName, $colonPos + 1 );
        }

      $this->gLogger->debug( "Constructed FilterTransformer instance: [type:" . $this->gType . "][elementNS:" . $this->gElementNS . "][element-name:" . $this->gElementName . "][count:" . $this->gCount . "][blocknr:" . $this->gBlockNum . "]" );
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the document.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws RunException if problem.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Running FilterTransformer" );

      $documentDOM = $inDOM;
      $rootNode = $documentDOM->documentElement;
      $this->gTranslatedDOM = new DOMDocument();
      $elementNS = $rootNode->namespaceURI;
      $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
      $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
      $this->gTranslatedDOM->appendChild( $newElement );
      try {
         $this->processNode( $inVariableStack, $rootNode, $newElement, Environment::$configuration[ 'sqlNamespace' ] );
         $this->gTranslatedDOM->formatOutput = true;
         // Utilities::outputStringVerbatim( $this->gTranslatedDOM->saveXML() );
         return $this->gTranslatedDOM;
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process a single node.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document (reference)
    * @param string $inSQLNamespace the namespace for all sql query tags
    * @throws RunException if problem.
    */

   private function processNode( VariableStack $inVariableStack, DOMNode $inParentNode, DOMNode &$inNewParentNode, $inSQLNamespace )
   {
      // Create a new node and update it with the old nodes attributes
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {
         // $this->gLogger->debug( "Processing parent node: <" . $inParentNode->localName . ">" );
         // At this point we have a list of attributes that may need translating to expand variables
         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = StringResolver::expandString( $inVariableStack, $attribute->name );
               $attribValue = StringResolver::expandString( $inVariableStack, $attribute->value );
               $inNewParentNode->setAttribute( $attribName, $attribValue );
               $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         }
      }

      // We have transferred the current node at this point - now we must traverse the children of the
      // old documents current node.
      $children = $inParentNode->childNodes;
      $count = 0;
      $block = 0;
      $lastBlockNode = NULL;

      try {
         foreach ( $children as $child ) {
            if ( $child->nodeType == XML_ELEMENT_NODE ) {
               $this->gLogger->debug( "Processing: <" . $child->namespaceURI . ":" . $child->localName . ">" . " [" . $inSQLNamespace . "]" );
            }
            if ( $child->localName == $this->gElementName && $child->namespaceURI == $this->gElementNS ) {
               $this->gLogger->debug( "Found filtered node: <" . $child->namespaceURI . ":" . $child->localName . ">" );
               if ( $count % $this->gCount == 0 ) {
                  // Start of a new block
                  $block++;
                  // Output a block tag
                  $blockElement = $this->gTranslatedDOM->createElementNS( $inSQLNamespace, 'block' );
                  $blockNode = $inNewParentNode->appendChild( $blockElement );
                  $blockNode->setAttribute( "id", $block );
                  $lastBlockNode = $blockNode;
               }
               // Within each block we output the element-name tag (assuming we are in the block)
               if ( $block == $this->gBlockNum ) {
                  if ( $child->nodeType == XML_ELEMENT_NODE ) {
                     // $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
                     $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
                     // Now process the child node - calls this recursively
                     $this->processNode( $inVariableStack, $child, $newElementNode, $inSQLNamespace );
                     $lastBlockNode->appendChild( $newElementNode );
                  } else if ( $child->nodeType == XML_TEXT_NODE ) {
                     // $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
                     $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
                     $lastBlockNode->appendChild( $newTextNode );
                  }
               }
               $count++;
            } else { 
               // Pass all others through
               if ( $child->nodeType == XML_ELEMENT_NODE ) {
                 // $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
                 $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
                 // Now process the child node - calls this recursively
                 $this->processNode( $inVariableStack, $child, $newElementNode, $inSQLNamespace );
                 $inNewParentNode->appendChild( $newElementNode );
               } else if ( $child->nodeType == XML_TEXT_NODE ) {
                  // $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
                  $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
                  $inNewParentNode->appendChild( $newTextNode );
               }
            }
         }
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
   }
  
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_FilterTransformer</i> holds the information for the component.
 *
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _FilterTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gGenericType = PipeElement::TRANSFORMER;
        $this->gPackageName = "FilterTransformer";
      
        $this->gLogger->debug( "Constructed _FilterTransformer component" );
   }

}
?>