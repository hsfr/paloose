<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2020 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/caching/CacheFile.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>PHPTransformer</i> uses PHP to change the DOM.
 * 
 * It takes an inputted DOM
 * from the pipeline, transforms it and return it into the pipeline as a new DOM.
 * All errors are caught by exception.
 * 
 * Typical use:
 *
 * @package paloose
 * @subpackage transforming
 */
 
class PHPTransformer extends TransformerPipeElement implements PipeElementInterface
{

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>PHPTransformer</i> pipeline element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element.
    * @param string $inSrc the src attribute (or package required in this case).
    * @param _PHPTransformer $inComponent the associated component instance (stores parameters etc).
    * @throws UserException if no <i>src</i> attribute.
    * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes.
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );
       
      $this->gLogger->debug( "Constructing cachable transformer: [{$this->gComponent->gIsCachable}]" );
      $this->gLogger->debug( "src : '" . $this->gSrc . "'" );
      if ( strlen( $inSrc ) == 0 ) {
         throw new UserException( "Must have a src attribute present", PalooseException::SITEMAP_TRANSFORMER_PARSE_ERROR, $inDOM );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process the document.
    *
    * The XSL Transformer processes the input DOM. If there are no errors it returns the transformed
    * DOM. All errors are caught by exception.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the transformed DOM.
    * @throws UserException if cannot find the XSL transformation file.
    * @throws InternalException if problem setting parameters.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      parent::run( $inVariableStack, $inURL, $inQueryString, $inDOM );
      $documentDOM = $inDOM;
      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Running PHPTransformer with raw '" . $this->gSrc . "'" );
      $fullScriptFileName = StringResolver::expandString( $inVariableStack, $this->gSrc );
      $this->gLogger->debug( "Result after substitution: '$fullScriptFileName'" );
      $fullScriptFileName = StringResolver::expandPseudoProtocols( $fullScriptFileName );
      $this->gLogger->debug( "Result after pseudo protocol expansion: '$fullScriptFileName'" );

      $validDOM = false;
      $validXSLFile = false;
      $validParameters = false;

      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $isCachable = $this->gComponent->gIsCachable;
      $this->gLogger->debug( "Global cachable: [{$this->gComponent->gIsCachable}]" );
      $useReqParams = Utilities::normalizeBoolean( $this->gComponent->getUseRequestParametersFlag() );
      $this->gLogger->debug( "User request parameters: [$useReqParams]" );
      
      // Check to see if we need to override global cachable flag
      $localCachable = $requestParameterModule->get( CacheFile::CACHABLE_ATTRIBUTE_NAME );
      $this->gLogger->debug( "Local cachable: [$localCachable]" );
      if ( $useReqParams && $localCachable != NULL ) {
         $this->gLogger->debug( "Using request parameter value" );
         $localCachable = Utilities::normalizeBoolean( $localCachable );
         $isCachable = $localCachable;
      }
      $this->gLogger->debug( "Cachable transformer: [$isCachable]" );

      $fullFileName = "";

      $this->gLogger->debug( "Transforming DOM from pipeline" );

      try {
         if ( !file_exists( $fullScriptFileName ) ) {
              throw new UserException(
                 "Cannot find script file '$fullScriptFileName'",
                 PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
         }

         $this->gLogger->debug( "Including script '$fullScriptFileName'" );
         
         require_once( $fullScriptFileName );
   
         $script = new PHPScript( $inVariableStack, $inURL, $inQueryString );
         $this->gLogger->debug( "Created user script" );
  
         $documentDOM = $script->run( $documentDOM, $this->gParameters->getParameterList() );
         $this->gLogger->debug( "Ran user script" );
  
      } catch ( RunException $e ) {
           throw new RunException( $e->getMessage(), $e->getCode() );
      }
      if ( $documentDOM->documentElement ) {
         $documentDOM->documentElement->setAttribute( "__file", $fullFileName );
      }
      // $this->gLogger->debug( $documentDOM->saveXML() );
      return $documentDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_PHPTransformer</i> holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component. For example
 *
 * <pre>   &lt;map:components>
 *      &lt;map:transformers default="xslt">
 *          &lt;map:transformer name="xslt" src="resource://lib/transforming/PHPTransformer">
 *              &lt;map:use-request-parameters>true&lt;/map:use-request-parameters>
 *          &lt;/map:transformer>
 *      &lt;/map:transformers>
 *   &lt;/map:components></pre>
 *
 * The attributes are similar to those used in Cocoon: 
 * 
 * <ul>
 *   <li><i>name</i> : the name of the transformer used within the pipelines.</li>
 * 
 *   <li><i>src</i> : the package to be used. In Paloose this is the filename of the PHP file (this file)
 *       without the php extension.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _PHPTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "PHPTransformer";
   }

}
?>
