<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage transforming
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/i18n/Catalogues.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * <p>The i18n Transformer looks after multi-language/locale information.
 *
 * Although it
 * is based on the Cocoon version there are some differences (especially in formatting
 * patterns) that should be noted. The namespace URI of i18n transformer is identical
 * to that of cocoon: "http://apache.org/cocoon/i18n/2.1".
 *
 * @package paloose
 * @subpackage transforming
 */

class I18nTransformer extends TransformerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

     /** The id of the  translator to use */
   private $gId;

    /** The id of the  translator to use */
   private $gLocale;

    /** The DOM of the pipeline document we are processing */
   private $gDOM;

    /** No translation file etc  */
   private $gTranslationPossible;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Create an instance of the I18nTransformer.
    *
    * Labels and other parameters are
    * gathered and stored.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _I18nTransformer $inComponent the associated component instance (stores parameters etc)
    * throws UserException if problem
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
      $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The i18n Transformer processes the input DOM (in $inDOM) and returns a
    * transformed DOM.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the document DOM through the pipeline to transform.
    * @retval DOMDocument the transformed DOM.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gDOM = $inDOM;
     
      // Always get the query string for this tranformer (we need the 'locale' parameter)
      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $params = $requestParameterModule->getParams();
      if ( !empty( $params ) ) {
         foreach ( $params as $name => $value ) {
            $this->gParameters->setParameter( $name, $value );
         }
      }
      $this->gLocale = $this->gParameters->getParameter( "locale" );

      if ( $this->gLocale == "" ) {
         $this->gLocale = "en_GB";
         // Did not have a locale requested so exit with input DOM unchanged.
         // We need to eat all i18n tags here. !!!
         // return $inDOM;
      }
      $this->gLogger->debug( "Locale: '". $this->gLocale . "'" );

      // Get the default value of the catalog to use
      $catalogues = $this->gComponent->getCatalogues();
      $catalogueName = $catalogues->getDefaultCatalogue();
      if ( !empty( $this->gParameters->getParameter( "default-catalogue-id" ) ) ) {
         $catalogueName = $this->gParameters->getParameter( "default-catalogue-id" );
      }
      $expandedValue = StringResolver::expandString( $inVariableStack, $catalogueName );
      $catalogueName = StringResolver::expandPseudoProtocols( $expandedValue );
      
      // Get the catalogue for this translator      
      try {
         $catalogue = $catalogues->getCatalogue( $catalogueName );
         // Get the location for this translator data
         $location = $catalogue->getLocation();
         $id = $catalogue->getId();
         $name = $catalogue->getName();
         $untranslatedText = $this->gComponent->getUntranslatedText();
   
         // Get the location
         $expandedValue = StringResolver::expandString( $inVariableStack, $location );
         $expandedValue = StringResolver::expandPseudoProtocols( $expandedValue );
                
         // We have to try each translation file in turn. First the language specific one.
         $languageCountryFileName = $expandedValue . Environment::getPathSeparator() . $name . "_" . $this->gLocale . ".xml";
         if ( strstr( $this->gLocale, "_" ) !== false ) {
            $pieces = explode( "_", $this->gLocale );
            $country = "_" . $pieces[ 0 ];
         } else {
            $country = "";
         }
         $languageFileName = $expandedValue . Environment::getPathSeparator() . $name . $country . ".xml";
         $neutralFileName = $expandedValue . Environment::getPathSeparator() . $name . ".xml";
         $this->gLogger->debug( "languageFileName : $languageFileName'" );
         $this->gLogger->debug( "neutralFileName : $neutralFileName'" );
         $this->gTranslationPossible = true;
         if ( file_exists( $languageCountryFileName ) ) {
            $fullFileName = $languageCountryFileName;
         } else if ( file_exists( $languageFileName ) ) {
            $fullFileName = $languageFileName;
         } else if ( file_exists( $neutralFileName ) ) {
            $fullFileName = $neutralFileName;
         } else {
            // Oops - there is no suitable translation file
            $this->gTranslationPossible = false;
            $this->gLogger->debug( "No translation possible" );
         }
      } catch ( RunException $e ) {
         $this->gTranslationPossible = false;
      }


      if ( $this->gTranslationPossible ) {
         $this->gLogger->debug( "Translation file: $fullFileName" );
         
         // Now to read the translation file into a DOM
         $catalogueDOM = new DOMDocument();
         if ( !$catalogueDOM->load( $fullFileName ) ) {
            throw new UserException( "Could not load document from translation file '$fullFileName'", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
         }
   
         // We have to scan the DOM and replace all i18n tags with their translation replacement.
         // Very long-winded - there must be a better way.
         // echo "<pre>" . htmlspecialchars( $inDOM->saveXML() ) . "</pre>";
         $rootNode = $inDOM->documentElement;
   
         $this->gTranslatedDOM = new DOMDocument();
         $elementNS = $rootNode->namespaceURI;
         $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
         $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
         $this->gTranslatedDOM->appendChild( $newElement );
         
         $this->processNode( $catalogueDOM, $rootNode, $newElement );
         return $this->gTranslatedDOM;
      } else {
         // Although we cannot translate there may be date/time translations to do
         $rootNode = $inDOM->documentElement;
   
         $this->gTranslatedDOM = new DOMDocument();
         $elementNS = $rootNode->namespaceURI;
         $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
         $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
         $this->gTranslatedDOM->appendChild( $newElement );
         
         $this->processNode( null, $rootNode, $newElement );
         return $this->gTranslatedDOM;
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param DOMDocument $inCatalogueDOM the document that holds catalogue which has the translation scraps.
    *  null if there is no catalogue file.
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document
    */

   private function processNode( $inCatalogueDOM, DOMNode $inParentNode, DOMNode $inNewParentNode )
   {

      
      // Create a new node and update it with the old nodes attributes
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {

         // First of all make a list of all the i18n namespace attributes
         $i18nAttribs = array();
         $i = 0;
         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               // Main wrinkle here is to check for i18n namespace
               $attribName = $attribute->name;
               $attribValue = $attribute->value;
               if ( $attribute->namespaceURI == Environment::$configuration[ 'i18nNamespace' ] and $attribName == "attr" ) {
                  $this->gLogger->debug( "   Found i18n attribute" );
                  $i18nAttribs[ $i++ ] = $attribValue;
               }
               $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         }
         //At this point we have a list of attributes that need translating in $i18nAttribs
         // Now we go through and copy the attributes across to the new node and lose the i18n
         // attributes.

         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               // Main wrinkle here is to check for i18n namespace
               $attribName = $attribute->name;
               $attribValue = $attribute->value;
               if ( in_array( $attribName, $i18nAttribs ) and $this->gTranslationPossible === true ) {
                  // An attribute to translate if possible
                  if ( $inCatalogueDOM != null ) {
                     $attribValue = $this->getAttributeTranslation( $inCatalogueDOM, $attribValue );
                  } else {
                     $attribValue = $this->gComponent->getUntranslatedText();
                  }
               }
               // Must not add any i18n attributes
               if ( $attribute->namespaceURI !== Environment::$configuration[ 'i18nNamespace' ] ) {
                  $inNewParentNode->setAttribute( $attribName, $attribValue );
                  $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
               }
            }
         } else {
            // No attributes
         }
      } 

      // We have transfered the current node at this point - now we must traverse the children of the
      // old documments current node.
      $children = $inParentNode->childNodes;

      // Walk this list of children and check for each one being a i18n namespace candidate
      foreach ( $children as $child ) {
         $this->gLogger->debug( "Processing child node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
         if ( $child->nodeType == XML_ELEMENT_NODE ) {
            if ( $child->namespaceURI == Environment::$configuration[ 'i18nNamespace' ] ) {
               // If it is a i18n element we do not create a new child for the parent but take
               // the new parent into the function 
               $this->gLogger->debug( "Processing i18n child node: <" . $child->nodeName . "> : " . $child->textContent );
               $nodeName = $child->localName;
               // Check to see which i18n element is required
               if ( $nodeName == "text" ) {
                  $this->processText( $inCatalogueDOM, $child, $inNewParentNode );
               } else if ( $nodeName == "date" ) {
                  $this->processDate( $child, $inNewParentNode );
               } else if ( $nodeName == "time" ) {
                  $this->processTime( $child, $inNewParentNode );
               }
               // We do not call processNode recursively since we eat the i18n elements 
           } else {
              // Just an ordinary element not in to be translated so make a new element and append it.
              $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
              $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
              // Now process the child node - calls this recursively
              $this->processNode( $inCatalogueDOM, $child, $newElementNode );
              $inNewParentNode->appendChild( $newElementNode );
           }
         } else if ( $child->nodeType == XML_TEXT_NODE ) {
            // Straight copy by appending to child list of new parent node
            $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
            $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
            $inNewParentNode->appendChild( $newTextNode );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param DOMNode $inChildNode the node we are processing.
    * @param DOMNode $inNewParentNode the parent node where we will add the child based on $inChildNode.
    */

   private function processTime( DOMNode $inChildNode, DOMNode $inNewParentNode )
   {

      $this->gLogger->debug( ">>>> Processing i18n time node for locale: " . $this->gLocale );

      if ( $inChildNode->hasAttribute( "value" ) ) {
         $this->gLogger->debug( "   input time: '" . $inChildNode->getAttribute( "value" ) . "'" );
         $timeValue = strtotime( $inChildNode->getAttribute( "value" ) );
      } else if ( $inChildNode->textContent == "" ) {
         $this->gLogger->debug( "   input time: 'now'" );
         $timeValue = strtotime( "now" );
      } else {
         // Take the default value and use that
         $this->gLogger->debug( "   input date: '" . $inChildNode->textContent . "'" );
         $timeValue = strtotime( $inChildNode->textContent );
      }

      if ( $inChildNode->hasAttribute( "pattern" ) ) {
         $timeFormat = $inChildNode->getAttribute( "pattern" );
         $this->gLogger->debug( "   found format pattern: '" . $timeFormat . "'" );
         // Format occording to the src-pattern
      } else {
         $timeFormat = "%H:%M:%S";      // Default to a reasonable look
         $this->gLogger->debug( "   setting default format pattern: '" . $timeFormat . "'" );
      }

      $this->gLogger->debug( "   time: '" . $timeValue . "'" );

      setlocale( LC_ALL, $this->gLocale );
      $newTextNode = $this->gTranslatedDOM->createTextNode( strftime( $timeFormat, $timeValue ) );
      $inNewParentNode->appendChild( $newTextNode );
      $this->gLogger->debug( "   translated time: '" . $newTextNode->textContent . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param DOMNode $inChildNode the node we are processing.
    * @param DOMNode $inNewParentNode the parent node where we will add the child based on $inChildNode.
    */

   private function processDate( DOMNode $inChildNode, DOMNode $inNewParentNode )
   {

      $this->gLogger->debug( ">>>> Processing i18n date node for locale: " . $this->gLocale );

      if ( $inChildNode->hasAttribute( "value" ) ) {
         $this->gLogger->debug( "   input date: '" . $inChildNode->getAttribute( "value" ) . "'" );
         $dateValue = strtotime( $inChildNode->getAttribute( "value" ) );
      } else if ( $inChildNode->textContent == "" ) {
         $this->gLogger->debug( "   input date: 'now'" );
         $dateValue = strtotime( "now" );
      } else {
         // Take the default value and use that
         $this->gLogger->debug( "   input date: '" . $inChildNode->textContent . "'" );
         $dateValue = strtotime( $inChildNode->textContent );
      }
      $this->gLogger->debug( "   input date: '" . $dateValue . "'" );

     if ( $inChildNode->hasAttribute( "pattern" ) ) {
         $dateFormat = $inChildNode->getAttribute( "pattern" );
         $this->gLogger->debug( "   found format pattern: '" . $dateFormat . "'" );
         // Format occording to the src-pattern
      } else {
         $dateFormat = "%d %b %Y";      // Default to a reasonable look
         $this->gLogger->debug( "   setting default format pattern: '" . $dateFormat . "'" );
      }

      $this->gLogger->debug( "   date: '" . $dateValue . "'" );

      setlocale( LC_ALL, $this->gLocale );
      $newTextNode = $this->gTranslatedDOM->createTextNode( strftime( $dateFormat, $dateValue ) );
      $inNewParentNode->appendChild( $newTextNode );
      $this->gLogger->debug( "   translated date: '" . $newTextNode->textContent . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param DOMDocument $inCatalogueDOM the document that holds catalogue which has the translation scraps.
    * @param DOMNode $inChildNode the node we are processing.
    * @param DOMNode $inNewParentNode the parent node where we will add the child based on $inChildNode.
    */

   private function processText( $inCatalogueDOM, DOMNode $inChildNode, DOMNode $inNewParentNode )
   {

      $this->gLogger->debug( "Processing i18n text node: '" . $inChildNode->textContent . "'" );

      if ( $this->gTranslationPossible === true and $inCatalogueDOM != null ) {
         // Two conditions here detected by presence if i18n:key attribute
         if ( $inChildNode->hasAttribute( "key" ) ) {
            // Use the key attribute value as the key
            $keyValue = $inChildNode->getAttribute( "key" );
         } else {
            // We use the content text as the key
            $keyValue = $inChildNode->textContent;
         }
         $this->gLogger->debug( "Key found: '$keyValue'" );
         $xpath = new domxpath( $inCatalogueDOM );
         $translatedContent = $xpath->query( "//message[ @key = '$keyValue' ]" );
         // The list of nodes in $translatedContent must be inserted in place of the child $inNode
         $i = 0;
         if ( $translatedContent->length > 0 ) {
            while ( $i < $translatedContent->length ) {
               $node = $this->gTranslatedDOM->importNode( $translatedContent->item( $i ), true );
               if ( $node->nodeType == XML_ELEMENT_NODE ) {
                  $this->gLogger->debug( "Append element node: <" . $node->localName . ">" );
               } else if ( $node->nodeType == XML_TEXT_NODE ) {
                  $this->gLogger->debug( "Append text node: '" . $node->textContent . "'" );
               }
               $inNewParentNode->appendChild( $node );
               $i++;
            }
         } else {
            $newTextNode = $this->gTranslatedDOM->createTextNode( $this->gComponent->getUntranslatedText() );
            $inNewParentNode->appendChild( $newTextNode );
         }
      } else {
         $node = $this->gTranslatedDOM->importNode( $inChildNode, true );
         $inNewParentNode->appendChild( $node );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   private function getAttributeTranslation( DOMDocument $inCatalogueDOM, $inValue )
   {

      $this->gLogger->debug( "Getting attribute translation with key: '$inValue'" );

      $xpath = new domxpath( $inCatalogueDOM );
      $newValue = Utilities::getXPathListStringItem( 0, $xpath, "/message[ @key = '$inValue' ]/descendant::node()" );
      $this->gLogger->debug( "Translated value: '$newValue'" );
      return $newValue;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_I18nTransformer</i> class holds the information for the component.
 *
 * The root sitemap needs to have the Gallery Transformer declared as a component:
 *
 * <pre>   &lt;map:transformers default="xslt">
 *          ...
 *          &lt;map:transformer name="i18n" src="resource://lib/transforming/I18nTransformer">
 *             &lt;map:catalogues default="index">
 *                &lt;map:catalogue id="index" name="index" location="context://content/translations"/>
 *                ...
 *             &lt;/map:catalogues>
 *          &lt;map:untranslated-text>untranslated text&lt;/map:untranslated-text>
 *        &lt;/map:transformer>
 *        ...
 *       &lt;/map:transformers></pre>
 * where
 * 
 * <ul>
 *    <li><i>name</i>  - the name of this selector (in this case i18n).</li>
 *    <li><i>src</i>  - the location of the PHP package for this component.</li>
 * </ul>
 * 
 * There is a sub-element, &lt;map:catalogues>, that defines the catalogue (translation)
 * files. In this case a single catalogue file is shown, where
 * 
 * <ul>
 *    <li><i>id</i>  - the id of the catalogue file and its various language files.</li>
 *    <li><i>name</i>  - the name of the catalogue series (they take the form index_xx.xml, where xx is the language code).</li>
 *    <li><i>location</i>  - the location of the translation files for this catalogue.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage transforming
 */
 
class _I18nTransformer extends Transformer {

    /** Logger instance for this class */
   private $gLogger;
   
    /** Untranslated text */
   private $gUntranslatedText;
   
    /** Catalogues for this translator (Class Catalogues) */
   private $gCatalogues;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "I18nTransformer";

      $parameterDOM = new DOMDocument;
      $parameterDOM->appendChild( $parameterDOM->importNode( $inParameterNode, 1 ) );

      $xpath = new domxpath( $parameterDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $catalogues = $xpath->query( "//m:catalogues" );

      $untranslatedNode = $parameterDOM->getElementsByTagName( "untranslated-text" );
      $this->gUntranslatedText = $untranslatedNode->item( 0 )->textContent;
      $this->gLogger->debug( "Untranslated text: '" . $this->gUntranslatedText . "'" );

      try {
         $this->gCatalogues = new Catalogues( $catalogues->item( 0 ) );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getCatalogues()
   {
      return $this->gCatalogues;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getUntranslatedText()
   {
      return $this->gUntranslatedText;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string.
    *
    * @retval the string representation of this class instance.
    */

   public function toString()
   {
      if ( $this->gCatalogues != NULL ) {
         $mess = "   <transformer name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'>\n";
           $mess .= $this->gCatalogues->toString();
         $mess .= "   </transformer>\n";
      } else {
         $mess = "   <transformer name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'/>\n";
      }
      return $mess;
   }

 }
?>