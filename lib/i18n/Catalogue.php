<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage i18n
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * @package paloose
 * @subpackage i18n
 */
 
class Catalogue {

    /** The id of this catalogue (used in XML) */
   private $gId;

    /** The name of this catalogue */
   private $gName;
   
    /** The location of this catalogue */
   private $gLocation;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of the Catalogue.
    *
    * @param $inNode the node that contains the contents of this catalogue element.
    */
    
   public function __construct( DOMNode $inNode )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $catalogueDOM = new DOMDocument;
      $catalogueDOM->appendChild( $catalogueDOM->importNode( $inNode, 1 ) );

      $catalogueNode = $catalogueDOM->getElementsByTagName( "catalogue" );
      $this->gId = $catalogueNode->item( 0 )->getAttribute( "id" );
      $this->gName = $catalogueNode->item( 0 )->getAttribute( "name" );
      $this->gLocation = $catalogueNode->item( 0 )->getAttribute( "location" );

      if ( strlen ( $this->gId ) == 0 ) {
         throw new UserException( "Must have an associated id attribute with catalogue", PalooseException::SITEMAP_I18N_PARSE_ERROR, $catalogueDOM );
      }
      if ( strlen ( $this->gName ) == 0 ) {
         throw new UserException( "Must have an associated name attribute with catalogue", PalooseException::SITEMAP_I18N_PARSE_ERROR, $catalogueDOM );
      }
      if ( strlen ( $this->gLocation ) == 0 ) {
         throw new UserException( "Must have an associated location attribute with catalogue", PalooseException::SITEMAP_I18N_PARSE_ERROR, $catalogueDOM );
      }

      //echo "<pre>" . htmlspecialchars( $catalogueDOM->saveXML() ) . "</pre>";
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getId()
   {
      return $this->gId;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getName()
   {
      return $this->gName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getLocation()
   {
      return $this->gLocation;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function toString()
   {
      $mess = "       <catalogue id='{$this->gId}' name='{$this->gName}' location='{$this->gLocation}'/>\n";
      return $mess;
   }

 }
?>
