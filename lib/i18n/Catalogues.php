<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage i18n
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/i18n/Catalogue.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The Catalogues class maintains a list of Catalogue Class items for the I18nTransformer.
 *
 * @package paloose
 * @subpackage i18n
 */
 
class Catalogues {

    /** The text to be used when cannot translate. */
   private $gUntranslatedText;

    /** The list of the Catalogue entries. */
   private $gCatalogueList;

    /** The name of the default catalogue. */
   private $gDefaultCatalogue;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of the Catalogues.
    *
    * @param inDOM the DOM that contains the contents of this catalogues element.
    */

   public function __construct( DOMNode $inNode )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );

      $cataloguesDOM = new DOMDocument;
      $cataloguesDOM->appendChild( $cataloguesDOM->importNode( $inNode, 1 ) );
      //echo "<pre>" . htmlspecialchars( $cataloguesDOM->saveXML() ) . "</pre>";

      $cataloguesNode = $cataloguesDOM->getElementsByTagName( "catalogues" );
      $this->gDefaultCatalogue = $cataloguesNode->item( 0 )->getAttribute( "default" );
      if ( strlen ( $this->gDefaultCatalogue ) == 0 ) {
         throw new UserException( "Must have an associated default attribute with catalogues", PalooseException::SITEMAP_I18N_PARSE_ERROR, $cataloguesDOM );
      }
      $this->gLogger->debug( "Default catalogue: " . $this->gDefaultCatalogue );

      //Get the list of catalogues
      $xpath = new domxpath( $cataloguesDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $catalogueNodeList = $xpath->query( "//m:catalogue" );
      
      $defaultIdPresent = false;
      $this->gCatalogueList = array();
      try {
         foreach ( $catalogueNodeList as $node ) {
            $catalogue = new Catalogue( $node );
            $id = $catalogue->getId();
            $this->gCatalogueList[ $id ] = $catalogue;
            if ( $this->gDefaultCatalogue == $catalogue->getId() ) $defaultIdPresent = true;
            $this->gLogger->debug( "Added catalogue: [name=" . $catalogue->getId() . "] [id=" . $catalogue->getName() . "][location=" . $catalogue->getLocation() . "]" );
          }
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      
      if ( $defaultIdPresent === false ) {
         throw new UserException( "Default id '{$this->gDefaultCatalogue}' not found in catalogue list", PalooseException::SITEMAP_I18N_PARSE_ERROR, $cataloguesDOM );
      } else {
         $this->gLogger->debug( "Default catalogue '{$this->gDefaultCatalogue}' found" );
      }
      
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getDefaultCatalogue()
   {
      return $this->gDefaultCatalogue;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getCatalogue( $inName )
   {
      if ( isset( $this->gCatalogueList[ $inName ] ) ) {
         return $this->gCatalogueList[ $inName ];
      } else {
         throw new RunException( "Cannot find translation catalogue series id: '$inName'", PalooseException::I18N_CATALOGUE_NOT_FOUND_TYPE );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function toString()
   {
      $mess = "     <catalogues default='{$this->gDefaultCatalogue}'>\n";
      foreach ( $this->gCatalogueList as $catalogueItem ) {
         $mess .= $catalogueItem->toString(); 
      }
      $mess .= "     </catalogues>\n";
      return $mess;
   }

 }
?>
