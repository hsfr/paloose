<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 *   \mainpage Paloose: Cocoon without Java
 *
 *   Paloose is a Cocoon variant written entirely in PHP without Java.
 *   It resulted from scratching a long standing personal itch: that there are very few
 *   ISPs who will support Java/Tomcat for web sites, other than as a very expensive
 *   "professional" addition. Almost all will support PHP5 (sorry, Paloose does not use PHP4)
 *   and so I decided to write my version of a simple, cut-down Cocoon in PHP5.
 *
 *   The code that builds Paloose is a little rough and ready in places. This is my
 *   first PHP program (other than the odd embedded line within HTML) and it shows. I am
 *   more used to program in strongly typed languages having had a lifetime using
 *   Algol, Coral, Algol68, Pascal, Java at al. I have programmed in Perl for many
 *   years and so PHP was not a wholly new experience. I am sure that I have used 10 lines
 *   in places where PHP5 would let me use one; never mind - it suffices, the future will
 *   refine the code.
 *
 * LICENSE
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version 1.19.6-c
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 *
 * For version history see associated VERSION file
 */
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/* Do not change anything in this file. All changes are done in the calling
 * file (paloose-inc.php or equivalent for a Paloose server) in your web site folder.
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/*
 * Set up the logging system outside the classes so that it is available
 * for all of them. Check for the log4php being present. If this is a
 * compact system then it may not be and we do not load it. If it is a
 * compact system then there will be no Logger statements so this check
 * is necessary.
 */

// Keep these two lines the same (braces position important for compact version!)
if ( file_exists( PALOOSE_LOG4PHP_DIRECTORY . "/Logger.php" ) ) {
    include_once( PALOOSE_LOG4PHP_DIRECTORY . "/Logger.php" ); }

Logger::configure( PALOOSE_LOGGER_CONFIGURATION_FILE );
$logger = Logger::getLogger( "main" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The main Paloose class where everything happens.
 *
 * This is run when the http request
 * is redirected here, courtesy of the .htaccess file in the user's site home folder.
 * It is called from the paloose-ini.php file that is in the user's site folder; that
 * paloose-ini.php file remains the only means to configure Paloose.
 *
 * @package paloose
 * @subpackage Paloose
 */

class Paloose {
    
    /** Logger instance for this class */
    private $gLogger;
    
    /** Root sitemap */
    private $gSitemap;
    
    /** Request Parameter Module */
    private $gRequestParameterModule;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * The constructor for the Paloose class.
     *
     * Sets up all the required variables (global and local), initialises
     * the logger.
     */
    
    function __construct()
    {
        $this->gLogger = Logger::getLogger( __CLASS__ );
        
        // Will need this to resolve all pseudo-protocols below
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
        
        // The environment class is a useful means of providing context information and allows this info
        // to be passed simply between classes. It also contains useful utilities for processing these
        // variables. It must come before all other Environment::$configuration stuff.
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/Defaults.php" );
        // A set of common useful methods.
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
        
        $this->setEnvironmentFromConfigurationVariables();
        
        // Prime the Environment::$configuration array with user requirements.
        Environment::$isPalooseClient = ( Environment::$configuration[ 'client' ] == 'yes' );
        
        // Note!!!!! - Do not change this line as version is put in here at build time!
        // The build.properties file contains the version number - change it there!
        //
        // For example
        //    version.major = 1
        //    version.minor = 17
        //    version.patch = 2
        
        Environment::$configuration[ 'palooseVersion' ] = "1.19.6-c";
        
        // Set up the various Modules. These are stored in a single list
        // rather than a lot of separate instances which would each be required to
        // be made global.
        Environment::$modules = array();
        
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/RequestParameterModule.php" );
        $this->gRequestParameterModule = new RequestParameterModule( 'request-param' );
        Environment::$modules[ $this->gRequestParameterModule->getName() ] = $this->gRequestParameterModule;
        
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/FlowModule.php" );
        $flowModule = new FlowModule( 'flow' );
        Environment::$modules[ $flowModule->getName() ] = $flowModule;
        
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/ParameterModule.php" );
        $paramsModule = new ParameterModule( 'params' );
        Environment::$modules[ $paramsModule->getName() ] = $paramsModule;
        
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/CookiesModule.php" );
        $cookiesModule = new CookiesModule( 'cookies' );
        Environment::$modules[ $cookiesModule->getName() ] = $cookiesModule;
        Environment::$gCookiesActive = false; // Don't know whether there is a cookie action pipeline happening yet.
        
        include_once( PALOOSE_LIB_DIRECTORY . "/environment/GlobalModule.php" );
        $globalsModule = new ParameterModule( 'global' );
        Environment::$modules[ $globalsModule->getName() ] = $globalsModule;
        
        // The sitemap is the main repository of all that happens in Paloose.
        // The master sitemap is the root of the stack and sub-sitemaps are pushed
        // onto the sitemap stack as required.
        include_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Sitemap.php" );
        include_once( PALOOSE_LIB_DIRECTORY . "/sitemap/SitemapStack.php" );
        Environment::$sitemapStack = new SitemapStack();
        
        // Create the session handler instance even though we may not need it.
        // We have to set up the session module first.
        if ( Environment::$configuration[ 'sessionsRequired' ] == "yes" ) {
            ini_set( 'arg_separator.output','&amp;' );
            include_once( PALOOSE_LIB_DIRECTORY . "/environment/SessionModule.php" );
            $sessionModule = new SessionModule( 'session' );
            Environment::$modules[ $sessionModule->getName() ] = $sessionModule;
            
            include_once( PALOOSE_LIB_DIRECTORY . "/environment/Sessions.php" );
            Environment::$sessionHandler = new Sessions();
        }
        
        // Each level of match has its own associated variables. These are stored as arrays
        // on the variable stack.
        include_once( PALOOSE_LIB_DIRECTORY . "/sitemap/VariableStack.php" );
        Environment::$variableStack = new VariableStack();
        
        include_once( PALOOSE_LIB_DIRECTORY . "/serialization/OutputStream.php" );
        include_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/PalooseException.php" );
        include_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
        include_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
        include_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );
        include_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/ExitException.php" );
        include_once( PALOOSE_LIB_DIRECTORY . "/error-handling/ErrorPage.php" );
        
        require_once( PALOOSE_LIB_DIRECTORY . "/environment/Browser.php" );
        
        $this->gLogger->debug( '==================================================');
        $this->gLogger->debug( 'Client working: ' . ( ( Environment::$isPalooseClient ) ? "yes" : "no" ) );
        $this->gLogger->debug( 'Paloose directory: ' . PALOOSE_DIRECTORY );
        $this->gLogger->debug( 'Paloose library directory: ' . PALOOSE_LIB_DIRECTORY );
        $this->gLogger->debug( 'log4php directory: ' . PALOOSE_LOG4PHP_DIRECTORY );
        $this->gLogger->debug( 'loggerConfig: ' . PALOOSE_LOGGER_CONFIGURATION_FILE );
        $this->gLogger->debug( "rootSiteMap: " . Environment::$configuration[ 'rootSiteMap' ] );
        
    } // __construct
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * The run method is the root of all of the functionality for Paloose.
     * It finishes setting up all the required variables (global and local), initialises
     * the logger, parses and runs the root sitemap. All exception errors and early exit
     * track back to here.
     *
     * The run method is called from the paloose-ini.php file that sits in the user's
     * web site root. It is called through the following typical code in that file.
     *
     *    ini_set( 'track_errors', '1' );
     *    date_default_timezone_set( defined( 'TIME_ZONE' ) ? TIME_ZONE : "GMT");
     *    require_once( PALOOSE_DIRECTORY . '/lib/Paloose.php' );
     *    $paloose = new Paloose();
     *    $paloose->run( $_SERVER[ 'REQUEST_URI' ], $_SERVER[ 'QUERY_STRING' ] );
     *    unset( $paloose );
     *
     * Do not change this code unless you are absolutely sure you know what you are doing!
     *
     * Typical inputs to the method would be
     *
     *    $inRequestURI : /pp/documentation/XIncludeTransformer.html?paloose-view=transform
     *    $inQueryString : url=documentation/XIncludeTransformer.html&paloose-view=transform
     *
     * @param string $inRequestURI the full URL of the requested page
     * @param string $inQueryString the query string of the requested page as processed by .htaccess file
     */
    
    function run( $inRequestURI, $inQueryString )
    {
        Environment::$gRequestURI = $inRequestURI;
        Environment::$gQueryString = $inQueryString;
        
        $this->gLogger->debug( '--------------------------------------------------------');
        $this->gLogger->debug( 'Raw Request URI: ' . Environment::$gRequestURI );
        $this->gLogger->debug( 'Raw Query string: ' . Environment::$gQueryString );
        
        // Must be done before sessions are set up
        $this->gRequestParameterModule->parseQuery( Environment::$gQueryString );
        
        $rootSitemapDir = Environment::$configuration[ 'siteRootDirectory' ];
        // Remove trailing path separator if required (all directory names are stored without
        // separator, which is put in as required when used).
        if ( substr( $rootSitemapDir, strlen( $rootSitemapDir ) - 1, 1 ) == Environment::getPathSeparator() ) {
            $rootSitemapDir = substr( $rootSitemapDir, 0, strlen( $rootSitemapDir ) - 1 );
        }
        
        // Correct the URI and Query string to that requested. This is done
        // because of the redirect in the .htaccess file. Change that way of
        // doing things and you will have to change this routine.
        if ( Environment::$isPalooseClient ) {
            // If coming from another server then it is a normal URL
            $urlString = Environment::$gRequestURI;
            if ( strpos( $urlString, "?" ) ) {
                $matchableURI = substr( $urlString, 0, strpos( $urlString, "?" ) );
            } else {
                $matchableURI = $urlString;
            }
        } else {
            // Paloose acting as server (using the .htaccess file)
            $matchableURI = Utilities::correctURI();
            $this->gRequestParameterModule = Environment::$modules[ 'request-param' ];
            $this->gRequestParameterModule->add( "resource", $matchableURI );
        }
        $this->gLogger->debug( "Actual URI : " . Environment::$gRequestURI );
        $this->gLogger->debug( "Actual Query string : " . Environment::$gQueryString );
        $this->gLogger->debug( "Matchable URI : " . $matchableURI );
        Environment::$modules[ 'global' ]->add( "query-string", Environment::$gQueryString );
        
        $browser = new Browser();
        $this->gLogger->debug( "User agent string '" . $_SERVER[ 'HTTP_USER_AGENT' ] . "'" );
        $this->gLogger->debug( "Found '" . $browser->getName() . "' browser, version '". $browser->getVersion() . "' using platform '" . $browser->getPlatform() . "'" );
        Environment::$modules[ 'global' ]->add( "browser-user-agent", $browser->getName() );
        
        // All errors and early exit exceptions come back to here
        try {
            // To start with the output stream is the standard one to reply to the client
            ob_start();
            $outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
            
            // Set up the root sitemap, parse it and run it
            Environment::$gRootSitemapDir = $rootSitemapDir;
            $this->gSitemap = new Sitemap( $rootSitemapDir, Environment::$configuration[ 'rootSiteMap' ], NULL, $outputStream );
            Environment::$sitemapStack->push( $this->gSitemap );
            $this->gLogger->debug( "Parse sitemaps ----------------------------------" );
            $this->gSitemap->parse();
            $this->gLogger->debug( "Running sitemaps ----------------------------------" );
            // Not an internal request so mark false
            $this->gSitemap->run( $matchableURI, Environment::$gQueryString, false );
            Environment::$sitemapStack->pop();
        } catch ( ExitException $e ) {
            // It is a little suspect to use exceptions to handle what is really
            // a normal exit (albeit an early exit), however it works quite nicely.
            $this->gLogger->debug( "Clean exit" );
            // Clean exit so finish
        } catch( InternalException $e ) {
            $this->gLogger->fatal( $e->getMessage() );
            $errorPage = new ErrorPage( $e->getMessage(), $e->getCode(), NULL );
            $errorPage->display();
        } catch( UserException $e ) {
            $this->gLogger->fatal( $e->getMessage() );
            $errorPage = new ErrorPage( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            $errorPage->display();
        } catch( RunException $e ) {
            $this->gLogger->fatal( $e->getMessage() );
            $errorPage = new ErrorPage( $e->getMessage(), $e->getCode(), NULL );
            $errorPage->display();
        }
        ob_end_flush();
    } // function run
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set up the environment configuration variables defined in paloose-ini.php
     * or equivalent in the user's web site environment.
     */
    
    private function setEnvironmentFromConfigurationVariables()
    {
        Environment::$configuration[ 'siteRootDirectory' ] = defined( 'PALOOSE_SITE_ROOT_DIRECTORY' ) ? PALOOSE_SITE_ROOT_DIRECTORY : PALOOSE_SITE_ROOT_DIRECTORY_DEFAULT ;
        Environment::$configuration[ 'rootSiteMap' ] = defined( 'PALOOSE_ROOT_SITEMAP' ) ? PALOOSE_ROOT_SITEMAP : PALOOSE_ROOT_SITEMAP_DEFAULT;
        Environment::$configuration[ 'cacheDir' ] = defined( 'PALOOSE_CACHE_DIR' ) ? PALOOSE_CACHE_DIR : PALOOSE_CACHE_DIR_DEFAULT;
        Environment::$configuration[ 'timeZone' ] = defined( 'TIME_ZONE' ) ? TIME_ZONE : TIME_ZONE_DEFAULT ;
        Environment::$configuration[ 'userExceptionPage' ] = defined( 'PALOOSE_USER_EXCEPTION_PAGE' ) ? PALOOSE_USER_EXCEPTION_PAGE : PALOOSE_USER_EXCEPTION_PAGE_DEFAULT;
        Environment::$configuration[ 'userExceptionTransform' ] = defined( 'PALOOSE_USER_EXCEPTION_TRANSFORM' ) ? PALOOSE_USER_EXCEPTION_TRANSFORM : PALOOSE_USER_EXCEPTION_TRANSFORM_DEFAULT;
        Environment::$configuration[ 'userExceptionStyle' ] = defined( 'PALOOSE_USER_EXCEPTION_STYLE' ) ? PALOOSE_USER_EXCEPTION_STYLE : PALOOSE_USER_EXCEPTION_STYLE_DEFAULT;
        Environment::$configuration[ 'internalExceptionPage' ] = defined( 'PALOOSE_INTERNAL_EXCEPTION_PAGE' ) ? PALOOSE_INTERNAL_EXCEPTION_PAGE : PALOOSE_INTERNAL_EXCEPTION_PAGE_DEFAULT;
        Environment::$configuration[ 'internalExceptionTransform' ] = defined( 'PALOOSE_INTERNAL_EXCEPTION_TRANSFORM' ) ? PALOOSE_INTERNAL_EXCEPTION_TRANSFORM : PALOOSE_INTERNAL_EXCEPTION_TRANSFORM_DEFAULT;
        Environment::$configuration[ 'internalExceptionStyle' ] = defined( 'PALOOSE_INTERNAL_EXCEPTION_STYLE' ) ? PALOOSE_INTERNAL_EXCEPTION_STYLE : PALOOSE_INTERNAL_EXCEPTION_STYLE_DEFAULT;
        Environment::$configuration[ 'sitemapNamespace' ] = defined( 'PALOOSE_SITEMAP_NAMESPACE' ) ? PALOOSE_SITEMAP_NAMESPACE : PALOOSE_SITEMAP_NAMESPACE_DEFAULT;
        Environment::$configuration[ 'sourceNamespace' ] = defined( 'PALOOSE_SOURCE_NAMESPACE' ) ? PALOOSE_SOURCE_NAMESPACE : PALOOSE_SOURCE_NAMESPACE_DEFAULT;
        Environment::$configuration[ 'dirNamespace' ] = defined( 'PALOOSE_DIR_NAMESPACE' ) ? PALOOSE_DIR_NAMESPACE : PALOOSE_DIR_NAMESPACE_DEFAULT;
        Environment::$configuration[ 'i18nNamespace' ] = defined( 'PALOOSE_I18N_NAMESPACE' ) ? PALOOSE_I18N_NAMESPACE : PALOOSE_I18N_NAMESPACE_DEFAULT;
        Environment::$configuration[ 'sqlNamespace' ] = defined( 'PALOOSE_SQL_QUERY_NAMESPACE' ) ? PALOOSE_SQL_QUERY_NAMESPACE : PALOOSE_SQL_QUERY_NAMESPACE_DEFAULT;
        Environment::$configuration[ 'pageHitTransform' ] = defined( 'PALOOSE_PAGE_HIT_TRANSFORM' ) ? PALOOSE_PAGE_HIT_TRANSFORM : PALOOSE_PAGE_HIT_TRANSFORM_DEFAULT;
        Environment::$configuration[ 'palooseNamespace' ] = defined( 'PALOOSE_NAMESPACE' ) ? PALOOSE_NAMESPACE : PALOOSE_NAMESPACE_DEFAULT;
        Environment::$configuration[ 'XHTMLNamespace' ] = defined( 'PALOOSE_XHTML_NAMESPACE' ) ? PALOOSE_XHTML_NAMESPACE : PALOOSE_XHTML_NAMESPACE_DEFAULT;
        Environment::$configuration[ 'galleryIndex' ] = defined( 'PALOOSE_GALLERY_INDEX' ) ? PALOOSE_GALLERY_INDEX : PALOOSE_GALLERY_INDEX_DEFAULT;
        Environment::$configuration[ 'ImageMagickBin' ] = defined( 'PALOOSE_IMAGEMAGICK_BIN' ) ? PALOOSE_IMAGEMAGICK_BIN : PALOOSE_IMAGEMAGICK_BIN_DEFAULT;
        Environment::$configuration[ 'sessionsRequired' ] = defined( 'PALOOSE_SESSIONS_REQUIRED' ) ? PALOOSE_SESSIONS_REQUIRED : PALOOSE_SESSIONS_REQUIRED_DEFAULT;
        Environment::$configuration[ 'client' ] = defined( 'PALOOSE_CLIENT' ) ? PALOOSE_CLIENT : PALOOSE_CLIENT_DEFAULT;
    } // function setEnvironment
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * The destructor for the Paloose class.
     */
    
    function __destruct()
    {
        // All done - so clean up and exit
        Logger::shutdown();
    } // function __destruct
    
} // class Paloose

?>
