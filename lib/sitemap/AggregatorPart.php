<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage sitemap
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>AggregatorPart</i> class stores information on each XML sub-document.
 *
 * @package paloose
 * @subpackage sitemap
 */
 
class AggregatorPart {

    /** The source file to aggregate. */
   private $gSrc;

    /** The element within which to build the part */
   private $gElement;
   
    /** Whether to strip the root element from the inputted source (true = strip) */
   private $gStripRoot;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of AggregatorPart.
    *
    * @param DOMNode $inDOM the DOM that contains the contents of this aggregate element.
    */
    
   public function __construct( $inNode )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );

      $this->gSrc = $inNode->getAttribute( "src" );
      if ( strlen ( $this->gSrc ) == 0 ) {
         $dom = new DOMDocument;
         $dom->appendChild( $dom->importNode( $inNode, 1 ) );
         throw new UserException(
            "Must have an associated src attribute with aggregate part",
            PalooseException::SITEMAP_AGGREGATE_PARSE_ERROR,
            $dom );
      }

      $this->gElement = $inNode->getAttribute( "element" );
      $this->gStripRoot = $inNode->getAttribute( "strip-root" );
      if ( strlen( $this->gStripRoot ) > 0 ) {
         $this->gStripRoot = Utilities::normalizeBoolean( $this->gStripRoot );
      }

  }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the src file address which is being aggregated.
    *
    * @retval string the src address.
    */
    
   public function getSrc()
   {
      return $this->gSrc;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the element name to enclose this part.
    *
    * @retval string the element name.
    */
    
   public function getElement()
   {
      return $this->gElement;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the strip root flag.
    *
    * @retval boolean the strip root flag.
    */
    
   public function getStripRoot()
   {
      return $this->gStripRoot;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string.
    *
    * @retval string the string representation of this class instance.
    */

   public function toString()
   {
      $mess = "     <part src='{$this->gSrc}' element='{$this->gElement}' strip-root='{$this->gStripRoot}'/>\n";
      return $mess;
   }

 }
?>
