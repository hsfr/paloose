<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage sitemap
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * Redirect
 *
 * @package paloose
 * @subpackage sitemap
 */

class Redirect extends PipeElement implements PipeElementInterface
{

    /** The uri defines where the redirect goes to. */
   private $gURI;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a new instance of Redirect element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if src attribute note given.
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gGenericType = PipeElement::REDIRECT;
      $this->gSitemapSrc = $inSrc;
      $this->gType = $inType;
      $this->gComponent = $inComponent;

      // Extract the parts to be used from the inDOM
      $xpath = new domxpath( $inDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $this->gURI = Utilities::getXPathListStringItem( 0, $xpath, "//@uri" );

      $this->gLogger->debug( "Creating new redirect [dir=" . Environment::$gCurrentSitemapDir . "][src=" . $this->gSitemapSrc . "][uri=" . $this->gURI . "]" );
       if ( $this->gURI == NULL or strlen( $this->gURI ) == 0 ) {
         throw new UserException(
            "Must have a uri attribute defined",
            PalooseException::SITEMAP_REDIRECT_PARSE_ERROR,
            $inDOM );
       }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Redirect to a new URI.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument The document DOM representing the input.
    * @throws ExitException if an early exit is required
    * @throws UserException if an malformed URI
    * @throws RunException if Illegal return code for string expansion
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      // Now to check whether it is an internal redirect
      $this->gLogger->debug( "Redirect URI: $this->gURI" );
      $expandedURI = StringResolver::removePseudoProtocols( $this->gURI );
      $expandedURI = StringResolver::expandString( $inVariableStack, $expandedURI );
      $this->gLogger->debug( "Expanded URI: $expandedURI" );
      
      $urlComponents = parse_url( $expandedURI );
      if ( $urlComponents === false ) {
         throw new UserException( "Malformed URI '". $expandedURI . "' in redirect", PalooseException::SITEMAP_REDIRECT_PARSE_ERROR );
      } else if ( array_key_exists( "scheme", $urlComponents ) and ( $urlComponents[ 'scheme' ] == "http" or $urlComponents[ 'scheme' ] == "https" ) ) {
        // External link, so clean what we have output already and redirect
        ob_end_clean();
         header( "Location: {$expandedURI}" );
         throw new ExitException();
      }
        
     // Must be an internal affair so we redirect to this sitemap or higher
      $uriSource = Environment::getSource( $this->gURI );
      // If it is a naked uri (no pseudo variables) then assume that it is a resubmission to the root sitemap
      if ( $uriSource == Environment::UNKNOWN_SOURCE ) $uriSource = Environment::SUBMIT_ROOT ;
      switch ( $uriSource ) {

         case Environment::SUBMIT_ROOT :
            $this->gLogger->debug( "Redirecting '$expandedURI' to root sitemap" );
            // Get the appropriate sitemap
            $sitemap = Environment::$sitemapStack->root();
            $sitemap->run( $expandedURI, $inQueryString, true  );  // Internal request so mark true
            break;

         case Environment::SUBMIT_CURRENT :
            $this->gLogger->debug( "Redirecting '$expandedURI' to current sitemap" );
            // Get the appropriate sitemap
            $sitemap = Environment::$sitemapStack->peek();
            $sitemap->run( $expandedURI, $inQueryString, true  );  // Internal request so mark true
            break;

         default :
            throw new RunException(
               "Illegal return code '$uriSource' for string expansion source processing '$this->gURI'",
               PalooseException::UNKNOWN_STRING_EXPANSION_TYPE );

      }
    
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Gets the redirect URI.
    * This will be the unexpanded URI
    *
    * @retval string the string representation of redirect URI.
    */

   public function getURI()
   {
      return $this->gURI;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Gets the class instance represented as a string.
    *
    * @retval string the string representation of this class instance.
    */

   public function toString()
   {
      $mess = "    <redirect uri='{$this->gURI}'/>\n";
      return $mess;
   }

}
?>
