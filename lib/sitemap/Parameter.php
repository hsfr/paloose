<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage sitemap
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * @package paloose
 * @subpackage sitemap
 */

class Parameter {

   /** Associative array of parameters (address by name as key) */
   private $gParameterList;

   /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of Parameter.
    */
    
   public function __construct()
   {
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gParameterList = array();
  }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get parameters as array.
    *
    * @retvals array entire parameter list
    */

   public function getParameterList()
   {
      return $this->gParameterList;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get named parameter from array.
    *
    * @param string $inName name of required parameter
    * @retvals string value if entry exists, otherwise NULL
    */

   public function getParameter( $inName )
   {
      if ( array_key_exists( $inName, $this->gParameterList ) ) {
         return $this->gParameterList[ $inName ];
      }
      return NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set value into named parameter.
    *
    * @param string $inName name of required parameter
    * @param string $inValue value of required parameter
    */

   public function setParameter( $inName, $inValue )
   {
      $this->gParameterList[ $inName ] = $inValue;
      $paramsModule = Environment::$modules[ "params" ];
      $paramsModule->add( $inName, $inValue );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the input string and insert values. Clear first according to flag.
    * The format of the string is the array string representation produced by
    * toArrayString().
    *
    * @retval string the XML string representation of this class instance.
    */

   public function parse( $inString, $inClear = false )
   {
      if ( $inClear ) {
         $this->gParameterList = array();
      }

      $this->gLogger->debug( "Parsing: $inString" );
      $valuesArray = explode( ',', $inString );
      foreach( $valuesArray as $pair ) {
         preg_match( "/\"([^\"]+)\"\=\>\"([^\"]+)\"/", $pair, $match );
         $name = $match[1];
         $value = $match[2];
         if ( strlen( $name ) > 0 ) {
            $this->gLogger->debug( "Pair: {$name} > {$value}" );
            $this->gParameterList[ $name ] = $value;
            $paramsModule = Environment::$modules[ "params" ];
            $paramsModule->add( $name, $value );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return the array as a suitable string for cache file storage.
    *
    * @retval string the string representation of this class instance.
    */

   public function toArrayString()
   {
      $mess = "";
      foreach ( $this->gParameterList as $name => $value ) {
         $mess .= "\"$name\"=>\"$value\",";
      }
      return $mess;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return the array as a suitable string for cache file storage.
    *
    * @retval string the string representation of this class instance.
    */

   public function getHash()
   {
      return md5( $this->toArrayString() );
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string. This is used purely for debugging purposes.
    *
    * @retval string the XML string representation of this class instance.
    */

   public function toString()
   {
      $mess = "";
      foreach ( $this->gParameterList as $name => $value ) {
         $mess .= "<parameter name='{$name}' value='{$value}'/>\n";
      }
      return $mess;
   }

}
?>
