<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage sitemap
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/generation/Generators.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/Transformers.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/Serializers.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/reading/Readers.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/flows/Flow.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/OutputStream.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/Pipelines.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/matching/Matchers.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/selection/Selectors.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/Actions.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/viewing/Views.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/resources/Resources.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/ExitException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The Sitemap class controls how Paloose responds to URL requests and is the heart of Paloose.
 *
 * @package paloose
 * @subpackage sitemap
 */

#[AllowDynamicProperties]
class Sitemap {
    
    /** The filename of the sitemap (usually sitemap.xmap) */
    private $gSitemapFileName;
    
    /** The path of this sitemap */
    private $gSitemapPathName;
    
    /** Holds an instance of the class Generators that stores all info about the generator components */
    public $gGenerators;
    
    /** Holds an instance of the class Transformers that stores all info about the transformer components */
    public $gTransformers;
    
    /** Holds an instance of the class Serializers that stores all info about the serializer components */
    public $gSerializers;
    
    /** Holds an instance of the class Readers that stores all info about the reader components */
    public $gReaders;
    
    /** Holds an instance of the class Selectors that stores all info about the selector components */
    public $gSelectors;
    
    /** Holds an instance of the class Pipelines that stores all info about the pipeline components */
    public $gPipelines;
    
    /** Holds an instance of the class Matchers that stores all info about the matcher components */
    public $gMatchers;
    
    /** Holds an instance of the class Views that stores all info about the views for this sitemap */
    public $gViews;
    
    /** Holds an instance of the class Resources that stores all info about the resource pipelines for this sitemap */
    public $gResources;
    
    /** Holds an instance of the class Actions that stores all info about the actions for this sitemap */
    public $gActions;
    
    /** Holds an instance of the class Flow that stores all the scripts loaded */
    public $gFlow;
    
    /** General Xpath variable for convenience */
    private $gXPath;
    
    /** The output stream for this sitemap action */
    private $gOutputStream;
    
    /** Logger instance for this class */
    private $gLogger;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Construct a new instance of a sitemap.
     *
     * <p>This is used for both the root
     * site map and for sub-sitemaps. Note that the sub-sitemaps do not need to
     * create existing components which have already been declared from a higher
     * sitemap. All we have to do is set the default again if it is given (this is
     * done within the appropriate component).</p>
     *
     * @param string $inSitemapPathName name of sitemap path to use
     * @param string $inSitemapFileName name of sitemap to use
     * @param SitemapStack $inSitemapStack the stack of sitemaps (null when first called at root/master level)
     * @param OutputStream $inOutputStream the current output stream
     * @exception UserException if sitemap does not exist
     */
    
    public function __construct( $inSitemapPathName, $inSitemapFileName, $inSitemapStack, $inOutputStream )
    {
        $this->gLogger = Logger::getLogger( __CLASS__ );
        $this->gSitemapFileName = $inSitemapPathName . Environment::getPathSeparator() . $inSitemapFileName;
        $this->gOutputStream = $inOutputStream;
        $this->gSitemapPathName = $inSitemapPathName;
        
        Environment::$gCurrentSitemapDir = $inSitemapPathName;
        
        if ( !file_exists( $this->gSitemapFileName ) ) {
            // Non existent sitemaps are the user's problem
            throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not exist.", PalooseException::MISSING_SITEMAP, NULL );
        }
        
        $this->gLogger->debug( "Sitemap '" . $this->gSitemapFileName . "' found" );
        if ( $inSitemapStack == NULL ) {
            // Root sitemap so set everything up
            $this->gGenerators = new Generators();
            $this->gTransformers = new Transformers();
            $this->gSerializers = new Serializers();
            $this->gReaders = new Readers();
            $this->gMatchers = new Matchers();
            $this->gPipelines = new Pipelines();
            $this->gSelectors = new Selectors();
            $this->gActions = new Actions();
            $this->gViews = new Views();
            $this->gResources = new Resources();
            $this->gFlow = new Flow();
        } else {
            // Sub sitemap here so we get the already declared components. We can
            // add/override to the list later and reset the defaults also.
            $this->gLogger->debug( "Processing sub-sitemap '" . $this->gSitemapFileName . "'" );
            $callingSitemap = $inSitemapStack->peek();
            $this->gGenerators = $callingSitemap->getGenerators();
            $this->gTransformers = $callingSitemap->getTransformers();
            $this->gSerializers = $callingSitemap->getSerializers();
            $this->gReaders = $callingSitemap->getReaders();
            $this->gMatchers = $callingSitemap->getMatchers();
            $this->gPipelines = $callingSitemap->getPipelines();
            $this->gSelectors = $callingSitemap->getSelectors();
            $this->gActions = $callingSitemap->getActions();
            $this->gViews = new Views();               // Views are local to each sitemap
            $this->gResources = new Resources();       // Resources are local to each sitemap (at present)
            $this->gFlow = new Flow();
        }
        $this->gLogger->debug( "Sitemap '" . $this->gSitemapFileName . "' instance constructed ok." );
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Parse the sitemap instance.
     *
     * @exception UserException if sitemap does not parse
     */
    
    public function parse()
    {
        $this->SitemapDOM = new DOMDocument();
        // Now check for well-formed etc (no check for valid as that will take too long)
        $check = new XML_check();
        if ( !$check->check_url( $this->gSitemapFileName ) ) {
            throw new UserException( "Sitemap error '{$this->gSitemapFileName}'" . "\n" . $check->get_full_error(),
                                    PalooseException::SITEMAP_PARSE_ERROR );
        }
        if ( $this->SitemapDOM->load( $this->gSitemapFileName ) === false ) {
            throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse.", PalooseException::SITEMAP_PARSE_ERROR );
        }
        $this->gLogger->debug( "Sitemap '" . $this->gSitemapFileName . "' loaded ok." );
        // Set up the XPath mechanism together with its namespace
        $this->gXPath = new domxpath( $this->SitemapDOM );
        $this->gXPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
        
        // Now parse the various components of the sitemap. Very important the order in which we do things here.
        // We must do the components before the rest.
        try {
            $this->parseComponents();
            $this->parsePipelines();
        } catch( UserException $e ) {
            throw new UserException( $e->getmessage(), $e->getCode(), $e->getDOMScrap() );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Parse the components section.
     *
     * @throws UserException if parse error occurs with a component.
     */
    
    private function parseComponents()
    {
        try {
            $this->parseGenerators();
            $this->parseTransformers();
            $this->parseSerializers();
            $this->parseReaders();
            $this->parseMatchers();
            $this->parseViews();
            $this->parseSelectors();
            $this->parseActions();
            $this->parseResources();
            $this->parseFlow();
        } catch( UserException $e ) {
            throw new UserException( $e->getmessage(), $e->getCode(), $e->getDOMScrap() );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Sets the output stream to be used.
     *
     * @param OutputStream $inOutStream the output stream to use.
     */
    
    public function setOutputStream( $inOutStream )
    {
        $this->gOutputStream = $inOutStream;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the the output stream in use.
     *
     * @retval OutputStream Current output stream.
     */
    
    public function getOutputStream()
    {
        return $this->gOutputStream;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the current sitemap stack.
     *
     * @retval SitemapStack Current sitemap stack.
     */
    
    public function getSitemapStack()
    {
        return $this->gSitemapStack;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the generators and includes them in an array
     *
     * @throws UserException if sitemap does not parse
     */
    
    private function parseGenerators()
    {
        $this->gLogger->debug( "Parsing generators" );
        $component = $this->gXPath->query( "//m:components/m:generators" );
        
        // Should be only one here
        foreach ( $component as $node ) {
            try {
                $this->gGenerators->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in generator declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
        
        // Finally extract the default, checking for whether it is present as a component
        $root = $this->gXPath->query( "//m:components/m:generators" );
        // Must be a cleaner way to do this.
        foreach ( $root as $node ) {
            $default = $node->getAttribute( "default" );
            try {
                $this->gGenerators->setDefault( $default );
            } catch ( UserException $e ) {
                // Something wrong so display problem and exit
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in generator declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the list of generators.
     *
     * @retval Generators The generators list.
     */
    
    public function getGenerators()
    {
        return $this->gGenerators;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the transformers and includes them in an array
     *
     * @throws UserException if sitemap does not parse
     */
    
    private function parseTransformers()
    {
        $component = $this->gXPath->query( "//m:components/m:transformers" );
        
        // Should be only one here
        foreach ( $component as $node ) {
            try {
                $this->gTransformers->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in transformer declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
        
        // Finally extract the default, checking for whether it is present as a component
        $root = $this->gXPath->query( "//m:components/m:transformers" );
        // Must be a cleaner way to do this.
        foreach ( $root as $node ) {
            $default = $node->getAttribute( "default" );
            try {
                $this->gTransformers->setDefault( $default );
            } catch ( UserException $e ) {
                // Something wrong so display problem and exit
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in transformer declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the list of transformers.
     *
     * @retval Transformers The transformers list.
     */
    
    public function getTransformers()
    {
        return $this->gTransformers;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the serializers and includes them in an array
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parseSerializers()
    {
        $component = $this->gXPath->query( "//m:components/m:serializers" );
        
        foreach ( $component as $node ) {
            try {
                $this->gSerializers->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in serializer declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
        
        // Finally extract the default checking for whether it is present as a component
        $root = $this->gXPath->query( "//m:components/m:serializers" );
        // Must be a cleaner way to do this.
        foreach ( $root as $node ) {
            $default = $node->getAttribute( "default" );
            try {
                $this->gSerializers->setDefault( $default );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in serializer declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the list of serializers.
     *
     * @retval Serializers The serializers list.
     */
    
    public function getSerializers()
    {
        return $this->gSerializers;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the readers and includes them in an array
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parseReaders()
    {
        $component = $this->gXPath->query( "//m:components/m:readers" );
        
        // Should be only one here
        foreach ( $component as $node ) {
            try {
                $this->gReaders->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in readers declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
        
        // Finally extract the default, checking for whether it is present as a component
        $root = $this->gXPath->query( "//m:components/m:readers" );
        // Must be a cleaner way to do this.
        foreach ( $root as $node ) {
            $default = $node->getAttribute( "default" );
            try {
                $this->gReaders->setDefault( $default );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in readers declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the list of readers.
     *
     * @retval Readers The readers list.
     */
    
    public function getReaders()
    {
        return $this->gReaders;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the selectors and includes them in an array
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parseSelectors()
    {
        $component = $this->gXPath->query( "//m:components/m:selectors" );
        
        // Should be only one here
        foreach ( $component as $node ) {
            try {
                $this->gSelectors->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in selectors declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
        
        // Finally extract the default, checking for whether it is present as a component
        $root = $this->gXPath->query( "//m:components/m:selectors" );
        // Must be a cleaner way to do this.
        foreach ( $root as $node ) {
            $default = $node->getAttribute( "default" );
            try {
                $this->gSelectors->setDefault( $default );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in selectors declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get list of selectors for this sitemap.
     *
     * @retval Selectors the selectors list.
     */
    
    public function getSelectors()
    {
        return $this->gSelectors;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the matchers and includes them in an array
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parseMatchers()
    {
        $component = $this->gXPath->query( "//m:components/m:matchers" );
        
        foreach ( $component as $node ) {
            try {
                $this->gMatchers->parse( $node );
            } catch ( UserException $e ) {
                // Something wrong so display problem and exit
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in matchers declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
        
        // Finally extract the default checking for whether it is present as a component
        $root = $this->gXPath->query( "//m:components/m:matchers" );
        // Must be a cleaner way to do this.
        foreach ( $root as $node ) {
            $default = $node->getAttribute( "default" );
            try {
                $this->gMatchers->setDefault( $default );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in matchers declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get list of Matchers for this sitemap.
     *
     * @retval Matchers the matchers list.
     */
    
    public function getMatchers()
    {
        return $this->gMatchers;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the views and includes them in an array
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parseViews()
    {
        $component = $this->gXPath->query( "//m:views" );
        foreach ( $component as $node ) {
            try {
                $this->gViews->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in views declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get list of Views for this sitemap.
     *
     * @retval Views the views list.
     */
    
    public function getViews()
    {
        return $this->gViews;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the resources and includes them in an array
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parseResources()
    {
        $resources = $this->gXPath->query( "//m:resources" );
        foreach ( $resources as $node ) {
            try {
                $this->gResources->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in resources declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the list of resources.
     *
     * @retval Resources The resources list.
     */
    
    public function getResources()
    {
        return $this->gResources;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the pipelines and includes them in an array.
     *
     * Note that this array is
     * indexed sequentially via an integer field. Pipelines are a little different to the
     * rest. There can be several pipelines within a single pipelines tag, and each pipeline
     * definition can consist of one or more match tags.
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parsePipelines()
    {
        $component = $this->gXPath->query( "//m:pipelines" );
        // Should only go around this once ?
        foreach ( $component as $node ) {
            try {
                $this->gPipelines->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in pipelines declaration :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the list of pipelines.
     *
     * @retval Pipelines The pipelines list.
     */
    
    public function getPipelines()
    {
        return $this->gPipelines;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the actions and includes them in an array
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parseActions()
    {
        $component = $this->gXPath->query( "//m:components/m:actions" );
        // Should be only one here
        foreach ( $component as $node ) {
            try {
                $this->gActions->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in actions declaration" . " :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
        // No default so no need to set it up here
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the list of actions.
     *
     * @retval The actions as Actions.
     */
    
    public function getActions()
    {
        return $this->gActions;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes all the Flow scripts
     *
     * @exception UserException if sitemap does not parse
     */
    
    private function parseFlow()
    {
        $component = $this->gXPath->query( "//m:flow" );
        // Should be only one here
        foreach ( $component as $node ) {
            try {
                $this->gFlow->parse( $node );
            } catch ( UserException $e ) {
                throw new UserException( "Sitemap '" . $this->gSitemapFileName . "' does not parse in flow declaration" . " :\n" . $e->getMessage(),
                                        $e->getCode(),
                                        $e->getDOMScrap() );
            }
        } // foreach
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Take the inputted URL and query string and offer it to the various
     * pipelines.
     *
     * @param string $inURL the calling URL
     * @param string $inQueryString the query string associated with this URL
     * @param string $inInternalRequest true if the request comes from another Paloose sitemap
     * @throws ExitException when early success exit occurs
     * @throws UserException when lower level UserException is thrown
     * @throws RunException when lower level RunException is thrown
     */
    
    public function run( $inURL, $inQueryString, $inInternalRequest )
    {
        $this->gLogger->debug( "Running sitemap '" . $this->gSitemapFileName . " [url=$inURL] [query string=$inQueryString]" );
        $this->gLogger->debug( "Output stream type: " . $this->gOutputStream->toString() );
        Environment::$gCurrentSitemapDir = $this->gSitemapPathName;
        
        // Push our instance onto the stack
        Environment::$sitemapStack->push( $this );
        // We offer the arguments to each pipeline (in matcher)
        try {
            $success = $this->gPipelines->run( $inURL, $inQueryString, $inInternalRequest );
            if ( $success === false ) {
                throw new RunException( "URL not found: " . $inURL, "404" );
            }
        } catch ( ExitException $e ) {
            $this->gLogger->debug( "Finished processing: " . $this->gSitemapFileName );
            throw new ExitException();
        } catch ( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        } catch ( RunException $e ) {
            throw new RunException( $e->getMessage(), $e->getCode() );
        }
        // Finally remove this sitemap from the stack
        $this->gLogger->debug( "Removing sitemap from stack: " . $this->gSitemapFileName );
        Environment::$sitemapStack->pop();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the sitemap file name.
     *
     * @retval string The filename.
     */
    
    public function getFilename()
    {
        return $this->gSitemapFileName;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the class instance represented as a string.
     *
     * @retval string The string representation of this class instance.
     */
    
    public function toString()
    {
        $m = "<sitemap>\n";
        $m .= " <components>\n";
        $m .= $this->gGenerators->toString();
        $m .= $this->gTransformers->toString();
        $m .= $this->gSerializers->toString();
        $m .= $this->gReaders->toString();
        $m .= $this->gMatchers->toString();
        $m .= $this->gSelectors->toString();
        $m .= $this->gActions->toString();
        $m .= $this->gFlow->toString();
        $m .= " </components>\n";
        $m .= $this->gResources->toString();
        $m .= $this->gViews->toString();
        $m .= $this->gPipelines->toString();
        $m .= "</sitemap>\n";
        return $m;
    }
    
}

?>
