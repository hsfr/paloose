<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage sitemap
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>Component</i> class provides the base class for all the declared components.
 *
 * It stores all the common data that each component requires: type, name etc.
 *
 * @package paloose
 * @subpackage sitemap
 */
 
#[AllowDynamicProperties]
class Component {

   /** Component type : generator, transformer etc) */
   protected $gType;
   
   /** Component name : "file", etc. */   
   protected $gName;

   /** The package name for this particular component. In Paloose it is really the file name
   (less the extension - always ".php") rather than a true package name as would be in Cocoon.
   Note that the component name associated with this starts with "_". So that the component
   information for the pipeline element FileGenerator is in "_FileGenerator". Messy but it works.*/   
   protected $gPackageName;
   
   /** The parameters and extra information for this particular component. Since components all use separate
      parameters (with a couple of exceptions such as PARAMETER) I have elected to take the entire node into
      a component which then processes what it requires locally. I may change all this later when I can. */   
   protected $gParameterDOM;
   
   /** Parameter list */
   protected $gParameters;

   /** True if request parameters are to be passed into component */
   protected $gUseRequestParameters;
   
   /** boolean cachable flag (accesable from component instances) */
   public $gIsCachable;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the component.
    *
    * @param string $inName the name of this component
    * @param string $inSrc the package name of this component (destination PHP class)
    * @param DOMNode $inNode the node associated with this component (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable?
    */

   public function __construct( $inName, $inSrc, DOMNode $inNode, $inIsCachable )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
       
      $this->gSrc = $inSrc;
      $this->gName = $inName;
      $this->gIsCachable = $inIsCachable;
      $this->gPackageName = $inSrc;      // Should be set higher in actual component code
      $this->gParameters = array();
      $this->gLogger->debug( "Constructing new component '". $inName . "', package '" . $inSrc . "': cachable=" . $this->gIsCachable );
      $this->gUseRequestParameters = false;

      // First thing is make node into a local DOM (this will include an enclosed tags such parameter)
      $this->gParameterDOM = new DOMDocument;
      $this->gParameterDOM->appendChild( $this->gParameterDOM->importNode( $inNode, 1 ) );

        // Set the namespace for the sitemap.
      $xpath = new domxpath( $this->gParameterDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $component = $xpath->query( "//*" );

      foreach ( $component as $node ) {
         if ( $node->localName == "parameter" ) {
            $name = $node->getAttribute( "name" );
            $value = $node->getAttribute( "value" );
            $this->gLogger->debug( "Adding parameter name = '". $name . "', value '" . $value . "'" );
            $this->gParameters[ $name ] = $value;
         } else if ( $node->localName == "use-request-parameters" ) {
            $this->setUseRequestParametersFlag( Utilities::normalizeBoolean( $node->textContent ) );
         }
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get component name.
    *
    * @retval string name of component.
    */

   public function getName()
   {
      return $this->gName;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get DOM of the parameter.
    *
    * @retval DOMDocument parameter DOM.
    */

   public function getParameterDOM()
   {
      return $this->gParameterDOM;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get list of parameters associated with this component.
    *
    * @retval array parameter list.
    */

   public function getParameters()
   {
      return $this->gParameters;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get component type.
    *
    * @retval string type of component.
    */

   public function getType()
   {
      return $this->gType;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set package name of this component.
    *
    * @param string package name of this component.
    */

   public function setPackageFileName( $inName )
   {
      $this->gPackageName = $inName;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the name of the associated component's package (BrowserSelector etc)
    *
    * @retval the name of the package
    */
    
   public function getPackageName()
   {
      return $this->gPackageName;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set the UseRequestParameters flag for this component.
    *
    * @param  boolean $inFlag the flag.
    */

   public function setUseRequestParametersFlag( $inFlag )
   {
      $this->gUseRequestParameters = $inFlag;
      $this->gLogger->debug( "user-request-params: " . $this->gUseRequestParameters );
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the UseRequestParameters flag for this component.
    *
    * @retval boolean the flag.
    */

   public function getUseRequestParametersFlag()
   {
      if ( $this->gUseRequestParameters == NULL ) return false;
      return $this->gUseRequestParameters;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Throw exception if this component is not valid.
    *
    * @throws UserException if component does not have name and package defined.
    */

   public function isValid()
   {
      if ( !( strlen( $this->gName ) > 0 and strlen( $this->gPackageName ) > 0 ) ) {
         throw new UserException(
            "Component must have a name and package",
            PalooseException::SITEMAP_GENERATOR_PARSE_ERROR,
            $this->gParameterDOM );
      }
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string.
    *
    * It is always overridden in derived classes.
    *
    * @retval the string representation of this class instance.
    */

   public function toString()
   {
   }

}

?>
