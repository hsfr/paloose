<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage sitemap
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * Mounting subsitemaps.
 *
 * One of the most useful facilities of the sitemap is the ability to
 * "divide and conquer". It is possible for a sitemap to invoke another "sub"
 * sitemap and transfer control to it. There is a certain amount of inheritance
 * of components so that they do not have to be redefined. Although this does
 * not preclude having extra components defined in the subsitemaps. Subsitemaps
 * allow a hierarchy or tree of sitemaps underneath the master sitemap. The
 * advantage of this is that a site with several different areas can be developed
 * separately using a sitemap for each area, making larger sites easier to
 * maintain. The master sitemap controls which sitemap is used dependent on the
 * request.
 *
 *    <pre>   &lt;map:match pattern="documentation/*.html"> 
 *       &lt;map:mount uri-prefix="documentation" src="documentation/sitemap.xmap"/>
 *   &lt;/map:match></pre>
 * 
 * The attributes are similar to those used in Cocoon: 
 * 
 * <ul>
 *   <li><b>src</b> : the file name os the subsitemap. If src ends is a path separator (for
 *       example "/") then the filename sitemap.xmap will be added, otherwise the
 *       defined filename will be used.</li>
 * 
 * <li><b>uri-prefix</b> : defines the part of the request URI that should be removed
 *       when passing the request into the subsitemap. For example using the mount
 *       element above if the requested URI was "documentation/sitemap.html" then
 *       "documentation/" would be removed from the request passed to the
 *       subsitemap (documentation/sitemap.xmap). Note that the trailing path
 *       separator is removed as well.</li>
 * </ul>
 *
 * @package paloose
 * @subpackage sitemap
 */

class Mount extends PipeElement implements PipeElementInterface
{

    /** The uri-prefix is the part that should be removed from the request URI.
       The engine will correctly check for a trailing slash (which you may write, of course).
       If in the example above "faq/cocoon" is requested, "faq/" is removed from the URI and "cocoon"
       is passed to the sub-sitemap which is loaded from "faq/sitemap.xmap". */
   private $gURIPrefix;

    /** The file name of the sub-sitemap. Relative to current sitemap. */
   private $gSitemapSrc;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Create a new instance of a Mount element.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if src attribute not given.
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gGenericType = PipeElement::MOUNT;
      $this->gSitemapSrc = $inSrc;
      $this->gType = $inType;
      $this->gComponent = $inComponent;

      // Extract the parts to be used from the inDOM
      $xpath = new domxpath( $inDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $this->gURIPrefix = Utilities::getXPathListStringItem( 0, $xpath, "//m:mount/@uri-prefix" );

      // Add trailing separator as required
      if ( $this->gURIPrefix != NULL and 
               strlen( $this->gURIPrefix ) > 0  and
               substr( $this->gURIPrefix, strlen( $this->gURIPrefix ) - 1, 1 ) != Environment::getPathSeparator() ) {
         $this->gURIPrefix .= Environment::getPathSeparator();
      }
      $this->gLogger->debug( "Creating new mount [dir=" . Environment::$gCurrentSitemapDir . "][src=" . $this->gSitemapSrc . "][uri prefix=" . $this->gURIPrefix . "]" );
      if ( $this->gSitemapSrc == NULL or strlen( $this->gSitemapSrc ) == 0 ) {
         throw new UserException( "Must have a src attribute defined",
            PalooseException::SITEMAP_MOUNT_PARSE_ERROR,
            $inDOM );
       }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Mount and run subsitemap.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument The document DOM representing the input.
    * @throws FileGeneratorException if an error occurs
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $subsitemap =  $this->gSitemapSrc;
      if ( ( strrpos( $subsitemap, Environment::getPathSeparator() ) + 1 ) == strlen( $subsitemap ) ) {
         // Trailing path separator
         $subsitemap .= "sitemap.xmap";
      }
      $subsitemap = StringResolver::expandString( $inVariableStack, $subsitemap );
      $this->gLogger->debug( "Result after substitution: '$subsitemap'" );
      $subsitemap = StringResolver::expandPseudoProtocols( $subsitemap );
      $this->gLogger->debug( "Result after pseudo protocol expansion: '$subsitemap'" );
      
      $this->gLogger->debug( "Running mount [URL=$inURL] [Query string=$inQueryString] [subsitemap=$subsitemap] [dir=" . Environment::$gCurrentSitemapDir . "]" );
      // Set up new output stream
      $outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
      try {
         // We have to resolve the directory where the subsitemap sits
         // $sitemapPathFileName = Environment::$gCurrentSitemapDir . Environment::getPathSeparator() . $subsitemap;
         $sitemapPathFileName = $subsitemap;
         $this->gLogger->debug( "sitemapPathFileName: '$sitemapPathFileName'" );
         $sitemapFileName = substr( $sitemapPathFileName, strrpos( $sitemapPathFileName, Environment::getPathSeparator() ) + 1 );
         $sitemapPathName = substr( $sitemapPathFileName, 0, strrpos( $sitemapPathFileName, Environment::getPathSeparator() ) );
      
         // Make a new instance of a sitemap here
         $sitemap = new Sitemap( $sitemapPathName, $sitemapFileName, Environment::$sitemapStack, $outputStream );
         $sitemap->parse();

         // Sort out the uri-prefix stuff
         $this->gLogger->debug( "prefix=". $this->gURIPrefix );
         if ( ( strrpos( $this->gURIPrefix, Environment::getPathSeparator() ) + 1 ) == strlen( $this->gURIPrefix ) ) {
            // Already a trailing path separator
            $uriPrefix = $this->gURIPrefix;
         } else {
            $uriPrefix = $this->gURIPrefix . Environment::getPathSeparator();
         }
         $url = $inURL;
         $this->gLogger->debug( "url='$url'" );
         $this->gLogger->debug( "prefix='". $this->gURIPrefix . "'" );

         // Now we have to strip the prefix if it exists
         if ( $this->gURIPrefix != NULL and strlen( $this->gURIPrefix ) > 0 ) {
            if ( strpos( $url, $this->gURIPrefix, 0 ) == 0 ) {
               $url = substr( $url, strlen( $this->gURIPrefix ) );
            }
         }
         $this->gLogger->debug( "url='$url'" );
         $this->gLogger->debug( "Transfering control to subsitemap ... '$sitemapPathFileName'" );
         // Mounting sitemaps can always be considered to come from external stimuli (might need changing later)
         // $this->gLogger->debug( "Globals: " . Environment::$modules[ 'global' ]->toString() );
         $sitemap->run( $url, $inQueryString, false );
         // Will never reach here as  there is no return from subsitemap
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string.
    *
    * @retval string the string representation of this class instance.
    */

   public function toString()
   {
      $mess = "    <mount src='{$this->gSitemapSrc}' package='" . get_class( $this ) . "'/>\n";
      return $mess;
   }

}
?>
