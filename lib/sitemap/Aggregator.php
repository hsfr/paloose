<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage sitemap
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/AggregatorPart.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * Aggregators are predefined generators that combine scraps of XML into a
 * single file.
 *
 * For example the sitemap might contain
 *
 * <pre>&lt;map:aggregate element="root" label="content">
 *    &lt;map:part src="context://content/menu.xml"/>
 *    &lt;map:part src="context://content/locales.xml"/>
 *    &lt;map:part src="context://content/{../0}"/>
 * &lt;/map:aggregate></pre>
 *
 * @package paloose
 * @subpackage sitemap
 */

#[AllowDynamicProperties]
class Aggregator extends PipeElement implements PipeElementInterface
{

    /** The root element within which to build the XML */
   private $gElement;

    /** The array of Parts for this aggregator (list of class AggregatorPart) */
   private $gParts;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The aggregator builds a new XML file from scraps of other XML files. They are
    * all contained within a single root (inElement).
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if there is no associated element attribute with aggregate
    * @throws UserException if AggregatorPart throws error
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {

      
      parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gGenericType = PipeElement::AGGREGATOR;

      $aggrNodeList = $inDOM->getElementsByTagName( "aggregate" );
      $this->gElement = $aggrNodeList->item( 0 )->getAttribute( "element" );
      if ( strlen ( $this->gElement ) == 0 ) {
         throw new UserException(
            "Must have an associated element attribute with aggregate",
            PalooseException::SITEMAP_AGGREGATE_PARSE_ERROR,
            $inDOM );
      }

      // Extract the label (in the inDOM document)
      $xpath = new domxpath( $inDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      // $this->gLabel = Utilities::getXPathListStringItem( 0, $xpath, "//@label" );

      // Extract the parts to be used from the inDOM
      $xpath = new domxpath( $inDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $part = $xpath->query( "//m:part" );
      $idx = 0;
      $this->gPartSrc = NULL;
      foreach ( $part as $node ) {
         try {
            $part = new AggregatorPart( $node );
            $this->gParts[ $idx++ ] = $part;
         } catch ( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The aggregator reads a set of XML files and returns a DOM of
    * the aggregated. Note that this pipeline instance is not cachable.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument The document DOM representing the input.
    * @throws UserException if could not load scrap file
    * @throws RunException could not add to root document
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Running Aggregator" );
      // First make a new document with a root of the appropriate type
      $documentDOM = new DomDocument();
      $root = $documentDOM->createElement( $this->gElement );
      $root = $documentDOM->appendChild( $root );
      
      $validAggregate = CacheFile::CACHE_VALID;
      $aggregateHash = "";
      
      // Get the various parts and bind them into one DOM
      for ( $i = 0; $i < count( $this->gParts ); $i++ ) {
         $part = $this->gParts[ $i ];
         $rawString = $part->getSrc();
         $aggregateHash = md5( $aggregateHash . $rawString );
         $this->gLogger->debug( "Src string '$rawString'" );
         // Wrinkle here is to decide whether this is a straight read from a file
         // or a resubmit to the appropriate sitemap (root or current). In that case we
         // have to be careful to adjust output stream temporarily.
         $stringExpansionCode = Environment::getSource( $rawString );
         switch ( $stringExpansionCode ) {

            case Environment::FILE_READ :
               $expandedString = StringResolver::removePseudoProtocols( $rawString );
               $expandedString = StringResolver::expandString( $inVariableStack, $expandedString );
               $this->gLogger->debug( "Getting scrap from '$expandedString'" );
               $scrapDOM = new DomDocument();
               if ( !$scrapDOM->load( $expandedString ) ) {
                  throw new RunException( "Could not load scrap from file '$expandedString'" );
               }
               break;

            case Environment::SUBMIT_ROOT :
               $expandedString = StringResolver::removePseudoProtocols( $rawString );
               $expandedString = StringResolver::expandString( $inVariableStack, $expandedString );
               $this->gLogger->debug( "Submitting '$expandedString' to root sitemap" );
               // Set up a new outputstream that uses an internal buffer
               $outputStream = new OutputStream( OutputStream::BUFFER );
               // Get the appropriate sitemap
               $sitemap = Environment::$sitemapStack->root();
               $oldOutputStream = $sitemap->getOutputStream();
               $sitemap->setOutputStream( $outputStream );
               // Internal request so mark true
               $sitemap->run( $expandedString, $inQueryString, true );
               
               // Pick up the contents of the pipeline buffer
               $scrapDOM = new DomDocument();
               if ( $scrapDOM->loadXML( $outputStream->getBuffer() ) === false ) {
                  throw new UserException( "Cannot load XML stream", PalooseException::SITEMAP_AGGREGATE_PARSE_ERROR );
               }
               // restore original output stream
               $sitemap->setOutputStream( $oldOutputStream );
               break;

            case Environment::SUBMIT_CURRENT :
               $expandedString = StringResolver::removePseudoProtocols( $rawString );
               $this->gLogger->debug( "Remove pseudo: '$expandedString'" );
               $expandedString = StringResolver::expandString( $inVariableStack, $expandedString );
               $this->gLogger->debug( "Submitting '$expandedString' to current sitemap" );
               // Set up a new outputstream that uses an internal buffer
               $outputStream = new OutputStream( OutputStream::BUFFER );
               // Get the appropriate sitemap
               $sitemap = Environment::$sitemapStack->peek();
               $oldOutputStream = $sitemap->getOutputStream();
               $sitemap->setOutputStream( $outputStream );
               // Internal request so mark true
               $sitemap->run( $expandedString, $inQueryString, true );

               // Pick up the contents of the pipeline buffer
               $scrapDOM = new DomDocument();
               if ( $scrapDOM->loadXML( $outputStream->getBuffer() ) === false ) {
                  throw new UserException( "Cannot load XML stream (no root tag in part?)", PalooseException::SITEMAP_AGGREGATE_RUN_ERROR );
               }
               // restore original output stream
               $sitemap->setOutputStream( $oldOutputStream );
               break;

            default :
               throw new RunException( "Unknown return code '$stringExpansionCode' for string expansion source processing '$rawString'", PalooseException::UNKNOWN_STRING_EXPANSION_TYPE );

         }

         // First we see whether we have to remove the root of the included XML
         if ( $scrapDOM->documentElement->getAttribute( "__status" ) == CacheFile::CACHE_INVALID ) {
            $validAggregate = CacheFile::CACHE_INVALID;
         }

         $xpath = new domxpath( $scrapDOM );
         if ( $part->getStripRoot() ) {
             // Remove the root node from the included document
            $nodeList = $xpath->query( "/*/*" );
         } else {
             // Extract the root node from the included document
            $nodeList = $xpath->query( "/*" );
         }

         $dom = new DomDocument();
         // Next wrap the nodes in an element if there is one. This is a bit of a fudge
         // as the element is not there nothing will be added. Bit crude but works :-)
         $elementNode = $dom->createElement( $part->getElement() );
         $dom->appendChild( $elementNode );
         // Now add the node list above
         foreach ( $nodeList as $node ) {
            $newNode = $dom->importNode( $node, true );
            $elementNode->appendChild( $newNode );
         }

         // Add the nodes as children of the root node
         try {
             $xpath = new domxpath( $dom );
            $nodeList = $xpath->query( "/*" );
            foreach ( $nodeList as $node ) {
               $newNode = $documentDOM->importNode( $node, true );
               $root->appendChild( $newNode );
            }
         } catch ( DOMException $e ) {
            throw new RunException( $e->getMessage(), $e->getCode );
         }
       
      } // end for
            
      $documentDOM->documentElement->setAttribute( "__status", $validAggregate );
      $documentDOM->documentElement->setAttribute( "__file", $aggregateHash );
      // Utilities::outputStringVerbatim( $documentDOM->saveXML() );
      return $documentDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string.
    *
    * @retval the string representation of this class instance.
    */
   public function toString()
   {
      if ( $this->gParts != NULL ) {
         $mess = "    <aggregate element='{$this->gElement}'>\n";
         foreach ( $this->gParts as $node ) {
            $mess .= $node->toString();
         }
         $mess .= "    </aggregate>\n";
      }
      return $mess;
   }

}
?>
