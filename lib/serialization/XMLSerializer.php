<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage serialization
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/OutputStream.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/OutputStream.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/SerializerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>XMLSerializer</i> outputs the XML within the pipeline DOM directly.
 * 
 * This is of particular use in the aggregation process when a document is
 * made up of several XML parts. As an example consider this from the Paloose
 * site content sitemap:
 *
 *   <pre>   &lt;map:components>
 *      ...
 *      &lt;map:serializers default="xml">
 *         &lt;map:serializer name="xhtml" src="resource://lib/serialization/XHTMLSerializer">
 *            &lt;doctype-public>-//W3C//DTD XHTML 1.0 Strict//EN&lt;/doctype-public>
 *            &lt;doctype-system>http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd&lt;/doctype-system>
 *            &lt;encoding>iso-8859-1&lt;/encoding>
 *         &lt;/map:serializer>
 *         &lt;map:serializer name="xml" src="resource://lib/serialization/XMLSerializer"/>
 *      &lt;/map:serializers>
 *         
 *   &lt;/map:components>
 *      
 *   &lt;map:pipelines>
 *      &lt;map:pipeline>
 *      
 *          &lt;map:match pattern="**.html">
 *            &lt;map:aggregate element="root" label="aggr-content">
 *               &lt;map:part src="cocoon:/menus.xml" element="menus" strip-root="true"/>
 *               ...
 *            &lt;/map:aggregate>
 *            ...
 *            &lt;map:serialize type="xhtml" />
 *         &lt;/map:match>
 *         
 *         &lt;map:match pattern="menus.xml">
 *            &lt;map:generate src="context://content/menus.xml" label="menus-content"/>
 *            &lt;map:transform type="i18n">
 *               &lt;map:parameter name="default-catalogue-id" value="menus"/>
 *            &lt;/map:transform>
 *            &lt;map:serialize/>
 *         &lt;/map:match>
 *
 *   &lt;/map:pipelines></pre>
 *
 * The output menus matcher would give the following into the aggregator:
 *
 *   <pre>   &lt;?xml version="1.0"?>
 *   &lt;page:page xmlns:link="http://www.hsfr.org.uk/Schema/Link"
 *      xmlns:list="http://www.hsfr.org.uk/Schema/List" 
 *      ... >
 *      ...
 *   &lt;/page:page></pre>
 *
 * @package paloose
 * @subpackage serialization
 */

class XMLSerializer extends SerializerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Create an instance of XMLSerializer.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * The XML serializer takes a DOM and outputs it to the output stream.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM tp output.
    * @retval NULL.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Running XMLSerializer with stream type: " . Environment::$sitemapStack->peek()->getOutputStream()->getType() );
      // Should be UTF-8 here
      if ( $this->gComponent->gOmitXMLDeclaration ) {
         // this will also output doctype and comments at top level
         foreach( $inDOM->childNodes as $node )
            $normalisedOutput .= $inDOM->saveXML( $node )."\n";
      } else {
         $normalisedOutput = $inDOM->saveXML();
      }
      if ( $this->gComponent->gEncoding != "UTF-8" ) {
         $normalisedOutput = Utilities::utf8ToUnicodeEntities( $normalisedOutput );
      }
      $this->gLogger->debug( $normalisedOutput );
      Environment::$sitemapStack->peek()->getOutputStream()->out( $normalisedOutput );
      return NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage serialization
 */
 
class _XMLSerializer extends Serializer {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "XMLSerializer";
   }

}
?>
