<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage serializers
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/Serializer.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Serializers</i> maintains a list of the serializers.
 * 
 * @package paloose
 * @subpackage serializers
 */

class Serializers {

   private $gDefault;
   
   /** Array of serializers (each entry a class: Serializer) and indexed by name */
   private $gSerializers;

   /** The DOM object for this class */
   private $gDOM;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gSerializers = NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the contained serializer tags.
    *
    * For example 
    *
    * <pre>   &lt;map:serializers default="xml">
    *     &lt;map:serializer name="html" src="resource://lib/serialization/HTMLSerializer">
    *       &lt;doctype-public>-//W3C//DTD HTML 4.01 Transitional//EN&lt;/doctype-public>
    *       &lt;doctype-system>http://www.w3.org/TR/html4/loose.dtd&lt;/doctype-system>
    *       &lt;encoding>iso-8859-1&lt;/encoding>
    *     &lt;/map:serializer>
    *     &lt;map:serializer name="xhtml" src="resource://lib/serialization/XHTMLSerializer">
    *       &lt;doctype-public>-//W3C//DTD XHTML 1.0 Strict//EN&lt;/doctype-public>
    *       &lt;doctype-system>http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd&lt;/doctype-system>
    *       &lt;encoding>iso-8859-1&lt;/encoding>
    *     &lt;/map:serializer>
    *     &lt;map:serializer name="text" src="resource://lib/serialization/TextSerializer"/>
    *     &lt;map:serializer name="xml" src="resource://lib/serialization/XMLSerializer"/>
    *   &lt;/map:serializers></pre>
    *
    * The default attribute specifies the type of serializer to use if none is specified in a pipeline.
    *
    * @param DOMNode $inNode the DOM that contains the contents of this pipe element.
    * @throws UserException if component does not have <i>name</i> and <i>src</i> attributes
    * @throws UserException if component constructor throws UserException
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $this->gDOM = new DOMDocument;
      $this->gDOM->appendChild( $this->gDOM->importNode( $inNode, 1 ) );
   
      // Set the namespace for the sitemap. Let us hope people use it.
      $xPath = new domxpath( $this->gDOM );
      $xPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $component = $xPath->query( "//m:serializer" );
      // Scan around each serializer and set up the list in $gSerializers. Exit false if problem
      foreach ( $component as $node ) { 
         $name = $node->getAttribute( "name" );
         $src = $node->getAttribute( "src" );
         $isCachable = Utilities::normalizeBoolean( $node->getAttribute( "cachable" ) );
         $this->gLogger->debug( "Adding serializer name = '". $name . "' using src '" . $src . "'" );
         if ( !strlen( $name ) or !strlen( $src ) ) {
            throw new UserException(
               "Sitemap component serializer must have name and src attributes defined",
               PalooseException::SITEMAP_SERIALIZER_PARSE_ERROR, 
               $this->gDOM );
         }
         
         //The source may have path details attached (for example "../lib/FileGenerator"). We need to
         // strip the path off to input the correct class.
         if ( strstr( $src, Environment::getPathSeparator() ) == 1 ) {
            $packageFilename = $src;
         } else if ( !strstr( $src, ":" . Environment::getPathSeparator() ) ) {
            // Relative file name so process
            $packageFilename = PALOOSE_LIB_DIRECTORY . Environment::getPathSeparator() . $src;
         } else {
            // Relative file name with pseudo protocol
            $packageFilename = StringResolver::expandPseudoProtocols( $src );
         }
         
         $package = Utilities::stripFileName( $packageFilename );
         $path = Utilities::stripPath( StringResolver::expandPseudoProtocols( $packageFilename ) );
         $this->gLogger->debug( "Package='". $package . "' : path='" . $path . "'" );
         
         // We have something to add, so first of all check for duplicate names
         if ( $this->gSerializers and array_key_exists( $name, $this->gSerializers ) ) {
            $this->gLogger->debug( "Overriding serializer: '". $name . "'" );
         } else {
            $this->gLogger->debug( "Adding serializer: '". $name . "'" );
         }
         // Get the necessary for forming a specific serializer
         require_once( $path . $package. ".php" );
         // Form up the component class name
         $serializerName = "_" . $package;
         $serializer = new $serializerName( $name, $path.$package, $node, $isCachable );
         // Check that the serializer thinks it is valid
         try {
            $serializer->isValid();
            $this->gSerializers[ $name ] = $serializer;
         } catch( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $this->gDOM );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set the default serializer. Will cause exception if not found in list.
    *
    * @param inDefault the default serializer name.
    * @exception UserException if cannot find serializer
    */

   public function setDefault( $inDefault )
   {
      if ( !array_key_exists( $inDefault, $this->gSerializers ) ) {
           throw new UserException(
              "Default serializer '". $inDefault . "' not found",
              PalooseException::SITEMAP_SERIALIZER_PARSE_ERROR,
              $this->gDOM );
      }
      $this->gDefault = $inDefault;
      $this->gLogger->debug( "Set default serializer: '". $inDefault . "'" );
      return true;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getDefault()
   {
      return $this->gDefault;
   }

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the named serializer. Will cause exception if not found in list.
    *
    * @param inName the default serializer type.
    * @retval the serializer instance.
    * @exception UserException cannot find serializer.
    */

   public function getSerializer( $inType )
   {
      if ( !( array_key_exists( $inType, $this->gSerializers ) ) ) {
           throw new UserException(
              "Serializer '". $inType . "' not found",
              PalooseException::SITEMAP_SERIALIZER_PARSE_ERROR,
              $this->gDOM );
      } else {
         return $this->gSerializers[ $inType ];
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class of the named serializer. Will cause exception if not found in list.
    *
    * @param inName the default serializer type.
    * @retval the serializer instance class name.
    * @exception UserException cannot find serializer.
    */

   public function getSerializerClassName( $inType )
   {
      if ( !( array_key_exists( $inType, $this->gSerializers ) ) ) {
           throw new UserException(
              "Serializer class '". $inType . "' not found",
              PalooseException::SITEMAP_SERIALIZER_PARSE_ERROR,
              $this->gDOM );
      } else {
         return $this->gSerializers[ $inType ]->getPackageName();
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getCount()
   {
      return count( $this->gSerializers );
   }

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function toString()
   {
      $mess = "  <serializers default='{$this->gDefault}'>\n";
      foreach ( $this->gSerializers as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "  </serializers>\n";
      return $mess;
   }

}

?>