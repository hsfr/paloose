<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage serialization
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/OutputStream.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/SerializerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>XHTMLSerializer</i> are similar to the HTMLSerializer except that
 * they produce valid XHTML as an XML document.
 * 
 * For example:
 *
 *<pre>   &lt;map:components>
 *      ...
 *      &lt;map:serializers default="xml">
 *        &lt;map:serializer name="xhtml" mime-type="text/html" src="resource://lib/serialization/XHTMLSerializer">
 *           &lt;map:property name="doctype-public" value="-//W3C//DTD XHTML 1.0 Strict//EN"/>
 *           &lt;map:property name="doctype-system" value="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>
 *           &lt;map:property name="encoding" value="iso-8859-1&lt"/>
 *        &lt;/map:serializer>
 *     &lt;/map:serializers>
 *        
 *  &lt;/map:components>
 *     
 *  &lt;map:pipelines>
 *     &lt;map:pipeline>
 *     
 *        &lt;map:match pattern="*.html">
 *           &lt;!-- generate -->
 *           &lt;!-- some transformations -->
 *           &lt;map:serialize type="html"/>
 *        &lt;/map:match>
 *     &lt;/map:pipeline>
 *  &lt;/map:pipelines></pre>
 *
 * @package paloose
 * @subpackage serialization
 */

class XHTMLSerializer extends SerializerPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Create an instance of XHTMLSerializer.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The XHTML serializer takes a DOM and outputs to the output stream.
    *
    * We assume
    * the DOM contains an HTML formatted document here.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM tp output.
    * @retval NULL.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Running XHTMLSerializer with stream type: " . Environment::$sitemapStack->peek()->getOutputStream()->getType() );
      // Utilities::outputStringVerbatim( $inDOM->saveXML() );

      // I am not proud of what follows, there must be a better way of doing all this. I do a lot of the
      // XHTML/XML stuff by hand so that I have greater control over the output. Primarily because some ISP's
      // configurations are not happy with the leading XML declaration etc.
      
      // First make up a framework HTML document.
      if ( strlen( $this->gComponent->gPublicDoctype ) > 0 ) {
 	    $domImpl = new DOMImplementation();
         $doctype = $domImpl->createDocumentType(
            "html",
            $this->gComponent->gPublicDoctype,
            $this->gComponent->gSystemDoctype );
         $dom = $domImpl->createDocument( null, 'html', $doctype );
         $this->gLogger->debug( "Public doctype found" );
      } else {
         $dom = new DOMDocument();
         $html = $dom->createElement( "html" );
         $dom->appendChild( $html );
         $this->gLogger->debug( "No public doctype" );
      }

      $root = $dom->getElementsByTagName( "html" )->item( 0 );
      // $root->setAttribute( "xmlns", Environment::$configuration[ 'XHTMLNamespace' ] );

      // Create the head element
      $head = $dom->createElement( "head" );
      $root->appendChild( $head );
      
      // Add the encoding data if required
      if ( strlen( $this->gComponent->gEncoding ) > 0 ) {
         $meta = $dom->createElement( "meta" );
         $meta->setAttribute( "http-equiv", "Content-Type" );
         $meta->setAttribute( "content", "text/html; charset=" . $this->gComponent->gEncoding );
         $head->appendChild( $meta );
         $dom->encoding = $this->gComponent->gEncoding;
      }
      
      $meta = $dom->createElement( "meta" );
      $meta->setAttribute( "name", "Generator" );
      $meta->setAttribute( "content", "Paloose Version " . Environment::$configuration[ 'palooseVersion' ] );
      $head->appendChild( $meta );
      
      $meta = $dom->createElement( "meta" );
      $meta->setAttribute( "name", "PHP" );
      $meta->setAttribute( "content", "PHP Version " . phpversion() );
      $head->appendChild( $meta );
      
      $xPath = new domxpath( $inDOM );
      // Add the attributes from the root html tag
      $docRootList = $xPath->query( "/*/@*" );
      if ( $docRootList->length > 0 ) {
         // There are some html attribute to add
        foreach ( $docRootList as $attribNode ) {
           $name = $attribNode->nodeName;
           $value = $attribNode->nodeValue;
           $root->setAttribute( $name, $value );
        }
      }
     
      // Now to add the contents of the input document. First of all get the head information.
      $docHeadList = $xPath->query( "/*[local-name() = 'html']/*[local-name() = 'head']/*" );
      if ( $docHeadList->length > 0 ) {
         // There are some head tags to add
         foreach ( $docHeadList as $headNode ) {
            $newNode = $dom->importNode( $headNode, true );
            $head->appendChild( $newNode );
         }
      }
      
      // $dom should now be a mixture of the pipeline DOM and the new meta data (encoding) so
      // now we add the body from the pipeline document
      $xPath = new domxpath( $inDOM );
      $docBody = $xPath->query( "/*[local-name() = 'html']/*[local-name() = 'body']" );
      // Check that we already have a body in the new document
      if ( $docBody->length > 0 ) {
         // There is a body to add (always should have)
         $body = $dom->createElement( "body" );
         $root->appendChild( $head );
         $bodyNode = $docBody->item( 0 );
         $newNode = $dom->importNode( $bodyNode, true );
         $root->appendChild( $newNode );
      } else {
         // Do nothing as it may be required to have no body
      }
      
      // Finally remove the Paloose attributes
      $root->removeAttribute( "__status" );
      $root->removeAttribute( "__file" );
      
      // After all that rather revolting code we have a new HTML document which we can output
      $outputString = $dom->saveXML(  );
      if ( $this->gComponent->gEncoding != "UTF-8" ) {
         $normalisedOutput = Utilities::utf8ToUnicodeEntities( $outputString );
      }
      // Before we output the html data update the cookies.

      if ( Environment::$gCookiesActive ) {
         $this->gLogger->debug( "Outputting cookies" );
         Environment::$modules[ 'cookies' ]->outputCookies();
      }

      $this->gLogger->debug( "Assembled output data, length: " . strlen( $outputString ) );
      $this->gLogger->debug( "CDATA tag: '" . $this->gComponent->gCdataEncode . "'" );

      $outputString = Utilities::cdataEncode( $outputString, $this->gComponent->gCdataEncode );

      $this->gLogger->debug( $outputString );

      Environment::$sitemapStack->peek()->getOutputStream()->out( $outputString );
      Environment::$sitemapStack->peek()->getOutputStream()->clearBuffer();
      $this->gLogger->debug( "Finished outputting: " . Environment::$sitemapStack->peek()->getOutputStream()->getType() );
      return NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 * 
 * There will be only
 * one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage serialization
 */
 
class _XHTMLSerializer extends Serializer {

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "XHTMLSerializer";
   }

}
?>
