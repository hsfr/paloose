<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage serialization
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2022 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/OutputStream.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/serialization/SerializerPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>HTMLSerializer</i> takes an XML stream (assumed to be valid HTML) and
 * outputs it to the client.
 * 
 * A typical use of HTMLSerializer would be
 *
 *  <pre>   &lt;map:components>
 *     ...
 *     &lt;map:serializers default="xml">
 *        &lt;map:serializer name="html" mime-type="text/html" src="resource://lib/serialization/HTMLSerializer">
 *           &lt;doctype-public>-//W3C//DTD HTML 4.01 Transitional//EN&lt;/doctype-public>
 *           &lt;doctype-system>http://www.w3.org/TR/html4/loose.dtd&lt;/doctype-system>
 *           &lt;encoding>iso-8859-1&lt;/encoding>
 *        &lt;/map:serializer>
 *     &lt;/map:serializers>
 *
 *  &lt;/map:components>
 *        
 *  &lt;map:pipelines>
 *     &lt;map:pipeline>
 *
 *         &lt;map:match pattern="*.html">
 *           &lt;!-- generate -->
 *           &lt;!-- some transformations -->
 *           &lt;map:serialize type="html"/>
 *        &lt;/map:match>
 *     &lt;/map:pipeline>
 *  &lt;/map:pipelines></pre>
 *
 * Given that the last transformer gave HTML as an output the HTML serializer
 * would produce the following typical output:
 *
 *  <pre>   &lt;!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 *  &lt;html>
 *     &lt;head>
 *        &lt;meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 *        &lt;!-- Remaining head tags from DOM -->
 *     &lt;/head>
 *     &lt;body>
 *        &lt;!-- Remaining body tags from DOM -->
 *     &lt;/body>
 *  &lt;/html></pre>
 *
 * @package paloose
 * @subpackage serialization
 */

class HTMLSerializer extends SerializerPipeElement implements PipeElementInterface
{
    
    /** Logger instance for this class */   
    private $gLogger;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
    /**
     * Create an instance of HTMLSerializer.
     *
     * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
     * @param string $inType the type of this pipe element
     * @param string $inSrc the src attribute (or package required in this case)
     * @param Component $inComponent the associated component instance (stores parameters etc)
     */
    
    public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
    {
        try {
            parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
        $this->gLogger = Logger::getLogger( __CLASS__ );
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
    /**
     * Run the serializer on pipeline DOM.
     *
     * The HTML serializer takes a DOM and outputs to the output stream. We assume
     * the DOM contains an HTML formatted document here.
     *
     * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
     * @param string $inURL the URL that triggered this run.
     * @param string $inQueryString the associated query string.
     * @param DOMDocument $inDOM the pipeline DOM tp output.
     * @retval NULL.
     */
    
    public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
    {
        $this->gLogger->debug( "Running HTMLSerializer with stream type: " . Environment::$sitemapStack->peek()->getOutputStream()->getType() );
        
        if ( strlen( $this->gComponent->gPublicDoctype ) > 0 ) {
            $domImpl = new DOMImplementation();
            $doctype = $domImpl->createDocumentType(
                                                    "html",
                                                    $this->gComponent->gPublicDoctype,
                                                    $this->gComponent->gSystemDoctype );
            $dom = $domImpl->createDocument( null, 'html', $doctype );
        } else {
            $dom = new DOMDocument();
            $html = $dom->createElement( "html" );
            $dom->appendChild( $html );
        }
        $root = $dom->getElementsByTagName( "html" )->item( 0 );
        $dom->formatOutput = true;
        
        // Create the head element
        $head = $dom->createElement( "head" );
        $root->appendChild( $head );
        
        // Add the encoding data if required
        if ( strlen( $this->gComponent->gEncoding ) > 0 ) {
            $meta = $dom->createElement( "meta" );
            $meta->setAttribute( "http-equiv", "Content-Type" );
            $meta->setAttribute( "content", "text/html; charset=" . $this->gComponent->gEncoding );
            $head->appendChild( $meta );
        }
        
        $meta = $dom->createElement( "meta" );
        $meta->setAttribute( "name", "Generator" );
        $meta->setAttribute( "content", "Paloose Version " . Environment::$configuration[ 'palooseVersion' ] );
        $head->appendChild( $meta );
        
        $meta = $dom->createElement( "meta" );
        $meta->setAttribute( "name", "PHP" );
        $meta->setAttribute( "content", "PHP Version " . phpversion() );
        $head->appendChild( $meta );
        
        // Now to add the contents of the input document. First of all get the head information.
        $xPath = new domxpath( $inDOM );
        $docHeadList = $xPath->query( "/*[local-name() = 'html']/*[local-name() = 'head']" );
        if ( $docHeadList->length > 0 ) {
            // There are some head tags to add
            foreach ( $docHeadList as $headNode ) {
                $newNode = $dom->importNode( $headNode, true );
                $head->appendChild( $newNode );
            }
        }
        
        // $dom should now be a mixture of the pipeline DOM and the new meta data (encoding) so
        // now we add the body from the pipeline document
        $xPath = new domxpath( $inDOM );
        $docBody = $xPath->query( "/*[local-name() = 'html']/*[local-name() = 'body']" );
        // Check that we already have a body in the new document
        if ( $docBody->length > 0 ) {
            // There is a body to add (always should have)
            $body = $dom->createElement( "body" );
            $root->appendChild( $head );
            $bodyNode = $docBody->item( 0 );
            $newNode = $dom->importNode( $bodyNode, true );
            $root->appendChild( $newNode );
        } else {
            // Do nothing as it may be required to have no body
        }
        
        // Finally remove the Palose attributes
        $root->removeAttribute( "__status" );
        $root->removeAttribute( "__file" );
        
        // After all that rather revolting code we have a new HTML document which we can output
        $outputString = $dom->saveXML();
        if ( $this->gComponent->gEncoding != "UTF-8" ) {
            $normalisedOutput = Utilities::utf8ToUnicodeEntities( $outputString );
        }
        
        // Before we output the html data update hte cookies.
        if ( Environment::$gCookiesActive ) {
            $this->gLogger->debug( "Outputting cookies" );
            Environment::$modules[ 'cookies' ]->outputCookies();
        }
        
        $this->gLogger->debug( "Assembled output data: " . strlen( $outputString ) );
        
        $outputString = Utilities::cdataEncode( $outputString, $this->gComponent->gCdataEncode );
        
        Environment::$sitemapStack->peek()->getOutputStream()->out( $outputString );
        Environment::$sitemapStack->peek()->getOutputStream()->clearBuffer();
        $this->gLogger->debug( "Finished outputting: " . Environment::$sitemapStack->peek()->getOutputStream()->getType() );
        return NULL;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
    /**
     * Get a string representation of this PipeElement.
     *
     * @retval string the representation of the element as a string
     *
     */
    
    public function toString()
    {
        return parent::toStringWithType( $this->gGenericType );
    }
    
}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 * 
 * There will be only
 * one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage serialization
 */

class _HTMLSerializer extends Serializer {
    
    /** Logger instance for this class */
    private $gLogger;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
    /**
     * Construct an instance of this component.
     *
     * Only package name is set here, all else is done in parent class. The <i>name</i> and
     * <i>src</i> are taken from the sitemap component declaration.
     *
     * @param string $inName the name of this transformer
     * @param string $inSrc the package name of this transformer (destination PHP class)
     * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
     * @param boolean $inIsCachable is this component cachable
     */
    
    public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
    {
        
        
        parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
        $this->gLogger = Logger::getLogger( __CLASS__ );
        $this->gPackageName = "HTMLSerializer";
    }
    
}
?>
