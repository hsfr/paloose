<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage serializers
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Component.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Serializer</i> is the base class for all serializers.
 *
 * @package paloose
 * @subpackage serializers
 */

class Serializer extends Component {
   
   /** Logger instance for this class */
   private $gLogger;
   
   /** The encoding of the character set */
   public $gEncoding;
   
   /** Specifies the public identifier to be used in the document type declaration (only in HTML and XHTML serializers). */
   public $gPublicDoctype;
   
   /** Specifies the system identifier to be used in the document type declaration (only in HTML and XHTML serializers). */
   public $gSystemDoctype;
   
   /** True (yes) if no XML declaration required (only in XML, HTML and XHTML serializers). */
   public $gOmitXMLDeclaration;
   
   /** Set to the element name enclosing text to be enclosed by <![CDATA[ ... ]]/> */
   public $gCdataEncode = "";
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * @param string $inName the name of this transformer
    * @param string $inClassName the package name of this transformer (destination PHP class)
    * @param DOMNode $inNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */
   
   public function __construct( $inName, $inClassName, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inClassName, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gGenericType = PipeElement::SERIALIZER;
      $this->gType = "serializer";
      
      $parameterDOM = new DOMDocument;
      $parameterDOM->appendChild( $parameterDOM->importNode( $inParameterNode, 1 ) );
      
      $xpath = new domxpath( $parameterDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      
      $this->gSystemDoctype = Utilities::getXPathListStringItem( 0, $xpath, "//*[local-name()='property' and @name = 'doctype-system']/@value" );
      $this->gLogger->debug( "doctype-system: '" . $this->gSystemDoctype . "'" );
      
      $this->gPublicDoctype = Utilities::getXPathListStringItem( 0, $xpath, "//*[local-name()='property' and @name = 'doctype-public']/@value" );
      $this->gLogger->debug( "doctype-public: '" . $this->gPublicDoctype . "'" );
      
      $omitEncoding = Utilities::getXPathListStringItem( 0, $xpath, "//*[local-name()='property' and @name = 'omit-xml-declaration']/@value" );
      $this->gOmitXMLDeclaration = false;
      if ( strlen( $omitEncoding ) > 0 && $omitEncoding == "yes" ) {
         $this->gOmitXMLDeclaration = true;
      }
      $this->gLogger->debug( "doctype-public: '" . $this->gOmitXMLDeclaration . "'" );
      
      $this->gEncoding = Utilities::getXPathListStringItem( 0, $xpath, "//*[local-name()='property' and @name = 'encoding']/@value" );
      if ( strlen( $this->gEncoding ) == 0 ) {
         $this->gEncoding = "UTF-8";
      }
      $this->gLogger->debug( "encoding: '" . $this->gEncoding . "'" );
      
      $this->gCdataEncode = Utilities::getXPathListStringItem( 0, $xpath, "//*[local-name()='property' and @name = 'cdata-encode']/@value" );
      $this->gLogger->debug( "cdata-encode: '" . $this->gCdataEncode . "'" );
      
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   public function isValid()
   {
      try {
         parent::isValid();
      } catch ( UserException $e ) {
         throw new UserException( "Serializer: " . $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   public function toString()
   {
      if ( $this->gParameters != NULL ) {
         $mess = "   <serializer name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'>\n";
         while ( list( $name, $value ) = each( $this->gParameters ) ) {
            $mess .= "    <parameter name='$name' value='$value'/>\n";
         }
         reset( $this->gParameters ); //Insurance
         $mess .= "   </serializer>\n";
      } else {
         $mess = "   <serializer name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'/>\n";
      }
      return $mess;
   }
   
}

?>
