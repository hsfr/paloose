<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage serialization
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The OutputStream class control where output goes.
 *
 * At present this is either
 * directly to the browser (via echo etc.) or to a String buffer which is fetched
 * later. I confess this is all rather crude and is ripe for sorting.
 *
 * @package paloose
 * @subpackage serialization
 */

class OutputStream {

    /** "Byte bucket" */
    const NULL = -1;
    /** Output (echo) */
    const STANDARD_OUTPUT = 1;
    /** Local DOM */
    const BUFFER = 2;

    /** Logger instance for this class */
   private $gLogger;

    /** A buffer to hold the output */
   private $gOutputBuffer;

    /** A buffer to hold the output when a client */
   private $gClientBuffer;

    /** Which output stream */
   private $gOutputType;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make an instance of the appropriate output stream of type (inType).
    *
    * @param inType The output stream type.
    */

   public function __construct( $inType )
   {
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gOutputType = $inType;
       switch ( $this->gOutputType ) {
         case self::NULL :
            $this->gLogger->debug( "Creating: NULL" );
            break;
         case self::STANDARD_OUTPUT :
            $this->gLogger->debug( "Creating: STANDARD_OUTPUT" );
            break;
         case self::BUFFER :
            $this->gLogger->debug( "Creating: BUFFER" );
            break;
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function out( $inString )
   {
      switch ( $this->gOutputType ) {
         case self::NULL :
            $this->gLogger->debug( "Current output to: NULL" );
            break;
         case self::STANDARD_OUTPUT :
            $this->gLogger->debug( "Current output to: STANDARD_OUTPUT" );
            if ( Environment::$isPalooseClient ) {
               Environment::$gClientBuffer = $inString;
            } else {
               $this->gLogger->debug( "Output: $inString" );
               print $inString;
            }
            break;
         case self::BUFFER :
            $this->gLogger->debug( "Current output to: BUFFER" );
            $this->gOutputBuffer .= $inString;
            break;
      }
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function clearBuffer()
   {
      $this->gOutputBuffer = "";
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getBuffer()
   {
      return $this->gOutputBuffer;
   } 

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getClientBuffer()
   {
            $this->gLogger->debug( "Getting client output: " . $this->gClientBuffer );
      return $this->gClientBuffer;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getType()
   {
      return $this->gOutputType;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function toString()
   {
      switch ( $this->gOutputType ) {
         case self::NULL :
            return "NULL";
            break;
         case self::STANDARD_OUTPUT :
            return "STANDARD_OUTPUT";
            break;
         case self::BUFFER :
            return "BUFFER";
            break;
         case self::CLIENT_OUTPUT :
            return "CLIENT_OUTPUT";
            break;
      }
   } 

}
?>
