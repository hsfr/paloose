<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage scss
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2020 Hugh Field-Richards
 *
 * Large parts inspired and stolen from work by Leaf Corcoran <leafot@gmail.com>
 * and remains under the same copyright.
 *
 * My grateful thanks to him. In order to use this transform the SCSSPHP plugin should be
 * installed in the paloose/plugins folder.
 *
 * CAVEAT
 * ======
 *
 * The class "OutputStyle" should be moved from the SCSSPHP::OutputStyle.php file and
 * placed into the Compiler.php file. It should cure a strange error where the class
 * was not being found by the PHP compiler. The "use",statement for OutputStyle.php
 * should be removed also. This has been reported!
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/caching/CacheFile.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/TransformerPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/../plugins/scssphp/scss.inc.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/SCSSException.php" );

use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\OutputStyle;
use ScssPhp\ScssPhp\Version;

define( "PALOOSE_SCSS_SRC_ATTRIB", "src" );
define( "PALOOSE_SCSS_DST_ATTRIB", "dst" );
define( "PALOOSE_SCSS_CACHE", "scss_cache" );
define( "PALOOSE_SCSS_COMPACT_ATTRIB", "compact" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>SCSSCompiler</i> transformer. This is not really a transformer like the
 * others but it sits in the pipeline as a transformer and thus is convenient to
 * label it so.
 *
 * @package paloose
 * @subpackage scss
 */

class SCSSCompiler extends TransformerPipeElement implements PipeElementInterface
{
    
    /** Logger instance for this class */
    private $gLogger;
    
    /** Where the CSS files will go */
    private $gCSSFolder;
    
    /** Where the SCSS files will be sourced */
    private $gSCSSFolder;
    
    /** Where the SCSS cache files will be held */
    private $gCacheFolder;
    
    /** Does compiler output compact code */
    private $gIsCompact;
    
    /** SCSS compiler instance */
    public $gSCSS;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Construct an instance of the <i>SCSSCompiler</i> pipeline element.
     *
     * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
     * @param string $inType the type of this pipe element
     * @param string $inSrc the src attribute (or package required in this case)
     * @param _SCSSCompiler $inComponent the associated component instance (stores parameters etc)
     * @throws UserException if no <i>src</i> attribute
     * @throws UserException if parameters do not have <i>name</i> and <i>value</i> attributes
     */
    
    public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
    {
        $gSCSS = new Compiler();
        try {
            parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
            
            $this->gLogger = Logger::getLogger( __CLASS__ );
            
            $this->gCSSFolder = $inComponent->gDefaultCSSFolder;
            $this->gSCSSFolder = $inComponent->gDefaultSCSSFolder;
            $this->gCacheFolder = $inComponent->gCacheFolder;
            $this->gIsCompact = $inComponent->gIsCompact;
            
            $this->gLogger->debug( "Constructing cachable transformer: [{$this->gComponent->gIsCachable}]" );
            $this->gLogger->debug( $this->gIsCompact ? "compact : Yes" : "compact : No" );
            $this->gLogger->debug( "src     : '" . $this->gSCSSFolder . "'" );
            $this->gLogger->debug( "dst     : '" . $this->gCSSFolder . "'" );
            $this->gLogger->debug( "cache   : '" . $this->gCacheFolder . "'" );
            
            $this->gSCSS = $inComponent->gSCSS;
            
        } catch ( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
        
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Process the document.
     *
     * The XSL Transformer processes the input DOM. If there are no errors it returns the transformed
     * DOM. All errors are caught by exception.
     *
     * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
     * @param string $inURL the URL that triggered this run.
     * @param string $inQueryString the associated query string.
     * @param DOMDocument $inDOM the pipeline DOM to transform.
     * @return DOMDocument the transformed DOM.
     * @throws UserException if cannot find the XSL transformation file.
     * @throws InternalException if problem setting parameters.
     */
    
    public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
    {
        parent::run( $inVariableStack, $inURL, $inQueryString, $inDOM );
        
        // Input possible changes to the default values
        
        $parameters = [];
        if ( !empty( $this->gParameters->getParameterList() ) ) {
            $parameterList = $this->gParameters->getParameterList();
            foreach ( $parameterList as $name => $value ) {
                $expandedValue = StringResolver::expandPseudoProtocols( StringResolver::expandString( $inVariableStack, $value ) );
                $parameters[ $name ] =  $value;
                $this->gLogger->debug( "Setting parameter [$name] = [$expandedValue]" );
            }
        }
        
        if ( isset( $parameters[ PALOOSE_SCSS_SRC_ATTRIB ] ) && strlen( $parameters[ PALOOSE_SCSS_SRC_ATTRIB ] ) > 0 ) {
            $this->gSCSSFolder = StringResolver::expandPseudoProtocols( $parameters[ PALOOSE_SCSS_SRC_ATTRIB ] );
        }
        if ( isset( $parameters[ PALOOSE_SCSS_DST_ATTRIB ] ) && strlen( $parameters[ PALOOSE_SCSS_DST_ATTRIB ] ) > 0 ) {
            $this->gCSSFolder = StringResolver::expandPseudoProtocols( $parameters[ PALOOSE_SCSS_DST_ATTRIB ] );
        }
        
        // We cannot assume the SCSS exists because this may be the root theme (no parent)
        if ( !file_exists( $this->gSCSSFolder ) ) {
            return $inDOM;
        }
        
        if ( !file_exists( $this->gCSSFolder ) ) {
            mkdir( $this->gCSSFolder, 0767 );
        }
        
        $this->gCacheFolder = Utilities::join( $this->gSCSSFolder, PALOOSE_SCSS_CACHE );
        if ( !file_exists( $this->gCacheFolder ) ) {
            mkdir( $this->gCacheFolder, 0767 );
        }
        
        $this->gLogger->debug( "Compiling CSS from SCSS" );
        $newDocument = $this->serve( $inDOM );
        return $newDocument;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * @param DOMDocument $inDOM the pipeline DOM to transform.
     * @retval DOMDocument the transformed DOM (identical to input unless error).
     *
     * Compile requested SCSS into CSS folder.  Outputs HTTP response.
     */
    
    public function serve( DOMDocument $inDOM )
    {
        
        $fileList = $this->findFiles();
        
        foreach ( $fileList as $file ) {
            $this->gLogger->debug( "Scanning: $file" );
            
            $output = $this->cacheName( $file );
            $this->gLogger->debug( "cache file: $output" );
            
            $cssFileName = $this->gCSSFolder . Environment::getPathSeparator() . UTILITIES::stripExtension( $file ) . ".css";
            $this->gLogger->debug( "CSS file: $cssFileName" );
            
            if ( $this->getIfNoneMatchHeader() == null ) {
                $etag = $noneMatch = $this->getIfNoneMatchHeader();
            } else {
                $etag = $noneMatch = trim( $this->getIfNoneMatchHeader(), '"' );
            }
            $this->gLogger->debug( "etag: $etag" );
            
            $this->gLogger->debug( "Needs compiling" );
            if ( $this->needsCompile( $output, $etag ) || !file_exists( $cssFileName ) ) {
                try {
                    list( $css, $etag ) = $this->compile( UTILITIES::join( $this->gSCSSFolder, $file ), $output );
                    $lastModified = gmdate('r', filemtime( $output ) );
                    // At this point we have a compiled file in the cache which must be copied
                    // to the CSS folder. $css is the text of the file so we can just output
                    // that to the folder.
                    $this->gLogger->debug( "Updating CSS file: $cssFileName" );
                    file_put_contents( $cssFileName, $css ) ;
                } catch ( SCSSException $e ) {
                    // Add error details into the DOM as a child after the <body>. Slightly scruffy way
                    // to do this but it works.
                    $data = $inDOM->saveXML() . "\n";
                    $bodyPattern = "/<body([^>]*)>/i";
                    // We can aford to lose the attributes for body here
                    $errorData = "<body>\n" . $this->createErrorBlock( $e );
                    $inDOM->loadXML( preg_replace( $bodyPattern, $errorData, $data ) );
                }
            }
        }
        return $inDOM;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Compile .scss file
     *
     * @param string $in  Input path (.scss)
     * @param string $out Output path (.css)
     *
     * @return array
     *
     * @throws SCSSException
     */
    
    protected function compile( $in, $out )
    {
        $this->gLogger->debug( "Compile input: $in" );
        $this->gLogger->debug( "Compile output: $out" );
        $start = microtime( true );
        try {
            $cssResult = $this->gSCSS->compileString( file_get_contents( $in ), $in );
            $returnedCSString = $cssResult->getCss();
            $elapsed = round( ( microtime( true ) - $start ), 4 );
            
            $v    = Version::VERSION;
            $t    = gmdate( 'r' );
            $css  = "/* compiled within Paloose by scssphp $v on $t ({$elapsed}s) */\n\n" . $returnedCSString;
            $etag = md5( "" );
            
            file_put_contents( $out, $css );
            file_put_contents(
                              $this->metadataName( $out ),
                              serialize([
                                        'etag'    => $etag,
                                        'imports' => $cssResult->getIncludedFiles(),
                                        'vars'    => crc32(serialize($this->gSCSS->getVariables())),
                                        ])
                              );
            
            return [ $css, $etag ];
        } catch ( Exception $e ) {
            throw new SCSSException( 'Compile error ' . $e->getMessage(), PalooseException::SCSS_COMPILE_ERROR );
        }
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Format error as a pseudo-element in CSS
     *
     * @param SCSSException $error
     *
     * @return string
     */
    
    protected function createErrorBlock( SCSSException $error )
    {
        return "<div style='background: red; color: white; width: 500px; font-size: 14px; display: block !important; font-family: mono; padding: 1em;'>\n" .
        $error->getMessage(). "</div>";
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get path to cached .css file
     *
     * @return string
     */
    
    protected function cacheName( $fname )
    {
        return Utilities::join( $this->gCacheFolder, md5( $fname ) . '.css' );
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get path to meta data
     *
     * @return string
     */
    
    protected function metadataName( $out )
    {
        return $out . '.meta';
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Determine whether .scss file needs to be re-compiled.
     *
     * @param string $out Output path
     * @param string $etag ETag
     *
     * @return boolean True if compile required.
     */
    
    protected function needsCompile( $out, &$etag )
    {
        if ( !is_file( $out ) ) {
            return true;
        }
        
        $mtime = filemtime( $out );
        $metadataName = $this->metadataName( $out );
        if ( is_readable( $metadataName ) ) {
            $metadata = unserialize( file_get_contents( $metadataName ) );
            
            foreach ( $metadata['imports'] as $import => $originalMtime ) {
                if ( file_exists( $import ) ) {
                    $currentMtime = filemtime( $import );
                    if ( $currentMtime !== $originalMtime || $currentMtime > $mtime ) {
                        return true;
                    }
                }
            }
            
            $metaVars = crc32( serialize( $this->gSCSS->getVariables() ) );
            if ( $metaVars !== $metadata['vars'] ) {
                return true;
            }
            $etag = $metadata['etag'];
            return false;
        }
        return true;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get If-Modified-Since header from client request
     *
     * @return string|null
     */
    
    protected function getIfModifiedSinceHeader()
    {
        $modifiedSince = null;
        if ( isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) ) {
            $modifiedSince = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
            if ( false !== ( $semicolonPos = strpos( $modifiedSince, ';') ) ) {
                $modifiedSince = substr( $modifiedSince, 0, $semicolonPos );
            }
        }
        return $modifiedSince;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get If-None-Match header from client request
     *
     * @return string|null
     */
    
    protected function getIfNoneMatchHeader()
    {
        $noneMatch = null;
        if ( isset( $_SERVER['HTTP_IF_NONE_MATCH'] ) ) {
            $noneMatch = $_SERVER['HTTP_IF_NONE_MATCH'];
        }
        return $noneMatch;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Compile .scss file
     *
     * @param string $in  Input file (.scss)
     * @param string $out Output file (.css) optional
     *
     * @return string|bool
     *
     * @throws SCSSException
     */
    
    public function compileFile($in, $out = null)
    {
        if ( !is_readable( $in ) ) {
            throw new SCSSException( 'load error: failed to find ' . $in, SCSS_COMPILE_ERROR );
        }
        
        $pi = pathinfo( $in );
        $this->scss->addImportPath( $pi['dirname'] . '/' );
        $compiled = $this->gSCSS->compile( file_get_contents( $in ), $in );
        
        if ( $out !== null ) {
            return file_put_contents( $out, $compiled );
        }
        
        return $compiled;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Check if file need compiling
     *
     * @param string $in  Input file (.scss)
     * @param string $out Output file (.css)
     *
     * @return bool
     */
    
    public function checkedCompile( $in, $out )
    {
        if ( !is_file( $out ) || filemtime( $in ) > filemtime( $out ) ) {
            $this->compileFile( $in, $out );
            return true;
        }
        return false;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get name of requested .scss file
     *
     * @return string|null
     */
    
    protected function inputName()
    {
        $this->gLogger->debug("");
        switch ( true ) {
            case isset( $_GET['p'] ):
                $this->gLogger->debug( "1" . $_GET['p'] );
                return $_GET['p'];
                
            case isset( $_SERVER['PATH_INFO'] ):
                $this->gLogger->debug( "2" . $_SERVER['PATH_INFO'] );
                return $_SERVER['PATH_INFO'];
                
            case isset( $_SERVER['DOCUMENT_URI'] ):
                $this->gLogger->debug( "3" . $_SERVER['DOCUMENT_URI'] );
                return substr( $_SERVER['DOCUMENT_URI'], strlen($_SERVER['SCRIPT_NAME'] ) );
        }
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get path to requested .scss file
     *
     * @return array of files to process
     */
    
    protected function findFiles()
    {
        $this->gLogger->debug("");
        
        $fileList = [];
        $this->gLogger->debug( "Scanning: [$this->gSCSSFolder]" );
        $rawFileList = array_diff( scandir( $this->gSCSSFolder ), array('..', '.') );
        
        foreach ( $rawFileList as $file ) {
            $this->gLogger->debug( "Finding files: [$file]" );
            if ( substr( $file, 0, 1 ) !== "_"  && substr( $file, -5 ) === '.scss' ) {
                $fullFileName = UTILITIES::join( $this->gSCSSFolder, $file );
                $this->gLogger->debug( "Valid full file name: [$fullFileName]" );
                if ( is_file( $fullFileName ) && is_readable( $fullFileName ) ) {
                    $this->gLogger->debug( "Readable file: [$file]" );
                    array_push( $fileList, $file );
                }
            }
        }
        return $fileList;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Based on explicit input/output files does a full change check on cache before compiling.
     *
     * @param string  $in
     * @param string  $out
     * @param boolean $force
     *
     * @return string Compiled CSS results
     *
     * @throws SCSSException
     */
    
    public function checkedCachedCompile($in, $out, $force = false)
    {
        if ( !is_file($in) || ! is_readable($in)) {
            throw new SCSSException( "Invalid or unreadable input file specified.", PalooseException.SCSS_FILE_NOT_FOUND_ERROR );
        }
        
        if (is_dir($out) || ! is_writable(file_exists($out) ? $out : dirname($out))) {
            throw new SCSSException( "Invalid or unwritable output file specified.", PalooseException.SCSS_FILE_NOT_FOUND_ERROR );
        }
        
        if ($force || $this->needsCompile($out, $etag)) {
            list($css, $etag) = $this->compile($in, $out);
        } else {
            $css = file_get_contents($out);
        }
        
        return $css;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Execute scssphp on a .scss file or a scssphp cache structure
     *
     * The scssphp cache structure contains information about a specific
     * scss file having been parsed. It can be used as a hint for future
     * calls to determine whether or not a rebuild is required.
     *
     * The cache structure contains two important keys that may be used
     * externally:
     *
     * compiled: The final compiled CSS
     * updated: The time (in seconds) the CSS was last compiled
     *
     * The cache structure is a plain-ol' PHP associative array and can
     * be serialized and unserialized without a hitch.
     *
     * @param mixed   $in    Input
     * @param boolean $force Force rebuild?
     *
     * @return array scssphp cache structure
     */
    
    public function cachedCompile($in, $force = false)
    {
        // assume no root
        $root = null;
        
        if (is_string($in)) {
            $root = $in;
        } elseif (is_array($in) and isset($in['root'])) {
            if ($force or ! isset($in['files'])) {
                // If we are forcing a recompile or if for some reason the
                // structure does not contain any file information we should
                // specify the root to trigger a rebuild.
                $root = $in['root'];
            } elseif (isset($in['files']) and is_array($in['files'])) {
                foreach ($in['files'] as $fname => $ftime) {
                    if (! file_exists($fname) or filemtime($fname) > $ftime) {
                        // One of the files we knew about previously has changed
                        // so we should look at our incoming root again.
                        $root = $in['root'];
                        break;
                    }
                }
            }
        } else {
            return null;
        }
        
        if ($root !== null) {
            // If we have a root value which means we should rebuild.
            $out = [];
            $out['root'] = $root;
            $out['compiled'] = $this->compileFile($root);
            $out['files'] = $this->gSCSS->getParsedFiles();
            $out['updated'] = time();
            return $out;
        } else {
            // No changes, pass back the structure
            // we were given initially.
            return $in;
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get a string representation of this PipeElement.
     *
     * @retval string the representation of the element as a string
     *
     */
    
    public function toString()
    {
        return parent::toStringWithType( $this->gGenericType );
    }
    
}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>_SCSSCompiler</i> holds the information for the component.
 *
 * @package paloose
 * @subpackage scss
 */

class _SCSSCompiler extends Transformer {
    
    /** Logger instance for this class */
    private $gLogger;
    
    /** Where the CSS files will go ($cacheDir)*/
    public $gDefaultCSSFolder;
    
    /** Where the SCSS files will be sourced ($dir)*/
    public $gDefaultSCSSFolder;
    
    /** Where server for converting */
    public $gSCSSserver;
    
    /** Does compiler output compact code */
    public $gIsCompact;
    
    /** SCSS compiler instance */
    public $gSCSS;
    
    /** SCSS OutputStyle instance */
    public $gOutputStyle;
    
    /** Cachge fiolder for compiling CSS */
    public $gCacheFolder;
    
    /** @var boolean */
    public $gShowErrorsAsCSS;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Construct an instance of this component.
     *
     * Only package name is set here, all else is done in parent class. The <i>name</i> and
     * <i>src</i> are taken from the sitemap component declaration.
     *
     * @param string $inName the name of this transformer
     * @param string $inSrc the package name of this transformer (destination PHP class)
     * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
     * @param boolean $inIsCachable is this component cachable
     */
    
    public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
    {
        parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
        
        $this->gLogger = Logger::getLogger( __CLASS__ );
        $this->gPackageName = "SCSSCompiler";
        
        $this->gDefaultSCSSFolder = "context://resources/scss";
        $this->gDefaultCSSFolder = "context://resources/styles";
        $this->gIsCompact = false;
        
        if ( isset( $this->gParameters[ PALOOSE_SCSS_SRC_ATTRIB ] ) && strlen( $this->gParameters[ PALOOSE_SCSS_SRC_ATTRIB ] ) > 0 ) {
            $this->gDefaultSCSSFolder = StringResolver::expandPseudoProtocols( $this->gParameters[ PALOOSE_SCSS_SRC_ATTRIB ] );
        }
        
        if ( isset( $this->gParameters[ PALOOSE_SCSS_DST_ATTRIB ] ) && strlen( $this->gParameters[ PALOOSE_SCSS_DST_ATTRIB ] ) > 0 ) {
            $this->gDefaultCSSFolder = StringResolver::expandPseudoProtocols( $this->gParameters[ PALOOSE_SCSS_DST_ATTRIB ] );
        }
        
        if ( isset( $this->gParameters[ PALOOSE_SCSS_COMPACT_ATTRIB ] ) && strlen( $this->gParameters[ PALOOSE_SCSS_COMPACT_ATTRIB ] ) > 0 ) {
            $this->gIsCompact = UTILITIES::normalizeBoolean( $this->gParameters[ PALOOSE_SCSS_COMPACT_ATTRIB ] );
        }
        
        $this->gDefaultSCSSFolder = StringResolver::expandPseudoProtocols( $this->gDefaultSCSSFolder );
        $this->gDefaultCSSFolder = StringResolver::expandPseudoProtocols( $this->gDefaultCSSFolder);
        
        if ( !isset( $this->gSCSS ) ) {
            $this->gSCSS = new Compiler();
            $this->gSCSS->setImportPaths( $this->gDefaultSCSSFolder );
        }
        
        if ( $this->gIsCompact ) {
            $this->gLogger->debug( "compact : Yes" );
            $this->gSCSS->setOutputStyle( OutputStyle::COMPRESSED );
        } else {
            $this->gLogger->debug( "compact : No" );
            $this->gSCSS->setOutputStyle( OutputStyle::EXPANDED );
        }
        
        $this->gShowErrorsAsCSS = false;
        date_default_timezone_set( 'UTC' );
        
        $this->gLogger->debug( "src     : '" . $this->gDefaultSCSSFolder . "'" );
        $this->gLogger->debug( "dst     : '" . $this->gDefaultCSSFolder . "'" );
        $this->gLogger->debug( "cache   : '" . $this->gCacheFolder . "'" );
        
    }
    
}
?>
