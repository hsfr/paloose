<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage reading
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/reading/Reader.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Readers</i> maintains a list of the readers.
 * 
 * @package paloose
 * @subpackage reading
 */

class Readers {

   private $gDefault;

   /** Array of Readers (each entry a class: Reader) and indexed by name */
   private $gReaders;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of Readers.
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the contained reader tags.
    *
    * For example 
    *
    * <pre>  &lt;map:readers default="resource">
    *      &lt;map:reader name="resource" src="resource://lib/reading/ResourceReader"/>
    *   &lt;/map:readers></pre>
    *
    * The default attribute specifies the type of reader to use if none is specified in a pipeline.
    *
    * @param DOMNode $inNode the DOM that contains the contents of this pipe element.
    * @throws UserException if component does not have <i>name</i> and <i>src</i> attributes
    * @throws UserException if component constructor throws UserException
    */

   public function parse( $inNode )
   {
      // First thing is make node into a local DOM (this will inlcude an enclosed tags such parameter)
      $readersDom = new DOMDocument;
      $readersDom->appendChild( $readersDom->importNode( $inNode, 1 ) );
      // echo "<pre>" . htmlspecialchars( $readersDom->saveXML() ) . "</pre>";
   
      //Set the namespace for the sitemap. Let us hope people use it.
      $xpath = new domxpath( $readersDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $component = $xpath->query( "//m:reader" );
      // Scan around each reader and set up the list in $gReaders. Exit false if problem
      foreach ( $component as $node ) { 
         $name = $node->getAttribute( "name" );
         $src = $node->getAttribute( "src" );
         $isCachable = Utilities::normalizeBoolean( $node->getAttribute( "cachable" ) );
         $this->gLogger->debug( "Adding Reader name = '". $name . "' using src '" . $src . "'" );
         if ( !strlen( $name ) or !strlen( $src ) ) {
            $dom = new DOMDocument;
            $dom->appendChild( $dom->importNode( $node, 1 ) );
            throw new UserException( "Component Reader must have name and src attributes defined", PalooseException::SITEMAP_READER_PARSE_ERROR, $dom );
         }
         
         // The source may have path details attached so strip the path off to input the correct class.
         if ( strstr( $src, Environment::getPathSeparator() ) == 1 ) {
            $packageFilename = $src;
         } else if ( !strstr( $src, ":" . Environment::getPathSeparator() ) ) {
            // Relative file name so process
            $packageFilename = PALOOSE_LIB_DIRECTORY . Environment::getPathSeparator() . $src;
         } else {
            // Relative file name with pseudo protocol
            $packageFilename = StringResolver::expandPseudoProtocols( $src );
         }

         $package = Utilities::stripFileName( $packageFilename );
         // Must expand pseudo protocols first
         $path = Utilities::stripPath( StringResolver::expandPseudoProtocols( $packageFilename ) );
         $this->gLogger->debug( "Package='". $package . "' : path='" . $path . "'" );
         
         // We have something to add, so first of all check for duplicate names
         if ( $this->gReaders and array_key_exists( $name, $this->gReaders ) ) {
            $this->gLogger->debug( "Overriding reader: '". $name . "'" );
         } else {
            $this->gLogger->debug( "Adding reader: '". $name . "'" );
         }
            
         // Get the necessary for forming a specific Reader
         require_once( $path . $package . ".php" );
         // Form up the component class name
         $readerName = "_" . $package;
         $reader = new $readerName( $name, $path.$package, $node, $isCachable );
         // Check that the Reader thinks it is valid
         try {
            $reader->isValid();
            $this->gReaders[ $name ] = $reader;
         } catch( UserException $e ) {
            $readerDom = new DOMDocument;
            $readerDom->appendChild( $readerDom->importNode( $node, 1 ) );
            throw new UserException( $e->getMessage(), $e->getCode(), $readerDom );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set the default Reader.
    *
    * @param string $inDefault the default Reader name.
    * @exception UserException cannot find Reader
    */

   public function setDefault( $inDefault )
   {
      if ( !( array_key_exists( $inDefault, $this->gReaders ) ) ) {
           throw new UserException( "Default Reader '". $inDefault . "' not found", PalooseException::SITEMAP_READER_PARSE_ERROR );
      }
      $this->gDefault = $inDefault;
      $this->gLogger->debug( "Set default Reader: '". $inDefault . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the default Reader.
    *
    * @retval string the default Reader name.
    */

   public function getDefault()
   {
      return $this->gDefault;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the named Reader.
    *
    * @param string $inName the Reader name.
    * @retval Reader the Reader instance.
    * @exception InternalException cannot find Reader.
    */

   public function getReader( $inName )
   {
      if ( !( array_key_exists( $inName, $this->gReaders ) ) ) {
           throw new InternalException( "Reader '". $inName . "' not found", PalooseException::SITEMAP_READER_PARSE_ERROR );
      } else {
         return $this->gReaders[ $inName ];
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class of the named Reader. Will cause exception if not found in list.
    *
    * @param string $inName the Reader name.
    * @retval string the Reader class name.
    * @exception InternalException cannot find Reader.
    */

   public function getReaderClassName( $inName )
   {
      if ( !( array_key_exists( $inName, $this->gReaders ) ) ) {
           throw new InternalException( "Reader '". $inName . "' not found", PalooseException::SITEMAP_READER_PARSE_ERROR );
      } else {
         $className = $this->gReaders[ $inName ]->getPackageName();
         return $className;
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this component.
    *
    * @retval string the representation of the component as a string
    *
    */
   public function toString()
   {
      $mess = "  <readers default='{$this->gDefault}'>\n";
      foreach ( $this->gReaders as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "  </readers>\n";
      return $mess;
   }

}

?>