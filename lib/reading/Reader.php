<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage reading
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Component.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Reader</i> is the base class for all the reader components.
 * 
 * @package paloose
 * @subpackage reading
 */

class Reader extends Component {

    /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of <i>Reader</i>.
    *
    * @param string $inName the name of this transformer
    * @param string $inClassName the package name of this transformer (destination PHP class)
    * @param DOMNode $inNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inClassName, DOMNode $inNode, $inIsCachable )
   {
      parent::__construct( $inName, $inClassName, $inNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gGenericType = PipeElement::READER;
      $this->gType = "reader";
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Is this component valid?
    *
    * @throws UserException if not valid
    */
    
   public function isValid()
   {
      try {
         parent::isValid();
      } catch ( UserException $e ) {
         throw new UserException( "Reader: " . $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return a string representation of this class (used for debugging).
    *
    * @retval string the string representation.
   */
    
   public function toString()
   {
      if ( $this->gParameters != NULL ) {
         $mess = "   <reader name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'>\n";
         while ( list( $name, $value ) = each( $this->gParameters ) ) {
            $mess .= "    <parameter name='$name' value='$value'/>\n";
         }
         reset( $this->gParameters ); //Insurance
         $mess .= "   </reader>\n";
      } else {
         $mess = "   <reader name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'/>\n";
      }
      return $mess;
   }

}

?>