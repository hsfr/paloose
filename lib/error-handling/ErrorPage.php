<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage error-handling
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>ErrorPage</i> Class is the base class for displaying and error page either
 * from the user or the internal workings of Paloose.
 *
 * @package paloose
 * @subpackage error-handling
 */
 
class ErrorPage {

   /** Error message associated with this page */
   private $gMessage;

   /** Error code associated with this page */
   private $gErrorCode;

   /** DOM scrap nearest error */
   private $gDOMScrap;

   /** The user error page */   
   private $userExceptionPage;

   /** The associated transform */   
   private $userExceptionTransform;

   /** The associated style sheet */   
   private $gStyleSheet;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct new instance of <i>ErrorPage</i>.
    *
    * @param string $inErrorMessage the error message to be displayed
    * @param int $inErrorCode the error code to be displayed
    * @param DOMDocument $inDOMScrap the document scrap near the error
    */

   function __construct( $inErrorMessage, $inErrorCode, $inDOMScrap )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gMessage = $inErrorMessage;
      $this->gErrorCode = $inErrorCode;
      $this->gDOMScrap = $inDOMScrap;

      $this->userExceptionPage = Environment::$configuration[ 'userExceptionPage' ];
      $this->userExceptionTransform = Environment::$configuration[ 'userExceptionTransform' ];
      $this->gStyleSheet = Environment::$configuration[ 'userExceptionStyle' ];
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /** 
    * Display error page.
    *
    * One of the advantages of using a DOM approach instead of SAX is that we
    * have complete control of the entire page that is output as an error page.
    * Using SAX we would have some output already displayed before the error
    * occurred. 
    */
    
   function display()
   {
      $userExceptionPage = StringResolver::expandPseudoProtocols( $this->userExceptionPage );
      $userExceptionTransform = StringResolver::expandPseudoProtocols( $this->userExceptionTransform );
      $styleSheet = StringResolver::expandPseudoProtocols( $this->gStyleSheet );
      $this->gLogger->debug( "Page: '$userExceptionPage'" );
      $this->gLogger->debug( "Transform: '$userExceptionTransform'" );
      $this->gLogger->debug( "Style sheet: '$styleSheet'" );

      // Load the transform. If an error occurs we let things blow up conventionally.
      // I do not let infinite loops start :-)
      $xsl = new DomDocument(); 
      $xsl->load( $userExceptionTransform );
      // create the processor and import the stylesheet
      $proc = new XsltProcessor(); 
      $xsl = $proc->importStylesheet( $xsl );
      // Null namespace
      $proc->setParameter( '', "errorCode", PalooseException::getErrorText( $this->gErrorCode ) );
      $this->gLogger->debug( "Error code: '$this->gErrorCode'" );
      $proc->setParameter( '', "errorMessage", $this->gMessage );
      $this->gLogger->debug( "Error message: '$this->gMessage'" );

      if ( $this->gDOMScrap != null ) {
         $proc->setParameter( '', "errorContext", $this->gDOMScrap->saveXML() );
      }
      $proc->setParameter( '', "styleSheet", $styleSheet );
      $this->gLogger->debug( "Error style sheet: '$styleSheet'" );
      // transform and output the xml document
      
      $dom = new DOMDocument();
      $dom->load( $userExceptionPage );
      $xformDOM = $proc->transformToDoc( $dom );
      echo $xformDOM->saveXML();
   }

}
?>
