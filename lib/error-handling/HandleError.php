<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage error-handling
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/ErrorModule.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>HandleError</i> Class handles the errors in the pipelines.
 * 
 * It contains a pipe
 * definition to run when an error occurs. It is a lot simpler than the normal
 * Cocoon variety which allows selectors etc.
 *
 * @package paloose
 * @subpackage error-handling
 */
 
#[AllowDynamicProperties]
class HandleError {

   /** Pipe associated with this match (Class Pipe) */   
   private $gPipe;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPipe = NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse an error handler pipeline group.
    *
    * @param DOMNode $inNode the error handler node and its children
    * @throws UserException if there is a parse error
    */
    
   public function parse( $inNode )
   {
      // First thing is make node into a local DOM
      $errorHandlerDom = new DOMDocument;
      $errorHandlerDom->appendChild( $errorHandlerDom->importNode( $inNode, 1 ) );

      $this->gXPath = new domxpath( $errorHandlerDom );
      $this->gXPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      // Scan the enclosed pipe
      $component = $this->gXPath->query( "//m:handle-errors/*" );
      $this->gLogger->debug( "Making new Pipe to handle-errors" );
      $this->gPipe = new Pipe();
      foreach ( $component as $pipeNode ) {
         $name = $pipeNode->localName;
         $this->gLogger->debug( "pipe node: '". $name . "'" );
         // This is where we build a pipe. The validity of these pipes is carried out in that class (Pipe)
         switch ( $name ) {
            case "generate" :
               try {
                  $generator = PipeElement::createGenerator( $pipeNode );
                  $this->gLogger->debug( "Creating generator: type='" . $generator->getType() . "' src='" . $generator->getSrc() . "' using component '" . get_class( $generator ) . "'" );
                  // Put it into the pipe
                  $this->gPipe->addComponent( $generator );      
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $errorHandlerDom );
               }
               break;
            case "transform" :
               try {
                    $transformer = PipeElement::createTransformer( $pipeNode );
                  $this->gLogger->debug( "Creating transformer: type='" . $transformer->getType() . "' src='" . $transformer->getSrc() . "' using component '" . get_class( $transformer ) . "'" );
                  // Put it into the pipe
                  $this->gPipe->addComponent( $transformer );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $errorHandlerDom );
               }
               break;
            case "serialize" :
               try {
                  $serializer = PipeElement::createSerializer( $pipeNode );
                  $this->gLogger->debug( "Creating serializer: type='" . $serializer->getType() . "' src='" . $serializer->getSrc() . "' using component '" . get_class( $serializer ) . "'" );
                  // Put it into the pipe
                  $this->gPipe->addComponent( $serializer );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $errorHandlerDom );
               }
               break;
         }
      }
      $this->gLogger->debug( "Finished processing handle-error" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Take the inputted URL and query string and offer it to the various
    * matchers in each pipeline.
    * 
    * @param string $inURL the calling URL
    * @param string $inMess associated error message
    * @retval boolean true if matched, otherwise false
    */

   public function run( $inURL, $inMess )
   {
      $this->gLogger->debug( "URL: $inURL" );   
      $this->gLogger->debug( "Message: $inMess" );   

      $errorModule = new ErrorModule( 'error' );
      Environment::$modules[ $errorModule->getName() ] = $errorModule;
      $errorModule->add( "message", $inMess );
      $errorModule->add( "style-sheet", Environment::$configuration[ 'userExceptionStyle' ] );

      // Make sure that we set the output stream to the browser not the internal buffer
      $outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
      // Get the appropriate sitemap
      $sitemap = Environment::$sitemapStack->peek();
      $sitemap->setOutputStream( $outputStream );

      try {
         // Need an error module to note the errors for expansion in the XML document !!!
         $this->gPipe->run( Environment::$variableStack, $inURL, Environment::$gQueryString, NULL );
      } catch ( RunException $e ) {
         $this->gLogger->debug( $e->getMessage() );
      }
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this Class.
    *
    * @retval string the representation of the Class as a string
    *
    */

   public function toString()
   {
      $mess = " <handle-errors>\n";
      $mess .= $this->gPipe->toString();
      $mess .= " </handle-errors>\n";
      return $mess;
   }

}
?>
