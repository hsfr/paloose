<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage exception-handling
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2020 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>PalooseException</i> is the base class for all Paloose exceptions.
 *
 * @package paloose
 * @subpackage exception-handling
 */

class PalooseException extends Exception {

   /** UserException errors */
   const MISSING_SITEMAP = "1001";
   const SITEMAP_PARSE_ERROR = "1002";
   const SITEMAP_GENERATOR_PARSE_ERROR = "1003";
   const SITEMAP_GENERATOR_RUN_ERROR = "1004";
   const SITEMAP_TRANSFORMER_PARSE_ERROR = "1005";
   const SITEMAP_TRANSFORMER_RUN_ERROR = "1006";
   const SITEMAP_SERIALIZER_PARSE_ERROR = "1007";
   const SITEMAP_SERIALIZER_RUN_ERROR = "1007";
   const SITEMAP_READER_PARSE_ERROR = "1009";
   const SITEMAP_READER_RUN_ERROR = "1010";
   const SITEMAP_MOUNT_PARSE_ERROR = "1011";
   const SITEMAP_AGGREGATE_PARSE_ERROR = "1012";
   const SITEMAP_AGGREGATE_RUN_ERROR = "1026";
   const SITEMAP_MATCH_PARSE_ERROR = "1013";
   const SITEMAP_VIEW_PARSE_ERROR = "1014";
   const SITEMAP_I18N_PARSE_ERROR = "1015";
   const SITEMAP_I18N_NOT_FOUND_ERROR = "1016";
   const SITEMAP_PIPE_ERROR = "1017";
   const SITEMAP_GALLERY_PARSE_ERROR = "1018";
   const SITEMAP_SELECTOR_PARSE_ERROR = "1019";
   const SITEMAP_CALL_PARSE_ERROR = "1020";
   const SITEMAP_CALL_RUN_ERROR = "1021";
   const SITEMAP_ACTION_PARSE_ERROR = "1022";
   const SITEMAP_ACTION_RUN_ERROR = "1023";
   const SITEMAP_REDIRECT_PARSE_ERROR = "1024";
   const SITEMAP_AUTHORISE_MANAGER_PARSE_ERROR = "1025";
   const SITEMAP_FLOW_PARSE_ERROR = "1026";
   const SITEMAP_GEDCOM_ERROR = "1027";
   const SITEMAP_MATCH_RUN_ERROR = "1028";

   /** Run Exception errors */
   const UNKNOWN_STRING_EXPANSION_TYPE = "1100";
   const I18N_CATALOGUE_NOT_FOUND_TYPE = "1101";
   const SQL_ERROR_TYPE = "1102";
   const GALLERY_ERROR = "1103";

  const SCSS_COMPILE_ERROR = "1104";
  const SCSS_FILE_NOT_FOUND_ERROR = "1105";

   /** Internal Exception errors */
   const SYSTEM_CACHE_ERROR = "1201";

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of the exception.
    *
    * @param string $inMessage the associated message
    * @param int $inCode the associated code
    */

   public function __construct( $inMessage, $inCode ) {
       parent::__construct( $inMessage, $inCode );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * These are fairly generic - the detailed error message is generated within
    * the exception raising.
    *
    * @param string $inNumb the error number
    *
    * @retval string the associated error message string
    */
   
   public static function getErrorText( $inNumb )
   {
      //There must be a way of using the const here.
      switch ( $inNumb ) {
         case self::MISSING_SITEMAP : return "Missing sitemap";
         case self::SITEMAP_PARSE_ERROR : return "Sitemap parse error";
         case self::SITEMAP_GENERATOR_PARSE_ERROR : return "Generator parse error";
         case self::SITEMAP_GENERATOR_RUN_ERROR : return "Generator run error";
         case self::SITEMAP_TRANSFORMER_PARSE_ERROR : return "Transformer parse error";
         case self::SITEMAP_TRANSFORMER_RUN_ERROR : return "Transformer run error";
         case self::SITEMAP_SERIALIZER_PARSE_ERROR : return "Serializer parse error";
         case self::SITEMAP_SERIALIZER_RUN_ERROR : return "Serializer run error";
         case self::SITEMAP_READER_PARSE_ERROR : return "Reader parse error";
         case self::SITEMAP_READER_RUN_ERROR : return "Reader run error";
         case self::SITEMAP_MOUNT_PARSE_ERROR : return "Mount parse error";
         case self::SITEMAP_AGGREGATE_PARSE_ERROR : return "Aggregate parse error";
         case self::SITEMAP_AGGREGATE_RUN_ERROR : return "Aggregate run error";
         case self::SITEMAP_MATCH_PARSE_ERROR : return "match parse error";
         case self::SITEMAP_VIEW_PARSE_ERROR : return "View parse error";
         case self::SITEMAP_I18N_PARSE_ERROR : return "I18n parse error";
         case self::SITEMAP_I18N_NOT_FOUND_ERROR : return "i18n file not found";
         case self::SITEMAP_PIPE_ERROR : return "Sitemap pipeline error";
         case self::SITEMAP_GALLERY_PARSE_ERROR : return "Sitemap gallery parse error";
         case self::SITEMAP_SELECTOR_PARSE_ERROR : return "Sitemap selector parse error";
         case self::SITEMAP_CALL_PARSE_ERROR : return "Sitemap call resource parse error";
         case self::SITEMAP_CALL_RUN_ERROR : return "Sitemap call resource run error";
         case self::SITEMAP_ACTION_PARSE_ERROR : return "Sitemap action parse error";
         case self::SITEMAP_ACTION_RUN_ERROR : return "Sitemap action run error";
         case self::SITEMAP_REDIRECT_PARSE_ERROR : return "Sitemap redirect error";
         case self::SITEMAP_AUTHORISE_MANAGER_PARSE_ERROR : return "Sitemap authorisation error";
         case self::SITEMAP_FLOW_PARSE_ERROR : return "Sitemap flow parse error";
         case self::SITEMAP_GEDCOM_ERROR : return "Sitemap Gedcom Generator error";
         case self::SITEMAP_MATCH_RUN_ERROR : return "Sitemap match run error (file not found in internal pipeline?)";

         case self::UNKNOWN_STRING_EXPANSION_TYPE : return "Unknown string expansion";
         case self::I18N_CATALOGUE_NOT_FOUND_TYPE : return "I18n catalogue error";
         case self::SQL_ERROR_TYPE : return "SQL query error";
         case self::GALLERY_ERROR : return "Gallery file processing error";
         case self::SCSS_COMPILE_ERROR : return "SCSS compile error";
 
         case self::SYSTEM_CACHE_ERROR : return "Paloose cache error";
 
         default : return "unknown error code: $inNumb!";
      }
   
   }

}
?>
