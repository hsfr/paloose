<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage resources
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/resources/Resource.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Resources</i> maintains a list of the resources.
 * 
 * @package paloose
 * @subpackage serialization
 */

class Resources {

   /** Array of resources (each entry a class: Selector) and indexed by name */
   private $gResources;

   /** The DOM object for this class */
   private $gDOM;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of <i>Resources</i>.
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gResources = array();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the contained resource tags.
    *
    * @param DOMNode $inNode the sitemap node containing the resources.
    * @throws UserException if cannot parse or duplicate resource name.
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $this->gDOM = new DOMDocument;
      $this->gDOM->appendChild( $this->gDOM->importNode( $inNode, 1 ) );

      // Set the namespace for the sitemap. Let us hope people use it.
      $xpath = new domxpath( $this->gDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $resource = $xpath->query( "//map:resources/*" );
      // Scan around each resource and set up the list in $gResources. Exception if problem.
      foreach ( $resource as $node ) { 
         $name = $node->getAttribute( "name" );
         $this->gLogger->debug( "Found resource [name:". $name . "]" );

         // We have something to add, so first of all check for duplicate names
         if ( array_key_exists( $name, $this->gResources ) ) {
            $dom = new DOMDocument;
            $dom->appendChild( $dom->importNode( $node, 1 ) );
            throw new UserException( "Duplicate resource name '" . $name . "' found", PalooseException::SITEMAP_VIEW_PARSE_ERROR, $dom );
         } else {
            try {
               $resource = new Resource( $name, $node );
               $resource->parse( $node );
               $this->gResources[ $name ] = $resource;
               $this->gLogger->debug( "Adding resource: '". $name . "' : " . count( $this->gResources) );
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the resource associated with $inName.
    *
     * @param string $inName the name of the node to fetch.
     * @retval Resource the resource accessed by this name.
    * @throws UserException if cannot parse or duplicate resource name.
    */
    
   public function getResource( $inName )
   {
      if ( array_key_exists( $inName, $this->gResources ) ) {
         return $this->gResources[ $inName ];
      } else {
           throw new UserException(
              "Could not find resource '$inName'",
              PalooseException::SITEMAP_CALL_RUN_ERROR,
              $this->gDOM );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return a string representation of this class (used for debugging).
    *
    * @retval string the string representation.
   */
    
   public function toString()
   {
      $mess = "";
      if ( $this->gResources != null ) {
         $mess = "  <resources>\n";
         foreach ( $this->gResources as $name => $resource ) {
            $mess .= $resource->toString();
         }
         $mess .= "  </resources>\n";
      }
      return $mess;
   }

}

?>