<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage resources
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006-2008 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Call</i> allows the pipeline to invoke predefined scripts or pipline fragments.
 *
 * @package paloose
 * @subpackage resources
 */

#[AllowDynamicProperties]
class Call extends PipeElement implements PipeElementInterface
{
   /** The name of the resource/function we are going to call */
   private $gResourceName;
   
   /** The type of this call */
   private $gCallType;

   const RESOURCE = 1;
   const FUNC = 2;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Create a new instance of a Call element
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if call tag not valid.
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gGenericType = PipeElement::CALL;
      $this->gSitemapSrc = $inSrc;
      $this->gType = $inType;
      $this->gComponent = $inComponent;

      // Extract the parts to be used from the inDOM
      $xpath = new domxpath( $inDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      // $this->gLabel = Utilities::getXPathListStringItem( 0, $xpath, "//@label" );

      $this->gResourceName = Utilities::getXPathListStringItem( 0, $xpath, "//@resource" );
      if ( strlen( $this->gResourceName ) > 0 ) {
         $this->gCallType = self::RESOURCE;
         $this->gLogger->debug( "Creating new call [resource='" . $this->gResourceName . "']" );
           return;
      }
      $this->gResourceName = Utilities::getXPathListStringItem( 0, $xpath, "//@function" );
      if ( strlen( $this->gResourceName ) > 0 ) {
         $this->gCallType = self::FUNC;
         $this->gLogger->debug( "Creating new call [function='" . $this->gResourceName . "']" );
           return;
      }
        throw new UserException( "Call must have a name/function attribute defined", PalooseException::SITEMAP_CALL_PARSE_ERROR, $inDOM );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Running the Call invokes the named Resource and transfers control to that.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval The document DOM representing the input.
    * @throws FileGeneratorException if an error occurs
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Running resource '{$this->gResourceName}'" );
      try {

         switch ( $this->gCallType ) {
         
            case self::RESOURCE :
               // Get the resource that we have to run, extract the pipe and run it
               $sitemap = Environment::$sitemapStack->peek();
               $resources = $sitemap->getResources();
               $resource = $resources->getResource( $this->gResourceName );
               $pipe = $resource->getPipe();
               return $pipe->run( $inVariableStack, $inURL, $inQueryString, $inDOM );

            case self::FUNC :
               // Process the encoded function (CLASS::METHOD)
               $pair = explode( "::", $this->gResourceName );
               if ( $pair != NULL and count( $pair ) == 2 ) {
                  // There is something to process
                  $class = $pair[0];
                  $method = $pair[1];
                  $this->gLogger->debug( "Running resource in class '$class' at function '$method'" );
               
                  if ( !empty( $this->gParameters->getParameterList() ) ) {
                     $parameterList = $this->gParameters->getParameterList();
                     foreach ( $parameterList as $name => $value ) {
                        $expandedValue = StringResolver::expandPseudoProtocols( StringResolver::expandString( $inVariableStack, $value ) );
                        // Put into paramaters etc.
                        $paramsModule = Environment::$modules[ 'params' ];
                        $paramsModule->add( $name, $value );
                        $this->gLogger->debug( "Setting parameter [$name] = [$expandedValue]" );
//                        if ( $xslProcessor->setParameter( null, $name, $expandedValue ) === false ) {
//                           throw new InternalException( "Problem setting parameter '$name' with value ' $value'",
//                                                       PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR );
//                        }
                     }
                  }
                  
                  // First of all construct an instance
                  $instance = new $class();
                  // Then run the method
                  $instance->$method();
               } else {
                  throw new RunException( "Must have class and method defined in function call", NULL );
               }
         }
      
      } catch ( ExitException $e ) {
           throw new ExitException();
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get resource name.
    *
    * @retval string the string name
    */

   public function getResourceName()
   {
      return $this->gResourceName;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get resource type.
    *
    * @retval string the string type
    */

   public function getCallType()
   {
      return $this->gCallType;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this class.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      switch ( $this->gCallType ) {
         case self::RESOURCE : return "<call name='{$this->gResourceName}' />\n";
         case self::FUNC : return "<call function='{$this->gResourceName}' />\n";
      }
   }

}
?>
