<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage resources
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Resource</i> is the base class for all the resource components.
 * 
 * @package paloose
 * @subpackage resources
 */

class Resource {

    /** Logger instance for this class */   
   private $gLogger;

    /** Name of this resource */   
   private $gName;

    /** Pipeline associated with this resource */
   private $gPipe;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of the selector.
    *
    * @param string $inName the name of this selector
    * @param DOMNode $inNode the node associated within selector (stores all parameters and other enclosed tags)
    * @throws UserException if cannot resource name not defined.
    */

   public function __construct( $inName, DOMNode $inNode )
   {
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gName = $inName;
       if ( !( strlen( $inName ) > 0  ) ) {
           $dom = new DOMDocument;
           $dom->appendChild( $dom->importNode( $inNode, 1 ) );
           throw new UserException( "Sitemap resource declaration must have name defined", PalooseException::SITEMAP_VIEW_PARSE_ERROR, $dom );
       }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the pipeline within the resource.
    * 
    * Pipelines consist of a simple set transforms and serializer.
    *
    * @param DOMNode $inNode the sitemap node containing the resources.
    * @throws UserException if cannot parse.
    */
    
   public function parse( DOMNode $inNode )
   {
      //First thing is make node into a local DOM
      $resourceDom = new DOMDocument;
      $resourceDom->appendChild( $resourceDom->importNode( $inNode, 1 ) );
      //echo "<pre>" . htmlspecialchars( $resourceDom->saveXML() ) . "</pre>";
      
      $xpath = new domxpath( $resourceDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      //Scan the enclosed pipe
      $component = $xpath->query( "//m:resource/*" );
      $this->gLogger->debug( "Making new Resource Pipe with " . count( $component ). " components" );
      $this->gPipe = new Pipe();
      foreach ( $component as $pipeNode ) {
         $name = $pipeNode->localName;
         $this->gLogger->debug( "pipe node: '". $name . "'" );
         //This is where we build a pipe. The validity of these pipes is carried out in that class (Pipe)
         switch ( $name ) {
            case "redirect-to" :
            case "mount" :
            case "read" :
               throw new UserException( "Resource pipe cannot have redirect-to, mount or read components", PalooseException::SITEMAP_VIEW_PARSE_ERROR, $resourceDom );

            case "select" :
               try {
                  $selector = PipeElement::createSelector( $pipeNode );
                  $this->gLogger->debug( "Creating selector: type='" . $selector->getType() . "' using component '" . get_class( $selector ) . "'" );
                  $this->gPipe->addComponent( $selector );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;

            case "aggregate" :
               try {
                  $aggregator = PipeElement::createAggregator( $pipeNode );
                  $this->gLogger->debug( "Creating aggregator using component '" . get_class( $aggregator ) . "'" );
                  //Put it into the pipe
                  $this->gPipe->addComponent( $aggregator );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "generate" :
               try {
                  $generator = PipeElement::createGenerator( $pipeNode );
                  $this->gLogger->debug( "Creating generator: type='" . $generator->getType() . "' src='" . $generator->getSrc() . "' using component '" . get_class( $generator ) . "'" );
                  //Put it into the pipe
                  $this->gPipe->addComponent( $generator );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;

            case "transform" :
               try {
                  $transformer = PipeElement::createTransformer( $pipeNode );
                  $this->gLogger->debug( "Creating transformer: type='" . $transformer->getType() . "' src='" . $transformer->getSrc() . "' using component '" . get_class( $transformer ) . "'" );
                  //Put it into the pipe
                  $this->gPipe->addComponent( $transformer );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;

            case "serialize" :
               try {
                  $serializer = PipeElement::createSerializer( $pipeNode );
                  $this->gLogger->debug( "Creating serializer: type='" . $serializer->getType() . "' src='" . $serializer->getSrc() . "' using component '" . get_class( $serializer ) . "'" );
                  //Put it into the pipe
                  $this->gPipe->addComponent( $serializer );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
         }
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    */
    
   public function getName()
   {
      return $this->gName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    */
    
   public function getPipe()
   {
      return $this->gPipe;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return the string representation of the object.
    *
    * @retval string representation of object
    */
    
   public function toString()
   {
      $mess = "   <resource name='{$this->gName}' >\n";
      $mess .= $this->gPipe->toString();
      $mess .= "  </resource>\n";
      return $mess;
   }

}

?>