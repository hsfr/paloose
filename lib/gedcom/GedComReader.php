<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage GedCom
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/GedComException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/gedcom/GedComLine.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class is the main class for handling GedCom files.
 *
 * @package paloose
 * @subpackage GedCom
 */
    
class GedComReader 
{

    /** Logger instance for this class */
   private $gLogger;

   /** Name of source GedCom file */
   private $gGedComFileName;

   /** Number of line being read */
   private $gLineNumber = 0;

   /** handle to source file */
   private $gFileHandle;

   /** Last raw line read */
   private $gRawLine = "";

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of GedComReader.
    *
    * @param string $inGedComFileName the name of the GedCom file to input
    */
    
   public function __construct( $inGedComFileName )
   {
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gGedComFileName = $inGedComFileName;
      if ( !file_exists( $this->gGedComFileName ) || !is_readable( $this->gGedComFileName ) ) {
         throw new GedComException( "File '$this->gGedComFileName' does not exist/cannot be read", PalooseException::SITEMAP_GENERATOR_RUN_ERROR );
      }
      // Read a line up to any filesystem line ending
      ini_set( 'auto_detect_line_endings', true );
      $this->gFileHandle = @fopen( $this->gGedComFileName, "r" );
      $this->gLogger->debug( "opened file '" . $this->gGedComFileName . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the next line.
    *
    * @retval GedcomLine the next line in GedcomLine form, otherwise null if at end of file.
    * @throws GedComException if a problem
    */

   public function getNextLine()
   {
      try {
         $this->gLogger->debug( "reading file '" . $this->gGedComFileName . "'" );
         if ( !feof( $this->gFileHandle ) ) {
            $this->gLineNumber++;
            $this->gLogger->debug( "reading line " . $this->gLineNumber );
            $this->gRawLine = fgets( $this->gFileHandle, 4096 );
            try {
               return new GedcomLine( $this->gRawLine, $this->gLineNumber );
            } catch ( GedComException $e ) {
                 throw new GedComException( $e->getMessage(), $e->getCode() );
            }
         } else {
            $this->gLogger->debug( "closing file '" . $this->gGedComFileName . "'" );
            fclose( $this->gFileHandle );
            return null;
         }
      } catch ( GedComException $e ) {
        throw new GedComException( $e->getMessage(), $e->getCode() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Return the original raw line.
    *
    * @retval string the original line
    */

   public function getRawLine()
   {
      return $this->gRawLine;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
  /**
    * Return the line number of the current line.
    *
    * @retval int the line number
    */


   public function getLineNumber()
   {
      return $this->gLineNumber;
   }

}

?>
