<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage GedCom
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/GedComException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class is the base class for all other tag classes. It is also the
 * root of the GedCom internal structure.
 *
 * @package paloose
 * @subpackage GedCom
 */
    
class GedComNode
{

   /** Logger instance for this class */
   private $gLogger;
   
   /** The line number of the original source associated with this node */
   private $gLineNumber = 0;

   /** The source line associated with this node */
   protected $gLine;
   
   /** The parent node */
   protected $gParentNode;
   
   /** Level number of this node */
   protected $gLevelNumber;
   
   /** Data associated with this node */
   protected $gData;
   
   /** List of tags in a level - stored as a Vector type array */
   protected $gNodeList;
   
   /** Id string for this node */
   protected $gIdentifier = "";
   
   /** Tag name string for this node */
   protected $gTagName = "gedcom";
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of GedComNode.
    *
    * @param GedcomLine $inLine the line to process
    */
    
   public function __construct( $inLine )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      if ( $inLine != NULL ) {
         $this->gData = rtrim( $inLine->getData() );
         $this->gTagName = $inLine->getTag();
         $this->gIdentifier = $inLine->getIdentifier();
         $this->gLevelNumber = $inLine->getLevel();
         $this->gLogger->debug( "created null child node" );
      } else {
         $this->gLevelNumber = 0;
         $this->gLogger->debug( "created root node" );
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Add a node to the child list.
    *
    * @param GedcomNode $inNode the node to add
    * @retval GedcomNode the added input node
    */
 
   public function addNode( GedcomNode $inNode )
   {
      if ( $this->gNodeList == NULL ) {
         $this->gNodeList = array();
      }
      $this->gNodeList[] = $inNode;
      $this->gLogger->debug( "Added node: " . $inNode->getTag() . " to " . $this->getTag() );
      return $inNode;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the current line.
    *
    * @retval GedcomNode next node
    */

   public function parse( GedcomReader $inReader )
   {
      $nextNode = null;
      try {
         $nextLine = $inReader->getNextLine();
         $this->gLogger->debug( "processing line : '" . $nextLine->toString() );
         $nextNode = new GedcomNode( $nextLine );
         $nextNode->gLineNumber = $nextLine->getLineNumber();
         while ( $nextNode->getLevel() != -1 ) {
            // First of all check to see whether this node should be dealt with at this level
            if ( $nextNode->getLevel() <= $this->gLevelNumber ) {
               // All finished at this level so go up
               $this->gLogger->debug( "Exit level ... [nextLevel:" . $nextLine->getLevel() . "][gLevelNumber:" . $this->gLevelNumber . "]" );
               return $nextNode;
            } else {
               // Is a child of this node so set the parent and add it
               $nextNode->gParentNode = $this;
               $this->gLogger->debug( "Adding [" . $nextNode->getTag() . "] to [" . $this->getTag() . "]" );
               $this->addNode( $nextNode );
               if ( $nextNode->getTag() == "TRLR" ) {
                  $this->gLogger->debug( "End of file found" );
                  return $nextNode;
               } else {
                  // Enter the node to parse next line. nextNode becomes a new descendant node
                  $nextNode = $nextNode->parse( $inReader );
                  if ( $nextNode->getTag() == "TRLR" ) {
                     $this->gLogger->debug( "End of file found" );
                     return $nextNode;
                  }
               }
            }
         }
      } catch ( GedComException $e ) {
         throw new GedComException( $e->getMessage(), $e->getCode );
      } 
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the parent of this node.
    *
    * @param GedcomNode $inNode parent node
    */

   public function setParentNode( $inNode )
   {
      $this->gParentNode = $inNode;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the line number of this line.
    *
    * @param int $inLineNumber line number of this line
    */

   public function setLineNumber( $inLineNumber )
   {
      $this->gLineNumber = $inLineNumber;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the tag name of this line.
    *
    * @retval string tag name of this line
    */

   public function getTag()
   {
      // echo "!!![" . $this->gTagName . "]</br>";
      return $this->gTagName;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the line number of this line.
    *
    * @retval int line number of this line
    */

   public function getLineNumber()
   {
      return $this->gLineNumber;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the level number of this line.
    *
    * @retval int level number of this line
    */

   public function getLevel()
   {
      return $this->gLevelNumber;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the data string.
    *
    * @retval string data string of this line
    */

   public function getData()
   {
      return $this->gData;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this GedCom file in original GedCom.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      $msg = $this->gLevelNumber . " " . $this->gTagName . " " . $this->gData . "\n";
      if ( !empty( $this->gNodeList ) ) {
         foreach ( $this->gNodeList as $node ) {
            $msg = $msg . $node->toString();
         }
      }
      return $msg;
   }

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this GedCom file in XML. It normalises the
    * tag name to lowercase.
    *
    * @retval string the representation of the element as an XML string
    */

   public function toXMLString()
   {
      $msg = "";
      $attribName = "data";
      if ( $this->gTagName == "CHIL" 
         || $this->gTagName == "SUBM"
         || $this->gTagName == "FAMS"
         || $this->gTagName == "FAMC"
         || $this->gTagName == "HUSB"
         || $this->gTagName == "WIFE" ) {
         // Reference tag so set up attribute name
         $attribName = "ref";
         $msg .= "<g:" . strtolower( $this->gTagName );
         if ( strlen( $this->gData ) > 0 ) {
            $msg .= " " . $attribName . "=\"" . rtrim( GedCom::convertString( $this->getData() ) ) . "\"";
         }
         $msg .= "/>\n";
      } else if ( $this->gTagName == "DATE"
            || $this->gTagName == "FORM"
            || $this->gTagName == "SURN"
            || $this->gTagName == "VERS"
            || $this->gTagName == "CHAR"
            || $this->gTagName == "NAME"
            || $this->gTagName == "EMAIL"
            || $this->gTagName == "WEB"
            || $this->gTagName == "CONT"
            || $this->gTagName == "TITL"
            || $this->gTagName == "OCCU"
            || $this->gTagName == "GIVN"
            || $this->gTagName == "SEX"
            || $this->gTagName == "PLAC"
            || $this->gTagName == "CONC"
            || $this->gTagName == "REFN"
            || $this->gTagName == "EDUC" ) {
         // Data tag so set up attribute name
         $msg .= "<g:" . strtolower( $this->gTagName );
         $attribName = "data";
         if ( strlen( $this->gData ) > 0 ) {
            $msg .= " " . $attribName . "=\"" . rtrim( GedCom::convertString( $this->getData() ) ) . "\"";
         }
         $msg .= "/>\n";
      } else if ( $this->gTagName == "QUAY") {
         // Level tag so set up attribute name
         $attribName = "level";
         $msg .= "<g:" . strtolower( $this->gTagName );
         if ( strlen( $this->gData ) > 0 ) {
            $msg .= " " . $attribName . "=\"" . rtrim( GedCom::convertString( $this->getData() ) ) . "\"";
         }
         $msg .= "/>\n";
      } else if ( $this->gData == "FAM"
            || $this->gData == "SOUR"
            || $this->gData == "INDI"
            || $this->gData == "SUBM" ) {
         // Identity tag so set up attribute name
         $attribName = "id";
         $tagName = strtolower( $this->gData );
         $msg .= "<g:" . $tagName;
         if ( strlen( $this->gData ) > 0 ) {
            $msg .= " " . $attribName . "=\"" . $this->gIdentifier . "\"";
         }
         $msg .= ">\n";
         if ( !empty( $this->gNodeList ) ) {
            foreach( $this->gNodeList as $node ) {
               $msg .= $node->toXMLString();
            }
         }
         $msg .= "</g:" . $tagName . ">\n";
      } else if ( $this->gTagName == "gedcom" ) {
         // The root of the gedcom so set up root tag with namespace declaration
         // with attribute set to "data".
         $msg .= "<gedcom xmlns:g=\"http://gedcom.org/dtd/gedxml55.dtd\">\n";
         if ( !empty( $this->gNodeList ) ) {
            foreach( $this->gNodeList as $node ) {
               $msg .= $node->toXMLString();
            }
         }
         $msg .= "</gedcom>\n";
      } else {
         // All other tags not caught by the above are "data" attributes
         $msg .= "<g:" . strtolower( $this->gTagName );
         if ( strlen( $this->gData ) > 0 ) {
            $msg .= " " . $attribName . "=\"" . rtrim( GedCom::convertString( $this->getData() ) ) . "\"";
         }
         $msg .= ">\n";
         if ( !empty( $this->gNodeList ) ) {
            foreach( $this->gNodeList as $node ) {
               $msg .= $node->toXMLString();
            }
         }
         $msg .= "</g:" . strtolower( $this->gTagName ) . ">\n";
      }
      return $msg;
   }
}

?>
