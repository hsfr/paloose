<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage GedCom
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/GedComException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class is the main class for storing the data in a single GedCom line.
 * the syntax of a GedCom line is:
 * 
 *    <gedcom-line> ::= <level> <white-space>+ <tag> <white-space>+ <line-data>
 *    <level> ::= <integer>+
 *    <tag> ::= ( SOUR | VERS | ... )
 *    <line-data> ::= context dependent on tag
 *
 * @package paloose
 * @subpackage GedCom
 */
    
class GedComLine
{

    /** Logger instance for this class */
   private $gLogger;

   /** raw line string  */
   private $gLineString;

   /** Number of this line in source file  */
   private $gLineNumber = 0;

   /** Level number of this line */
   private $gLineLevel = "";

   /** 4 character tag name */
   private $gTag = "";

   /** identifier */
   private $gIdentifier = "";

   /** String that follows tag */
   private $gLineData = "";

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of GedComLine.
    *
    * @param int $inLineNumber the number of this line within the file
    * @param string $inLineString the line for parsing
    */
    
   public function __construct( $inLineString, $inLineNumber  )
   {
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gLineNumber = $inLineNumber;
       $this->gLineString = $inLineString;

      $this->gLogger->debug( "Create line [" . $this->gLineNumber . "] : " . rtrim( $this->gLineString ) );
      if ( $this->gLineNumber == 0 ) {
         throw new GedcomException( "No line number associated with this line: " . $this->gLineString, PalooseException::SITEMAP_GEDCOM_ERROR );
      }

      // Use a simple Perl regexp to sort out the line
      $finished = false;
      $this->gLogger->debug( "Trying first match" );
      if ( preg_match( "/^(\d+)\s+([A-Z_]+)\s*(.*)$/", $this->gLineString, $matches ) ) {
         $this->gLineLevel = $matches[1];
         $this->gTag = $matches[2];
         $this->gLineData = $matches[3];
         $finished = true;
         $this->gLogger->debug( "Parsed line tag [" . $this->gLineLevel . "][" . $this->gTag  . "][" . rtrim( $this->gLineData ) . "]" );
      }
      if ( $finished == false ) {
         $this->gLogger->debug( "Trying second match" );
         if ( preg_match( "/^(\d+)\s+(@[^@]+@)\s*(.*)$/", $this->gLineString, $matches ) ) {
            $this->gLineLevel = $matches[1];
            $this->gIdentifier = $matches[2];
            $this->gLineData = $matches[3];
            $finished = true;
            $this->gLogger->debug( "Parsed line id [" . $this->gLineLevel . "][" . $this->gIdentifier  . "][" . rtrim( $this->gLineData ) . "]" );
         }
      }
      if ( $finished == false ) {
         throw new GedcomException( "Could not parse line: [" . $this->gLineNumber . "] : " . $this->gLineString, PalooseException::SITEMAP_GEDCOM_ERROR );
      }

}

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the line number of this line.
    *
    * @retval int line number of this line
    */

   public function getLineNumber()
   {
      return $this->gLineNumber;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the level number of this line.
    *
    * @retval int level number of this line
    */

   public function getLevel()
   {
      return $this->gLineLevel;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the tag name of this line.
    *
    * @retval string tag name of this line
    */

   public function getTag()
   {
      return $this->gTag;
   }
   
  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the tag name of this line.
    *
    * @retval string tag name of this line
    */

   public function getIdentifier()
   {
      return $this->gIdentifier;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the data string.
    *
    * @retval string data string of this line
    */

   public function getData()
   {
      return $this->gLineData;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this GedCom line.
    *
    * @retval string the representation as a string
    *
    */

   public function toString()
   {
      if ( $this->gTag != "" ) {
         return $this->gLineLevel . " " . $this->gTag . " " . $this->gLineData;
      } else if ( $this->gIdentifier != "" ){
         return $this->gLineLevel . " @" . $this->gIdentifier . "@ " . $this->gLineData;
      } else {
         return "";
      }
   }

}

?>
