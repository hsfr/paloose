<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage GedCom
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/gedcom/GedComReader.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/gedcom/GedComLine.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/gedcom/GedComNode.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/GedComException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class is the main class for handling GedCom files.
 *
 * @package paloose
 * @subpackage GedCom
 */
    
class GedCom 
{

    /** Logger instance for this class */
   private $gLogger;

   /** Name of source GedCom file */
   private $gGedComFileName;

   /** Root of internal structure */
   private $gRoot;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of GedComGenerator.
    *
    * @param string $inGedComFleName the name of the GedCom file to input
    */
    
   public function __construct( $inGedComFleName )
   {
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gGedComFileName = $inGedComFleName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The GEDCOM generator reads a GEDCOM file and generates a DOM representation of
    * it with the root node of the structure in gRoot. It returns a string of the XML.
    *
    * @retval String string representation of XML output.
    * @throws GedComException if an error occurs
    */

   public function processFile()
   {
      $this->gLogger->debug( "processing file '" . $this->gGedComFileName . "'" );
      if ( file_exists( $this->gGedComFileName ) ) {
         // Read the GedCom file into the data structure of lines
         try {
            $gedcomFile = new GedcomReader( $this->gGedComFileName );
            $this->gLogger->debug( "Set up file reader from ". $this->gGedComFileName );
            $this->gRoot = new GedcomNode( NULL );
            // Now get first line and start the whole process off
            $nextLine = $gedcomFile->getNextLine();
            $this->gLogger->debug( "Got next line " . $nextLine->getLineNumber() . " : " . $nextLine->toString() );
            $nextNode = new GedcomNode( $nextLine );
            $finished = false;
            $nextNode->setLineNumber( $nextLine->getLineNumber() );
            $this->gLogger->debug( "Set up new child for line " . $nextNode->getLineNumber() . " : " . $nextNode->getTag() );
            
            while ( $nextNode->getLevel() != -1 && !$finished ) {
               $nextNode->setParentNode( $this->gRoot );
               $this->gLogger->debug( "Adding [" . $nextNode->getTag() . "] to [" . $this->gRoot->getTag() . "]" );
               $this->gRoot->addNode( $nextNode );
               if ( $nextNode->getTag() == "TRLR" ) {
                  $this->gLogger->debug( "End of file found" );
                  $finished = true;
               } else {
                  // Enter the node to parse next line. nextNode becomes a new descendant node
                  $nextNode = $nextNode->parse( $gedcomFile );
                  if ( $nextNode->getTag() == "TRLR" ) {
                     $this->gLogger->debug( "End of file found" );
                     $finished = true;
                  }
               }
            }
            $domText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
            return $domText . $this->gRoot->toXMLString();
         } catch ( GedComException $e ) {
              throw new GedComException( $e->getMessage(), $e->getCode() );
         }
      } else {
           throw new GedComException( "File '{$this->gGedComFileName}' does not exist", PalooseException::SITEMAP_GEDCOM_ERROR );
      }
   }

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the path where all the GedCom classes are rooted.
    *
    * @retval string the path
    *
    */

   /* public static function getDegComLibPath()
   {
      return PALOOSE_LIB_DIRECTORY . Environment::getPathSeparator() . "gedcom". Environment::getPathSeparator();
   } */
      
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Load the package to support GedCom tag.
    *
    * @retval string the package name (Class name)
    * @throws GedComException if problem loading package
    */

   /* public static function loadPackage( $inNodeName )
   {
      $packageName = "GedCom" . $inNodeName;
      $fileName = GedCom::getDegComLibPath() . $packageName . ".php";
      if ( !file_exists( $fileName ) ) {
         // Use default node
         $packageName = "GedComNode";
         $fileName = GedCom::getDegComLibPath() . "GedComNode.php";
      }
      if ( !file_exists( $fileName ) ) {
         throw new GedComException( "File does not exist : " . $fileName, PalooseException::SITEMAP_GENERATOR_RUN_ERROR );
      }
      require_once( $fileName );
      if ( !class_exists( $packageName ) ) {
         throw new GedComException( "Class does not exist : " . $packageName, PalooseException::SITEMAP_GENERATOR_RUN_ERROR );
      }
      return $packageName;
   } */

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Returns an entity ref if the character is out of range.
    *
    * @param string $inChar the character to convert
    * @retval string converted character
    */

   public static function convertChar( $inChar )
   {
      if ( ord( $inChar ) >=0 and ord ( $inChar ) <= 128 ) return $inChar;
      return "&#" . ord( $inChar ) . ";";
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Returns an entity ref if the character is out of range.
    *
    * @retval string the package name (Class name)
    * @throws GedComException if problem loading package
    */

   public static function convertString( $inStr )
   {
      $str = htmlspecialchars( $inStr );
      $convertedStr = "";
      for ( $i = 0; $i <= strlen( $str ); $i++ ) {
         $convertedStr .= GedCom::convertChar( substr( $str, $i, 1 ) );
      }
      return $convertedStr;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this GedCom file (in GedCom).
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return $this->gRoot->toString();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this GedCom file (in XML).
    *
    * @retval string the representation of the element as an XML string
    *
    */

   public function toXMLString()
   {
      return $this->gRoot->toXMLString();
   }

}

?>
