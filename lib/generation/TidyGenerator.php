<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage generation
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/generation/GeneratorPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>TidyGenerator</i> reads an XML file and generates a DOM of
 * it.
 *
 * @package paloose
 * @subpackage generation
 */

class TidyGenerator extends GeneratorPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct ne instance of the class.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this transform element.
    * @param string $inType the type of this generator ("file", etc.)
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _TidyGenerator $inComponent the associated component instance (stores parameters etc)
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Run this class in the pipeline.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval The document DOM representing the input.
    * @throws TidyGeneratorException if an error occurs
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Running TidyGenerator with raw '" . $this->gSrc . "'" );
      $expandedString = StringResolver::expandString( $inVariableStack, $this->gSrc );
      $this->gLogger->debug( "Result after substitution: '$expandedString'" );
      $expandedString = StringResolver::expandPseudoProtocols( $expandedString );
      $this->gLogger->debug( "Result after pseudo protocol expansion: '$expandedString'" );
      // Go and read the file into a DOM
      
      $documentDOM = new DomDocument();
      // First of all does the file exist
      if ( file_exists( $expandedString ) ) {
      
         $configArray = $this->gComponent->getParameters();
         // foreach ( $configArray as $name => $value ) { echo "[$name => $value]"; }

         $fileString = file_get_contents( $expandedString );
         
         // The following is a vile collection (first two not mine) of register expression
         // bodges to cope with the appalling Microsoft output :-(
         // I am not particularly proud of this lot.
         $reg = "</?[[:alpha:]]+:[[:alnum:]]+[^>]*>";
         $fileString = ereg_replace($reg,"",$fileString);
         
         $reg = "</?\?[^>]*>";
         $fileString = ereg_replace($reg,"",$fileString);
         
         // Use Perl regex statements as I am more used to them
         // Remove namespace from root declaration
         $reg = "/xmlns:[^=]*\s*=\s*\"[^\"]*\"/";
         $fileString = preg_replace( $reg, "", $fileString );

         // Remove \newlines and make space - essential for what follows
         $reg = "/[\n\r]/";
         $fileString = preg_replace( $reg, " ", $fileString );
         
         // Remove all style. Elements and attributes
         $reg = "!<style>.*</style>!";
         $fileString = preg_replace( $reg, "", $fileString);
         $reg = "/style[^=]*\s*=\s*[\"\'][^\"\']*[\"\']/";
         $fileString = preg_replace( $reg, "", $fileString);
         
         // Remove remaining if...endif stuff
         $reg = "#<!--\[if[^>]*>[^!]*!\[endif[^>]*>#";
         $fileString = preg_replace( $reg, "", $fileString );
         $reg = "#<!\[if[^>]*>[^!]*!\[endif[^>]*>#";
         $fileString = preg_replace( $reg, "", $fileString );
         
         // Finally we can tidy it
         $tidy = tidy_parse_string( $fileString, $configArray );
         $tidy->cleanRepair();
         
         // Tidy puts in these ids which I have not been able to get rid of.
         // They occur in <a name="" id="">...</a>. It causes real problems
         // with the the DOM parser below. Remove as soon as a way round is found.
         $fileString = $tidy;
         
         $reg = "/id\s*=\s*[\"\'][^\"\']*[\"\']/";
         $fileString = preg_replace( $reg, "", $fileString );
         
         // echo "<pre>" . htmlspecialchars( $fileString ) . "</pre>";
         // exit;
         
       
          if ( !empty( $tidy->errorBuffer ) ) {
            // Do not exit (tidy generates warning) but output them as a WARN in the log file
            $this->gLogger->warn( "Found warnings in repair: " . $tidy->errorBuffer );
         }
         // echo "<pre>" . htmlspecialchars( $tidy ) . "</pre>";
         if ( !$documentDOM->loadHTML( $fileString ) ) {
            throw new UserException( "Could not load document from repaired file '$expandedString'", PalooseException::SITEMAP_GENERATOR_RUN_ERROR, NULL );
         }
      } else {
           throw new RunException( "File '$expandedString' does not exist", PalooseException::SITEMAP_GENERATOR_RUN_ERROR );
      }

      return $documentDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage generation
 */
 
class _TidyGenerator extends Generater {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
       $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "TidyGenerator";

   }

}
?>
