<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage generation
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/caching/CacheFile.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/generation/GeneratorPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>PXTemplateGenerator</i> reads an XML file and generates a DOM of it
 * expanding the embedded Paloose variables.
 *
 * @package paloose
 * @subpackage generation
 */
    
class PXTemplateGenerator extends GeneratorPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of PXTemplateGenerator.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (the file name)
    * @param _FileTransformer $inComponent the associated component instance (stores parameters etc)
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gLogger->debug( "Constructing cachable generator: [{$this->gComponent->gIsCachable}]" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Run class within pipeline.
    *
    * Read file and generate a DOM after expanding the variables. If there no errors
    * it returns the DOM (effectively passing it to the next in the pipeline. 
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the document DOM representing the input.
    * @throws RunException if an error occurs (no such file or could not parse file).
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      parent::run( $inVariableStack, $inURL, $inQueryString, $inDOM );

      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Raw src: '" . $this->gSrc . "'" );
      $fullFileName = StringResolver::expandString( $inVariableStack, $this->gSrc );
      $this->gLogger->debug( "Result after substitution: '$fullFileName'" );
      $fullFileName = StringResolver::expandPseudoProtocols( $fullFileName );
      $this->gLogger->debug( "Result after pseudo protocol expansion: '$fullFileName'" );
      $cacheKey = CacheFile::getHash( $fullFileName );

      $isCachable = $this->gComponent->gIsCachable;
      $this->gLogger->debug( "Global cachable: [{$this->gComponent->gIsCachable}]" );
      $useReqParams = Utilities::normalizeBoolean( $this->gComponent->getUseRequestParametersFlag() );
      $this->gLogger->debug( "User request parameters: [$useReqParams]" );
      
      // Check to see if we need to override global cachable flag
      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $localCachable = $requestParameterModule->get( CacheFile::CACHABLE_ATTRIBUTE_NAME );
      $this->gLogger->debug( "Local cachable: [$localCachable]" );
      if ( $useReqParams && $localCachable != NULL ) {
         $this->gLogger->debug( "Using request parameter value" );
         $localCachable = Utilities::normalizeBoolean( $localCachable );
         $isCachable = $localCachable;
      }

      $this->gLogger->debug( "Cachable generator: [$isCachable]" );
      if ( $isCachable ) {
         $this->gCacheFile = new CacheFile( Environment::$configuration[ 'cacheDir' ] );
         $oldParametersHash = $this->gCacheFile->getParameterHash( $cacheKey );
         $newParameterHash = CacheFile::getHash( $this->gParameters->toString() );
         $this->gLogger->debug( "Old Parameter hash : $oldParametersHash" );
         $this->gLogger->debug( "New Parameter hash : $newParameterHash" );
         $validParameters = ( $oldParametersHash == $newParameterHash );
         $this->gLogger->debug( "Parameters [{$this->gParameters->toString()}] : " . ( ( $validParameters ) ? "valid" : "invalid" ) );
         
         // Check the time stamp of the required files.
         $oldSrcTimeStamp = $this->gCacheFile->getTimeStamp( $cacheKey );
         $this->gLogger->debug( "Time stamp of old input file: $oldSrcTimeStamp" );
         $this->gLogger->debug( "Hash for file [$fullFileName]: $cacheKey" );
      } else {
         $oldParametersHash = 0;
         $validParameters = false;
         $oldSrcTimeStamp = 0;      // Set in case this is not cachable
      }

      $documentDOM = new DomDocument();
      
      try {
         // First of all does the file exist ?
         if ( file_exists( $fullFileName ) ) {
            $srcTimeStamp = filemtime( $fullFileName );
            $this->gLogger->debug( "Time stamp of current file: $srcTimeStamp" );
            $validSrcFile = ( $oldSrcTimeStamp == $srcTimeStamp );
            $this->gLogger->debug( "[$isCachable][$validSrcFile][$validParameters]" );
            if ( $isCachable && $validSrcFile && $validParameters ) {
               // File is up to date so use cached file
               $this->gLogger->debug( "Reading file from cache" );
               $documentDOM = $this->gCacheFile->getCacheDOM( $cacheKey );
               $documentDOM->documentElement->setAttribute( "__file", $cacheKey );
               $documentDOM->documentElement->setAttribute( "__status", CacheFile::CACHE_VALID );
            } else {
               // Check for well-formed etc (no check for valid as that will take too long)
               $this->gLogger->debug( "Reading file from original" );
               if ( !($fileText = file_get_contents( $fullFileName ) ) ) {
                  throw new RunException( "Could not load document string from file '$fullFileName'",
                     PalooseException::SITEMAP_GENERATOR_RUN_ERROR );
               }
               $documentDOM->loadXML( Utilities::xlateEntity( $fileText ) );
               $rootNode = $documentDOM->documentElement;
               $this->gTranslatedDOM = new DOMDocument();
               $elementNS = $rootNode->namespaceURI;
               $this->gLogger->debug( "Creating new root node: <" . $rootNode->nodeName . "> with namespace: '" . $elementNS . "'" );
               $newElement = $this->gTranslatedDOM->createElementNS( $elementNS, $rootNode->nodeName );
               $this->gTranslatedDOM->appendChild( $newElement );
               $this->processNode( $inVariableStack, $rootNode, $newElement );
               // Update the cache array
               $documentDOM = $this->gTranslatedDOM;
               if ( $isCachable ) {
                  $this->gLogger->debug( "Cachable file so updating" );
                  $this->gCacheFile->updateDOM2Cache( $cacheKey, $srcTimeStamp, $documentDOM, $newParameterHash );
               }
               // Make sure we carry the cache status (invalid means not from cache)
               // through to the following parts of the pipeline
               $documentDOM->documentElement->setAttribute( "__file", $cacheKey );
               // Validity depends on the time stamp regardless of where it comes from
               $documentDOM->documentElement->setAttribute( "__status", CacheFile::CACHE_INVALID );
               // Utilities::outputStringVerbatim( $documentDOM->saveXML() );
           }
         } else {
              throw new RunException( "File '$fullFileName' does not exist", PalooseException::SITEMAP_GENERATOR_RUN_ERROR );
         }
      } catch ( RunException $e ) {
           throw new RunException( $e->getMessage(), $e->getCode() );
      }
      return $documentDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Process a single node.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param DOMNode $inParentNode the node whose children we are just about to process
    * @param DOMNode $inNewParentNode the equivalent parent node in the new translated document (reference)
    */

   private function processNode( VariableStack $inVariableStack, DOMNode $inParentNode, DOMNode &$inNewParentNode )
   {
      // Create a new node and update it with the old nodes attributes
      if ( $inParentNode->nodeType == XML_ELEMENT_NODE ) {
         // At this point we have a list of attributes that may need translating to expand variables
         if ( $inParentNode->hasAttributes() ) {
            foreach ( $inParentNode->attributes as $attribute ) {
               $attribName = StringResolver::expandString( $inVariableStack, $attribute->name );
               $attribValue = StringResolver::expandPseudoProtocols( StringResolver::expandString( $inVariableStack, $attribute->value ) );
               $inNewParentNode->setAttribute( $attribName, $attribValue );
               $this->gLogger->debug( "   Attribute '$attribName' = '$attribValue'" );
            }
         }
      }

      // We have transferred the current node at this point - now we must traverse the children of the
      // old documents current node.
      $children = $inParentNode->childNodes;

      foreach ( $children as $child ) {
         $this->gLogger->debug( "Processing child node: <" . $child->nodeName . "> with namespace '" . $child->namespaceURI . "'" );
         if ( $child->nodeType == XML_ELEMENT_NODE ) {
           // Just an ordinary element not in to be translated so make a new element and append it.
           $this->gLogger->debug( "Creating new child element node: <" . $child->localName . ">" );
           $newElementNode = $this->gTranslatedDOM->createElementNS( $child->namespaceURI, $child->nodeName );
           // Now process the child node - calls this recursively
           $this->processNode( $inVariableStack, $child, $newElementNode );
           $inNewParentNode->appendChild( $newElementNode );
         } else if ( $child->nodeType == XML_TEXT_NODE ) {
            // Straight copy by appending to child list of new parent node after doing any
            // necessary expansion.
            $this->gLogger->debug( "Creating new child text node: '" . $child->textContent . "'" );
            $this->gLogger->debug( "   old text: '" . $child->textContent . "'" );
            $expandedContent = StringResolver::expandPseudoProtocols( StringResolver::expandString( $inVariableStack, $child->textContent ) );
            $this->gLogger->debug( "   new node: '" . $expandedContent . "'" );
            $newTextNode = $this->gTranslatedDOM->createTextNode( $expandedContent );
            // $newTextNode = $this->gTranslatedDOM->createTextNode( $child->textContent );
            $inNewParentNode->appendChild( $newTextNode );
         }
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this class.
    *
    * @retval string the representation of the class as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage generation
 */
 
class _PXTemplateGenerator extends Generater
{

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "PXTemplateGenerator";

      // $this->gLogger->debug( "Is cachable: " . $this->gComponent->gIsCachable );

   }

}
?>
