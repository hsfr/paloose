<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage generation
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2008 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/generation/Generator.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Generators</i> maintains a list of the generators.
 * 
 * @package paloose
 * @subpackage generation
 */

class Generators {

   private $gDefault;

   /** Array of generators (each entry a class: Generator) and indexed by name */
   private $gGenerators;

   /** The DOM object for this class */
   private $gDOM;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of Generators.
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the contained generator tags.
    *
    * For example 
    *
    * <pre>  &lt;map:generators default="file">
    *     &lt;map:generator name="file" src="resource://lib/generation/FileGenerator"/>
    *  &lt;/map:generators></pre>
    *
    * The default attribute specifies the type of generator to use if none is specified in a pipeline.
    *
    * @param DOMNode $inNode the DOM that contains the contents of this pipe element.
    * @throws UserException if component does not have <i>name</i> and <i>src</i> attributes
    * @throws UserException if component constructor throws UserException
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM (this will include 
      // enclosed tags such as parameter)
      $this->gDOM = new DOMDocument;
      $this->gDOM->appendChild( $this->gDOM->importNode( $inNode, 1 ) );
   
      // Set the namespace for the sitemap. Let us hope people use it.
      $xPath = new domxpath( $this->gDOM );
      $xPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $component = $xPath->query( "//m:generator" );
      // Scan around each generator and set up the list in $gGenerators. Exit false if problem
      foreach ( $component as $node ) { 
         $name = $node->getAttribute( "name" );
         $src = $node->getAttribute( "src" );
         $isCachable = Utilities::normalizeBoolean( $node->getAttribute( "cachable" ) );
         $this->gLogger->debug( "Adding generator name = '". $name . "' using src '" . $src . "': cachable=" . $isCachable );
         if ( !strlen( $name ) or !strlen( $src ) ) {
            throw new UserException(
               "Component generator must have name and src attributes defined",
               PalooseException::SITEMAP_GENERATOR_PARSE_ERROR,
               $this->gDOM );
         }
         
         // The source may have path details attached so strip the path off to input the correct class.
         if ( strstr( $src, Environment::getPathSeparator() ) == 1 ) {
            $packageFilename = $src;
         } else if ( !strstr( $src, ":" . Environment::getPathSeparator() ) ) {
            // Relative file name so process
            $packageFilename = PALOOSE_LIB_DIRECTORY . Environment::getPathSeparator() . $src;
         } else {
            // Relative file name with pseudo protocol
            $packageFilename = StringResolver::expandPseudoProtocols( $src );
         }

         $package = Utilities::stripFileName( $packageFilename );
         // Must expand pseudo protocols first
         $path = Utilities::stripPath( StringResolver::expandPseudoProtocols( $packageFilename ) );
         $this->gLogger->debug( "Package='". $package . "' : path='" . $path . "'" );
         
         // We have something to add, so first of all check for duplicate names
         if ( $this->gGenerators != NULL and array_key_exists( $name, $this->gGenerators ) ) {
            $this->gLogger->debug( "Overriding generator: '". $name . "'" );
         } else {
            $this->gLogger->debug( "Adding generator: '". $name . "'" );
         }
         require_once( $path . $package . ".php" );
         // Form up the component class name
         $generatorName = "_" . $package;
         $generator = new $generatorName( $name, $path.$package, $node, $isCachable );
         // Check that the generator thinks it is valid
         try {
            $generator->isValid();
            $this->gGenerators[ $name ] = $generator;
         } catch( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $this->gDOM );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set the default generator.
    *
    * @param string $inDefault the default generator name.
    * @exception UserException if cannot find generator
    */

   public function setDefault( $inDefault )
   {
      if ( !( array_key_exists( $inDefault, $this->gGenerators ) ) ) {
           throw new UserException(
              "Default generator '". $inDefault . "' not found",
              PalooseException::SITEMAP_GENERATOR_PARSE_ERROR,
              $this->gDOM );
      }
      $this->gDefault = $inDefault;
      $this->gLogger->debug( "Set default generator: '". $inDefault . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the default generator name.
    *
    * @return string the default generator name.
    */
   public function getDefault()
   {
      return $this->gDefault;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the named generator.
    *
    * @param string $inName the generator name.
    * @retval Generator the generator instance.
    * @exception UserException cannot find generator.
    */

   public function getGenerator( $inName )
   {
      if ( !( array_key_exists( $inName, $this->gGenerators ) ) ) {
           throw new UserException(
              "Generator '". $inName . "' not found",
              PalooseException::SITEMAP_GENERATOR_PARSE_ERROR,
              $this->gDOM );
      } else {
         return $this->gGenerators[ $inName ];
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class of the named generator.
    *
    * @param string $inName the generator name.
    * @retval string the generator instance class name.
    * @exception UserException cannot find generator.
    */

   public function getGeneratorClassName( $inType )
   {
      if ( !( array_key_exists( $inType, $this->gGenerators ) ) ) {
           throw new UserException(
              "Generator '". $inType . "' not found",
              PalooseException::SITEMAP_GENERATOR_PARSE_ERROR,
             $this->gDOM );
      } else {
         $className = $this->gGenerators[ $inType ]->getPackageName();
         return $className;
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return string representation of this class.
    *
    * @retval string the string representation of this component.
    */

   public function toString()
   {
      $mess = "  <generators default='{$this->gDefault}'>\n";
      foreach ( $this->gGenerators as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "  </generators>\n";
      return $mess;
   }

}

?>
