<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage generation
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/generation/GeneratorPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

define( "SORT_NAME", "name" );
define( "SORT_SIZE", 'size' );
define( "SORT_LAST_MODIFIED", 'lastmodified' );
define( "SORT_DIRECTORY", 'directory' );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The directory generator reads a directory of files and returns the
 * filename and their details.
 *
 * @package paloose
 * @subpackage generation
 */
    
class DirectoryGenerator extends GeneratorPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */
   private $gLogger;
   
   /** The depth that the scan should be done */
   private $gDepth = 1;

   /** The directory where we started */
   private $gStartDir = 1;

   /** Sets the format for the date attribute of each node as described in
   . If unset, the default format for the current locale will be used.*/
   private $gDateFormat = ""; 
   
   /** Reverses the sort order */
   private $gReverse = false;

   /** Specifies the directories and files that should be included. Also a regular expression. */
   private $gInclude = "/.*/";

   /** Specifies the directories and files that should be excluded. Also a regular expression. */
   private $gExclude = "";

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of DirectoryGenerator.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _TRAXTransformer $inComponent the associated component instance (stores parameters etc)
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The file generator merely reads directory and generates a DOM of it.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval The document DOM representing the input.
    * @throws RunException if an error occurs
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Running DirectoryGenerator with raw '" . $this->gSrc . "'" );
      $expandedString = StringResolver::expandString( $inVariableStack, $this->gSrc );
      $this->gLogger->debug( "Result after substitution: '$expandedString'" );
      $expandedString = StringResolver::expandPseudoProtocols( $expandedString );
      $this->gLogger->debug( "Result after pseudo protocol expansion: '$expandedString'" );
      $this->gStartDir = $expandedString;
      
      if ( $this->gParameters->getParameter( 'dateFormat' ) != "" ) {
         $this->gDateFormat = $this->gParameters->getParameter( 'dateFormat' );
      }
      if ( $this->gParameters->getParameter( 'depth' ) != "" ) {
         $this->gDepth = $this->gParameters->getParameter( 'depth' );
      }
      if ( $this->gParameters->getParameter( 'reverse' ) != "" ) {
         $this->gReverse = $this->gParameters->getParameter( 'reverse' );
      }
      if ( $this->gParameters->getParameter( 'include' ) != "" ) {
         $this->gInclude = $this->gParameters->getParameter( 'include' );
      }
      if ( $this->gParameters->getParameter( 'exclude' ) != "" ) {
         $this->gExclude = $this->gParameters->getParameter( 'exclude' );
      }

      // Where the directory structure will be built
      $documentDOM = new DomDocument();
      // First of all does the directory exist and is it a directory (not a file)
      if ( file_exists( $this->gStartDir ) && is_dir( $this->gStartDir ) ) {
         $rootElement = $documentDOM->appendChild( $documentDOM->createElementNS( Environment::$configuration[ 'dirNamespace' ], 'dir:directory' ) );
         $rootElement->setAttribute( "name" , $this->gStartDir );
         $rootElement->setAttribute( "lastmodified" , $this->getLastModified($this->gStartDir) );
         $rootElement->setAttribute( "reverse" , $this->gReverse );
         $rootElement->setAttribute( "requested" , "true" );
         $rootElement->setAttribute( "size" , filesize( $this->gStartDir ) );
         $childElement = $this->scanDirectory( $documentDOM, $rootElement, $this->gStartDir, $this->gDepth );
         $rootElement->appendChild( $childElement );
      } else {
           throw new RunException( "File '{$this->gStartDir}' does not exist", PalooseException::SITEMAP_GENERATOR_RUN_ERROR );
      }
      return $documentDOM;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Scan directory at this level.
    *
    */

   private function scanDirectory( $inDOMDocument, $inParentElement, $inDirectory, $inLevel )
   {
      $this->gLogger->debug( "Scanning directory: '{$inDirectory}'" );
      $inLevel--;
      $element = NULL;
      // Set up directory node in the DOM
      $dirFileListing = scandir( $inDirectory, ( $this->gReverse === false ) ? 0 : 1 );
      foreach( $dirFileListing as $key => $value ) {
         $fullName = "$inDirectory/$value";
         if ( $value == '.'
               || $value == '..'
               || ( strlen( $this->gInclude ) > 0 && preg_match( $this->gInclude, $value ) == 0 )
               || ( strlen( $this->gExclude ) > 0 && preg_match( $this->gExclude, $value ) != 0 ) ) {
            // Do nothing
         } else if ( is_dir( $fullName ) ) {
            $this->gLogger->debug( "Found directory: '$fullName'" );
            $element = $inParentElement->appendChild( $inDOMDocument->createElementNS( Environment::$configuration[ 'dirNamespace' ], 'dir:directory' ) );
            $element->setAttribute( "name" , $fullName );
            $element->setAttribute( "lastmodified" , $this->getLastModified( $fullName ) );
            $element->setAttribute( "size" , filesize( $fullName ) );
            if ( $inLevel > 0 ) {
               $scanString = $this->scanDirectory( $inDOMDocument, $element, $fullName, $inLevel );
            } else {
               $scanString = "";
            }
         } else {
            $this->gLogger->debug( "Found file: '$fullName'" );
            $element = $inParentElement->appendChild( $inDOMDocument->createElementNS( Environment::$configuration[ 'dirNamespace' ], 'dir:file' ) );
            $element->setAttribute( "name" , $value );
            $element->setAttribute( "lastmodified" , $this->getLastModified( $fullName ) );
            $element->setAttribute( "size" , filesize( $fullName ) );
         }
      }
      if ( $element == NULL ) {
		   $element = $inParentElement->appendChild( $inDOMDocument->createElementNS( Environment::$configuration[ 'dirNamespace' ], 'dir:empty' ) );
	   }
      return $element;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get last modified time for this dir/file.
    *
    * @param string $inDirectory the directory to retrieve the time of.
    * @retval the formatted time.
    */

   private function getLastModified( $inDirectory )
   {
      return date( $this->gDateFormat, filemtime( $inDirectory ));
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 *
 * There will be only one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage generation
 */
 
class _DirectoryGenerator extends Generater {

   /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
       $this->gLogger = Logger::getLogger( __CLASS__ );

      $this->gPackageName = "DirectoryGenerator";
   }

}
?>
