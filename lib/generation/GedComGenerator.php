<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage generation
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/caching/CacheFile.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/generation/GeneratorPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/external/class_xml_check.php" );

require_once( PALOOSE_LIB_DIRECTORY . "/gedcom/GedCom.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/GedComException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The file generator reads a GedCom file and generates a DOM of it.
 *
 * @package paloose
 * @subpackage generation
 */
    
class GedComGenerator extends GeneratorPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of GedComGenerator.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (the file name)
    * @param _FileTransformer $inComponent the associated component instance (stores parameters etc)
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gLogger->debug( "Constructing cachable generator: [{$this->gComponent->gIsCachable}]" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The GEDCOM generator reads a GEDCOM file and generates a DOM representation of
    * it. If no errors it puts the DOM in gOutputDOM and returns true.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval DOMDocument the document DOM representing the input.
    * @throws RunException if an error occurs (no such file or could not parse file).
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      parent::run( $inVariableStack, $inURL, $inQueryString, $inDOM );

      // Translate the string and expand the various inline strings, {1} etc
      $this->gLogger->debug( "Raw src: '" . $this->gSrc . "'" );
      $fullFileName = StringResolver::expandString( $inVariableStack, $this->gSrc );
      $this->gLogger->debug( "Result after substitution: '$fullFileName'" );
      $fullFileName = StringResolver::expandPseudoProtocols( $fullFileName );
      $this->gLogger->debug( "Result after pseudo protocol expansion: '$fullFileName'" );
      $cacheKey = CacheFile::getHash( $fullFileName );

      $isCachable = $this->gComponent->gIsCachable;
      $this->gLogger->debug( "Global cachable: [{$this->gComponent->gIsCachable}]" );
      $useReqParams = Utilities::normalizeBoolean( $this->gComponent->getUseRequestParametersFlag() );
      $this->gLogger->debug( "User request parameters: [$useReqParams]" );
      
      // Check to see if we need to override global cachable flag
      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $localCachable = $requestParameterModule->get( CacheFile::CACHABLE_ATTRIBUTE_NAME );
      $this->gLogger->debug( "Local cachable: [$localCachable]" );
      if ( $useReqParams && $localCachable != NULL ) {
         $this->gLogger->debug( "Using request parameter value" );
         $localCachable = Utilities::normalizeBoolean( $localCachable );
         $isCachable = $localCachable;
      }

      $this->gLogger->debug( "Cachable generator: [$isCachable]" );
      if ( $isCachable ) {
         $this->gCacheFile = new CacheFile( Environment::$configuration[ 'cacheDir' ] );
         $oldParametersHash = $this->gCacheFile->getParameterHash( $cacheKey );
         $newParameterHash = CacheFile::getHash( $this->gParameters->toString() );
         $this->gLogger->debug( "Old Parameter hash : $oldParametersHash" );
         $this->gLogger->debug( "New Parameter hash : $newParameterHash" );
         $validParameters = ( $oldParametersHash == $newParameterHash );
         $this->gLogger->debug( "Parameters [{$this->gParameters->toString()}] : " . ( ( $validParameters ) ? "valid" : "invalid" ) );
         
         // Check the time stamp of the required files.
         $oldSrcTimeStamp = $this->gCacheFile->getTimeStamp( $cacheKey );
         $this->gLogger->debug( "Time stamp of old input file: $oldSrcTimeStamp" );
         $this->gLogger->debug( "Hash for file [$fullFileName]: $cacheKey" );
      } else {
         $oldParametersHash = 0;
         $validParameters = false;
         $oldSrcTimeStamp = 0;      // Set in case this is not cachable
      }

      $documentDOM = new DomDocument();
      
      try {
         // First of all does the file exist ?
         if ( file_exists( $fullFileName ) ) {
            $srcTimeStamp = filemtime( $fullFileName );
            $this->gLogger->debug( "Time stamp of current file: $srcTimeStamp" );
            $validSrcFile = ( $oldSrcTimeStamp == $srcTimeStamp );
            $this->gLogger->debug( "[$isCachable][$validSrcFile][$validParameters]" );
            if ( $isCachable && $validSrcFile && $validParameters ) {
               // File is up to date so use cached file
               $this->gLogger->debug( "Reading file from cache" );
               $documentDOM = $this->gCacheFile->getCacheDOM( $cacheKey );
               $documentDOM->documentElement->setAttribute( "__file", $cacheKey );
               $documentDOM->documentElement->setAttribute( "__status", CacheFile::CACHE_VALID );
            } else {
               // Check for well-formed etc (no check for valid as that will take too long)
               $this->gLogger->debug( "Reading file from original" );
               $gedcom = new GedCom( $fullFileName );
               $gedcom->processFile();
               $documentDOM->loadXML( rtrim( $gedcom->toXMLString() ) );
               // Utilities::outputStringVerbatim( $documentDOM->saveXML() );
               if ( $isCachable ) {
                  $this->gLogger->debug( "Cachable file so updating" );
                  $this->gCacheFile->updateDOM2Cache( $cacheKey, $srcTimeStamp, $documentDOM, $newParameterHash );
               }
               // Make sure we carry the cache status (invalid means not from cache)
               // through to the following parts of the pipeline
               $documentDOM->documentElement->setAttribute( "__file", $cacheKey );
               // Validity depends on the time stamp regardless of where it comes from
               $documentDOM->documentElement->setAttribute( "__status", CacheFile::CACHE_INVALID );
               // Utilities::outputStringVerbatim( $documentDOM->saveXML() );
           }
         } else {
              throw new RunException( "File '$fullFileName' does not exist", PalooseException::SITEMAP_GENERATOR_RUN_ERROR );
         }
      } catch ( RunException $e ) {
        throw new RunException( $e->getMessage(), $e->getCode() );
      } catch ( GedComException $e ) {
        throw new RunException( $e->getMessage(), $e->getCode(), NULL );
     }
     return $documentDOM;
  }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this GedComGenerator.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      return parent::toStringWithType( $this->gGenericType );
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage generation
 */
 
class _GedComGenerator extends Generater {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of this component.
    *
    * Only package name is set here, all else is done in parent class. The <i>name</i> and
    * <i>src</i> are taken from the sitemap component declaration.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
       $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "GedComGenerator";
   }

}
?>
