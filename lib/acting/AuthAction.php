<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage acting
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/Action.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/AuthenticationActions.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The <i>AuthAction</i> action is part of the authentication mechanism and is used to
    * protect a pipeline.
    *
    * <pre>&lt;map:match pattern="**.html">
    *    &lt;map:act type="auth-protect">
    *       &lt;map:parameter name="handler" value="adminHandler"/>
    *       &lt;map:parameter name="application" value="adminApp"/>
    *       &lt;map:aggregate element="root" label="aggr-content">
    *          &lt;map:part src="cocoon:/headings.xml" element="headings" strip-root="true"/>
    *          &lt;map:part src="cocoon:/menus.xml" element="menus" strip-root="true"/>
    *          &lt;map:part src="cocoon:/newsArticles.xml" element="news-articles" strip-root="true"/>
    *          &lt;map:part src="cocoon:/{1}.xml" element="content" strip-root="true"/>
    *       &lt;/map:aggregate>
    *       &lt;map:call name="outputPage"/>
    *    &lt;/map:act>
    * &lt;/map:match></pre>
    *
    * @package paloose
    * @subpackage acting
    */

class AuthAction extends AuthenticationActions
{

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of <i>AuthAction</i>.
    *
    * This also parses the enclosed pipeline.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _AuthAction $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if problem
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Run the authorization action.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM (normal NULL).
    * @retval DOMDocument the transformed DOM.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      parent::run( $inVariableStack, $inURL, $inQueryString, $inDOM );
      // Make sure that a session is in progress
      $user = Environment::$sessionHandler->start();

      // First of all check to see whether this user is valid by asking the authentication manager.
      $userAuthorised = $this->gAuthenticationManager->isUserAuthorised();
      try {
         if ( $userAuthorised === false ) {
            // Need to authenticate use via login
            if ( $this->gAuthenticationManager->authenticateUser( $this->gHandler ) ) {
               // Authentication successful
               $this->gLogger->debug( "Authentication successful" );
            } else {
               // Authentication failed so pass back to pipeline outside the action
               // In the normal course of events the authorisation mechanism should deal
               // with failures.
               $this->gLogger->debug( "Authorisation failure" );
               return $inDOM;
            }
         } else {
            // Need to run what is in the <act ...> pipe
            $sessionModule = Environment::$modules[ 'session' ];
            $this->gLogger->debug( "User " . $sessionModule->get( "__username" ) . " authorised to run pipe" );
            $this->gPipe->run( $inVariableStack, $inURL, $inQueryString, $inDOM );
            // This completes the processing for this action and therefore we can safely exit.
            throw new ExitException();
         }
       } catch ( ExitException $e ) {
         throw new ExitException( $e );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode() );
      }
      return NULL;
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for <i>AuthAction</i>.
 * 
 * There will be only
 * one instance of this for each declaration of this component. For example
 * declare it in the sitemap
 *
 *
 * @package paloose
 * @subpackage acting
 */
 
class _AuthAction extends Action {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make new instance of component.
    *
    * Only package name is set here, all else is done in parent class.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );

      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "AuthAction";

      $parameterDOM = new DOMDocument;
      $parameterDOM->appendChild( $parameterDOM->importNode( $inParameterNode, 1 ) );
  }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string.
    *
    * @retval string the string representation of this class instance.
    */

   public function toString()
   {
      if ( $this->gParameters != NULL or strlen( $this->gSMTPHost ) > 0 or strlen( $this->gSMTPUser ) > 0 or strlen( $this->gSMTPPassword ) > 0 ) {
         $mess = "   <action name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'>\n";
         if ( $this->gParameters != NULL ) {
            while ( list( $name, $value ) = each( $this->gParameters ) ) {
               $mess .= "    <parameter name='$name' value='$value'/>\n";
            }
            reset( $this->gParameters ); //Insurance
         }
         $mess .= "   </action>\n";
      } else {
         $mess = "   <action name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'/>\n";
      }
      return $mess;
   }

}
?>