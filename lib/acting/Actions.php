<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage acting
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/transforming/Transformer.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>Actions</i> class maintains a list of actions.
 *
 * @package paloose
 * @subpackage acting
 */
 
class Actions {

   /** Array of Actions (each entry a class: Action) and indexed by name */
   private $gActions;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Create an instance of Actions class.
    */
    
   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gActions = array();

   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the contained action tags.
    * 
    * For example:
    *
    * <pre>      &lt;map:actions>
    *     &lt;map:action name="sendmail" src="resource://lib/acting/SendMailAction"/>
    *     &lt;map:action name="auth-protect" src="resource://lib/acting/AuthAction"/>
    *     &lt;map:action name="auth-login" src="resource://lib/acting/LoginAction"/>
    *     &lt;map:action name="auth-logout" src="resource://lib/acting/LogoutAction"/>
    *     &lt;map:action name="auth-loggedIn" src="resource://lib/acting/LoggedInAction"/>
    * &lt;/map:actions></pre>
    *
    * @param DOMNode $inNode the DOM that contains the contents of this pipe element.
    * @throws UserException if component does not have <i>name</i> and <i>src</i> attributes
    * @throws UserException if component constructor throws UserException
    */

   public function parse( $inNode )
   {
      // First thing is make node into a local DOM
      $actionsDom = new DOMDocument;
      $actionsDom->appendChild( $actionsDom->importNode( $inNode, 1 ) );

      // Set the namespace for the sitemap. Let us hope people use it.
      $xpath = new domxpath( $actionsDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $component = $xpath->query( "//m:action" );
      
      // Scan around each action and set up the list in $gActions. Exit false if problem
      foreach ( $component as $node ) { 
         $name = $node->getAttribute( "name" );
         $src = $node->getAttribute( "src" );
         $isCachable = Utilities::normalizeBoolean( $node->getAttribute( "cachable" ) );
         
         $this->gLogger->debug( "Adding action name = '". $name . "' using src '" . $src . "'" );
         if ( !strlen( $name ) or !strlen( $src ) ) {
            $actionDom = new DOMDocument;
            $actionDom->appendChild( $actionDom->importNode( $node, 1 ) );
            throw new UserException( "Sitemap component action must have name and src attributes defined",
                                     PalooseException::SITEMAP_ACTION_PARSE_ERROR,
                                     $actionDom );
         }

         // The source may have path details attached so strip the path off to input the correct class.
         if ( strstr( $src, Environment::getPathSeparator() ) == 1 ) {
            $packageFilename = $src;
         } else if ( !strstr( $src, ":" . Environment::getPathSeparator() ) ) {
            // Relative file name so process
            $packageFilename = PALOOSE_LIB_DIRECTORY . Environment::getPathSeparator() . $src;
         } else {
            // Relative file name with pseudo protocol
            $packageFilename = StringResolver::expandPseudoProtocols( $src );
         }
         
         // The source may have path details attached (for example "../lib/acting/SendMailAction"). We need to
         // strip the path off to input the correct class.
         $package = Utilities::stripFileName( $packageFilename );
         // Make sure pseudo protocols are expanded first (should be done above - this s just insurance)
         $path = Utilities::stripPath( StringResolver::expandPseudoProtocols( $packageFilename ) );
         $this->gLogger->debug( "Package='". $package . "' : path='" . $path . "'" );
         
         // We have something to add, so first of all check for duplicate names
         if ( array_key_exists( $name, $this->gActions ) ) {
            $this->gLogger->debug( "Overriding action: '". $name . "'" );
            // This bit taken out when overriding actions was removed
            /* $actionDom = new DOMDocument;
            $actionDom->appendChild( $actionDom->importNode( $node, 1 ) );
            throw new UserException( "Duplicate action name '" . $name . "' found", PalooseException::SITEMAP_ACTION_PARSE_ERROR, $actionDom ); */
         } else {
            $this->gLogger->debug( "Adding action: '". $name . "'" );
         }            
         // Get the necessary for forming a specific action
         require_once( $path . $package. ".php" );
         $actionName = "_" . $package;
         $action = new $actionName( $name, $path.$package, $node, $isCachable );
         // Check that the action thinks it is valid
         try {
            $action->isValid();
            $this->gActions[ $name ] = $action;
         } catch( UserException $e ) {
            $actionDom = new DOMDocument;
            $actionDom->appendChild( $actionDom->importNode( $node, 1 ) );
            throw new UserException( $e->getMessage(), $e->getCode(), $actionDom );
         }
      }
   }

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the named action. Will cause exception if not found in list.
    *
    * @param string $inName the action name.
    * @retval Action the action instance.
    * @exception UserException if cannot find action.
    */

   public function getAction( $inName )
   {
      if ( !( array_key_exists( $inName, $this->gActions ) ) ) {
           throw new UserException( "Action name '". $inName . "' not found", PalooseException::SITEMAP_ACTION_PARSE_ERROR, NULL );
      } else {
         return $this->gActions[ $inName ];
      }
   }

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the named action class name. Will cause exception if not found in list.
    *
    * @param string $inName the action name.
    * @retval Action the action instance.
    * @exception UserException if cannot find action.
    */

   public function getActionClassName( $inType )
   {
      if ( !( array_key_exists( $inType, $this->gActions ) ) ) {
           throw new UserException( "Action type '". $inType . "' not found", PalooseException::SITEMAP_ACTION_PARSE_ERROR, NULL );
      } else {
         return $this->gActions[ $inType ]->getPackageName();
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this Class.
    *
    * @retval string the representation of the Class as a string
    *
    */

   public function toString()
   {
      $mess = "  <actions>\n";
      foreach ( $this->gActions as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "  </actions>\n";
      return $mess;
   }

}

?>
