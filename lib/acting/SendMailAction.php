<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage acting
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/Action.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/ActionPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The SendMail action allows email messages to be constructed and sent within
 * the pipeline.
 * 
 * <pre>&lt;map:act type="sendmail">
 *         &lt;map:parameter name="smtp-host"   value="{request-param:smtphost}"/>
 *        &lt;map:parameter name="smtp-user"   value="{request-param:smtpuser}"/>
 *        &lt;map:parameter name="smtp-password" value="{request-param:smtppassword}"/>
 *        &lt;map:parameter name="from"        value="{request-param:from}"/>
 *        &lt;map:parameter name="to"          value="{request-param:to}"/>
 *        &lt;map:parameter name="subject"     value="{request-param:subject}"/>
 *        &lt;map:parameter name="body"        value="{request-param:body}"/>
 *        &lt;map:parameter name="cc"          value="{request-param:cc}"/>
 *        &lt;map:parameter name="bcc"         value="{request-param:bcc}"/>
 *        &lt;map:parameter name="attachments" value="attachment cocoon:///context://welcome.xml"/>
 *
 *        &lt;map:call resource="show-page">
 *           &lt;map:parameter name="target" value="done.xml"/>
 *           &lt;map:parameter name="remove" value="{0}"/>
 *        &lt;/map:call>
 *     &lt;/map:act></pre>
 *
 * @package paloose
 * @subpackage acting
 */

class SendMailAction extends ActionPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   private $gSMTPHost;
   
   private $gSMTPUser;
   
   private $gSMTPPassword;

   private $gFrom;

   private $gTo;

   private $gSubject;

   private $gBody;

   private $gCC;

   private $gBCC;

   private $gAttachments;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of <i>SendMailAction</i>.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _SendMailAction $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if problem
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gSMTPUser = $this->gComponent->gSMTPUser;
      $this->gSMTPHost = $this->gComponent->gSMTPHost;
      $this->gSMTPPassword = $this->gComponent->gSMTPPassword;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The XSL Transformer processes the input DOM (in $inDOM) and returns a
    * transformed DOM.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM (normal NULL).
    * @retval DOMDocument the transformed DOM.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Running SendMailAction" );
      $smtpHost = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "smtp-host" ) );
      $this->gLogger->debug( "smtp-host: $smtpHost" );
      $smtpUser = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "smtp-user" ) );
      $this->gLogger->debug( "smtp-user: $smtpUser" );
      $smtpPassword = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "smtp-password" ) );
      $this->gLogger->debug( "smtp-password: $smtpPassword" );
      $from = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "from" ) );
      $this->gLogger->debug( "from: $from" );
      $to = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "to" ) );
      $this->gLogger->debug( "to: $to" );
      $subject = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "subject" ) );
      $this->gLogger->debug( "subject: $subject" );
      $message = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "body" ) );
      $this->gLogger->debug( "body: $message" );
      $cc = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "cc" ) );
      $this->gLogger->debug( "cc: $cc" );
      $bcc = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "bcc" ) );
      $this->gLogger->debug( "bcc: $bcc" );
      $attachments = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "attachments" ) );
      $this->gLogger->debug( "attachments: $attachments" );
      //Assemble headers for mail function
      $headers = "From: $from" . "\r\n" .
                  "Cc: $cc" . "\r\n" .
                  "Bcc: $bcc" . "\r\n";
      
      //Now that we have all the ingredients for the email then send it
      $success = mail( $to, $subject, $message, $headers );
      
      try {
         if ( $success == 1 ) {
            $this->gLogger->debug( "Mail sent success" );
            $this->gPipe->run( Environment::$variableStack, $inURL, $inQueryString, $inDOM );
            //This completes the processing for this action and therefore we can safely exit. However I am not
            //100% sure about this as there is the case of act in act conditions. I will need to look at this
            //more carefully.
            throw new ExitException();
         } else {
            $this->gLogger->debug( "Mail sent failure" );
            //Pass back directly out and run (if there) remaining pipeline in matcher/act
            return $inDOM;
         }
      } catch ( ExitException $e ) {
         throw new ExitException();
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode() );
      }
      return NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      if ( $this->gPipe != null or $this->gParameters != NULL ) {
         $mess = "     <act type='{$this->gType}'>\n";
          if ( $this->gParameters != NULL ) {
            $mess .= $this->gParameters->toString();
         }
          if ( $this->gPipe != NULL ) {
            $mess .= $this->gPipe->toString();
         }
         $mess .= "     </act>\n";
         return $mess;
      } else {
         $mess = "   <act type='{$this->gType}'/>\n";
      }
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declarion of this component. For exmaple
 * declare it in the sitemap
 *
 * <pre>
 *    &lt;map:action name="sendmail" src="resource://lib/acting/SendMailAction">
 *       &lt;smtp-host>hsfr.org.uk&lt;/smtp-host>
 *       &lt;smtp-user>hsfr&lt;/smtp-user>
 *       &lt;smtp-password>******&lt;/smtp-password>
 *    &lt;/map:action></pre>
 *
 * @package paloose
 * @subpackage acting
 */
 
class _SendMailAction extends Action {

    /** Logger instance for this class */
   private $gLogger;
   
   public $gSMTPHost;
   
   public $gSMTPUser;
   
   public $gSMTPPassword;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make new instance of component.
    *
    * Only package name is set here, all else is done in parent class.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );

      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gSMTPHost = "";
      $this->gSMTPUser = "";
      $this->gSMTPPassword = "";
      $this->gPackageName = "SendMailAction";

      $parameterDOM = new DOMDocument;
      $parameterDOM->appendChild( $parameterDOM->importNode( $inParameterNode, 1 ) );

      $xpath = new domxpath( $parameterDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      
      $this->gSMTPHost = Utilities::getXPathListStringItem( 0, $xpath, "//smtp-host" );
      $this->gLogger->debug( "smtp-host: '" . $this->gSMTPHost . "'" );
      $this->gSMTPUser = Utilities::getXPathListStringItem( 0, $xpath, "//smtp-user" );
      $this->gLogger->debug( "smtp-user: '" . $this->gSMTPUser . "'" );
      $this->gSMTPPassword = Utilities::getXPathListStringItem( 0, $xpath, "//smtp-password" );
      $this->gLogger->debug( "smtp-password: '" . $this->gSMTPPassword . "'" );
  }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string. This is used purely for debugging purposes.
    *
    * @retval the string representation of this class instance.
    */

   public function toString()
   {
      if ( $this->gParameters != NULL or strlen( $this->gSMTPHost ) > 0 or strlen( $this->gSMTPUser ) > 0 or strlen( $this->gSMTPPassword ) > 0 ) {
         $mess = "   <action name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'>\n";
         if ( $this->gParameters != NULL ) {
            while ( list( $name, $value ) = each( $this->gParameters ) ) {
               $mess .= "    <parameter name='$name' value='$value'/>\n";
            }
            reset( $this->gParameters ); //Insurance
         }
         //Action specific parameters
         if ( strlen( $this->gSMTPHost ) > 0 ) {
            $mess .= "      <smtp-host>{$this->gSMTPHost}</smtp-host>\n";
         }
         if ( strlen( $this->gSMTPUser ) > 0 ) {
            $mess .= "      <smtp-user>{$this->gSMTPUser}</smtp-user>\n";
         }
         if ( strlen( $this->gSMTPPassword ) > 0 ) {
            $mess .= "      <smtp-password>{$this->gSMTPPassword}</smtp-password>\n";
         }
         $mess .= "   </action>\n";
      } else {
         $mess = "   <action name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'/>\n";
      }
      return $mess;
   }

}
?>