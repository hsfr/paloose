<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage acting
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/Action.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>ActionPipeElement</i> stores  the pipeline within an action..
 *
 * @package paloose
 * @subpackage acting
 */

class ActionPipeElement extends PipeElement implements PipeElementInterface
{
   
   /** Logger instance for this class */
   private $gLogger;
   
   /** The pipe that this action encloses */
   protected $gPipe;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Construct a new instance of <i>ActionPipeElement</i>.
    *
    * This also parses the enclosed pipeline.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _AuthAction $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if problem
    */
   
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gGenericType = PipeElement::ACTION;
      $this->gSrc = $inSrc;
      $this->gType = $inType;
      $this->gComponent = $inComponent;
      
      if ( strlen( $inType ) == 0 ) {
         throw new UserException( "Must have a type attribute present", PalooseException::SITEMAP_ACTION_PARSE_ERROR, $inDOM );
      }
      
      // Extract the parts to be used from the inDOM
      $xpath = new domxpath( $inDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      // Having sorted out the parameters there remains a pipeline set to deal with.
      // The following act of barbarism just selects the pipeline nodes
      $pipeElements = $xpath->query( "*[ not( local-name() = 'parameter') ]" );
      $this->gLogger->debug( "Making new Pipe with " . count( $pipeElements ). " components" );
      $this->gPipe = new Pipe();
      
      foreach ( $pipeElements as $pipeNode ) {
         $name = $pipeNode->localName;
         $this->gLogger->debug( "pipe node: '". $name . "'" );
         // This is where we build a pipe. The validity of these pipes is carried out in that class (Pipe)
         switch ( $name ) {
               
            case "aggregate" :
               try {
                  $aggregator = PipeElement::createAggregator( $pipeNode );
                  $this->gLogger->debug( "Creating aggregator using component '" . get_class( $aggregator ) . "'" );
                  $this->gPipe->addComponent( $aggregator );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "generate" :
               try {
                  $generator = PipeElement::createGenerator( $pipeNode );
                  $this->gLogger->debug( "Creating generator: type='" . $generator->getType() . "' src='" . $generator->getSrc() . "' using component '" . get_class( $generator ) . "'" );
                  $this->gPipe->addComponent( $generator );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "transform" :
               try {
                  $transformer = PipeElement::createTransformer( $pipeNode );
                  $this->gLogger->debug( "Creating transformer: type='" . $transformer->getType() . "' src='" . $transformer->getSrc() . "' using component '" . get_class( $transformer ) . "'" );
                  $this->gPipe->addComponent( $transformer );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "serialize" :
               try {
                  $serializer = PipeElement::createSerializer( $pipeNode );
                  $this->gLogger->debug( "Creating serializer: type='" . $serializer->getType() . "' src='" . $serializer->getSrc() . "' using component '" . get_class( $serializer ) . "'" );
                  $this->gPipe->addComponent( $serializer );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "read" :
               try {
                  $reader = PipeElement::createReader( $pipeNode );
                  $this->gLogger->debug( "Creating reader: type='" . $reader->getType() . "' src='" . $reader->getSrc() . "' using component '" . get_class( $reader ) . "'" );
                  $this->gPipe->addComponent( $reader );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "mount" :
               try {
                  $mount = PipeElement::createMount( $pipeNode );
                  $this->gLogger->debug( "Creating mount" );
                  $this->gPipe->addComponent( $mount );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "select" :
               try {
                  $selector = PipeElement::createSelector( $pipeNode );
                  $this->gLogger->debug( "Creating selector: type='" . $selector->getType() . "' using component '" . get_class( $selector ) . "'" );
                  $this->gPipe->addComponent( $selector );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "call" :
               try {
                  $call = PipeElement::createCall( $pipeNode );
                  $this->gLogger->debug( "Creating call: name='" . $call->getResourceName() . "' using component '" . get_class( $call ) . "'" );
                  $this->gPipe->addComponent( $call );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "act" :
               try {
                  $action = PipeElement::createAction( $pipeNode );
                  $this->gLogger->debug( "Creating action: name='" . $action->getType() . "' using component '" . get_class( $action ) . "'" );
                  $this->gPipe->addComponent( $action );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "redirect-to" :
               if ( $inComponent->getPackageName() == 'AuthAction' or $inComponent->getPackageName() == 'SendMailAction' ) {
                  throw new UserException( "Cannot have a redirect within '" . $inComponent->getPackageName() . "'", PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR, $inDOM );
                  break;
               } else {
                  try {
                     $redirect = PipeElement::createRedirect( $pipeNode );
                     $this->gLogger->debug( "Creating redirect-to: '" . $redirect->getURI() . "'" );
                     // echo "Creating redirect using component '" . get_class( $redirect ) . "'";
                     $this->gPipe->addComponent( $redirect );
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  }
                  break;
               }
         }
         
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Run this component in the pipeline.
    *
    * Always overridden.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval The document DOM representing the input.
    */
   
   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      return $inDOM;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
   
   public function toString()
   {
      if ( $this->gPipe != null or $this->gParameters != NULL ) {
         $mess = "     <act type='{$this->gType}'>\n";
         if ( $this->gParameters != NULL ) {
            $mess .= $this->gParameters->toString();
         }
         if ( $this->gPipe != NULL ) {
            $mess .= $this->gPipe->toString();
         }
         $mess .= "     </act>\n";
         return $mess;
      } else {
         $mess = "   <act type='{$this->gType}'/>\n";
      }
   }
   
}
?>
