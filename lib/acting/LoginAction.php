<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage acting
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/Action.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/AuthenticationActions.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>LoginAction</i> action is part of the authentication mechanism.
 *
 * @package paloose
 * @subpackage acting
 */

class LoginAction extends AuthenticationActions
{

    /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make instance of LoginAction.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _AuthAction $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if problem
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Run the login action.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM (normal NULL).
    * @retval DOMDocument the transformed DOM.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      parent::run( $inVariableStack, $inURL, $inQueryString, $inDOM );
      try {
         $this->gLogger->debug( "Runnning log-in ----------------------" );
         // If we get here the user is not logged in already
         $user = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "username" ) );
         $password = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( "password" ) );
         $this->gLogger->debug( "username: '$user'" );
         $this->gLogger->debug( "password: '$password'" );

         // We need to run the internal pipeline for authorisation and get the results back into a DOM
         // Get the authorisation handler 
         $authorizationDOM = $this->gAuthenticationManager->checkAuthorization( $this->gHandler, $user, $password );
     
         $xpath = new domxpath( $authorizationDOM );
         $dataNodes = $xpath->query( "/authentication/@verify" );
         $isUserVerified = Utilities::getXPathListStringItem( 0, $xpath, "/authentication/@verify" );
         $this->gLogger->debug(  "Is user verified: $isUserVerified" );

         if ( $isUserVerified == "true" ) {
            // User is authorized so start session. Make all the data in the authorisation
            // structure available as session variables, '__username', etc.
            
            $username = Utilities::getXPathListStringItem( 0, $xpath, "/authentication/users/user/username" );
            $this->gLogger->debug(  "Session user: $username" );
            Environment::$sessionHandler->setVar( '__username', $username );
            Environment::$sessionHandler->start();

            // Need to run what is in this classes pipe
            $this->gLogger->debug(  "Run login pipe" );
            $this->gPipe->run( $inVariableStack, $inURL, $inQueryString, $inDOM );
            // This completes the processing for this action and therefore we can safely exit.
            throw new ExitException();
         } else {
            $this->gLogger->debug(  "User '$user' not authorized" );
            // Run the associated code after the action so pass on the inputted DOM
            return $inDOM;
         }
      } catch ( ExitException $e ) {
         throw new ExitException();
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode() );
      }
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 *
 * There will be only
 * one instance of this for each declarion of this component. For exmaple
 * declare it in the sitemap
 *
 * @package paloose
 * @subpackage acting
 */
 
class _LoginAction extends Action {

    /** Logger instance for this class */
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make new instance of component.
    *
    * Only package name is set here, all else is done in parent class.
    *
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );

      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gSMTPHost = "";
      $this->gSMTPUser = "";
      $this->gSMTPPassword = "";
      $this->gPackageName = "LoginAction";

      $parameterDOM = new DOMDocument;
      $parameterDOM->appendChild( $parameterDOM->importNode( $inParameterNode, 1 ) );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class instance represented as a string. This is used purely for debugging purposes.
    *
    * @retval the string representation of this class instance.
    */

   public function toString()
   {
      if ( $this->gParameters != NULL or strlen( $this->gSMTPHost ) > 0 or strlen( $this->gSMTPUser ) > 0 or strlen( $this->gSMTPPassword ) > 0 ) {
         $mess = "   <action name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'>\n";
         if ( $this->gParameters != NULL ) {
            while ( list( $name, $value ) = each( $this->gParameters ) ) {
               $mess .= "    <parameter name='$name' value='$value'/>\n";
            }
            reset( $this->gParameters ); //Insurance
         }
         $mess .= "   </action>\n";
      } else {
         $mess = "   <action name='{$this->gName}' src='{$this->gSrc}' package='{$this->gPackageName}'/>\n";
      }
      return $mess;
   }

}
?>
