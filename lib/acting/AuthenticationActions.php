<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage acting
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/Action.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/acting/ActionPipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Parameter.php" );

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * The <i>AuthenticationActions</i> is the base class for the Authentication Actions.
    *
    * @package paloose
    * @subpackage acting
    */

class AuthenticationActions extends ActionPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */   
   private $gLogger;

   protected $gHandler;
   
   protected $gApplication;
   
   protected $gAuthenticationManager;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of <i>AuthenticationActions</i>.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param _AuthAction $inComponent the associated component instance (stores parameters etc)
    * @throws UserException if problem
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
        try {
         parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
        } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
        }
       $this->gLogger = Logger::getLogger( __CLASS__ );

      // Now to set up the handler and application for this.
      $handlerName = $this->gParameters->getParameter( "handler" );
      $applicationName = $this->gParameters->getParameter( "application" );
      
      // First we need to get the handler whose name is "$handlerName" which we get from the
      // authentication manager. Get the sitemap we are in first.
      $sitemap = Environment::$sitemapStack->peek();
      $components = $sitemap->getPipelines()->getComponentConfigurations();
      $this->gAuthenticationManager = $components->getComponent( "authentication-manager" );
      $this->gHandler = $this->gAuthenticationManager->getHandlers()->getHandler( $handlerName );
      // More arrows than Cr�cy :-)
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Run the authorization action.
    *
    * Always overridden.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM (normal NULL).
    * @retval DOMDocument the transformed DOM.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Running ---------------------------" );
      return $inDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */
  
   public function toString()
   {
      if ( $this->gPipe != null or $this->gParameters != NULL ) {
         $mess = "     <act type='{$this->gType}'>\n";
          if ( $this->gParameters != NULL ) {
            $mess .= $this->gParameters->toString();
         }
          if ( $this->gPipe != NULL ) {
            $mess .= $this->gPipe->toString();
         }
         $mess .= "     </act>\n";
         return $mess;
      } else {
         $mess = "   <act type='{$this->gType}'/>\n";
      }
   }

}
?>