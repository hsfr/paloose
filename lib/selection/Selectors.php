<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage selection
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/selection/Selector.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Selectors</i> maintains a list of the selectors.
 * 
 * @package paloose
 * @subpackage serialization
 */

#[AllowDynamicProperties]
class Selectors {

   private $gDefault;

   /** Array of selectors (each entry a class: Selector) and indexed by type */
   private $Selectors;

   /** The DOM object for this class */
   private $gDOM;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of <i>Selectors</i>.
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gSelectors = array();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the contained selector tags.
    *
    * For example 
    *
    * <pre>   &lt;map:selectors default="browser">
    *    &lt;map:selector name="browser" src="resource://lib/selection/BrowserSelector">
    *        &lt;browser name="explorer" useragent="MSIE"/>
    *        &lt;browser name="pocketexplorer" useragent="MSPIE"/>
    *        &lt;browser name="handweb" useragent="HandHTTP"/>
    *        &lt;browser name="avantgo" useragent="AvantGo"/>
    *        &lt;browser name="imode" useragent="DoCoMo"/>
    *        &lt;browser name="opera" useragent="Opera"/>
    *        &lt;browser name="lynx" useragent="Lynx"/>
    *        &lt;browser name="java" useragent="Java"/>
    *        &lt;browser name="wap" useragent="Nokia"/>
    *        &lt;browser name="wap" useragent="UP"/>
    *        &lt;browser name="wap" useragent="Wapalizer"/>
    *        &lt;browser name="mozilla5" useragent="Mozilla/5"/>
    *        &lt;browser name="mozilla5" useragent="Netscape6/"/>
    *        &lt;browser name="netscape" useragent="Mozilla"/>
    *        &lt;browser name="safari" useragent="Safari"/>
    *     &lt;/map:selector>
    *  &lt;/map:selectors></pre>
    *
    * The default attribute specifies the type of selector to use if none is specified in a pipeline.
    *
    * @param DOMNode $inNode the DOM that contains the contents of this pipe element.
    * @throws UserException if component does not have <i>name</i> and <i>src</i> attributes
    * @throws UserException if component constructor throws UserException
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM (this will inlcude an enclosed tags, when, otherwise etc)
      $this->gDOM = new DOMDocument;
      $this->gDOM->appendChild( $this->gDOM->importNode( $inNode, 1 ) );
   
      // Set the namespace for the sitemap. Let us hope people use it.
      $xpath = new domxpath( $this->gDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $component = $xpath->query( "//m:selector" );
      // Scan around each selector and set up the list in $gSelectors. Exit false if problem
      foreach ( $component as $node ) { 
         $name = $node->getAttribute( "name" );
         $src = $node->getAttribute( "src" );
         $isCachable = Utilities::normalizeBoolean( $node->getAttribute( "cachable" ) );
         $this->gLogger->debug( "Adding selector name = '". $name . "' using src '" . $src . "'" );
         if ( !strlen( $name ) or !strlen( $src ) ) {
            throw new UserException(
               "Component selector must have name and src attributes defined",
               PalooseException::SITEMAP_SELECTOR_PARSE_ERROR,
               $this->gDOM );
         }
         
         // The source may have path details attached so strip the path off to input the correct class.
         if ( strstr( $src, Environment::getPathSeparator() ) == 1 ) {
            $packageFilename = $src;
         } else if ( !strstr( $src, ":" . Environment::getPathSeparator() ) ) {
            // Relative file name so assemble with root directory
            $packageFilename = PALOOSE_LIB_DIRECTORY . Environment::getPathSeparator() . $src;
         } else {
            // Relative file name with pseudo protocol
            $packageFilename = StringResolver::expandPseudoProtocols( $src );
         }

         $package = Utilities::stripFileName( $packageFilename );
         // Must expand pseudo protocols first
         $path = Utilities::stripPath( StringResolver::expandPseudoProtocols( $packageFilename ) );
         $this->gLogger->debug( "Package='". $package . "' : path='" . $path . "'" );
         
         // We have something to add, so first of all check for duplicate names
         if ( array_key_exists( $name, $this->gSelectors ) ) {
            $this->gLogger->debug( "Overriding selector: '". $name . "'" );
         } else {
            $this->gLogger->debug( "Adding selector: '". $name . "'" );
         }
         // Get the necessary for forming a specific selector
         require_once( $path . $package . ".php" );
         // Form up the component class name
         $selectorName = "_" . $package;
         $selector = new $selectorName( $name, $path.$package, $node, $isCachable );
         // Check that the selector thinks it is valid
         try {
            $selector->isValid();
            $this->gSelectors[ $name ] = $selector;
         } catch( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $this->gDOM );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set the default selector.
    *
    * @param string $inDefault the default selector name.
    * @throws UserException cannot find selector
    */

   public function setDefault( $inDefault )
   {
      if ( !( array_key_exists( $inDefault, $this->gSelectors ) ) ) {
           throw new UserException(
              "Default selector '". $inDefault . "' not found",
              PalooseException::SITEMAP_SELECTOR_PARSE_ERROR,
              $this->gDOM );
      }
      $this->gDefault = $inDefault;
      $this->gLogger->debug( "Set default selector: '". $inDefault . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the default selector.
    *
    * @retval string the default selector name.
    */

   public function getDefault()
   {
      return $this->gDefault;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the named selector.
    *
    * @param string $inName the default selector type.
    * @retval string the selector instance.
    * @exception InternalException cannot find selector.
    */

   public function getSelector( $inName )
   {
      if ( !( array_key_exists( $inName, $this->gSelectors ) ) ) {
           throw new InternalException(
              "Selector '". $inName . "' not found", 
              PalooseException::SITEMAP_SELECTOR_PARSE_ERROR,
              $this->gDOM );
      } else {
         return $this->gSelectors[ $inName ];
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the class of the named selector.
    *
    * @param string $inName the default selector type.
    * @retval string the selector instance class name.
    * @exception InternalException cannot find selector.
    */

   public function getSelectorClassName( $inType )
   {
      if ( !( array_key_exists( $inType, $this->gSelectors ) ) ) {
           throw new InternalException( 
              "Selector '". $inType . "' not found",
              PalooseException::SITEMAP_SELECTOR_PARSE_ERROR,
              $this->gDOM );
      } else {
         $className = $this->gSelectors[ $inType ]->getPackageName();
         return $className;
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this PipeElement.
    *
    * @retval string the representation of the element as a string
    *
    */

   public function toString()
   {
      if ( $this->gSelectors == NULL ) return "";
      $mess = "  <selectors default='{$this->gDefault}'>\n";
      foreach ( $this->gSelectors as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "  </selectors>\n";
      return $mess;
   }

}

?>
