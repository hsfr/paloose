<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage selection
 *   @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Browser.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/selection/Selector.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/selection/SelectorPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>VariableSelector</i> selects a pipeline on basis of 
 * request parameter from the query string.
 * 
 * @package paloose
 * @subpackage selection
 */

class VariableSelector extends SelectorPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new browser selector.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Run this selector component in the pipeline.
    *
    *    <map:select type="variable-selector">
    *       <map:parameter name="parameter-name" value="{cookies:theme}"/>
    *       <map:when test="">
    *          <map:generate src="context://themes/astral/data/page.xml"/>
    *       </map:when>
    *       <map:otherwise>
    *          <map:generate src="context://themes/{cookies:theme}/data/page.xml"/>
    *       </map:otherwise>
    *    </map:select>
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM from the previous pipeline stage.
    * @retval The document DOM representing the input.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Running VariableSelector" );

      // The test-value is the same as a switch variable
      $testValue = StringResolver::expandString( $inVariableStack, $this->gParameters->getParameter( 'test-value' ) );
      $this->gLogger->debug( "Test value: '" . $testValue . "'" );
      
      if ( $testValue != NULL and $this->gWhenPipes[ $testValue ] != NULL ) { 
         $this->gLogger->debug( "Processing '" . $testValue . "' pipe" );
         return $this->gWhenPipes[ $testValue ]->run( Environment::$variableStack, $inURL, $inQueryString, $inDOM );
      } else {
         $this->gLogger->debug( "Processing otherwise pipe" );
         if ( $this->gOtherwisePipe->getComponentListSize() > 0 ) {
            return $this->gOtherwisePipe->run( Environment::$variableStack, $inURL, $inQueryString, $inDOM );
         }
      }   
      return $inDOM;
     }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 * 
 * There will be only one instance of this for each declaration of this component.
 *
 *   <map:selector name="request-parameter" src="resource://lib/selection/VariableSelector">
 *      <map:parameter-name>test</map:parameter-name>
 *   </map:selector>
 *
 * @package paloose
 * @subpackage selection
 */
 
class _VariableSelector extends Selector {

    /** Logger instance for this class */
   private $gLogger;
   
   /** Name of default parameter to test if none specified */
   private $gDefaultParameter;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {
      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
       $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "VariableSelector";
      
        // First thing is make node into a local DOM (this will include an enclosed tags such parameter)
      $requestParameterDOM = new DOMDocument;
      $requestParameterDOM->appendChild( $requestParameterDOM->importNode( $inParameterNode, 1 ) );

        // Set the namespace for the sitemap.
      $xpath = new domxpath( $requestParameterDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $component = $xpath->query( "//m:selector/*" );

      // Get the default parameter to test
      $this->gDefaultParameter = Utilities::getXPathListStringItem( 0, $xpath, "//m:selector/m:parameter-name" );
      $this->gLogger->debug( "parameter-name: '" . $this->gDefaultParameter . "'" );

  }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Is this object valid?
    *
    * @retval true is vallid, otherwise false.
    */
    
   public function isValid()
   {
      $parentIsValid = parent::isValid();
      //Add stuff to check for the useragents etc
      return $parentIsValid;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the names
    *
    * @retval name array.
    */
    
   public function getDefaultParameter()
   {
      return $this->gDefaultParameter;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return the string representation of the object.
    *
    * @retval string representation of object
    */
    
   public function toString()
   {
      if ( $this->gParameters == NULL && $this->gBrowserAgent == NULL ) {
         return "   <selector name='{$this->gType}' src='{$this->gSrc}' package='{$this->gPackageName}'/>\n";
      }
      $mess = "   <selector name='{$this->gType}' src='{$this->gSrc}' package='{$this->gPackageName}'>\n";
      if ( $this->gParameters != NULL ) {
         while ( list( $name, $value ) = each( $this->gParameters ) ) {
            $mess .= "    <parameter name='$name' value='$value'/>\n";
         }
         reset( $this->gParameters );
      }
      if ( $this->gDefaultParameter != NULL ) {
         $mess .= "    <parameter-name>" . $this->gDefaultParameter . "</parameter-name>\n";
      }
      $mess .= "   </selector>\n";
      return $mess;
   }


}
?>
