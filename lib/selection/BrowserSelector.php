<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage selection
 *   @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Browser.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/selection/Selector.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/selection/SelectorPipeElement.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>BrowserSelector</i> selects a pipeline on basis of the client's browser.
 * 
 * @package paloose
 * @subpackage selection
 */

class BrowserSelector extends SelectorPipeElement implements PipeElementInterface
{

    /** Logger instance for this class */
   private $gLogger;

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new browser selector.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
       $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Run this selector component in the pipeline.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval The document DOM representing the input.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Running BrowserSelector" );

      // Work out the browser that is being used.
      $browser = new Browser();
      $this->gLogger->debug( "User agent string '" . $_SERVER[ 'HTTP_USER_AGENT' ] . "'" );
      $this->gLogger->debug( "Found '" . $browser->getName() . "' browser, version '". $browser->getVersion() . "' using platform '" . $browser->getPlatform() . "'" );
      
      // The browser name should match up with one of the selectors if the 'when' test matches,
      // otherwise we process the pipe in the 'otherwise' clause
   
      $testFound = false;
      $userAgents = $this->gComponent->getUserAgents();
      $idx = -1;

        foreach ( $this->gWhenPipes as $name => $pipe ) {
            $idx = $this->gComponent->getNameIndex( $name );
            $this->gLogger->debug( "Matching name '$name' at position " . $idx );
            if ( $idx > -1 ) {
                // Name exists so get user agent
                $userAgent = $userAgents[ $idx ];
                $this->gLogger->debug( "User agent: " . $userAgent );
                if ( $browser->getName() == $userAgent ) {
                   $this->gLogger->debug( "Found matching test for '" . $userAgent . "'" );
                   $testFound = true;
                   break;
                }
            } else {
                // Name not found so exit and run otherwise pipe
                break;
            }
        }
      
      if ( $testFound === true ) {
         $pipe = $this->gWhenPipes[ $name ];
         $this->gLogger->debug( "Processing '" . $name . "' pipe" );
         $this->gLogger->debug( $pipe->toString() );
         $documentDOM = $pipe->run( Environment::$variableStack, $inURL, $inQueryString, $inDOM );
      } else {
         $this->gLogger->debug( "Processing otherwise pipe" );
         if ( $this->gOtherwisePipe->getComponentListSize() > 0 ) {
            $documentDOM = $this->gOtherwisePipe->run( Environment::$variableStack, $inURL, $inQueryString, $inDOM );
         } else {
            return $inDOM;
         }
      }
      return $documentDOM;
   }

}

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class holds the information for the component.
 * 
 * There will be only
 * one instance of this for each declaration of this component.
 *
 * @package paloose
 * @subpackage selection
 */
 
class _BrowserSelector extends Selector {

    /** Logger instance for this class */
   private $gLogger;
   
   /** List of browser names */
   private $gBrowserName;
   
   /** List of associated browser user agents */
   private $gBrowserAgent;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * @param string $inName the name of this transformer
    * @param string $inSrc the package name of this transformer (destination PHP class)
    * @param DOMNode $inParameterNode the node associated with this transformer (stores all parameters and other enclosed tags)
    * @param boolean $inIsCachable is this component cachable
    */

   public function __construct( $inName, $inSrc, DOMNode $inParameterNode, $inIsCachable )
   {


      parent::__construct( $inName, $inSrc, $inParameterNode, $inIsCachable );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPackageName = "BrowserSelector";
      
        // First thing is make node into a local DOM (this will include an enclosed tags such parameter)
      $browserDOM = new DOMDocument;
      $browserDOM->appendChild( $browserDOM->importNode( $inParameterNode, 1 ) );

        // Set the namespace for the sitemap.
      $xpath = new domxpath( $browserDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $component = $xpath->query( "//m:selector/*" );

      // A small remark about why there are two arrays for the name and agents. It is important to keep
      // the entries in order rather than use the name as a key in an associative array.
      $idx = 0;
      foreach ( $component as $node ) {
         if ( $node->localName == "browser" ) {
            $name = $node->getAttribute( "name" );
            $userAgent = $node->getAttribute( "useragent" );
            $this->gLogger->debug( "Adding browser name = '". $name . "', user agent '" . $userAgent . "'" );
            $this->gBrowserName[ $idx ] = $name;
            $this->gBrowserAgent[ $idx ] = $userAgent;
            $idx++;
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Is this object valid?
    *
    * @retval true is vallid, otherwise false.
    */
    
   public function isValid()
   {
      $parentIsValid = parent::isValid();
      //Add stuff to check for the useragents etc
      return $parentIsValid;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the names
    *
    * @retval name array.
    */
    
   public function getBrowserName()
   {
      return $this->gBrowserName;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the index of the array value.
    *
    * @param string $inName the name to find
    * @retval the index of the name (-1 if not found)
    */
    
   public function getNameIndex( $inName )
   {
      $idx = 0;
      foreach ( $this->gBrowserName as $name ) {
         if ( strcmp( $name, $inName ) == 0 ) return $idx;
         $idx++;
      }
      return -1;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the user agents
    *
    * @retval user agent array.
    */
    
   public function getUserAgents()
   {
      return $this->gBrowserAgent;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return the string representation of the object.
    *
    * @retval string representation of object
    */
    
   public function toString()
   {
      if ( $this->gParameters == NULL && $this->gBrowserAgent == NULL ) {
         return "   <selector name='{$this->gType}' src='{$this->gSrc}' package='{$this->gPackageName}'/>\n";
      }
      $mess = "   <selector name='{$this->gType}' src='{$this->gSrc}' package='{$this->gPackageName}'>\n";
      if ( $this->gParameters != NULL ) {
         while ( list( $name, $value ) = each( $this->gParameters ) ) {
            $mess .= "    <parameter name='$name' value='$value'/>\n";
         }
         reset( $this->gParameters );
      }
      if ( $this->gBrowserAgent != NULL ) {
         while ( list( $idx, $value ) = each( $this->gBrowserAgent ) ) {
            $mess .= "    <browser name='" . $this->gBrowserName[ $idx ] . "' useragent='$value'/>\n";
         }
         reset( $this->gBrowserAgent );
      }
      $mess .= "   </selector>\n";
      return $mess;
   }


}
?>
