<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage selection
 *   @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Browser.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElement.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/PipeElementInterface.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/selection/Selector.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>SelectorPipeElement</i> is the base class of the pipeline selectors.
 * 
 * @package paloose
 * @subpackage selection
 */

class SelectorPipeElement extends PipeElement implements PipeElementInterface
{

    /** Logger instance for this class */
   private $gLogger;

    /** The list of pipeline scraps that are associated with the "when" statements */
   protected $gWhenPipes;

    /** The list of pipeline scraps that are associated with the "otherwise" statement */
   protected $gOtherwisePipe;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance.
    *
    * @param DOMDocument $inDOM the DOM that contains the contents of this pipe element.
    * @param string $inType the type of this pipe element
    * @param string $inSrc the src attribute (or package required in this case)
    * @param Component $inComponent the associated component instance (stores parameters etc)
    */
    
   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      parent::__construct( $inDOM, $inType, $inSrc, $inComponent );
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gGenericType = PipeElement::SELECTOR;
      $this->gSrc = $inSrc;
      $this->gType = $inType;
      $this->gComponent = $inComponent;

      // Set the namespace for the sitemap.
      $xpath = new domxpath( $inDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
 
      // Very important XPath expression here to ensure just the top level tests are taken
      // of this root select - in otherwords nested selects are ignored.
      $components = $xpath->query( "/m:select[1]/*" );
      // $this->gLogger->debug( $inDOM->saveXML() );
      $this->gLogger->debug( "Components: " . $components->length );
      // foreach ( $components as $node ) {
      //    $this->gLogger->debug( "   name : " . $node->localName );
      // }

      // Should be a list of when and otherwise elements
      foreach ( $components as $node ) {
         $switchDOM = new DOMDocument;
         $switchDOM->appendChild( $switchDOM->importNode( $node, 1 ) );
         // $this->gLogger->debug( $switchDOM->saveXML() );
             
         // Scan the enclosed pipe. The following is a restricted set of stuff found in Match.
         $pipeXPath= new domxpath( $switchDOM );
         $pipeXPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
         $testString = $node->getAttribute( "test" );
         $pipeComponent = $pipeXPath->query( "/*/*" );
            
         if ( $node->localName == "when" ) {
            $this->gLogger->debug( "Making new Pipe with " . $pipeComponent->length . " components for test '" . $testString . "'" );
         } else if ( $node->localName == "otherwise" ) {
            $this->gLogger->debug( "Making new Pipe with " . $pipeComponent->length . " components for otherwise" );
         }
            
            $pipe = new Pipe();
   
            foreach ( $pipeComponent as $pipeNode ) {
               $name = $pipeNode->localName;
               $this->gLogger->debug( "pipe node: '". $name . "'" );
               // This is where we build a pipe. 
               switch ( $name ) {
                     
                  case "redirect-to" :
                     try {
                        $redirect = PipeElement::createRedirect( $pipeNode );
                        $this->gLogger->debug( "Creating redirect using component '" . get_class( $redirect ) . "'" );
                        // Put it into the pipe
                        $pipe->addComponent( $redirect );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;
                     
                  case "select" :
                     try {
                        $selector = PipeElement::createSelector( $pipeNode );
                        $this->gLogger->debug( "Creating selector using component '" . get_class( $selector ) . "'" );
                        // Put it into the pipe
                        $pipe->addComponent( $selector );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;
                  
                  case "aggregate" :
                     try {
                        $aggregator = PipeElement::createAggregator( $pipeNode );
                        $this->gLogger->debug( "Creating aggregator using component '" . get_class( $aggregator ) . "'" );
                        // Put it into the pipe
                        $pipe->addComponent( $aggregator );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;
                  
                  case "generate" :
                     try {
                        $generator = PipeElement::createGenerator( $pipeNode );
                        $this->gLogger->debug( "Creating generator: type='" . $generator->getType() . "' src='" . $generator->getSrc() . "' using component '" . get_class( $generator ) . "'" );
                        // Put it into the pipe
                        $pipe->addComponent( $generator );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;
                     
                  case "transform" :
                     try {
                        $transformer = PipeElement::createTransformer( $pipeNode );
                        $this->gLogger->debug( "Creating transformer: type='" . $transformer->getType() . "' src='" . $transformer->getSrc() . "' using component '" . get_class( $transformer ) . "'" );
                        // Put it into the pipe
                        $pipe->addComponent( $transformer );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;
   
                  case "serialize" :
                     try {
                        $serializer = PipeElement::createSerializer( $pipeNode );
                        $this->gLogger->debug( "Creating serializer: type='" . $serializer->getType() . "' src='" . $serializer->getSrc() . "' using component '" . get_class( $serializer ) . "'" );
                        // Put it into the pipe
                        $pipe->addComponent( $serializer );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;
                  
                  case "read" :
                     try {
                        $reader = PipeElement::createReader( $pipeNode );
                        $this->gLogger->debug( "Creating reader: type='" . $reader->getType() . "' src='" . $reader->getSrc() . "' using component '" . get_class( $reader ) . "'" );
                        // Put it into the pipe
                        $pipe->addComponent( $reader );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;
                     
                  case "call" :
                     try {
                        $call = PipeElement::createCall( $pipeNode );
                        $this->gLogger->debug( "Creating call: name='" . $call->getResourceName() . "' using component '" . get_class( $call ) . "'" );
                        // Put it into the pipe
                        $pipe->addComponent( $call );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;

                  case "mount" :
                     try {
                        $mount = PipeElement::createMount( $pipeNode );
                        $this->gLogger->debug( "Creating mount" );
                        $pipe->addComponent( $mount );
                     } catch ( UserException $e ) {
                        throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                     }
                     break;
   
               }
            }
         if ( $node->localName == "when" ) {
            $this->gLogger->debug( "Adding 'when' pipe (test: '$testString') to selector '$inType'" );
            $this->gWhenPipes[ $testString ] = $pipe;
            // Preset otherwise pipe with null pipe.
            $this->gOtherwisePipe = new Pipe();
         } else if ( $node->localName == "otherwise" ) {
            $this->gLogger->debug( "Adding 'otherwise' pipe to selector '$inType'" );
            $this->gOtherwisePipe = $pipe;
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Run this component in the pipeline.
    *
    * Always overridden.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval The document DOM representing the input.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      return $inDOM;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return the string representation of the object.
    *
    * @retval string representation of object
    */
    
   public function toString()
   {
      if ( $this->gWhenPipes != null ) {
         $mess = "     <select type='{$this->gType}' package='" . get_class( $this ) . "'>\n";
         foreach ( $this->gWhenPipes as $test => $pipe ) {
            $mess .= "       <when test='" . $test . "'>\n";
            $mess .= $pipe->toString();
            $mess .= "       </when>\n";
         }
         if ( $this->gOtherwisePipe != null ) {
            $mess .= "       <otherwise>\n";
            $mess .= $this->gOtherwisePipe->toString();
            $mess .= "       </otherwise>\n";
         }
         $mess .= "     </select>\n";
         return $mess;
      } else {
         return "";
      }
   }

}
?>
