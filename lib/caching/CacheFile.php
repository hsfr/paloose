<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage CacheFile
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @since 1.3.0
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The CacheFile Class is a class that stores variables and methods
 * that relate to the current running CacheFile.
 *
 * @package paloose
 * @subpackage CacheFile
 */

class CacheFile {

   /** Where the pipeline cache files are held */
   private $gCacheDirectory;

   /** The full path+name of the file to cache */
   private $gCacheFileName;

   /** The array of cache time (keyed by file name etc.) */
   private $gCacheTime;

   /** The parameter hash (keyed by file name etc.) */
   private $gParameterHash;

   /** The associated hash which gives the cached file name */
   private $gHashData;

   /** The state of the cache file  */
   private $gCacheEntryType;

   /** Logger instance for this class */
   private $gLogger;

   const CACHE_VALID = 1;
   const CACHE_INVALID = -1;
   
   /** Number of retries in gained locked cache before failure */
   const RETRIES = 10;
   
   const CACHE_FILE_NAME = 'paloose.cache';
   const CACHABLE_ATTRIBUTE_NAME = 'cachable';
   
   const CACHE_LINE_SEPARATOR = ",";

   const FILE_NAME_INDEX = 0;
   const FILE_MODIFY_TIME_INDEX = 1;
   const CACHED_FILE_NAME_INDEX = 2;
   const PARAMETER_HASH_INDEX = 3;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of CacheFile.
    */

   public function __construct( /* $inCacheEntryType */ )
   {

      $this->gLogger = Logger::getLogger( __CLASS__ );
  
      $rawCacheDirectory = Environment::$configuration[ 'cacheDir' ];
      $this->gCacheDirectory = StringResolver::expandPseudoProtocols( Environment::$configuration[ 'cacheDir' ] );
      $this->gCacheDirectory = Utilities::addTrailingSeparator( $this->gCacheDirectory );
      if ( !file_exists( $this->gCacheDirectory ) ) {
        $this->gLogger->debug( "Making dir [{$this->gCacheDirectory}]" );
        if ( !mkdir( $this->gCacheDirectory ) ) {
          throw new InternalException(
            "Cannot create cache directory (permissions?) : {$rawCacheDirectory}[{$this->gCacheDirectory}]",
            PalooseException::SYSTEM_CACHE_ERROR, NULL );
        }
      }
      $this->gCacheFileName = $this->gCacheDirectory . self::CACHE_FILE_NAME;
      $this->gLogger->debug( "Using 'CacheFile' : [{$this->gCacheFileName}]" );
      
      $this->gCacheTime = array();
      $this->gHashData = array();
      $this->gCacheEntryType = array();
      $this->gParameterHash = array();
      
      // Read the cache file into the array
      if ( file_exists( $this->gCacheFileName ) ) {
        $this->gLogger->debug( "Reading cache array:" );
        $cacheHandle = fopen( $this->gCacheFileName, 'r' );  
         while ( !feof( $cacheHandle ) ) {
            $line = trim( fgets( $cacheHandle ) );
            if ( strlen( $line ) > 0 ) {
               $this->gLogger->debug( "  [$line]" );
               $cacheData = explode( self::CACHE_LINE_SEPARATOR, $line );
               $cacheKey = $cacheData[ self::FILE_NAME_INDEX ];
               $this->gCacheTime[ $cacheKey ] = $cacheData[ self::FILE_MODIFY_TIME_INDEX ];
               $this->gHashData[ $cacheKey ] = $cacheData[ self::CACHED_FILE_NAME_INDEX ];
               $this->gParameterHash[ $cacheKey ] = $cacheData[ self::PARAMETER_HASH_INDEX ];
            }
         }
         fclose( $cacheHandle );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the modify time of the inputted file.
    *
    * @param string $inKey key of of cache entry
    * @retval int modify time in milliseconds
    */
    
    public function getTimeStamp( $inKey )
    {
       if ( !array_key_exists( $inKey, $this->gCacheTime ) ) {
          return 0;  // No file entry
       } else {
          return( $this->gCacheTime[ $inKey ] );
       }
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the parameter hash of keyed entry.
    *
    * @param string $inKey key of of cache entry
    * @retval string hash representing the parameters
    */
    
    public function getParameterHash( $inKey )
    {
       if ( !array_key_exists( $inKey, $this->gParameterHash ) ) {
          return 0;  // No file entry
       } else {
          return( $this->gParameterHash[ $inKey ] );
       }
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Update the cache file (timestamp and hash) and the file being cached.
    *
    * From a Generator we have
    *
    *    <pre>$fileHash, $srcTimeStamp, $documentDOM, 0</pre>
    *
    * From a Transformer we have
    *
    *    <pre>$cacheKey, $XSLTimeStamp, $documentDOM, $newParameterHash</pre>
    *
    * @param string $inKey usually the cache file name
    * @param string $inTimeStamp time stamp of appropriate file
    * @param DOMDocument $inDom DOM to store in the cache
    * @param string $inParameterHash hash of component instance parameters
    * @param int $inCacheEntryType what the type of the component is
    */
    
   public function updateDOM2Cache( $inKey, $inTimeStamp, DOMDocument $inDom, $inParameterHash/*, $inCacheEntryType */ )
   {
      $this->gLogger->debug( "Setting timestamp [$inKey] : $inTimeStamp" );
      $this->gCacheTime[ $inKey ] = $inTimeStamp;
      $this->gParameterHash[ $inKey ] = $inParameterHash;
      $cachedFileName = $this->gCacheDirectory . $this->getHash( $inKey );
      $this->gLogger->debug( "cached file [$cachedFileName]" );
      
      $cacheHandle = fopen( $this->gCacheFileName, 'w' );  
      if ( $cacheHandle === false ) {
         throw new InternalException(
            "Cannot open cache file for writing: [{$this->gCacheFileName}]",
            PalooseException::SYSTEM_CACHE_ERROR, NULL );
       }
       
      $domHandle = fopen( $cachedFileName, 'w' );
      if ( $domHandle === false ) {
         throw new InternalException(
            "Cannot open cached file for writing: [{$cachedFileName}]",
            PalooseException::SYSTEM_CACHE_ERROR, NULL );
       }
       
       $count = 0;
       $finished = false;
       while ( $count < self::RETRIES && !$finished ) {
          if ( flock( $domHandle, LOCK_EX ) ) { 
             if ( flock( $cacheHandle, LOCK_EX ) ) { 
                $this->gLogger->debug( "Both files locked and acquired for writing" );
                foreach( $this->gCacheTime as $key => $timeStamp ) {
                   $line = $key . self::CACHE_LINE_SEPARATOR .
                             // $inCacheEntryType . self::CACHE_LINE_SEPARATOR .
                             $timeStamp . self::CACHE_LINE_SEPARATOR .
                             $this->getHash( $key ) . self::CACHE_LINE_SEPARATOR .
                             $this->gParameterHash[ $key ];
                   fwrite( $cacheHandle, $line . "\n" );
                   $this->gLogger->debug( "Wrote: [$line]" );
                }
                flock( $cacheHandle, LOCK_UN );
                fclose( $cacheHandle );
                $this->gLogger->debug( "Written cache array : [{$this->gCacheFileName}]" );

                fwrite( $domHandle, $inDom->saveXML() );
                flock( $domHandle, LOCK_UN );
                fclose( $domHandle );
                $this->gLogger->debug( "Written cached file DOM : [$cachedFileName]" );
                $finished = true;
             }
             $count++;
          }
       }
       $this->gLogger->debug( "Finished update" );
       if ( $count >= self::RETRIES && !$finished ) {
          throw new InternalException(
             "Cannot write cache data (no locks:$count) : [{$this->gCacheFileName}][{$this->getHash($key)}]",
             PalooseException::SYSTEM_CACHE_ERROR, NULL );
       }
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the DOM from the cache file (keyed by file name etc.).
    */
    
   public function getCacheDOM( $inKey )
   {
      $cachedFileName = $this->gCacheDirectory . $this->getHash( $inKey );
      $this->gLogger->debug( "Reading cached file [$cachedFileName]" );
      $dom = new DOMDocument();
      if ( !$dom->load( $cachedFileName ) ) {
         throw new InternalException(
            "Cannot open cached file for reading: [$cachedFileName]",
            PalooseException::SYSTEM_CACHE_ERROR, NULL );
      }
      return $dom;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the hash of the input string. This is normally the input file
    * name/contents.
    *
    * @retval string directory path of the cache directory
    */
    
    private function getDOMHash( DOMDocument $inDom )
    {
       return md5( $inDom->saveXML() );
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the hash of the input string. This is normally the input file
    * name/contents.
    *
    * @retval string directory path of the cache directory
    */
    
    public static function getHash( $inString )
    {
       return md5( $inString );
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the path where the cache files are stored.
    *
    * @retval string directory path of the cache directory
    */
    
    public function getCacheDir()
    {
       return $this->gCacheDirectory;
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the path+name of the cachable file.
    *
    * @retval string path+name of the cacheable file
    */
    
    public function getCacheFileName()
    {
       return $this->gCacheFileName;
    }

}

?>