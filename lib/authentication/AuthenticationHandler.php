<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage authentication
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */
 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * <i>AuthenticationHandler</i> stores the necessary data for a named handler.
 *
 * It is initialised in the pipeline through the component-configurations
 * tag, for example:
 *
 * <pre>   &lt;handler name="adminHandler">
 *      &lt;redirect-to uri="cocoon:/login"/>
 *      &lt;authentication uri="cocoon:raw:/authenticate-user"/>
 *    &lt;/handler></pre>
 *
 * @package paloose
 * @subpackage authentication
 */
 
class AuthenticationHandler {

   private $gDefault;
   
   private $gName;
   
   private $gRedirectToURI;

   private $gAuthenticationURI;
   
   private $gAuthenticator;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of the <i>GalleryFolder</i>.
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __ClASS__ );
      $this->gName = "";
      $this->gRedirectToURI = "";
      $this->gAuthenticationURI = "";
      $this->gAuthenticator = "";
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the authentication-manager handler section of the sitemap.
    *
    * @param the node associated with the handler section.
    * @throws UserException if handler declaration invalid
    */

   public function parse( DOMNode $inNode )
   {
      //First thing is make node into a local DOM
      $pipelinesDom = new DOMDocument;
      $pipelinesDom->appendChild( $pipelinesDom->importNode( $inNode, 1 ) );

      $xpath = new domxpath( $pipelinesDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $this->gName = Utilities::getXPathListStringItem( 0, $xpath, "//@name" );
      $this->gRedirectToURI = Utilities::getXPathListStringItem( 0, $xpath, "//m:redirect-to/@uri" );
      $this->gAuthenticationURI = Utilities::getXPathListStringItem( 0, $xpath, "//m:authentication/@uri" );
      $this->gAuthenticator = Utilities::getXPathListStringItem( 0, $xpath, "//m:authentication/@authenticator" );
      if ( strlen( $this->gAuthenticationURI ) == 0 and strlen( $this->gAuthenticator ) == 0 ) {
         throw new UserException( "Must have authenticator or uri declared in handler authentication",
            PalooseException::SITEMAP_ACTION_PARSE_ERROR,
            $pipelinesDom );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the name of this handler.
    *
    * @retval string the handler name
    *
    */

   public function getName()
   {
      return $this->gName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the redirect-to URI of this handler.
    *
    * @retval string the redirect-to URI
    *
    */

   public function getRedirectToURI()
   {
      return $this->gRedirectToURI;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the authentication URI of this handler.
    *
    * @retval string the authentication URI
    *
    */

   public function getAuthenticationURI()
   {
      return $this->gAuthenticationURI;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the authenticator of this handler.
    *
    * @retval string the authenticator
    *
    */

   public function getAuthenticator()
   {
      return $this->gAuthenticator;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this Class.
    *
    * @retval string the representation of the Class as a string
    *
    */

   public function toString()
   {
      $mess = "    <handler name='{$this->gName}'>\n";
      if ( strlen( $this->gRedirectToURI ) > 0 ) {
         $mess .= "     <redirect-to uri='{$this->gRedirectToURI}'/>\n";
      }
      if ( strlen( $this->gAuthenticationURI ) > 0 ) {
         $mess .= "     <authentication uri='{$this->gAuthenticationURI}'/>\n";
      }
      if ( strlen( $this->gAuthenticator ) > 0 ) {
         $mess .= "     <authentication uri='{$this->gAuthenticator}'/>\n";
      }
      return $mess;
   }

}

?>