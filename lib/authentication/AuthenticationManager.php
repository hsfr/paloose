<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage authentication
 * @author   Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */
 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/authentication/AuthenticationHandler.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/authentication/AuthenticationHandlers.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/ExitException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * <i>AuthenticatioManager</i> controls the access to authenticated protected
 * pages. It is initialised in the pipeline through the component-configurations
 * tag, for example:
 *
 * <pre>   &lt;authentication-manager>
 *     &lt;handlers>
 *      &lt;handler name="adminHandler">
 *         &lt;redirect-to uri="cocoon:/login"/>
 *         &lt;authentication uri="cocoon:raw:/authenticate-user"/>
 *      &lt;/handler>
 *     &lt;/handlers>
 *   &lt;/map:authentication-manager></pre>
 *
 * @package paloose
 * @subpackage authentication
 */
 
class AuthenticationManager {

   private $gDefault;
   
   /** Handlers class for accessing an individual name handler */
   private $gHandlers;
   
   /** Are we using sessions or cookies for this request */
   private $sessionOrCookies; 

   const USE_COOKIES = 0;
   const USE_SESSIONS = 1;

   /** The login user name */
   private $gUserName; 

   /** The login password */
   private $gPassword; 

   /** Logger instance for this class */   
   private $gLogger;
   
   private $outputStream;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __ClASS__ );
      $this->gHandlers = NULL;
      $this->gUserName = "";
      $this->gPassword = "";
      $this->sessionOrCookies = self::USE_COOKIES;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the authentication-manager section of the sitemap.
    *
    * @param the node associated with the section.
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $pipelinesDom = new DOMDocument;
      $pipelinesDom->appendChild( $pipelinesDom->importNode( $inNode, 1 ) );

      $xPath = new domxpath( $pipelinesDom );
      $xPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $nodeList = $xPath->query( "//*" );
      $idx = 0;
      foreach ( $nodeList as $node ) {
         if ( $node->localName == "handlers" ) {
            try {
               $this->gHandlers = new AuthenticationHandlers();
               $this->gHandlers->parse( $node );
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Authenticate user.
    *
    * @param AuthenticateHandler the handler for this authentication.
    * @retval boolean true if user has successfully authenticated.
    * @throws UserException if malformed redirect URI
    * @throws UserException if external redirect URI
    */

   public function authenticateUser( AuthenticationHandler $inHandler )
   {
      $this->gLogger->debug( "Authenticating user using '" . $inHandler->getName() .  "' handler" );
      // Get the handler for authenticating the user
      $authenticationURI = $inHandler->getAuthenticationURI();
      $authenticator = $inHandler->getAuthenticator();
      $redirectURI = $inHandler->getRedirectToURI();
      $this->gLogger->debug( "   redirect URI: '" . $redirectURI );
      // The redirect must be fired into this sitemap
      $uri = StringResolver::removePseudoProtocols( $redirectURI );

      // Now to check whether it is an internal redirect
      $urlComponents = parse_url( $redirectURI );
      if ( $urlComponents === false ) {
         throw new UserException( "Malformed URI in redirect", PalooseException::SITEMAP_AUTHORISE_MANAGER_PARSE_ERROR );
      } else if ( array_key_exists( "scheme", $urlComponents ) and $urlComponents[ 'scheme' ] == "http" ) {
         //External link, so clean what we have output already and redirect
         throw new UserException( "External URI in redirect", PalooseException::SITEMAP_AUTHORISE_MANAGER_PARSE_ERROR );
      }

      // Must be an internal affair to we redirect to this sitemap or higher
       $uriSource = Environment::getSource( $redirectURI );
      // If it is a naked uri (no pseudo variables) then assume that it is a resubmition to the root sitemap
      if ( $uriSource == Environment::UNKNOWN_SOURCE ) $uriSource = Environment::SUBMIT_ROOT ;
       switch ( $uriSource ) {

         case Environment::SUBMIT_ROOT :
            $expandedString = StringResolver::removePseudoProtocols( $redirectURI );
            $this->gLogger->debug( "Redirecting '$expandedString' to root sitemap" );
            // Set up a new output stream that uses an internal buffer
             $this->outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
            // Get the appropriate sitemap
            $sitemap = Environment::$sitemapStack->root();
            $oldOutputStream = $sitemap->getOutputStream();
            $sitemap->setOutputStream( $this->outputStream );
            // Internal request so mark true
            $sitemap->run( $expandedString, "", true ); //Note no query string here
            $sitemap->setOutputStream( $oldOutputStream );
            break;
               
         case Environment::SUBMIT_CURRENT :
            $expandedString = StringResolver::removePseudoProtocols( $redirectURI );
            $this->gLogger->debug( "Redirecting '$expandedString' to current sitemap" );
            // Set up a new output stream that uses an internal buffer
             $this->outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
            // Get the appropriate sitemap
            $sitemap = Environment::$sitemapStack->peek();
            $oldOutputStream = $sitemap->getOutputStream();
            $sitemap->setOutputStream( $this->outputStream );
            // Internal request so mark true
            $sitemap->run( $expandedString, "", true ); //Note no query string here
            $sitemap->setOutputStream( $oldOutputStream );
            break;

         default :
            throw new RunException( "Illegal return code '$uriSource' for string expansion source processing '$redirectURI'", PalooseException::UNKNOWN_STRING_EXPANSION_TYPE );

      }
      // We will always have one of these
      if ( strlen( $authenticationURI ) > 0 ) {
         $this->gLogger->debug( "   authentication URI: '" . $authenticationURI );
         // Resubmit this to the sitemap (usually the current one)
      } else if ( strlen( $authenticator ) > 0 ) {
         $this->gLogger->debug( "   authenticator: '" . $authenticator );
      }
      $redirectURI = $inHandler->getRedirectToURI();
      return false;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Is the user authorised? 
    *
    * @retval boolean true if this user is authorised for current session.
    */

   public function isUserAuthorised()
   {
      $user = Environment::$sessionHandler->getVar( '__username' );
      if ( strlen( $user ) == 0 ) {
         $this->gLogger->debug( "User not authorised" );
         return false;
      }
      $this->gLogger->debug( "User '" . $user . "' authorised" );
      return true;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Check if a user and password is authorised. 
    *
    * @param AuthenticationHandler $inHandler handler for this request.
    * @param string $inUser the user name.
    * @param string $inPassword the associated password.
    * @retval DOMDocument $the user's authorisation DOM if ok, otherwise NULL
    */

   public function checkAuthorization( AuthenticationHandler $inHandler, $inUser, $inPassword )
   {
      $whichHandler = $inHandler->getName();
      $this->gLogger->debug( "Checking '$inUser' is authorised using '$whichHandler'" );
 
      // We use the authorisation for this handler.
      $authenticationURI = $inHandler->getAuthenticationURI();
      $authenticator = $inHandler->getAuthenticator();
      
      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $requestParameterModule->add( "username", $inUser );
      $requestParameterModule->add( "password", $inPassword );

      if ( strlen( $authenticationURI ) == 0 ) {
         throw new UserException( "No authorisation method specified for handler: " . $inHandler->getName(),
                                   PalooseException::SITEMAP_AUTHORISE_MANAGER_PARSE_ERROR, NULL );
      }
      
      // Only one of these will be valid
      if ( strlen( $authenticationURI ) > 0 ) {
         $this->gLogger->debug( "using '$authenticationURI'" );
         // Use this URI to resubmit into the sitemap
         $uriSource = Environment::getSource( $authenticationURI );
         // If it is a naked uri (no pseudo variables) then assume that it is a resubmition to the root sitemap
         if ( $uriSource == Environment::UNKNOWN_SOURCE ) $uriSource = Environment::SUBMIT_ROOT ;
       
         $expandedAuthenticationURI = StringResolver::removePseudoProtocols( $authenticationURI );

         switch ( $uriSource ) {
         
            case Environment::SUBMIT_ROOT :
               $this->gLogger->debug( "Redirecting '$expandedAuthenticationURI' in root sitemap" );
               // Set up a new output stream that uses an internal buffer
               $this->outputStream = new OutputStream( OutputStream::BUFFER );
               // Get the appropriate sitemap
               $sitemap = Environment::$sitemapStack->root();
               $oldOutputStream = $sitemap->getOutputStream();
               $sitemap->setOutputStream( $this->outputStream );
               // Internal request so mark true
               $sitemap->run( $expandedAuthenticationURI, "", true ); //Note no query string here
               $sitemap->setOutputStream( $oldOutputStream );
               break;
                  
            case Environment::SUBMIT_CURRENT :
               $this->gLogger->debug( "Redirecting '$expandedAuthenticationURI' in current sitemap" );
               // Set up a new output stream that uses an internal buffer
               $this->outputStream = new OutputStream( OutputStream::BUFFER );
               // Get the appropriate sitemap
               $sitemap = Environment::$sitemapStack->peek();
               $oldOutputStream = $sitemap->getOutputStream();
               $sitemap->setOutputStream( $this->outputStream );
               // Internal request so mark true
               $sitemap->run( $expandedAuthenticationURI, "", true ); // Note no query string here
               $sitemap->setOutputStream( $oldOutputStream );
               break;
         
            default :
               throw new RunException( "Illegal return code '$uriSource' for string expansion source processing '$authenticationURI'", PalooseException::UNKNOWN_STRING_EXPANSION_TYPE );
         
         }
         
         $this->gLogger->debug( "Finished processing pipe with output:\n" . $this->outputStream->getBuffer() );
         // At this point the output buffer should have what we want
         // Read into a DOM for returning
         $authenticationDOM = new DOMDocument();
         $authenticationDOM->loadXML( $this->outputStream->getBuffer() );

         // The DOM should look similar to the following if it is correct
         //
         //    <authentication verify="true">
         //       <users>
         //          <user>
         //             <username>hsfr</username>
         //             <password>$2y$10$Ba4Gqbd8YwNR...NesMRWXI9fYjSVO</password>
         //             <data>
         //                <roles>displayPages,editPages,addDeletePages,displayUsers,editUsers,addDeleteUsers</roles>
         //                <fullname>Hugh Field-Richards</fullname>
         //                <email>hsfr@hsfr.org.uk</email>
         //                <telephone>01234567</telephone>
         //                <comments>super-admin</comments>
         //             </data>
         //          </user>
         //       </users>
         //    </authentication>
         //
         // If not then
         //
         //    <authentication verify="false"/>
  
         $xpath = new domxpath( $authenticationDOM );
         $username = Utilities::getXPathListStringItem( 0, $xpath, "//username" );
    //     if ( strlen( $username ) > 0 ) {
    //        $this->gLogger->debug( "User '$username' authorised" );
         $this->gLogger->debug( "Returning:\n" . $authenticationDOM->saveXML() );
         return $authenticationDOM;
   //      } else {
   //         return NULL;
         
         
     } else if ( strlen( $authenticator ) > 0 ) {
         $this->gLogger->debug( "using '$authenticator'" );
     }
  
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the list of handlers. 
    *
    * @retval array The handlers.
    */

   public function getHandlers()
   {
      return $this->gHandlers;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this Class.
    *
    * @retval string the representation of the Class as a string
    *
    */

   public function toString()
   {
      $mess = "  <authentication-manager>\n";
      $mess .= $this->gHandlers->toString();
      $mess .= "  </authentication-manager>\n";
      return $mess;
   }

}

?>
