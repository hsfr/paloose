<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage authentication
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/authentication/AuthenticationHandler.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/ExitException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * <i>AuthenticationHandlers</i> stores the list of handlers declared in the
 * authentication manager.
 *
 * It is initialised in the pipeline through the component-configurations
 * tag, for example:
 *
 * <pre>   &lt;handlers>
 *      &lt;handler name="adminHandler">
 *         &lt;redirect-to uri="cocoon:/login"/>
 *         &lt;authentication uri="cocoon:raw:/authenticate-user"/>
 *      &lt;/handler>
 *     &lt;/handlers></pre>
 *
 * @package paloose
 * @subpackage authentication
 */
 
class AuthenticationHandlers {

   
   /** Array of handlers accessed by name */
   private $gHandlerList;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __ClASS__ );
      $this->gHandlerList = array();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the authentication-manager handlers section of the sitemap.
    *
    * @param the node associated with the handlers section.
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $pipelinesDom = new DOMDocument;
      $pipelinesDom->appendChild( $pipelinesDom->importNode( $inNode, 1 ) );

      $this->gXPath = new domxpath( $pipelinesDom );
      $this->gXPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $nodeList = $this->gXPath->query( "//*" );
      $idx = 0;
      foreach ( $nodeList as $node ) {
         if ( $node->localName == "handler" ) {
            try {
               $manager = new AuthenticationHandler();
               $manager->parse( $node );
               $this->gHandlerList[ $manager->getName() ] = $manager;
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the named handler.
    *
    * @param the handler associated with this name (NULL if does not exist)
    */
    
    public function getHandler( $inName )
    {
       if ( isset( $this->gHandlerList[ $inName ] ) ) {
          return $this->gHandlerList[ $inName ];
       } else {
          throw new UserException( "Handler $inName does not exist", PalooseException::SITEMAP_ACTION_PARSE_ERROR, NULL );  //Clean up and expand
       }
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this Class.
    *
    * @retval string the representation of the Class as a string
    *
    */

   public function toString()
   {
      $mess = "   <handlers>\n";
      foreach ( $this->gHandlerList as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "   </handlers>\n";
      return $mess;
   }

}

?>
