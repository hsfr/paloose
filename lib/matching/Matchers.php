<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage matching
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/matching/Matcher.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Matchers</i> maintains a list of the matchers.
 *
 * For example
 *
 * <pre>   &lt;map:matchers default="wildcard">
 *         &lt;map:matcher name="wildcard" src="resource://lib/matching/WildcardURIMatcher"/>
 *        &lt;map:matcher name="regexp" src="resource://lib/matching/RegexpURIMatcher"/>
 *     &lt;/map:matchers></pre>
 * 
 * @package paloose
 * @subpackage pipelines
 */

class Matchers {

   private $gDefault;
   
   /** Array of matchers (each entry a class: Matcher) and indexed by name */
   private $gMatchers;

   /** The DOM object for this class */
   private $gDOM;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of <i>Matchers</i>.
    *
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gMatchers = array();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the contained matcher tags.
    *
    * For example 
    *
    * <pre>  &lt;map:matchers default="wildcard">
    *       &lt;map:matcher name="wildcard" src="resource://lib/matching/WildcardURIMatcher"/>
    *       &lt;map:matcher name="regexp" src="resource://lib/matching/RegexpURIMatcher"/>
    *    &lt;/map:matchers></pre>
    *
    * The default attribute specifies the type of matcher to use if none is specified in a pipeline.
    *
    * @param DOMNode $inNode the DOM that contains the contents of this pipe element.
    * @throws UserException if component does not have <i>name</i> and <i>src</i> attributes
    * @throws UserException if component constructor throws UserException
    */

   public function parse( $inNode )
   {
      // First thing is make node into a local DOM
      $this->gDOM = new DOMDocument;
      $this->gDOM->appendChild( $this->gDOM->importNode( $inNode, 1 ) );
   
      // Set the namespace for the sitemap.
      $xpath = new domxpath( $this->gDOM );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $component = $xpath->query( "//m:matcher" );
      // Scan around each matcher and set up the list in $gMatchers. Exit false if problem
      foreach ( $component as $node ) { 
         $name = $node->getAttribute( "name" );
         $src = $node->getAttribute( "src" );
         $isCachable = Utilities::normalizeBoolean( $node->getAttribute( "cachable" ) );
         $this->gLogger->debug( "Adding matcher '". $name . "' : '" . $src . "'" );
         if ( strlen( $name ) == 0 or strlen( $src ) == 0 ) {
            throw new UserException(
               "Sitemap component matcher must have name and src defined",
               PalooseException::SITEMAP_MATCH_PARSE_ERROR,
               $this->gDOM );
         }

         // The source may have path details attached (for example "../lib/FileGenerator"). We need to
         // strip the path off to input the correct class.
         if ( strstr( $src, Environment::getPathSeparator() ) == 1 ) {
            $packageFilename = $src;
         } else if ( !strstr( $src, ":" . Environment::getPathSeparator() ) ) {
            // Relative file name so process
            $packageFilename = PALOOSE_LIB_DIRECTORY . Environment::getPathSeparator() . $src;
         } else {
            // Relative file name with pseudo protocol
            $packageFilename = StringResolver::expandPseudoProtocols( $src );
         }
         
         $package = Utilities::stripFileName( $packageFilename );
         $path = Utilities::stripPath( StringResolver::expandPseudoProtocols( $packageFilename ) );
         $this->gLogger->debug( "Package='". $package . "' : path='" . $path . "'" );
         
         // We have something to add, so first of all check for duplicate names
         if ( array_key_exists( $name, $this->gMatchers ) ) {
            $this->gLogger->debug( "Overriding matcher: '". $name . "'" );
         } else {
            $this->gLogger->debug( "Adding matcher: '". $name . "'" );
         }   
         // Get the necessary for forming a specific matcher
         require_once( $path . $package . ".php" );
         $matcher = new Matcher( $name, $package, $node, $isCachable );
         // Check that the matcher thinks it is valid
         try {
            $matcher->isValid();
            $this->gMatchers[ $name ] = $matcher;
         } catch( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $this->gDOM );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Set the default matcher.
    *
    * @param string $inDefault the name of the default matcher.
    * @throws UserException if the matcher is not found.
    */

   public function setDefault( $inDefault )
   {
      if ( !( array_key_exists( $inDefault, $this->gMatchers ) ) ) {
           throw new UserException(
              "Default matcher '". $inDefault . "' not found",
              PalooseException::SITEMAP_MATCH_PARSE_ERROR,
              $this->gDOM );
      }
      $this->gDefault = $inDefault;
      $this->gLogger->debug( "Set default matcher: '". $inDefault . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the name of default matcher.
    *
    * @retval string the name of the default matcher.
    */

   public function getDefault()
   {
      return $this->gDefault;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the class name of named matcher.
    *
    * @param string $inName the name of the matcher.
    * @retval string the class name of the matcher.
    */

   public function getMatcherClassName( $inName )
   {
      if ( !( array_key_exists( $inName, $this->gMatchers ) ) ) {
           throw new UserException(  "Matcher '". $inName . "' not found", PalooseException::SITEMAP_MATCH_PARSE_ERROR, $this->gDOM );
      } else {
         $className = $this->gMatchers[ $inName ]->getPackageName();
         return $className;
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this Class.
    *
    * @retval string the representation of the Class as a string
    *
    */

   public function toString()
   {
      $mess = "  <matchers default='{$this->gDefault}'>\n";
      foreach ( $this->gMatchers as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "  </matchers>\n";
      return $mess;
   }

}

?>