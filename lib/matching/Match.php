<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage matching
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Aggregator.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Mount.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Redirect.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/Pipe.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/selection/Selectors.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Match</i> manages a single matcher within a pipeline.
 *
 * They follow the following format
 *   
 * <pre>   &lt;map:pipeline xmlns:map="http://apache.org/cocoon/sitemap/1.0" internal-only="true/false">
 *       &lt;!-- List of match components -->
 *       &lt;!-- Error handler -->
 *    &lt;/map:pipeline></pre>
 *      
 * @package paloose
 * @subpackage matching
 */

class SimpleMatch {

   /** Pipe associated with this match (Class Pipe) */   
   private $gPipe;

   /** Match pattern */   
   private $gPattern;

   /** The type of matcher: regep, wildcard etc */   
   private $gType;

   /** The default matcher type */   
   private $gDefaultMatcher;

   /** The matcher (derived from type) */   
   private $gMatcher;

   /** Logger instance for this class */   
   private $gLogger;

   /** The DOMXPath object */
   private $gXPath;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the match tags.
    *
    * @param DOMNode $inNode the node representing all the match contents
    * @throws UserException if parse error
    */

   public function parse( $inNode ) 
   {
      // Get the default for this matchers
      $matchers = Environment::$sitemapStack->root()->getMatchers();
      $this->gDefaultMatcher = $matchers->getDefault();

      // Get the pattern that we must match the URL to
      $this->gPattern = $inNode->getAttribute( "pattern" );
      $this->gType = $inNode->getAttribute( "type" );
      if ( !$this->gType ) {
         $this->gType = $this->gDefaultMatcher;
      }
      $this->gLogger->debug( "match pattern: '". $this->gPattern . "' of type '". $this->gType . "'" );

      // Get the pattern package for this matcher
      try {
         $this->gMatcher = $matchers->getMatcherClassName( $this->gType );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }

      // First thing is make node into a local DOM
      $matchDom = new DOMDocument;
      $matchDom->appendChild( $matchDom->importNode( $inNode, 1 ) );

      // Check that pattern is present
      if ( $this->gPattern == "" ) {
         throw new UserException(
            "Must be a pattern attribute present and not NULL",
            PalooseException::SITEMAP_MATCH_PARSE_ERROR,
            $matchDom );
      }

      $this->gXPath = new domxpath( $matchDom );
      $this->gXPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      // Scan the enclosed pipe
      $component = $this->gXPath->query( "//m:match/*" );
      $this->gLogger->debug( "Making new Pipe with " . $component->length. " components" );
      $this->gPipe = new Pipe();
      foreach ( $component as $pipeNode ) {
         $name = $pipeNode->localName;
         $this->gLogger->debug( "pipe node: '". $name . "'" );
         // This is where we build a pipe. The validity of these pipes is carried out in that class (Pipe)
         switch ( $name ) {

               case "aggregate" :
                  try {
                     $aggregator = PipeElement::createAggregator( $pipeNode );
                     $this->gLogger->debug( "Creating aggregator using component '" . get_class( $aggregator ) . "'" );
                     // Put it into the pipe
                     $this->gPipe->addComponent( $aggregator );
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  }
                  break;
               
               case "generate" :
                  try {
                     $generator = PipeElement::createGenerator( $pipeNode );
                     $this->gLogger->debug( "Creating generator: type='" . $generator->getType() . "' src='" . $generator->getSrc() . "' using component '" . get_class( $generator ) . "'" );
                     $this->gPipe->addComponent( $generator );
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  }
                  break;
                  
               case "transform" :
                  try {
                     $transformer = PipeElement::createTransformer( $pipeNode );
                     $this->gLogger->debug( "Creating transformer: type='" . $transformer->getType() . "' src='" . $transformer->getSrc() . "' using component '" . get_class( $transformer ) . "'" );
                     $this->gPipe->addComponent( $transformer );
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  }
                  break;

               case "serialize" :
                  try {
                     $serializer = PipeElement::createSerializer( $pipeNode );
                     $this->gLogger->debug( "Creating serializer: type='" . $serializer->getType() . "' src='" . $serializer->getSrc() . "' using component '" . get_class( $serializer ) . "'" );
                     $this->gPipe->addComponent( $serializer );
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  }
                  break;
               
               case "read" :
                  try {
                     $reader = PipeElement::createReader( $pipeNode );
                     $this->gLogger->debug( "Creating reader: type='" . $reader->getType() . "' src='" . $reader->getSrc() . "' using component '" . get_class( $reader ) . "'" );
                     $this->gPipe->addComponent( $reader );
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  }
                  break;

            case "mount" :
               try {
                  $mount = PipeElement::createMount( $pipeNode );
                  $this->gLogger->debug( "Creating mount" );
                  $this->gPipe->addComponent( $mount );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;

            case "select" :
               try {
                  $selector = PipeElement::createSelector( $pipeNode );
                  $this->gLogger->debug( "Creating selector: type='" . $selector->getType() . "' using component '" . get_class( $selector ) . "'" );
                  $this->gPipe->addComponent( $selector );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;

            case "call" :
               try {
                  $call = PipeElement::createCall( $pipeNode );
                  $this->gLogger->debug( "Creating call: name='" . $call->getResourceName() . "' using component '" . get_class( $call ) . "'" );
                  //echo "Creating call: name='" . $call->getResourceName() . "' using component '" . get_class( $call ) . "'";
                  $this->gPipe->addComponent( $call );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
               
            case "act" :
               try {
                  $action = PipeElement::createAction( $pipeNode );
                  $this->gLogger->debug( "Creating action: name='" . $action->getType() . "' using component '" . get_class( $action ) . "'" );
                  $this->gPipe->addComponent( $action );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;

            case "redirect-to" :
               try {
                  $redirect = PipeElement::createRedirect( $pipeNode );
                  $this->gLogger->debug( "Creating redirect-to" );
                  $this->gPipe->addComponent( $redirect );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
}
         
      }
      $this->gLogger->debug( "Finished processing match" );
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Take the inputted URL and query string see whether there is a match.
    * 
    * It sets
    * matched variables into $map and pushes it onto the stack before invoking the
    * enclosed pipe if there was a match.
    * 
    * @param inURL the calling URL
    * @retval true if matched and processed, otherwise false
    * @throws ExitException if match requires an early exit (match succeeded)
    * @throws RunException if exception is raised when running (null pattern etc)
    */

   public function run( $inURL )
   {
      $this->gLogger->debug( "URL: $inURL" );   
      // First thing is to check whether the URL matches the pattern for this matcher.
      $matcher = new $this->gMatcher();
      $map = array();
      try {
         $isMatch = $matcher->match( $inURL, $this->gPattern );
      } catch ( MatcherException $e ) {
         $this->gLogger->debug( $e->getMessage() );
         throw new RunException( $e->getMessage(), $e->getCode() );
      }
      $map = $matcher->getHashMap();

      try {
         if ( $isMatch == 1 ) {
            $this->gLogger->debug( "Matched '$inURL' with '{$this->gPattern}' in '" . Environment::$sitemapStack->peek()->getFilename() . "'" );
            for ( $i = 0; $i < count( $map ); $i++ ) {
               $this->gLogger->debug( "map[ $i ] = '{$map[ $i ]}'" );
               // echo "map[ $i ] = '{$map[ $i ]}'";
            }
            // We have a match so kick off the pipe processor
            Environment::$variableStack->push( $map );
            // Do not take in DOM here as this is not a label pipe
            $this->gPipe->run( Environment::$variableStack, $inURL, Environment::$gQueryString, NULL );
            // Probably have to check for errors to run associated error pipe here
            // add later :-)
            Environment::$variableStack->pop();
            return true;
         } else {
            $this->gLogger->debug( "No match '$inURL' with '{$this->gPattern}' in '" . Environment::$sitemapStack->peek()->getFilename() . "'" );
            // NB - No exception here
         }
      } catch ( ExitException $e ) {
         throw new ExitException();
      } catch ( RunException $e ) {
         throw new RunException( $e->getMessage(), $e->getCode(), NULL );
      }
      return false;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getMatcher()
   {
      return $this->gMatcher;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return string representation of this class.
    *
    * @retval string the string representation of this component.
    */

   public function toString()
   {
      $mess = "   <match pattern='{$this->gPattern}' type='{$this->gType}' package='{$this->gMatcher}'>\n";
      $mess .= $this->gPipe->toString();
      $mess .= "   </match>\n";
      return $mess;
   } 

}

?>