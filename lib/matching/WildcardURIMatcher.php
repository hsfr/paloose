<?php

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Concepts of the software are Copyright 1999-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 ( the "License" );
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package paloose
 * @subpackage matching
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2023 Hugh Field-Richards, Apache Software Foundation (see below)
 *
 * I freely admit that I have plagiarised the function compilePattern() from the Apache
 * Cocoon WildcardHelper package on which it is based. However I have opted to use a
 * regular expression based approach for the remainder.
 *
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/MatcherException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>WildcardURIMatcher</i> a simplified matching scheme. For eaxmple
 *
 * <pre>   &lt;map:match pattern="* /*.html">
 *       ...
 *   &lt;/map:match></pre>
 *
 * The wildcards in the match pattern are assigned to internal pattern variables "{1}", "{2}"
 * etc, in a similar fashion to Perl regular expressions. So in the above case if the
 * pattern to be matched is "documentation/index.html" then there would be a match.
 * The internal variables would be set {1} = "documentation" and {2} = "index". These
 * variables are used within the various pipeline components to resolve string expressions.
 * For example
 *
 * <pre>   &lt;map:match pattern="* /*.html">
 *       &lt;map:generate src="context://{1}/{2}.xml"/>
 *       ...
 *    &lt;/map:match></pre>
 *
 * In this case the generator would fetch the file documentation/index.xml from the current
 * application context (see pseudo protocols below). Note that the constructions "**" and "*"
 * are different although identical to their Cocoon counterparts.
 *
 * <ul>
 *   <li>"*"  - Match any character except the path separator.</li>
 *   <li>"**"  - Match any character including the path separator.</li>
 * </ul>
 *
 * The values for the pattern variables are taken from the current matcher. If you want to
 * access a higher matcher (wherever it is) then you would have to use the string {../1},
 * which means access the pattern variable {1} in the previous matcher. These can be extended
 * as necessary, for example {../../1} to access a matcher two levels above the current one.
 *
 * @package paloose
 * @subpackage matching
 */

class WildcardURIMatcher {
    
    /** The int representing '*' in the pattern int [ ]. */
    const MATCH_FILE = -1;
    const MATCH_FILE_REGEXP = "([^\/]*)";
    
    /** The int representing '**' in the pattern int [ ]. */
    const MATCH_PATH = -2;
    const MATCH_PATH_REGEXP = "(.*)";
    
    /** The int representing begin in the pattern int [ ]. */
    const MATCH_BEGIN = -4;
    const MATCH_BEGIN_REGEXP = "/^";
    
    /** The int representing end in pattern int [ ]. */
    const MATCH_THEEND = -5;
    const MATCH_THEEND_REGEXP = "\$/";
    
    /** The int value that terminates the pattern int [ ]. */
    const MATCH_END = -3;
    const MATCH_END_REGEXP = "\$/";
    
    private $gHashMap;
    
    /** Logger instance for this class */
    private $gLogger;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Construct a new instance of <i>WildcardURIMatcher</i>.
     */
    
    public function __construct()
    {
        $this->gLogger = Logger::getLogger( __CLASS__ );
        $this->gHashMap = array();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * This function translates a String into an int array
     * converting the special '*' and '\' characters.
     * <br>
     * Here is how the conversion algorithm works:
     * <ul>
     *   <li>The '*' character is converted to MATCH_FILE, meaning that zero
     *        or more characters ( excluding the path separator '/' ) are to
     *        be matched.</li>
     *   <li>The '**' sequence is converted to MATCH_PATH, meaning that zero
     *       or more characters ( including the path separator '/' ) are to
     *        be matched.</li>
     *   <li>The '\' character is used as an escape sequence ( '\*' is
     *       translated in '*', not in MATCH_FILE ). If an exact '\' character
     *       is to be matched the source string must contain a '\\'.
     *       sequence.</li>
     * </ul>
     * When more than two '*' characters, not separated by another character,
     * are found their value is considered as '**' ( MATCH_PATH ).
     * <br>
     * The array is always terminated by a special value ( MATCH_END ).
     * <br>
     * All MATCH* values are less than zero, while normal characters are equal
     * or greater.
     *
     * For example $inData is "**.html" the preparedPattern will be an array
     *
     *      preparedPattern[ 0 ]: 'MATCH_BEGIN'
     *      preparedPattern[ 1 ]: 'MATCH_PATH'
     *      preparedPattern[ 2 ]: '.'
     *      preparedPattern[ 3 ]: 'h'
     *      preparedPattern[ 4 ]: 't'
     *      preparedPattern[ 5 ]: 'm'
     *      preparedPattern[ 6 ]: 'l'
     *      preparedPattern[ 7 ]: 'MATCH_THEEND'
     *
     * @param string $inData The pattern string to translate.
     * @retval array The encoded string as an int array.
     */
    
    private function compilePattern( $inData )
    {
        $expr = "";
        unset( $expr );
        $buff = $this->toCharArray( $inData );
        
        //Prepare variables for the translation loop
        $y = 0;
        $slash = false;
        
        //Must start from beginning
        $expr[ $y++ ] = self::MATCH_BEGIN;
        
        if ( !empty( $buff ) ) {
            if ( $buff[ 0 ]=='\\' ) {
                $slash = true;
            } else if ( $buff[ 0 ] == '*' ) {
                $expr[ $y++ ] = self::MATCH_FILE;
            }  else {
                $expr[ $y++ ] = $buff[ 0 ];
            }
            
            //Main translation loop
            for ( $x = 1; $x < count( $buff ); $x++ ) {
                // If the previous char was '\' simply copy this char.
                if ( $slash ) {
                    $expr[ $y++ ] = $buff[ $x ];
                    $slash = false;
                    // If the previous char was not '\' we have to do a bunch of checks
                } else {
                    // If this char is '\' declare that and continue
                    if ( $buff[ $x ] == '\\' ) {
                        $slash = true;
                        // If this char is '*' check the previous one
                    } else if ( $buff[ $x ] == '*' ) {
                        // If the previous character als was '*' match a path
                        if ( $expr[ $y-1 ] <= self::MATCH_FILE ) {
                            $expr[ $y-1 ] = self::MATCH_PATH;
                        } else {
                            $expr[ $y++ ] = self::MATCH_FILE;
                        }
                    } else {
                        $expr[ $y++ ]=$buff[ $x ];
                    }
                }
            }
        }
        // Must match end at the end (not used)
        $expr[ $y ] = self::MATCH_THEEND;
        return $expr;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Convert the compiled pattern to a regular expression.
     *
     * @param array $inPattern The pattern to translate as an array of bytes.
     * @param string $inPatternString The pattern to translate (for tracing).
     * @retval string the regular expression.
     */
    
    public function pattern2regexp( $inPattern, $inPatternString )
    {
        
        $regexp = "";
        
        for ( $i = 0; $i < count( $inPattern ); $i++ ) {
            $chr = $inPattern[ $i ];
            
            switch ( $chr ) {
                    
                case self::MATCH_BEGIN :
                    $regexp .= self::MATCH_BEGIN_REGEXP;
                    $inText = false;
                    break;
                    
                case self::MATCH_PATH :
                    
                    $regexp .= self::MATCH_PATH_REGEXP;
                    break;
                    
                case self::MATCH_FILE:
                    $regexp .= self::MATCH_FILE_REGEXP;
                    break;
                    
                case self::MATCH_THEEND :
                    $regexp .= self::MATCH_THEEND_REGEXP;
                    break;
                    
                case "$" :
                    $regexp .= "\\\$";
                    break;
                    
                case "^" :
                    $regexp .= "\^";
                    break;
                    
                case "/" :
                    $regexp .= "\/";
                    break;
                    
                case "." :
                    
                    $regexp .= "\.";
                    break;
                    
                default :
                    $regexp .= $chr;
                    
            }
            
            $this->gLogger->debug( "inPattern[ $i ]: '$chr'" );
        }
        
        $this->gLogger->debug( "regexp: '$regexp' from '$inPatternString'" );
        return $regexp;
        
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Match a pattern against a string and isolates wildcard replacement into a
     * Stack.
     *
     * @param string $inData The string to match.
     * @param string $inPattern The pattern against which to map.
     * @retval boolean true or false on the match success.
     * @exception MatcherException If data or pattern is null.
     */
    
    public function match( $inData, $inPattern )
    {
        $globalsModule = Environment::$modules[ 'global' ];
        $this->gLogger->debug( "Trying to match '$inData' with '$inPattern'" );
        
        $preparedPattern = $this->compilePattern( $inPattern );
        $this->gLogger->debug( "Trying to match '$inData' with '$inPattern'" );
        
        $regExprPattern = $this->pattern2regexp( $preparedPattern, $inPattern );
        
        $this->gHashMap = array();
        $m = preg_match( $regExprPattern, $inData, $this->gHashMap );
        
        // Transfer into globals module
        $i = 0;
        foreach( $this->gHashMap as $value ) {
            $this->gLogger->debug( "Adding $value to map at $i" );
            $globalsModule->add( "__$i", $value );
            $i++;
        }
        return $m;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get the hash map of this matcher.
     *
     * @retval string the hash map.
     */
    
    public function getHashMap()
    {
        return $this->gHashMap;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Converts a string to an array of characters
     *
     * @param string $inString string to convert
     * @retval array character array of string
     */
    
    function  toCharArray( $inString )
    {
        $len = strlen( $inString );
        for ( $j = 0; $j < strlen ( $inString ); $j++ ) {
            $char[ $j ]  =  substr( $inString,  $j,  1 );
        }
        return $char;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Return a string representation of this class (used for debugging).
     *
     * @retval string the string representation.
     */
    
    public function toString( )
    {
        $mess = "<matcher name='{$this->gType}' src='{$this->gSrc}' package='{$this->classname( )}'/>\n";
        return $mess;
    }
    
}
?>
