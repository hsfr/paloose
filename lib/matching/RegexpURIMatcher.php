<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage matching
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/MatcherException.php" );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>RegexpURIMatcher</i> supports regular expression matching.
 *
 * Since version 0.7.0 it is possible to carry out matching using regular expressions.
 * Paloose regular expressions are not the same as Cocoon regular expressions.
 * The PHP5 version based on Perl is used and a full description of the regexp can be
 * found on the PHP5 manual page. The pattern variables work in similar fashion as
 * the wildcard matcher. For example:
 *
 * <pre>   &lt;map:match type="regexp" pattern="/.*\.(html)|(tex)|(xml)/">
 *       ...
 *    &lt;/map:match></pre>
 *
 * would match all requests that end in "html", "tex" and "xml". In the following all requests
 * for documents between "nb1996" and "nb1999" would read the html file directly:
 *
 * <pre>   &lt;map:match type="regexp" pattern="/nb199([6789]).html/">
 *       &lt;map:read src="context://nb/199{1}/nb199{1}.html"/>
 *    &lt;/map:match></pre>
 *
 * and the following would take all others in 2000 or later and process them from an XML file.
 *
 * <pre>   &lt;map:match type="regexp" pattern="/nb20(.+).html/">
 *       &lt;map:generate src="cocoon:/20{1}/nb20{1}.xml" label="xml-content"/>
 *       &lt;map:transform src="context://resources/transforms/notebooks-html.xsl" label="page-transform">
 *          &lt;map:parameter name="page" value="nb20{1}"/>
 *       &lt;/map:transform>
 *       &lt;map:serialize/>
 *    &lt;/map:match></pre>
 *
 * @package paloose
 * @subpackage matching
 * @since 0.7.0
 */

class RegexpURIMatcher {

    private $gHashMap;
    
    /** Logger instance for this class */
    private $gLogger;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Construct a new instance of <i>RegexpURIMatcher</i>.
     */
    
    public function __construct()
    {
        $this->gLogger = Logger::getLogger( __CLASS__ );
        $this->gHashMap = array();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Match a pattern against a regular expression (Perl) and isolate wildcard
     * matched variables ($0, $1 etc).
     *
     * @param string $inData The string to match.
     * @param string $inPattern The pattern against which to map.
     * @retval boolean true or false on the match success.
     */
    
    public function match( $inData, $inPattern )
    {
        $globalsModule = Environment::$modules[ 'global' ];
        
        $this->gHashMap = array(); //_pad( array(), 1, 0 );
        $m = preg_match( $inPattern, $inData, $this->gHashMap );
        // Transfer into globals module
        $i = 0;
        foreach( $this->gHashMap as $value ) {
            $globalsModule->add( "__$i", $value );
            $i++;
        }
        return $m;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get the hash map of this matcher.
     *
     * @retval string the hash map.
     */
    
    public function getHashMap()
    {
        return $this->gHashMap;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Return a string representation of this class (used for debugging).
     *
     * @retval string the string representation.
     */
    
    public function toString( )
    {
        $mess = "<matcher name='{$this->gType}' src='{$this->gSrc}' package='{$this->classname( )}'/>\n";
        return $mess;
    }
    
}
?>
