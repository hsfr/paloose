<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage environment
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */
 
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/VariableStack.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>StringResolver</i> Class expands pseudo-protocols and
 * sitemap variables. It is a purely static class.
 *
 * Parts are based on the Cocoon Java equivalent.
 *
 * @package paloose
 * @subpackage environment
 */
 
class StringResolver {

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Expands a string with a previously analysed regexp. It caters for previous
    * context constructions such as "{module:variable}".
    *
    * @param string $inData the string to expand.
    * @retval string after parameter substitution.
    */

   public static function expandModuleVariable( $inData )
   {
      $inputString = $inData;
      $finished = false;
      $matchPattern = "|([\s\S]+)\{([^\{]+):([\w-]+)\}([\s\S]+)|";
      // echo "expandModuleVariable === [inData: $inData]";

      while ( !$finished ) {
         $result = preg_match( $matchPattern, $inputString, $matches, PREG_OFFSET_CAPTURE);
         if ( $result ) {
            $module = $matches[2][0];
            $var = $matches[3][0];
            $module = Environment::$modules[ $module ];
            $replacementString = $module->get( $var );
            $inputString = preg_replace( $matchPattern, $replacementString, $inputString );
         } else {
            $finished = true;
         }
      }
      // echo " ==> [inputString: $inputString]<br/>";
      return $inputString;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Expands a string with a previously analysed regexp. It caters for previous
    * context constructions such as "{../../1}".
    *
    * @param array $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inData the string to expand.
    * @retval string after parameter substitution.
    */

   public static function expandString( $inVariableStack, $inData )
   {
       $inputString = $inData;
      $finished = false;
      $modulePattern = "|([\s\S]*)\{([^\{]+):([\w-]+)\}([\s\S]*)|";
      $variablePattern = "|([\s\S]*)\{((../)*\d+)\}([\s\S]*)|";
      // echo "expandString === [inData: $inData]<br/>";

      while ( !$finished ) {
         $result = preg_match( $modulePattern, $inputString, $matches, PREG_OFFSET_CAPTURE);
          if ( $result ) {
            $module = $matches[2][0];
            $var = $matches[3][0];
            $module = Environment::$modules[ $module ];
            $replacementString = $module->get( $var );
            $replacementString = "$1$replacementString$4";
            $inputString = preg_replace( $modulePattern, $replacementString, $inputString );
         } else {
            // Might be a Paloose variable (eg {../../1}) so check for that.
            $result = preg_match( $variablePattern, $inputString, $matches, PREG_OFFSET_CAPTURE );
            if ( $result ) {
               $replacementString = self::processVar( $inVariableStack, $matches[2][0] );
               $replacementString = $matches[1][0] . $replacementString . $matches[4][0];
               $inputString = preg_replace( $variablePattern, $replacementString, $inputString );
            } else {
               $finished = true;
            }
         }
      }
      // echo " ==> [returnString: $inputString]<br/>";
      return $inputString;
   }

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Process the variable string. This analyses strings of the form "../../n" and
    * returns the appropriate string resolved from the variable stack level (2 in
    * the above form), and entry position (n) on the variable list.
    *
    * @param array $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inVar the variable string to resolve.
    * @retval string the resolved string.
    */

   private static function processVar( $inVariableStack, $inVar )
   {
      $p = explode( "/", $inVar );
      //echo "[var: " . $inVar . "]<br/>";
      $level = count( $p ) - 1;
      //echo "[level: " . $level . "]<br/>";
      $map = $inVariableStack->entryAt( $level );
      $varIndex = $p[ count( $p ) - 1 ];
      //echo "[varIndex: $varIndex]<br/>";
      //echo "[map entry: " . $map[ $varIndex ] . "]<br/>";
      if ( array_key_exists( $varIndex, $map ) ) {
         return $map[ $varIndex ];
      }
      return "";
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Expand the pseudo protocols. For example "context://".
    *
    * @param string $inString the string to be expanded
    * @retval string the expanded string
    */

   public static function expandPseudoProtocols( $inString )
   {
      if ( !is_string( $inString ) ) {
         return $inString;
      }
      $orig = $inString;
      $srcString = "resource://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return str_replace( $srcString, PALOOSE_DIRECTORY . Environment::getPathSeparator(), $inString );
      }
      $srcString = "context://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return str_replace( $srcString, Environment::$configuration[ 'siteRootDirectory' ] . Environment::getPathSeparator(), $inString );
      }
      $srcString = "cocoon://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return str_replace( $srcString, Environment::$gRootSitemapDir . Environment::getPathSeparator(), $inString );
      }
      $srcString = "cocoon:/";
      if ( strpos( $inString, $srcString ) !== false ) {
         return str_replace( $srcString, Environment::$gCurrentSitemapDir . Environment::getPathSeparator(), $inString );
      }
      return $inString;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Remove the pseudo protocols. This is a special case of the above where we
    * never have relative directories. In other words we eat "cocoon://" etc.,
    * but not "context://" or "resource://" which we expand. Nasty but necessary.
    *
    * @param string $inString the string to be altered
    * @retval string the altered string
    */

   public static function removePseudoProtocols( $inString )
   {
      $srcString = "resource://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return str_replace( $srcString, PALOOSE_DIRECTORY . Environment::getPathSeparator(), $inString );
      }
      $srcString = "context://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return str_replace( $srcString, Environment::getContext(), $inString );
      }
      $srcString = "cocoon://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return str_replace( $srcString, "", $inString );
      }
      $srcString = "cocoon:/";
      if ( strpos( $inString, $srcString ) !== false ) {
         return str_replace( $srcString, "", $inString );
      }
      return $inString;
   }

}

?>
