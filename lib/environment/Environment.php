<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage environment
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The Environment Class is a static class that stores variables and methods
 * that relate to the current running environment. It also includes the global
 * variables, This is because programs like WordPress fiddle with the global system.
 *
 * @package paloose
 * @subpackage environment
 */

class Environment {

    /** Read from a file. */
    const UNKNOWN_SOURCE = -1;
    /** Read from a file. */
    const FILE_READ = 1;
    /** Submit the file request back into the sitemap. */
    const SUBMIT_ROOT = 2;
    /** Submit the file request back into the sitemap. */
    const SUBMIT_CURRENT = 3;

   /** Context for sitemap */
   private static $gContext;

   /** The directory of the root site directory */
   public static $gCurrentSiteRootDir;
   
   /** The directory of the current sitemap directory */
   public static $gCurrentSitemapDir;
   
   /** Directory of root Sitemap being processed */
   public static $gRootSitemapDir;

   /** The OS we are running under */
   private static $gOS;

   /** Root directory where documents are held */
   public static $documentRootDir;

   /** Originating URI */
   public static $gRequestURI;

   /** Originating Query string */
   public static $gQueryString;

   /** Paloose is acting as a client not the main server. Used when working with WordPress etc. */
   public static $isPalooseClient;
   
   /** The putput buffer for client working. */
   public static $gClientBuffer;
   
   /** The stack that holds all the sitemaps and subsitemaps */
   public static $sitemapStack;

   /** The stack that holds all the sitemap etc. variables for a particular level */
   public static $variableStack;

   /**  Convenience array to hold config info */
   public static $configuration;

   /**  Array of Paloose modules */
   public static $modules;
   
   /**  The handler for sessions */
   public static $sessionHandler;

   /**  Client output buffer */
   public static $clientOutputBuffer;

   /**  True if we are using cookies - set by the CookiesModule */
   public static $gCookiesActive;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Note that the context is an absolute path in Paloose.
    *
    * @retval string the context as an absolute path
    */
    
    public static function getContext()
    {
      $filename = $_SERVER[ 'SCRIPT_FILENAME' ];
       return substr( $filename, 0, strrpos( $filename, self::getPathSeparator() ) + 1 );
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the path separator for this operating system.
    *
    * @retval string path separator character
    */

    public static function getPathSeparator()
    {
      if ( stripos( PHP_OS, "WINNT" ) ) {
         return "\\";
      }
      return "/";
    }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Decide where the source is coming from (do we resubmit? etc.)
    *
    * @param string $inString the string to be analysed
    * @retval string the resubmit/read code
    */

   public static function getSource( $inString )
   {
      $srcString = "resource://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return self::FILE_READ;
      }
      $srcString = "context://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return self::FILE_READ;
      }
      $srcString = "cocoon://";
      if ( strpos( $inString, $srcString ) !== false ) {
         return self::SUBMIT_ROOT;
      }
      $srcString = "cocoon:/";
      if ( strpos( $inString, $srcString ) !== false ) {
         return self::SUBMIT_CURRENT;
      }
      return self::UNKNOWN_SOURCE;
   }

}

?>