<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage environment
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The <i>Utilities</i> Class is a collection of useful static methods.
 *
 * @package paloose
 * @subpackage environment
 */

class Utilities {

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Encode array as XML structure.
    *
    * @param array $inArray the array to translate
    * @retval string the encoded array
   */

   public static function arrayToXML( $inArray, $inRootName )
   {
      $str = "<$inRootName>";
      foreach ( $inArray as $name => $value ) {
         $str .= "   <$name>$value</$name>";
      }
      $str .= "</$inRootName>";
      return $str;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Extract a node from an XPath result list.
    *
    * @param int $inIndex the index number of the item on the list
    * @param DOMXpath $inXPath the XPath used to get the list
    * @param string $inExpress the XPath expression
    * @retval DOMNode the returned node (null if nothing)
   */

   public static function getXPathListNode( $inIndex, DOMXpath $inXPath, $inExpress )
   {
      $nodeList = $inXPath->query( $inExpress );
      if ( $nodeList == NULL or $nodeList->length == 0 ) return NULL;
      return $nodeList->item( $inIndex );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Extract an item from an XPath result list.
    *
    * @param int $inIndex the index number of the item on the list
    * @param DOMXpath $inXPath the XPath used to get the list
    * @param string $$inExpression the XPath expression
    * @retval string the returned string (zero length string if nothing)
   */

   public static function getXPathListStringItem( $inIndex, DOMXpath $inXPath, $inExpression )
   {
      $nodeList = $inXPath->query( $inExpression );
      if ( $nodeList == NULL or $nodeList->length == 0 ) return "";
      return $nodeList->item( $inIndex )->textContent;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Strip the path from the filename. If a pseudo protocol is present it expands it.
    *
    * @param string $inString the path/filename to be expanded and stripped
    * @retval string the expanded path (or empty string if there is no path)
   */

   public static function stripPath( $inString )
   {
      $expStr = StringResolver::expandPseudoProtocols( $inString );
      $path = "";
      if ( strstr( $expStr, Environment::getPathSeparator() ) ) {
         $posOfLastSep = strrpos( $expStr, Environment::getPathSeparator() );
         $path = substr( $expStr, 0, $posOfLastSep + 1 );
      }
      return $path;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Strip the filename from the path.
    *
    * @param string $inString the path/filename to be stripped
    * @retval string the file name
    */

   public static function stripFileName( $inString )
   {
      if ( strstr( $inString, Environment::getPathSeparator() ) ) {
         $posOfLastSep = strrpos( $inString, Environment::getPathSeparator() );
         return substr( $inString, $posOfLastSep + 1 );
      } else {
         return $inString;
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Strip the extension from the file name.
    *
    * @param string $inString the path/filename to be stripped
    * @retval string the file name without extension
    */

   public static function stripExtension( $inString )
   {
      $posOfLastSep = strrpos( $inString, '.' );
      return substr( $inString, 0, $posOfLastSep ); 
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Add a trailing path separator if necessary.
    *
    * @param string $inDirectory the path to be modified
    * @retval string the amended path
    */

   public static function addTrailingSeparator( $inDirectory )
   {
      if ( $inDirectory != NULL and strlen( $inDirectory ) > 0 and strrpos( $inDirectory, Environment::getPathSeparator() ) < strlen( $inDirectory ) - 1 ) {
         return $inDirectory . Environment::getPathSeparator();
      } else {
         return $inDirectory;
      }
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
     * Join path components
     *
     * @param string $left  Path component, left of the directory separator
     * @param string $right Path component, right of the directory separator
     *
     * @return string
     */
     
    public static function join( $left, $right )
    {
        return rtrim( $left, '/\\' ) . DIRECTORY_SEPARATOR . ltrim( $right, '/\\') ;
    }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Normalize boolean entry. 'true', '1', 'yes' are all resolved (case insensitive)
    * to 'true'. Everything else is false.
    *
    * @param string $inString the boolean string
    * @retval string the normalized string
    */

   public static function normalizeBoolean( $inBoolean )
   {
      if ( strnatcasecmp( $inBoolean, "true" ) == 0 or strnatcasecmp( $inBoolean, "yes" ) == 0 or $inBoolean === true  or $inBoolean == 1  ) {
         return 1;
      } else {
         return 0;
      }
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Output string as inline verbatim text.
    *
    * @param string $inString the string
    */

   public static function outputStringVerbatim( $inString )
   {
      echo "<pre>" . htmlspecialchars( $inString ) . "</pre>";
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Translate string into hex output.
    *
    * @param string $inString the string
    * @param boolean $inWithChar true to add character
    */

   public static function makeStringHex( $inString, $inWithChar = false )
   {
      $str = "";
      for ( $i = 0; $i < strlen( $inString ); $i++ ) {
         $ch = substr( $inString, $i, 1 );
         if ( $inWithChar ) {
            $str .= "{" . $ch . ":" . bin2hex( $ch ) . "}";
         } else {
            $str .= "{" . bin2hex( $ch ) . "}";
         }
      }
      return $str;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Enclose tagged data in string with CDATA.
    *
    * It only detects strings within, for example, "<code>...</code>".
    *
    * @param String $inData the string to process.
    * @param String $inTag the element which encloses the processed string.
    * @retval String the transformed string.
    */
   
   public static function cdataEncode( $inData, $inTag )
   {
      if ( $inTag == "" ) {
         return $inData;
      }
      
      $inputString = $inData;
      $pattern = "/([\s\S]+)(<" . $inTag . "[^>]*>)([\s\S]+)(<\/" . $inTag . ">)([\s\S]+)/i";
      $result = preg_match( $pattern, $inputString, $matches, PREG_OFFSET_CAPTURE);
   
      if ( $result ) {
         $leftString = $matches[1][0];
         $startTag = $matches[2][0];
         $codeString = $matches[3][0];
         $endTag = $matches[4][0];
         $rightString = $matches[5][0];
         // Convert all &, < and > from code string
         $patterns = array();
         $patterns[0] = '/&/';
         $patterns[1] = '/\</';
         $patterns[2] = '/\>/';
         $replacements = array();
         $replacements[2] = '&amp;';
         $replacements[1] = '&lt;';
         $replacements[0] = '&gt;';
         $codeString = preg_replace( $patterns, $replacements, $codeString );
         
         $replacementString = $leftString . $startTag. $codeString . $endTag . $rightString;
         $inputString = preg_replace( $pattern, $replacementString, $inputString ) . "\r";
      }
      return $inputString;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Enclose tagged data in string with CDATA.
    *
    * It only detects strings within, for example, "<code>...</code>".
    *
    * @param DOMDocument $$inDOM the document to process.
    * @param String $inTag the element which encloses the processed string.
    * @retval DOMDocument the transformed document.
    */
   
   public function processCDATA( DOMDocument $inDOM, $inTag )
   {
      $newString = $inDOM->saveXML();
      
      if ( $inTag == "" ) {
         return $inDOM;
      }
      
      $newString = Utilities::cdataEncode( $newString, $inTag );
      $outputDocument = new DOMDocument();
      $outputDocument->loadXML( $documentString );
      return $outputDocument;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Query string to URL converter.
    *
    * <p>Because of the way that Paloose works I have to extract the proper URI
    * from a wrapped version (done in the .htaccess file). That file has the
    * following typical rewrite rules:</p>
    *
    * <pre>   RewriteEngine On
    *   RewriteRule (.+)\.html paloose.php?url=$1.html [L,qsappend]
    *   RewriteRule (.+)\.rng paloose.php?url=$1.rng [L,qsappend]</pre>
    *
    * <p>which means we have to take a typical URI of, say</p>
    *
    * <pre>   /palooseTest/index.html?rng=test</pre>
    * 
    * <p>which is rewritten to </p>
    *
    * <pre>   /paloose.php?url=index.html&rng=test</pre>
    *
    * <p>which we then have to extract the URI as </p>
    * 
    * <pre>   index.html</pre>
    *
    * <p>with a query string "rng=test"
    *
    * @retval string corrected URL string
    */
    
   public static function correctURI()
   {
      // Split the input query string
      $urlString = Environment::$gQueryString;
      $url = substr( $urlString, strpos( $urlString, "=" ) + 1 );
      Environment::$gRequestURI = $url;
      $newQueryString = "";
      if ( strpos( $url, "&" ) > 0 ) {
         $url = substr( $url, 0, strpos( $url, "&" ) );
         $newQueryString = substr( $urlString, strpos( $urlString, "&" ) + 1 );
      }
      Environment::$gQueryString = $newQueryString;
      return $url;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Translate entity to numerical form. This data gathered from ISO:
    * 
    * (C) International Organization for Standardization 1986
    * Permission to copy in any form is granted for use with
    * conforming SGML systems and applications as defined in
    * ISO 8879, provided this notice is included in all copies.
    *
    * Prepared: Rick Jelliffe, ricko@allette.com.au, (using HTMLlat1)
    */
    
   public static function xlateEntity( $inData )
   {
      $trans = array(
         '&Agrave;'=>'&#192;',    // capital A, grave accent 
         '&Aacute;'=>'&#193;',    // capital A, acute accent 
         '&Acirc;'=>'&#194;',     // capital A, circumflex accent 
         '&Atilde;'=>'&#195;',    // capital A, tilde 
         '&Auml;'=>'&#196;',      // capital A, dieresis or umlaut mark 
         '&Aring;'=>'&#197;',     // capital A, ring 
         '&AElig;'=>'&#198;',     // capital AE diphthong (ligature) 
         '&Ccedil;'=>'&#199;',    // capital C, cedilla 
         '&Egrave;'=>'&#200;',    // capital E, grave accent 
         '&Eacute;'=>'&#201;',    // capital E, acute accent 
         '&Ecirc;'=>'&#202;',     // capital E, circumflex accent 
         '&Euml;'=>'&#203;',      // capital E, dieresis or umlaut mark 
         '&Igrave;'=>'&#204;',    // capital I, grave accent 
         '&Iacute;'=>'&#205;',    // capital I, acute accent 
         '&Icirc;'=>'&#206;',     // capital I, circumflex accent 
         '&Iuml;'=>'&#207;',      // capital I, dieresis or umlaut mark 
         '&ETH;'=>'&#208;',       // capital Eth, Icelandic 
         '&Ntilde;'=>'&#209;',    // capital N, tilde 
         '&Ograve;'=>'&#210;',    // capital O, grave accent 
         '&Oacute;'=>'&#211;',    // capital O, acute accent 
         '&Ocirc;'=>'&#212;',     // capital O, circumflex accent 
         '&Otilde;'=>'&#213;',    // capital O, tilde 
         '&Ouml;'=>'&#214;',    // capital O, dieresis or umlaut mark 
         '&Oslash;'=>'&#216;',    // capital O, slash 
         '&Ugrave;'=>'&#217;',    // capital U, grave accent 
         '&Uacute;'=>'&#218;',    // capital U, acute accent 
         '&Ucirc;'=>'&#219;',    // capital U, circumflex accent 
         '&Uuml;'=>'&#220;',    // capital U, dieresis or umlaut mark 
         '&Yacute;'=>'&#221;',    // capital Y, acute accent 
         '&THORN;'=>'&#222;',    // capital THORN, Icelandic 
         '&szlig;'=>'&#223;',    // small sharp s, German (sz ligature) 
         '&agrave;'=>'&#224;',    // small a, grave accent    
         '&aacute;'=>'&#225;',    // small a, acute accent 
         '&acirc;'=>'&#226;',    // small a, circumflex accent 
         '&atilde;'=>'&#227;',    // small a, tilde 
         '&auml;'=>'&#228;',    // small a, dieresis or umlaut mark 
         '&aring;'=>'&#229;',    // small a, ring 
         '&aelig;'=>'&#230;',    // small ae diphthong (ligature) 
         '&ccedil;'=>'&#231;',    // small c, cedilla 
         '&egrave;'=>'&#232;',    // small e, grave accent 
         '&eacute;'=>'&#233;',    // small e, acute accent 
         '&ecirc;'=>'&#234;',    // small e, circumflex accent 
         '&euml;'=>'&#235;',    // small e, dieresis or umlaut mark 
         '&igrave;'=>'&#236;',    // small i, grave accent 
         '&iacute;'=>'&#237;',    // small i, acute accent 
         '&icirc;'=>'&#238;',    // small i, circumflex accent 
         '&iuml;'=>'&#239;',    // small i, dieresis or umlaut mark 
         '&eth;'=>'&#240;',    // small eth, Icelandic 
         '&ntilde;'=>'&#241;',    // small n, tilde 
         '&ograve;'=>'&#242;',    // small o, grave accent 
         '&oacute;'=>'&#243;',    // small o, acute accent 
         '&ocirc;'=>'&#244;',    // small o, circumflex accent 
         '&otilde;'=>'&#245;',    // small o, tilde 
         '&ouml;'=>'&#246;',    // small o, dieresis or umlaut mark 
         '&oslash;'=>'&#248;',    // small o, slash 
         '&ugrave;'=>'&#249;',    // small u, grave accent 
         '&uacute;'=>'&#250;',    // small u, acute accent 
         '&ucirc;'=>'&#251;',    // small u, circumflex accent 
         '&uuml;'=>'&#252;',    // small u, dieresis or umlaut mark 
         '&yacute;'=>'&#253;',    // small y, acute accent 
         '&thorn;'=>'&#254;',    // small thorn, Icelandic 
         '&yuml;'=>'&#255;',    // small y, dieresis or umlaut mark 
         '&emsp;'=>'&#x2003;',    // em space
         '&ensp;'=>'&#x2002;',    // en space (1/2-em)
         '&emsp13;'=>'&#x2004;',    // 1/3-em space
         '&emsp14;'=>'&#x2005;',    // 1/4-em space
         '&numsp;'=>'&#x2007;',    // digit space (width of a number)
         '&puncsp;'=>'&#x2008;',    // punctuation space (width of comma)
         '&thinsp;'=>'&#x2009;',    // thin space (1/6-em)
         '&hairsp;'=>'&#x200A;',    // hair space
         '&mdash;'=>'&#x2014;',    // em dash
         '&ndash;'=>'&#x2013;',    // en dash
         '&dash;'=>'&#x2010;',    // hyphen (true graphic)
         '&blank;'=>'&#x2423;',    // significant blank symbol
         '&hellip;'=>'&#x2026;',    // ellipsis (horizontal)
         '&nldr;'=>'&#x2025;',    // double baseline dot (en leader)
         '&frac13;'=>'&#x2153;',    // fraction one-third
         '&frac23;'=>'&#x2154;',    // fraction two-thirds
         '&frac15;'=>'&#x2155;',    // fraction one-fifth
         '&frac25;'=>'&#x2156;',    // fraction two-fifths
         '&frac35;'=>'&#x2157;',    // fraction three-fifths
         '&frac45;'=>'&#x2158;',    // fraction four-fifths
         '&frac16;'=>'&#x2159;',    // fraction one-sixth
         '&frac56;'=>'&#x215a;',    // fraction five-sixths
         '&incare;'=>'&#x2105;',    // in-care-of symbol
         '&block;'=>'&#x2588;',    // full block
         '&uhblk;'=>'&#x2580;',    // upper half block
         '&lhblk;'=>'&#x2584;',    // lower half block
         '&blk14;'=>'&#x2591;',    // 25% shaded block
         '&blk12;'=>'&#x2592;',    // 50% shaded block
         '&blk34;'=>'&#x2593;',    // 75% shaded block
         '&marker;'=>'&#x25AE;',    // histogram marker
         '&cir;'=>'&#x25CB;',    // circ B: =circle, open
         '&squ;'=>'&#x25A1;',    // square, open
         '&rect;'=>'&#x25AD;',    // rectangle, open
         '&utri;'=>'&#x25B5;',    // triangle =up triangle, open
         '&dtri;'=>'&#x25BF;',    // triangledown =down triangle, open
         '&star;'=>'&#x2606;',    // star, open
         '&bull;'=>'&#x2022;',    // bullet B: =round bullet, filled
         '&squf;'=>'&#x25AA;',    // blacksquare =sq bullet, filled
         '&utrif;'=>'&#x25B4;',    // blacktriangle =up tri, filled
         '&dtrif;'=>'&#x25BE;',    // blacktriangledown =dn tri, filled
         '&ltrif;'=>'&#x25C2;',    // blacktriangleleft R: =l tri, filled
         '&rtrif;'=>'&#x25B8;',    // blacktriangleright R: =r tri, filled
         '&clubs;'=>'&#x2663;',    // clubsuit =club suit symbol
         '&diams;'=>'&#x2662;',    // diamondsuit =diamond suit symbol
         '&hearts;'=>'&#x2661;',    // heartsuit =heart suit symbol
         '&spades;'=>'&#x2660;',    // spadesuit =spades suit symbol
         '&malt;'=>'&#x2720;',    // maltese =maltese cross
         '&dagger;'=>'&#x2020;',    // dagger B: =dagger
         '&Dagger;'=>'&#x2021;',    // ddagger B: =double dagger
         '&check;'=>'&#x2713;',    // checkmark =tick, check mark
         '&cross;'=>'&#x2717;',    // ballot cross
         '&sharp;'=>'&#x266F;',    // sharp =musical sharp
         '&flat;'=>'&#x266D;',    // flat =musical flat
         '&male;'=>'&#x2642;',    // male symbol
         '&female;'=>'&#x2640;',    // female symbol
         '&phone;'=>'&#x26E0;',    // telephone symbol
         '&telrec;'=>'&#x2315;',    // telephone recorder symbol
         '&copysr;'=>'&#x2117;',    // sound recording copyright sign
         '&caret;'=>'&#x2041;',    // caret (insertion mark)
         '&lsquor;'=>'&#x201A;',    // rising single quote, left (low)
         '&ldquor;'=>'&#x201E;',    // rising dbl quote, left (low)
         '&fflig;'=>'&#xFB00;',    // small ff ligature
         '&filig;'=>'&#xFB01;',    // small fi ligature
         '&fjlig;'=>'fj',    // small fj ligature
         '&ffilig;'=>'&#xFB03;',    // small ffi ligature
         '&ffllig;'=>'&#xFB04;',    // small ffl ligature
         '&fllig;'=>'&#xFB02;',    // small fl ligature
         '&mldr;'=>'&#x2025;',    // em leader
         '&rdquor;'=>'&#x201D;',    // rising dbl quote, right (high)
         '&rsquor;'=>'&#x2019;',    // rising single quote, right (high)
         '&vellip;'=>'&#x22EE;',    // vertical ellipsis
         '&hybull;'=>'&#x2043;',    // rectangle, filled (hyphen bullet)
         '&loz;'=>'&#x2727;',    // lozenge - lozenge or total mark
         '&lozf;'=>'&#x2726;',    // blacklozenge - lozenge, filled
         '&ltri;'=>'&#x25C3;',    // triangleleft B: l triangle, open
         '&rtri;'=>'&#x25B9;',    // triangleright B: r triangle, open
         '&starf;'=>'&#x2605;',    // bigstar - star, filled
         '&natur;'=>'&#x266E;',    // natural - music natural
         '&rx;'=>'&#x211E;',    //pharmaceutical prescription (Rx)
         '&sext;'=>'&#x2736;',    //sextile (6-pointed star)
         '&target;'=>'&#x2316;',    //register mark or target
         '&dlcrop;'=>'&#x230D;',    //downward left crop mark 
         '&drcrop;'=>'&#x230C;',    //downward right crop mark 
         '&ulcrop;'=>'&#x230F;',    //upward left crop mark 
         '&urcrop;'=>'&#x230E;',    //upward right crop mark 
         '&half;'=>'&#189;',    // fraction one-half
         '&frac12;'=>'&#189;',    // fraction one-half
         '&frac14;'=>'&#188;',    // fraction one-quarter
         '&frac34;'=>'&#190;',    // fraction three-quarters
         '&frac18;'=>'&#x215B;',    // fraction one-eighth
         '&frac38;'=>'&#x215C;',    // fraction three-eighths
         '&frac58;'=>'&#x215D;',    // fraction five-eighths
         '&frac78;'=>'&#x215E;',    // fraction seven-eighths
         '&sup1;'=>'&#185;',    // superscript one
         '&sup2;'=>'&#178;',    // superscript two
         '&sup3;'=>'&#179;',    // superscript three
         '&plus;'=>'+',    // plus sign B:
         '&plusmn;'=>'&#xB1;',    // pm B: =plus-or-minus sign
         '&equals;'=>'=',    // equals sign R:
         '&divide;'=>'&#247;',    // div B: =divide sign
         '&times;'=>'&#215;',    // times B: =multiply sign
         '&curren;'=>'&#164;',    // general currency sign
         '&pound;'=>'&#163;',    // pound sign
         '&dollar;'=>'$',    // dollar sign
         '&cent;'=>'&#162;',    // cent sign
         '&yen;'=>'&#165;',    // yen =yen sign
         '&num;'=>'#',    // number sign
         '&percnt;'=>'&#38;#37;',    // percent sign
         '&ast;'=>'*',    // ast B: =asterisk
         '&commat;'=>'@',    // commercial at
         '&lsqb;'=>'[',    // lbrack O: =left square bracket
         '&bsol;'=>'\\',    // backslash =reverse solidus
         '&rsqb;'=>']',    // rbrack C: =right square bracket
         '&lcub;'=>'{',    // lbrace O: =left curly bracket
         '&horbar;'=>'&#x2015;',    // horizontal bar
         '&verbar;'=>'|',    // vert =vertical bar
         '&rcub;'=>'}',    // rbrace C: =right curly bracket
         '&micro;'=>'&#181;',    // micro sign
         '&ohm;'=>'&#2126;',    // ohm sign
         '&deg;'=>'&#176;',    // degree sign
         '&ordm;'=>'&#186;',    // ordinal indicator, masculine
         '&ordf;'=>'&#170;',    // ordinal indicator, feminine
         '&sect;'=>'&#167;',    // section sign
         '&para;'=>'&#182;',    // pilcrow (paragraph sign)
         '&middot;'=>'&#183;',    // centerdot B: =middle dot
         '&larr;'=>'&#x2190;',    // leftarrow /gets A: =leftward arrow
         '&rarr;'=>'&#x2192;',    // rightarrow /to A: =rightward arrow
         '&uarr;'=>'&#x2191;',    // uparrow A: =upward arrow
         '&darr;'=>'&#x2193;',    // downarrow A: =downward arrow
         '&copy;'=>'&#169;',    // copyright sign
         '&reg;'=>'&#174;',    // circledR =registered sign
         '&trade;'=>'&#8482;',    // trade mark sign
         '&brvbar;'=>'&#xA6;',    // bren (vertical) bar
         '&not;'=>'&#xAC;',    // neg /lnot =not sign
         '&sung;'=>'&#x266A;',    // music note (sung text sign)
         '&excl;'=>'!',    // exclamation mark
         '&iexcl;'=>'&#xA1;',    // inverted exclamation mark
         '&apos;'=>'&#39;',    // apostrophe
         '&lpar;'=>'(',    //O: =left parenthesis
         '&rpar;'=>')',    //C: =right parenthesis
         '&comma;'=>',',    //P: =comma
         '&lowbar;'=>'_',    // low line
         '&hyphen;'=>'&#x2010;',    // hyphen
         '&period;'=>'.',    // full stop, period
         '&sol;'=>'/',    // solidus
         '&colon;'=>':',    // colon P:
         '&semi;'=>';',    // semicolon P:
         '&quest;'=>'?',    // question mark
         '&iquest;'=>'&#xBF;',    // inverted question mark
         '&laquo;'=>'&#x2039;',    // angle quotation mark, left
         '&raquo;'=>'&#x203A;',    // angle quotation mark, right
         '&lsquo;'=>'&#x2018;',    // single quotation mark, left
         '&rsquo;'=>'&#x2019;',    // single quotation mark, right
         '&ldquo;'=>'&#x201C;',    // double quotation mark, left
         '&rdquo;'=>'&#x201D;',    // double quotation mark, right
         '&nbsp;'=>'&#160;',    // no break (required) space
         '&shy;'=>'&#173;',    // soft hyphen
         '&rcaron;'=>'&#x159;',  // small r, caron
         '&scaron;'=>'&#x353;',  // small s, caron

          // Currency Symbols
         '&euro;'=>'&#8364;'   // euro sign, U+20AC NEW 
      );
      return strtr( $inData, $trans );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Based on PHP documentation example
    */
    
   public static function is_utf8( $inString )
   {
      return ( preg_match( '/^([\x00-\x7f]|[\xc0-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xf7][\x80-\xbf]{3}|[\xf8-\xfb][\x80-\xbf]{4}|[\xfc-\xfd][\x80-\xbf]{5})*$/',
         $inString ) === 1);
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
  /**
   * Takes a string of utf-8 encoded characters and converts it to a string of unicode entities
   * each unicode entitiy has the form &#nnnnn; n={0..9} and can be displayed by utf-8 supporting
   * browsers. (Taken from PHP documentation example)
   *
   * @param $inString string encoded using utf-8 [STRING]
   * @retval string of unicode entities [STRING]
   * @access public
   */
   
   public static function utf8ToUnicodeEntities( $inString )
   {
      // array used to figure what number to decrement from character order value 
      // according to number of characters used to map unicode to ascii by utf-8
      $decrement[4] = 240;
      $decrement[3] = 224;
      $decrement[2] = 192;
      $decrement[1] = 0;
    
      // the number of bits to shift each charNum by
      $shift[1][0] = 0;
      $shift[2][0] = 6;
      $shift[2][1] = 0;
      $shift[3][0] = 12;
      $shift[3][1] = 6;
      $shift[3][2] = 0;
      $shift[4][0] = 18;
      $shift[4][1] = 12;
      $shift[4][2] = 6;
      $shift[4][3] = 0;
    
      $pos = 0;
      $len = strlen ( $inString );
      $encodedString = '';
      while ( $pos < $len ) {
         $singleLetter = false;
         $ch = substr( $inString, $pos, 1 );
         //echo "[ch: $ch]<br/>";
         $asciiPos = ord( substr( $inString, $pos, 1 ) );
         //echo "[asciiPos: $asciiPos]<br/>";
         if ( ( $asciiPos >= 240 ) && ( $asciiPos <= 255 ) ) {
            // 4 chars representing one unicode character
            $thisLetter = substr($inString, $pos, 4 );
            $pos += 4;
         } else if ( ( $asciiPos >= 224 ) && ( $asciiPos <= 239 ) ) {
            // 3 chars representing one unicode character
            $thisLetter = substr( $inString, $pos, 3 );
            $pos += 3;
         } else if (($asciiPos >= 192) && ($asciiPos <= 223)) {
            // 2 chars representing one unicode character
            $thisLetter = substr( $inString, $pos, 2 );
            $pos += 2;
         } else {
            // 1 char (lower ascii)
            $thisLetter = substr( $inString, $pos, 1 );
            $pos += 1;
            $singleLetter = true;
         }
      
         // process the string representing the letter to a unicode entity
         if ( $singleLetter ) {
            $encodedString .= $ch;
         } else {
            $thisLen = strlen ( $thisLetter );
            $thisPos = 0;
            $decimalCode = 0;
            while ( $thisPos < $thisLen ) {
               $thisCharOrd = ord( substr( $thisLetter, $thisPos, 1 ) );
               if ( $thisPos == 0 ) {
                  $charNum = intval( $thisCharOrd - $decrement[$thisLen] );
                  $decimalCode += ( $charNum << $shift[$thisLen][$thisPos] );
               } else {
                  $charNum = intval ($thisCharOrd - 128);
                  $decimalCode += ( $charNum << $shift[$thisLen][$thisPos] );
               }
               $thisPos++;
            }
            if ( $thisLen == 1 )
               $encodedLetter = "&#". str_pad($decimalCode, 3, "0", STR_PAD_LEFT) . ';';
            else
               $encodedLetter = "&#". str_pad($decimalCode, 5, "0", STR_PAD_LEFT) . ';';
            $encodedString .= $encodedLetter;
         }
    } // while

    return $encodedString;
   }

}

?>
