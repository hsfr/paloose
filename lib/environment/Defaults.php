<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage environment
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The default values for the constants defined in the site's paloose-ini.php.
 * It is insurance in case that file misses anything out.
 *
 * @package paloose
 * @subpackage defaults
 */

   const PALOOSE_SITE_ROOT_DIRECTORY_DEFAULT = ".";
   const PALOOSE_ROOT_SITEMAP_DEFAULT = "sitemap.xmap";
   const PALOOSE_CACHE_DIR_DEFAULT = "resources/cache";
   const TIME_ZONE_DEFAULT = "Europe/London" ;
   const PALOOSE_USER_EXCEPTION_PAGE_DEFAULT = "resource://resources/errorHandling/userError.html";
   const PALOOSE_USER_EXCEPTION_TRANSFORM_DEFAULT = "resource://resources/transforms/errorPage2html.xsl";
   const PALOOSE_USER_EXCEPTION_STYLE_DEFAULT = "/paloose/resources/styles/userError.css";
   const PALOOSE_INTERNAL_EXCEPTION_PAGE_DEFAULT = "resource://resources/errorHandling/internalError.html";
   const PALOOSE_INTERNAL_EXCEPTION_TRANSFORM_DEFAULT = "resource://resources/transforms/errorPage2html.xsl";
   const PALOOSE_INTERNAL_EXCEPTION_STYLE_DEFAULT = "/paloose/resources/styles/internalError.css";
   const PALOOSE_SITEMAP_NAMESPACE_DEFAULT = "http://apache.org/cocoon/sitemap/1.0";
   const PALOOSE_SOURCE_NAMESPACE_DEFAULT = "http://apache.org/cocoon/source/1.0";
   const PALOOSE_DIR_NAMESPACE_DEFAULT = "http://apache.org/cocoon/directory/2.0";
   const PALOOSE_I18N_NAMESPACE_DEFAULT = "http://apache.org/cocoon/i18n/2.1";
   const PALOOSE_SQL_QUERY_NAMESPACE_DEFAULT = "http://apache.org/cocoon/SQL/2.0";
   const PALOOSE_PAGE_HIT_TRANSFORM_DEFAULT = "resource://resources/transforms/PageHit.xsl";
   const PALOOSE_NAMESPACE_DEFAULT = "http://www.paloose.org/schemas/Paloose/1.0";
   const PALOOSE_XHTML_NAMESPACE_DEFAULT = "http://www.w3.org/1999/xhtml";
   const PALOOSE_GALLERY_INDEX_DEFAULT = "gallery.xml";
   const PALOOSE_IMAGEMAGICK_BIN_DEFAULT = "";
   const PALOOSE_SESSIONS_REQUIRED_DEFAULT = "no";
   const PALOOSE_CLIENT_DEFAULT = "no";

?>