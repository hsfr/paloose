<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage environment
 * @author   Gary White with some additions from Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @copyright Gary White and Hugh Field-Richards
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The Browser Class identifies the user's Operating system, browser and
 * version.
 *
 * This is done by parsing the HTTP_USER_AGENT string sent to the server.
 * It is a port of browser.php by Gary White with suitable additions and
 * deletions.
 *
 * @package paloose
 * @subpackage environment
 */
 
class Browser{ 

   private $gName = "Unknown";
   
   private $gVersion = "Unknown";
   
   private $gPlatform = "Unknown";
   
   private $gUserAgent = "Not reported";
   
   private $gAOL = false;

   private $gIsMobile = false;

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new Browser detector. Parses the user agent string.
    */

   public function __construct()
   { 
      $agent = $_SERVER[ 'HTTP_USER_AGENT' ];
      // Initialize properties
      $bd[ 'platform' ] = "Unknown";
      $bd[ 'browser' ] = "Unknown";
      $bd[ 'version' ] = "Unknown";
      $this->gUserAgent = $agent;
      $this->gIsMobile = false;

      // Find operating system
      if ( preg_match( "/win/i", $agent ) )
         $bd[ 'platform' ] = "Windows";
      elseif ( preg_match( "/mac/i", $agent ) ) 
         $bd[ 'platform' ] = "MacIntosh";
      elseif ( preg_match( "/linux/i", $agent ) ) 
         $bd[ 'platform' ] = "Linux";
      elseif ( preg_match( "/OS\/2/i", $agent ) ) 
         $bd[ 'platform' ] = "OS/2";
      elseif ( preg_match( "/BeOS/i", $agent ) ) 
         $bd[ 'platform' ] = "BeOS";

      // test for Opera      
      if ( preg_match( "/opera/i", $agent ) )  {
         $val = stristr( $agent, "opera" );
         if ( preg_match( "/\//i", $val ) ) {
            $val = explode( "/", $val );
            $bd[ 'browser' ] = $val[0];
            $val = explode( " ", $val[1] );
            $bd[ 'version' ] = $val[0];
         } else { 
            $val = explode( " ", stristr( $val, "opera" ) );
            $bd[ 'browser' ] = $val[0];
            $bd[ 'version' ] = $val[1];
         }

      // test for WebTV
      } elseif ( preg_match( "/webtv/i", $agent ) ) {
         $val = explode( "/", stristr( $agent, "webtv" ) );
         $bd[ 'browser' ] = $val[0];
         $bd[ 'version' ] = $val[1];
      
      //test for MS Internet Explorer version 1
      } elseif ( preg_match( "/microsoft internet explorer/i", $agent ) ) {
         $bd[ 'browser' ] = "MSIE";
         $bd[ 'version' ] = "1.0";
         $var = stristr( $agent, "/" );
         if ( ereg( "308|425|426|474|0b1", $var ) )  {
            $bd[ 'version' ] = "1.5";
         }

      // test for NetPositive
      } elseif ( preg_match( "/NetPositive/i", $agent ) )  {
         $val = explode( "/", stristr( $agent, "NetPositive" ) );
         $bd[ 'platform' ] = "BeOS";
         $bd[ 'browser' ] = $val[0];
         $bd[ 'version' ] = $val[1];

      // test for MS Internet Explorer
      } elseif ( preg_match( "/msie/i", $agent ) && !preg_match( "/opera/i", $agent ) )  {
         $val = explode( " ", stristr( $agent, "msie" ) );
         $bd[ 'browser' ] = $val[0];
         $bd[ 'version' ] = $val[1];
      
      // test for MS Pocket Internet Explorer
      } elseif ( preg_match( "/mspie/i", $agent ) || preg_match( "/pocket/i", $agent ) )  {
         $val = explode( " ", stristr( $agent, "mspie" ) );
         $bd[ 'browser' ] = "MSPIE";
         $bd[ 'platform' ] = "WindowsCE";
         if ( preg_match( "/mspie/i", $agent ) )
            $bd[ 'version' ] = $val[1];
         else { 
            $val = explode( "/", $agent );
            $bd[ 'version' ] = $val[1];
         }
         
      // test for Galeon
      } elseif ( preg_match( "/galeon/i", $agent ) )  {
         $val = explode( " ", stristr( $agent, "galeon" ) );
         $val = explode( "/", $val[0] );
         $bd[ 'browser' ] = $val[0];
         $bd[ 'version' ] = $val[1];
         
      // test for Konqueror
      } elseif ( preg_match( "/Konqueror/i", $agent ) ) { 
         $val = explode( " ", stristr( $agent, "Konqueror" ) );
         $val = explode( "/", $val[0] );
         $bd[ 'browser' ] = $val[0];
         $bd[ 'version' ] = $val[1];
         
      // test for iCab
      } elseif ( preg_match( "/icab/i", $agent ) )  {
         $val = explode( " ", stristr( $agent, "icab" ) );
         $bd[ 'browser' ] = $val[0];
         $bd[ 'version' ] = $val[1];

      // test for OmniWeb
      } elseif ( preg_match( "/omniweb/i", $agent ) )  {
         $val = explode( "/", stristr( $agent, "omniweb" ) );
         $bd[ 'browser' ] = $val[0];
         $bd[ 'version' ] = $val[1];

      // test for Phoenix
      } elseif ( preg_match( "/Phoenix/i", $agent ) )  {
         $bd[ 'browser' ] = "Phoenix";
         $val = explode( "/", stristr( $agent, "Phoenix/" ) );
         $bd[ 'version' ] = $val[1];
      
      // test for Firebird
      } elseif ( preg_match( "/firebird/i", $agent ) )  {
         $bd[ 'browser' ]="Firebird";
         $val = stristr( $agent, "Firebird" );
         $val = explode( "/", $val );
         $bd[ 'version' ] = $val[1];
         
      // test for Firefox
      } elseif ( preg_match( "/Firefox/i", $agent ) )  {
         $bd[ 'browser' ]="Firefox";
         $val = stristr( $agent, "Firefox" );
         $val = explode( "/", $val );
         $bd[ 'version' ] = $val[1];
         
     // test for Mozilla Alpha/Beta Versions
      } elseif ( preg_match( "/mozilla/i", $agent ) && 
         preg_match( "/rv:[0-9].[0-9][a-b]/i", $agent ) && !preg_match( "/netscape/i", $agent ) )  {
         $bd[ 'browser' ] = "Mozilla";
         $val = explode( " ", stristr( $agent, "rv:" ) );
         preg_match( "/rv:[0-9].[0-9][a-b]/i", $agent, $val );
         $bd[ 'version' ] = str_replace( "rv:", "", $val[0] );
         
      // test for Mozilla Stable Versions
      } elseif ( preg_match( "/mozilla/i", $agent ) &&
         preg_match( "/rv:[0-9]\.[0-9]/i", $agent ) && !preg_match( "/netscape/i", $agent ) )  {
         $bd[ 'browser' ] = "Mozilla";
         $val = explode( " ", stristr( $agent, "rv:" ) );
         preg_match( "/rv:[0-9]\.[0-9]\.[0-9]/i", $agent, $val );
         $bd[ 'version' ] = str_replace( "rv:", "", $val[0] );
      
      // test for Lynx & Amaya
      } elseif ( preg_match( "/libwww/i", $agent ) )  {
         if ( preg_match( "/amaya/i", $agent ) )  {
            $val = explode( "/", stristr( $agent, "amaya" ) );
            $bd[ 'browser' ] = "Amaya";
            $val = explode( " ", $val[1] );
            $bd[ 'version' ] = $val[0];
         } else { 
            $val = explode( "/", $agent );
            $bd[ 'browser' ] = "Lynx";
            $bd[ 'version' ] = $val[1];
         }
      
      // test for iPhone
      } elseif ( preg_match( "/iphone/i", $agent ) )  {
         $bd[ 'browser' ] = "iPhone";
         $bd[ 'version' ] = "";

      // test for Safari
      } elseif ( preg_match( "/safari/i", $agent ) )  {
         $bd[ 'browser' ] = "Safari";
         $bd[ 'version' ] = "";

      // remaining two tests are for Netscape
      } elseif ( preg_match( "/netscape/i", $agent ) )  {
         $val = explode( " ", stristr( $agent, "netscape" ) );
         $val = explode( "/", $val[0] );
         $bd[ 'browser' ] = $val[0];
         $bd[ 'version' ] = $val[1];
      } elseif ( preg_match( "/mozilla/i", $agent ) && !preg_match( "/rv:[0-9]\.[0-9]\.[0-9]/i", $agent ) )  {
         $val = explode( " ", stristr( $agent, "mozilla" ) );
         $val = explode( "/", $val[0] );
         $bd[ 'browser' ] = "Netscape";
         $bd[ 'version' ] = $val[1];
      }
      
      // clean up extraneous garbage that may be in the name
      $bd[ 'browser' ] = preg_replace( "/[^a-z,A-Z]/", "", $bd[ 'browser' ] );
      // clean up extraneous garbage that may be in the version      
      $bd[ 'version' ] = preg_replace( "/[^0-9,.,a-z,A-Z]/", "", $bd[ 'version' ] );
      
      // check for AOL
      $bd[ 'aol' ] = "";
      if ( preg_match( "/AOL/i", $agent ) )  {
         $var = stristr( $agent, "AOL" );
         $var = explode( " ", $var );
         $bd[ 'aol' ] = ereg_replace( "[^0-9, ., a-z, A-Z]", "", $var[1] );
      }
      
       if ( preg_match( "/Mobile/i", $agent ) ) {
         $this->gIsMobile = true;
      }

      // finally assign our properties
      $this->gName = $bd[ 'browser' ];
      $this->gVersion = $bd[ 'version' ];
      $this->gPlatform = $bd[ 'platform' ];
      $this->gAOL = $bd[ 'aol' ];
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the browser name.
    *
    * @retval string representing the browser name.
    */

   public function getName()
   {
      return $this->gName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the browser version.
    *
    * @retval string representing the browser version.
    */

   public function getVersion()
   {
      return $this->gVersion;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the browser platform.
    *
    * @retval string representing the browser platform.
    */

   public function getPlatform()
   {
      return $this->gPlatform;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the browser AOL flag.
    *
    * @retval 'aol' if AOL used.
    */

   public function getAOL()
   {
      return $this->gAOL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Is the request from a mobile platform?
    *
    * @retval true if mobile request.
    */

   public function isMobile()
   {
      return $this->gIsMobile;
   }

    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Static method to do a simple pattern check of user agent string.
    *
    * @param string $inPattern pattern to check for (must be Perl regex).
    * @retval string representing the browser platform.
    */

   public static function patternCheck( $inPattern )
   { 
      $agent = $_SERVER[ 'HTTP_USER_AGENT' ];
      return preg_match( $inPattern, $agent );
   }
   

}

?>
