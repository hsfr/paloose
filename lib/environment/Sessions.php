<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage environment
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Modules.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * This class supports sessions.
 *
 * It is really for a convenience so that all session stuff can be brought into one place.
 *
 * @package paloose
 * @subpackage environment
 */
 
 class Sessions
 {

   /** Logger instance for this class */   
   private $gLogger;
   
   /** SID for this session */
   private $gSessionId = "";

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of Sessions.
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      session_start();
      $this->gSessionId = session_id();
      $this->setVar( '__sid', $this->gSessionId );
      $this->gLogger->debug( "Sessions enabled: '" . $this->gSessionId . "'" );
      // Make sure that the session variables are stored in the sessions module
      $sessionModule = Environment::$modules[ 'session' ];
      foreach ( $_SESSION as $key => $value ) {
         $this->gLogger->debug( "Setting session var: '$key' = '$value'" );
         $sessionModule->add( $key, $value );
      }
  }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Start a session.
    *
    * @retval string value of variable or NULL is not exist.
    */
    
   public function start()
   {
        $this->gLogger->debug( "Session started: '" . $this->gSessionId . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Destroy current session.
    */
   
   public function destroy()
   {
      $this->gLogger->debug( "Destroying session: " . $this->gSessionId );
      // Destroy all session module variables
      if ( session_id() != "" ) {
         $sessionModule = Environment::$modules[ 'session' ];
         $sessionModule->clear();
         $this->gSessionId = "";
         $_SESSION = array();
         session_destroy();
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Add session variable.
    *
    * @param string $inName the name of the variable o set.
    * @param string $inValue the value to set the variable.
    */
    
   public function setVar( $inName, $inValue )
   {
      $_SESSION[ $inName ] = $inValue;
      $sessionModule = Environment::$modules[ 'session' ];
      $sessionModule->add( $inName, $inValue );
        $this->gLogger->debug( "Set session variable: '" . $inName. "' = '" . $inValue . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Add session variable.
    *
    * @param string $inName the name of the variable o set.
    * @param string $inValue the value to set the variable.
    */
    
   public function getVar( $inName )
   {
      if ( $this->isSessionVarSet( $inName ) ) {
           $this->gLogger->debug( "Getting session variable: '" . $inName . "' = '" . $_SESSION[ $inName ] . "'" );
           return $_SESSION[ $inName ];
      } else {
           $this->gLogger->debug( "Session variable: '" . $inName . "' not set" );
         return "";
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Ssession variable set.
    *
    * @param string $inName the name of the variable o set.
    * @retval boolean true if variable is set.
    */
    
   public function isSessionVarSet( $inName )
   {
      return isset( $_SESSION[ $inName ] );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Is a session in progress.
    *
    * @retval string value of variable or NULL is not exist.
    */
    
   public function isValidSession()
   {
      if ( session_id() != "" ) {
         $this->gLogger->debug( "Session running: " . session_id() );
         return session_id();
      } else {
         $this->gLogger->debug(  "No session running" );
         return "";
      }
   }   

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Are the client's cookies enabled.
    *
    * @retval boolean true if cookies are enabled, otherwise false.
    */
    
   public function areCookiesEnabled()
   {
      return $this->gCookiesEnabled;
   }   

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Encode the current session variable as a string.
    *
    * @retval string value of variable or NULL is not exist.
    */
    
   public function encode()
   {
      return session_encode();
   }   

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Name this session.
    *
    * @param string $inName The name for this session
    */
   
   public function setSessionName( $inName )
   {
      $this->gLogger->debug( "Setting session user: '$inName'" );
      $this->gParams[ "__name" ] = $inName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Get session name.
    *
    * @retval string The current session name.
    */
   
   public function getSessionName()
   {
      if ( array_key_exists( '__name', $_SESSION ) ) {
         return $this->gParams[ "__name" ];
      } else {
         $this->gLogger->debug(  "Session name unknown" );
         return "";
      }
   }

}

?>