<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage environment
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Modules.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>RequestParameterModule</i> class supports the query string that contains the request
 * parameters.
 *
 * It also caters for POST type queries.
 *
 * @package paloose
 * @subpackage environment
 */
 
 class RequestParameterModule extends Modules {

   /** Logger instance for this class */   
   private $gLogger;
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of RequestParameterModule
    *
    * @param string $inName the name of this module.
    */

   public function __construct( $inName )
   {
      parent::__construct( $inName );
      $this->gLogger = Logger::getLogger( __CLASS__ );
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the query string.
    *
    * Split the query string into parts and store it in the
    * class variable gParams using the name of the variable as a key.
    * If more than one value is found for a particular name then
    * it is added onto what exists to form a comma separated list.
    *
    * Single quotes are replaced with their Entity equvalent to prevent
    * XPath errors when double and single quotes appear together in
    * the same string.
    *
    * @param string $inQueryString the query string of the requested URL
    */
    
    public function parseQuery( $inQueryString )
    {
      $this->gParams = array();
      $numberOfPostParameters = count( $_POST );
      $this->gLogger->debug( "Parsing POST: $numberOfPostParameters POST parameters" );
      foreach ( $_POST as $name => $value ) {
         // Must convert this into intername UTF format
         // First of all check for array
         if ( is_array( $value ) ) {
            $this->gLogger->debug( "Found array, size " . count( $value ) );
            $this->gParams[ $name ] = "";
            foreach ( $value as $str ) {
               $str = utf8_encode( $str );
               $str = html_entity_decode( $str, ENT_COMPAT, 'UTF-8' );
               $str = str_replace( "'", "&apos;", $str );
               $this->gLogger->debug( "Found array value '$str'" );
               $this->gParams[ $name ] .= $str . ",";
            }
            // Remove trailing comma - lazy doing it like this :-)
            $this->gParams[ $name ] = trim( $this->gParams[ $name ], "," );
         } else {
            $this->gLogger->debug( "Found single POST variable: '$name'='$value'" );
             // Not an array
            $value = utf8_encode( $value );
            $value = html_entity_decode( $value, ENT_COMPAT, 'UTF-8' );
            $value = str_replace( "'", "&apos;", $value );
            $this->gLogger->debug( "Found single value" );
            if ( array_key_exists( $name, $this->gParams ) ) {
               $this->gParams[ $name ] .= "," . $value;
            } else {
               $this->gParams[ $name ] = $value;
            }
         }
      }

      $this->gLogger->debug( "Parsing query string: '$inQueryString'" );
      if ( strlen( $inQueryString ) > 1 ) {
         $pairs = explode( "&", $inQueryString );
         if ( empty( $pairs ) ) return; 
         foreach ( $pairs as $pair ) {
            list( $name, $value ) = explode( "=", $pair );
            // Next line ensures this is a Paloose internal
            if ( $name == "url" ) $name = "__" . $name;
            if ( array_key_exists( $name, $this->gParams ) ) {
               $this->gParams[ $name ] .= "," . $value;
            } else {
               $this->gParams[ $name ] = $value;
            }
         }
      }
      foreach ( $this->gParams as $name => $value ) {
         $this->gLogger->debug( "'$name'='$value'" );
      }
   }

 }

?>
