<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage gallery
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/StringResolver.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/gallery/GalleryImage.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/gallery/GalleryFolder.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * <p>The GalleryIndex maintains a list images for a particluar gallery level.</p>
 *
 * @package paloose
 * @subpackage gallery
 */

class GalleryIndex {

    /** The name of this gallery. */
   private $gGalleryName;

    /** The name of this gallery for the breadcrumb trail. */
   private $gGalleryBreadcrumbName;

    /** The description of this gallery (a DOMDocument). */
   private $gGalleryDescription;

    /** The list of the folder entries (an array of GalleryFolder). */
   private $gGalleryFolderList;

    /** The list of the image entries (an array of GalleryImage). */
   private $gGalleryImageList;

    /** The cache directory for the images. */
   private $gCacheDirectory;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of the GalleryIndex.
    *
    * @param DOMNode $inNode the DOM of the gallery index.
    * @param string $inGalleryPathName the directory path of this gallery.
    * @param _GalleryTransformer $inComponent the component for the gallery (needed to access all the parameters).
    */

   public function __construct( DOMDocument $inDOM, $inGalleryPathName, _GalleryTransformer $inComponent )
   {

      
      $this->gLogger = Logger::getLogger( __CLASS__ );

      $this->gCacheDirectory = StringResolver::expandPseudoProtocols( $inComponent->gCacheDirectory );

      // Get the list of GalleryIndex
      $xpath = new domxpath( $inDOM );
      $xpath->registerNamespace( "p", Environment::$configuration[ 'palooseNamespace' ] );  // 1.1.1
      $catalogueNodeList = $xpath->query( "/p:gallery/p:name" );
      $this->gGalleryName = $catalogueNodeList->item(0)->nodeValue;
      $this->gLogger->debug( "Gallery name: '" . $this->gGalleryName . "'" );
      
      $catalogueNodeList = $xpath->query( "/p:gallery/p:breadcrumb" );
      $this->gGalleryBreadcrumbName = $catalogueNodeList->item(0)->nodeValue;
      $this->gLogger->debug( "Breadcrumb name: '" . $this->gGalleryBreadcrumbName . "'" );
      
      $catalogueNodeList = $xpath->query( "/p:gallery/p:description/*" );
      $this->gGalleryDescription = NULL;
      if ( $catalogueNodeList->length > 0 ) {
         $doc = new DOMDocument();
         $root = $doc->createElement( "description" );
         $root = $doc->appendChild( $root );
         foreach ( $catalogueNodeList as $node ) {
            $node = $doc->importNode( $node, true );
            $node = $root->appendChild( $node );
         }
         $this->gGalleryDescription = $doc;
      }

      // First of all make sure that the cache directory exists
      if ( !file_exists( $this->gCacheDirectory ) ) {
         $this->gLogger->debug( "Creating cache directory: '" . $this->gCacheDirectory . "'" );
          if ( mkdir( $this->gCacheDirectory, 0775 ) === false ) {
            throw new UserException(
               "Cannot create image cache: " . $this->gCacheDirectory, 
               PalooseException::GALLERY_ERROR, 
               NULL );
         } else {
            chmod( $this->gCacheDirectory, 0775 );
         }
      }
      $imageNodeList = $xpath->query( "/p:gallery/p:images/*" );
      if ( $imageNodeList->length > 0 ) {
          $this->gLogger->debug( "Processing image list" );
         foreach ( $imageNodeList as $node ) {
            try {
               $image = new GalleryImage( $node, $inGalleryPathName, $inComponent );
               $this->gGalleryImageList[] = $image; 
            } catch( UserException $e ) {
               throw new UserException( $e->getmessage(), $e->getCode(), $e->getDOMScrap() );
            }
         }
      }

      $folderNodeList = $xpath->query( "/p:gallery/p:folders/*" );
      if ( $folderNodeList->length > 0 ) {
          $this->gLogger->debug( "Processing folder list" );
         foreach ( $folderNodeList as $node ) {
            $folder = new GalleryFolder( $node, $inGalleryPathName );
            $this->gGalleryFolderList[] = $folder;
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getGalleryName()
   {
      return $this->gGalleryName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getBreadcrumbName()
   {
      return $this->gGalleryBreadcrumbName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the description for this gallery.
    *
    * @retval DOMDocument the DOM of the description
    */

   public function getGalleryDescription()
   {
      return $this->gGalleryDescription;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getGalleryImageList()
   {
      return $this->gGalleryImageList;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getGalleryFolderList()
   {
      return $this->gGalleryFolderList;
   }


}
?>
