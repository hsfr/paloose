<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage gallery
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The GalleryImage maintains the data for a particular image.
 * 
 * It is quite a busy
 * class as it has to sort out and maintain the thumbnail and full size image cache.
 *
 * @package paloose
 * @subpackage gallery
 */

class GalleryImage {

    /** The description of this gallery (a node). */
   private $gDescriptionNode;

    /** The piclens extension id. */
   private $gPicLensId;

    /** The file name of this image */
   private $gCacheDirectory;

    /** The file name of this image */
   private $gFileName;

    /** The file name of this image with path */
   private $gFullFileName;

    /** The file name of this image */
   private $gCachedThumbnailName;

    /** The cached file name of this image */
   private $gCachedImageName;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of the GalleryImage.
    *
    * @param DOMNode $inNode the node that contains the contents of this catalogue element.
    * @param string $inGalleryPathName the directory path of this image.
    * @param _GalleryTransformer $inComponent the component for the gallery (needed to access all the parameters).
    * @exception UserException if problem manipulating image.
    */
    
   public function __construct( DOMNode $inNode, $inGalleryPathName, _GalleryTransformer $inComponent )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $dom = new DOMDocument;
      $dom->appendChild( $dom->importNode( $inNode, 1 ) );

      $this->gCacheDirectory = StringResolver::expandPseudoProtocols( $inComponent->gCacheDirectory );

      $xpath = new domxpath( $dom );
      $xpath->registerNamespace( "p", Environment::$configuration[ 'palooseNamespace' ] );  // 1.1.1
     
      $this->gFullFileName = Utilities::getXPathListStringItem( 0, $xpath, "/p:image/@src" );
      $this->gFileName = Utilities::getXPathListStringItem( 0, $xpath, "/p:image/@src" );

      if ( strlen ( $this->gFullFileName ) == 0 ) {
         throw new UserException( 
            "Must have an associated src attribute with gallery image",
            PalooseException::SITEMAP_GALLERY_PARSE_ERROR,
            $dom );
      }

      //$this->gDescription = Utilities::getXPathListStringItem( 0, $xpath, "/p:image/p:description" );
      $this->gDescriptionNode = Utilities::getXPathListNode( 0, $xpath, "/p:image/p:description" );
    
      $this->gPicLensId = Utilities::getXPathListStringItem( 0, $xpath, "/p:image/@piclens" );
      $this->gFullFileName = $inGalleryPathName . $this->gFullFileName;

      // Make hash of this image name
      $parts = pathinfo( $this->gFullFileName );
      $imageType = $parts[ "extension" ];
      $this->gCachedImageName = $this->generateNewCacheImageName( $this->gFullFileName ); // No path yet
      $this->gCachedThumbnailName = $this->generateNewCacheThumbnailName( $this->gFullFileName ); // No path yet

      $this->gLogger->debug( "File name: '" . $this->gFullFileName . "'" );
      $this->gLogger->debug( "  type: '" . $imageType . "'" );
      $this->gLogger->debug( "  hash: '" . $this->gCachedImageName . "'" );
      $this->gLogger->debug( "  thumbnail: '" . $this->gCachedThumbnailName . "'" );
      // $this->gLogger->debug( "  description: '" . $this->gDescription . "'" );
      $this->gLogger->debug( "  description: '" . $this->gDescriptionNode->textContent . "'" );
      $this->gLogger->debug( "  cache directory: '" . $this->gCacheDirectory . "'" );
      $this->gLogger->debug( "  PicLens id: '" . $this->gPicLensId . "'" );

       if ( !file_exists( $this->gFullFileName ) ) {
         $msg = "Could not find image file: '" . $this->gFullFileName . "'";
          $this->gLogger->debug( $msg );
           throw new UserException( $msg, PalooseException::GALLERY_ERROR, NULL );
      }

      if ( !file_exists( $this->gCacheDirectory . $this->gCachedImageName ) ) {
          $this->gLogger->debug( "Creating full size cache image: '" . $this->gCachedImageName . "'" );
          try {
             $this->generateNewImage( $this->gCacheDirectory . $this->gCachedImageName,
                (int)($inComponent->gMaxWidth), (int)($inComponent->gMaxHeight) );
         } catch( UserException $e ) {
            throw new UserException( $e->getmessage(), $e->getCode(), $e->getDOMScrap() );
         }
      }

      if ( !file_exists( $this->gCacheDirectory . $this->gCachedThumbnailName ) ) {
          $this->gLogger->debug( "Creating thumbnail cache image: '" . $this->gCachedThumbnailName . "'" );
          try {
             $this->generateNewImage( $this->gCacheDirectory . $this->gCachedThumbnailName,
                (int)($inComponent->gMaxThumbnailWidth), (int)($inComponent->gMaxThumbnailHeight) );
          } catch( UserException $e ) {
            throw new UserException( $e->getmessage(), $e->getCode(), $e->getDOMScrap() );
         }
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Generate and store a new image.
    *
    * @param string $inFileName the file name of the new image.
    * @param int $inMaxWidth the maximum width of this image.
    * @param int $inMaxHeight the maximum height of this image.
    * @exception UserException if problem creating/resizing image.
    */
    
   public function generateNewImage( $inFileName, $inMaxWidth, $inMaxHeight  )
   {

 
      $parts = pathinfo( $this->gFullFileName );
      $imageType = $parts[ "extension" ];
      if ( function_exists( "imagecreatefromjpeg" ) ) {
         switch( $imageType ) {
              case "jpeg":
            case "jpg":
                  $originalImage = imagecreatefromjpeg( $this->gFullFileName );
                  break;
            case "gif":
                  $originalImage = imagecreatefromgif( $this->gFullFileName );
                  break;
            case "png":
                  $originalImage = imagecreatefrompng( $this->gFullFileName );
                  break;
         }
   
         $origWidth = imagesx( $originalImage );
         $origHeight = imagesy( $originalImage );
         
         $xRatio = $inMaxWidth/( $origWidth );
         $yRatio = $inMaxHeight/( $origHeight );
   
          $this->gLogger->debug( "Image size: [$origWidth,$origHeight][$inMaxWidth,$inMaxHeight][$xRatio,$yRatio]" );
         
         touch( $inFileName );      // Making sure permissions are set - bug, see http://bugs.php.net/?id=35060
   
         // If either of the above ratios are less than 1 (the original image is smaller then he max) then
         // we scale, otherwise just write the image into the cache unchanged.
         if ( $xRatio > 1 && $yRatio > 1 ) {
             $this->gLogger->debug( "Caching image unchanged: $inFileName" );
            imagejpeg( $originalImage, $inFileName );
            chmod( $inFileName, 0775 );
            imagedestroy( $originalImage );
            return;
           }
   
           // Ah .. we have to scale it
           if ( $xRatio < $yRatio )
              $scaleRatio =  $xRatio;
           else
              $scaleRatio =  $yRatio;
           $this->gLogger->debug( "Scaling image '$inFileName' at $scaleRatio" );
           $newWidth = (int)( $origWidth * $scaleRatio );
           $newHeight = (int)( $origHeight * $scaleRatio );
           $this->gLogger->debug( "Overall scaling: $scaleRatio => [$newWidth,$newHeight]" );
         $newImage = imagecreatetruecolor( $newWidth, $newHeight );
         imagecopyresampled( $newImage, $originalImage, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight );
         imagejpeg( $newImage, $inFileName );
         chmod( $inFileName, 0775 );
         imagedestroy( $newImage );
         imagedestroy( $originalImage );
      } else {
         // No support in PHP for jpeg etc so use ImageMagick
         $this->gLogger->debug( "Using ImageMagick" );
         $cmd = Environment::$configuration[ 'ImageMagickBin' ] . "identify " . $this->gFullFileName;
         $this->gLogger->debug( "$cmd" );
         $err = false;
         $output = "";
         exec( "$cmd 2>&1", $output, $err );
         if ( $err ) {
            throw new UserException(
               "Could not run image info for " . $this->gFullFileName . " : " . $output . "[$cmd]",
               PalooseException::GALLERY_ERROR,
               NULL );
          }
         $infoString = $output[0];
         // infoString should contain something similar to 
         //
         //   <path>/<filename> JPEG 750x500 750x500+0+0 DirectClass 8-bit 47.7598kb

         // Construct a size pattern for wwwwwxhhhhh
         $sizePattern = "/(\d\d*)x(\d\d*)/";
         if ( preg_match( $sizePattern, $infoString, $regs ) ) {
            $origWidth = $regs[1];
            $origHeight = $regs[2];
         } else {
            throw new UserException(
               "Invalid info format: " . $infoString,
               PalooseException::GALLERY_ERROR,
               NULL );
         }

         $xRatio = $inMaxWidth/( $origWidth );
         $yRatio = $inMaxHeight/( $origHeight );
   
          $this->gLogger->debug( "Image size: [$origWidth,$origHeight][$inMaxWidth,$inMaxHeight][$xRatio,$yRatio]" );
         
         touch( $inFileName );      // Making sure permissions are set - bug, see http://bugs.php.net/?id=35060
        
         // If either of the above ratios are less than 1 (the original image is smaller then he max) then
         // we scale, otherwise just write the image into the cache unchanged.
         if ( $xRatio > 1 && $yRatio > 1 ) {
             $this->gLogger->debug( "Caching image unchanged: $this->gFullFileName -> $inFileName" );
            if ( !copy( $this->gFullFileName, $inFileName ) ) {
               throw new UserException(
                  "Could not copy " . $this->gFullFileName . " to " . $inFileName, 
                  PalooseException::GALLERY_ERROR,
                  NULL );
            };
            chmod( $inFileName, 0775 );
            return;
           }
   
         // Ah .. we have to scale it
           if ( $xRatio < $yRatio )
              $scaleRatio =  $xRatio;
           else
              $scaleRatio =  $yRatio;
         $scaleRatio =  $scaleRatio * 100;  // Make a percentage for conversion
           $this->gLogger->debug( "Scaling image '$inFileName' at $scaleRatio %" );
         $cmd = Environment::$configuration[ 'ImageMagickBin' ] . "convert " . $this->gFullFileName . " -resize " . $scaleRatio . "% " . $inFileName;
         $this->gLogger->debug( "$cmd" );
         exec( "$cmd 2>&1", $output, $err );
         if ( $err ) {
            throw new UserException(
               "Could not run image info for " . $this->gFullFileName . " : " . $output,
               PalooseException::GALLERY_ERROR,
               NULL );
          }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    */
    
   public function generateNewCacheImageName( $inFileName )
   {
      $hash = md5( $inFileName );
      $parts = pathinfo( $inFileName );
      $newName = $hash . "." . $parts[ "extension" ];
      return $newName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    */
    
   public function generateNewCacheThumbnailName( $inFileName )
   {
      $hash = md5( $inFileName );
      $parts = pathinfo( $inFileName );
      $newName = $hash . "-th." . $parts[ "extension" ];
      return $newName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getDescription()
   {
        return $this->gDescriptionNode;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getPicLensId()
   {
      return $this->gPicLensId;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getCacheDirectory()
   {
      return $this->gCacheDirectory;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getOriginalFileName()
   {
      return $this->gFileName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getCachedImageName()
   {
      return $this->gCachedImageName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getCachedThumbnailName()
   {
      return $this->gCachedThumbnailName;
   }

}
?>
