<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage gallery
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * <i>GalleryFolder</i> maintains the data for a particular sub folder in the gallery.
 *
 * @package paloose
 * @subpackage gallery
 */
 
class GalleryFolder {

    /** The description of this gallery (a node). */
   private $gDescriptionNode;

    /** The name for the sub-folder */
   private $gFolderName;

    /** The name for the sub-folder */
   private $gRelativeFolderName;

    /** Logger instance for this class */
   private $gLogger;

   // private $gDescription;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of the <i>GalleryFolder</i>.
    *
    * @param DOMNode $inNode the node that contains the contents of this gallery element.
    * @param string $inGalleryPathName the directory path of this subfolder.
    */
    
   public function __construct( DOMNode $inNode, $inGalleryPathName )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $dom = new DOMDocument;
      $dom->appendChild( $dom->importNode( $inNode, 1 ) );
    
      $xpath = new domxpath( $dom );
      $xpath->registerNamespace( "p", Environment::$configuration[ 'palooseNamespace' ] );  // 1.1.1
      
      $this->gFolderName = Utilities::getXPathListStringItem( 0, $xpath, "/p:folder/@src" );
      if ( strlen ( $this->gFolderName ) == 0 ) {
         throw new UserException( 
            "Must have an associated folder name here", 
            PalooseException::SITEMAP_GALLERY_PARSE_ERROR,
            $dom );
      }
      // $this->gDescription = Utilities::getXPathListStringItem( 0, $xpath, "/p:folder/p:description" );
      $this->gDescriptionNode = Utilities::getXPathListNode( 0, $xpath, "/p:folder/p:description" );

      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $galleryDirectory = $requestParameterModule->get( 'src' );
      // Must add where we are (via src in query string)
      $this->gRelativeFolderName = Utilities::addTrailingSeparator( $galleryDirectory ) . $this->gFolderName;
      $this->gFolderName = $inGalleryPathName . $this->gFolderName;
      $this->gLogger->debug( "Folder name: '" . $this->gFolderName . "'" );
      $this->gLogger->debug( "  relative name: '" . $this->gRelativeFolderName . "'" );
      // $this->gLogger->debug( "  description: '" . $this->gDescription . "'" );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the description node for this folder.
    *
      * @retval string the description node.
    */
    
   public function getDescription()
   {
      return $this->gDescriptionNode;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the folder name.
    *
      * @retval string the folder name.
    */
    
   public function getFolderName()
   {
      return $this->gFolderName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the relative folder name (where the folder is).
    *
      * @retval string the relative folder name.
    */
    
   public function getRelativeFolderName()
   {
      return $this->gRelativeFolderName;
   }

}
?>
