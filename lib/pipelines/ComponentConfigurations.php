<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage pipelines
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/authentication/AuthenticationManager.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/ExitException.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * <i>ComponentConfigurations</i> are used to provide details of how components
 * are to be configured.
 *
 * At present this only supports a single type: the
 * authentication manager. A typical example is
 * 
 * <pre>   &lt;map:component-configurations>
 *       &lt;authentication-manager>
 *        &lt;handlers>
 *         &lt;handler name="adminHandler">
 *          &lt;redirect-to uri="cocoon:/login"/>
 *        &lt;authentication uri="cocoon:raw:/authenticate-user"/>
 *          &lt;/handler>
 *        &lt;/handlers>
 *      &lt;/authentication-manager>
 *   &lt;/map:component-configurations></pre>
 *
 * @package paloose
 * @subpackage pipelines
 */
 
class ComponentConfigurations {

   private $gDefault;
   
   /** Array of components */
   private $gComponentList;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __ClASS__ );
      $this->gComponentList = array();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the configuration section of the sitemap.
    *
    * @param the node associated with the pipelines section.
    */

   public function parse( DOMNode $inNode )
   {
      //First thing is make node into a local DOM
      $pipelinesDom = new DOMDocument;
      $pipelinesDom->appendChild( $pipelinesDom->importNode( $inNode, 1 ) );

      $xPath = new domxpath( $pipelinesDom );
      $xPath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $nodeList = $xPath->query( "//*" );
      $idx = 0;
      foreach ( $nodeList as $node ) {
         if ( $node->localName == "authentication-manager" ) {
            try {
               $manager = new AuthenticationManager( $node );
               $manager->parse( $node );
               $this->gComponentList[ "authentication-manager" ] = $manager;
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         } else if ( $node->localName == "global-variables" ) {
            $globalsNodeList = $xPath->query( "*/*" );
            foreach ( $globalsNodeList as $node ) {
               if ( $node->localName == "variable" ) {
                  $varName = $node->getAttribute( "name" );
                  $varValue = StringResolver::expandModuleVariable( $node->getAttribute( "value" ) );
                  $this->gLogger->debug( "Adding global [" . $varName . "=" . $varValue . "]" );
                  
                  $globalsModule = Environment::$modules[ 'global' ];
                  if ( $globalsModule == null ) echo "Globals module not found!";
                  $globalsModule->add( $varName, $varValue );
               }
            }
            $this->gLogger->debug( "Globals: " . Environment::$modules[ 'global' ]->toString() );
         }
      }
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the list of components (globals/authorization etc). 
    *
    * @retval array The components for this declaration.
    */

   public function getComponentList()
   {
      return $this->gComponentList;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the list of components (globals/authorization etc). 
    *
    * @retval array The components for this declaration.
    */

   public function getComponent( $inName )
   {
       if ( isset( $this->gComponentList[ $inName ] ) ) {
          return $this->gComponentList[ $inName ];
       } else {
          throw new UserException( "Component $inName does not exist", 0, NULL );  //Clean up and expand
       }
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this Class.
    *
    * @retval string the representation of the Class as a string
    *
    */

   public function toString()
   {
      $mess = " <component-configurations>\n";
      foreach ( $this->gComponentList as $node ) {
         $mess .= $node->toString();
      }
      $mess .= " </component-configurations>\n";
      return $mess;
   }

}

?>
