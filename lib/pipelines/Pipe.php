<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage pipelines
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/ExitException.php" );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * Stores a single pipe.
 *
 * @package paloose
 * @subpackage pipelines
 */

class Pipe {

   /** The array of pipeline elements, generator, transform etc */
   private $gPipeComponentList;
   
   private $gPipeComponentListIndex;
  
   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gPipeComponentListIndex = 0;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function addComponent( $inNode )
   {
      $this->gLogger->debug( "Adding component to pipe at {$this->gPipeComponentListIndex}" );
      $this->gPipeComponentList[ $this->gPipeComponentListIndex ] = $inNode;
      $this->gPipeComponentListIndex++;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Where all the work gets done for driving the pipeline. The variable $gPipeComponentList
    * has the various pipeline elements that we need to drive.
    *
    * @param inVariableStack stack containing the arrays of the various regexp expansions.
    * @param inURL URL that triggered pipeline.
    * @param inQueryString associated query string.
    * @param inLabelDOM the input DOM when used in a label pipe.
    * @retval documentDOM of the pipeline if not exit (used for selectors)
    * @throws UserException if problem parsing
    */
    
   public function run( $inVariableStack, $inURL, $inQueryString, $inLabelDOM )
   {
      $this->gLogger->debug( "Running pipe [URL=$inURL] [QS=$inQueryString]" );
      $requestParams = Environment::$modules[ "request-param" ];
      $label = $requestParams->get( "paloose-view" );
      // If there is a label find the correct view
      $fromLabel = false;
      $fromPosition = false;
      if ( !empty( $label ) ) {
         $views = Environment::$sitemapStack->peek()->getViews();
         $view = $views->getView( $label );
         if ( $view != NULL ) {
            $fromLabel = $view->getFromLabel();
            $fromPosition = $view->getFromPosition();
         }
      }
      $this->gLogger->debug( "[label:$label][fromLabel=$fromLabel][fromPosition=$fromPosition]" );

      // First of check that the pipe has a generator at the front. We will have
      // modify this slightly when other pipeline elements are added, such as selectors.
      $firstStage = $this->gPipeComponentList[ 0 ];
      // $this->gLogger->debug( "First stage component: " . $firstStage->getGenericType() );
      // The DOM that will hold the document as it progresses through the pipeline.
      $documentDOM = $inLabelDOM;
      $finished = false;
      if ( $documentDOM != NULL or
            $firstStage->getGenericType() == PipeElement::REDIRECT or
            $firstStage->getGenericType() == PipeElement::MOUNT or
            $firstStage->getGenericType() == PipeElement::READER or
            $firstStage->getGenericType() == PipeElement::GENERATOR or
            $firstStage->getGenericType() == PipeElement::CALL or
            $firstStage->getGenericType() == PipeElement::ACTION or
            $firstStage->getGenericType() == PipeElement::SELECTOR or
            $firstStage->getGenericType() == PipeElement::AGGREGATOR ) {
         // There is a generator in the first stage or there is an existing DOM (from a label) so run it.
         for ( $i = 0; $i < count( $this->gPipeComponentList ) and $finished !== true; $i++ ) {
            $component = $this->gPipeComponentList[ $i ];
            $this->gLogger->debug( "$i: " . $component->getGenericType() );

            switch ( $component->getGenericType() ) {

               case PipeElement::REDIRECT :
                  try {
                     $component->run( $inVariableStack, $inURL, $inQueryString, new DomDocument() );
                  } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;

              case PipeElement::MOUNT :
                  try {
                     $component->run( $inVariableStack, $inURL, $inQueryString, new DomDocument() );
                  } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;

               case PipeElement::AGGREGATOR :
                  try {
                     $documentDOM = $component->run( $inVariableStack, $inURL, $inQueryString, new DomDocument() );
                     // Decide whether we invoke a view here. There should be a suitable query string parameter
                     // that gives us this information in $label.
                     $componentLabel = $component->getLabel();
                     if ( $fromLabel !== false and $fromLabel == $componentLabel ) {
                        // We have a match 
                        $this->gLogger->debug( ">>>>> Label match found in aggregator [from label=$fromLabel]" );
                        $views = Environment::$sitemapStack->peek()->getViews();
                        $view = $views->getView( $label );
                        $pipe = $view->getPipe();
                        // Must set output stream back to immediate (not internal)
                        $outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
                        Environment::$sitemapStack->peek()->setOutputStream( $outputStream );
                        $pipe->run( $inVariableStack, $inURL, $inQueryString, $documentDOM );
                        // Nothing more to run
                        $this->gLogger->debug( "Finished processing" );
                        throw new ExitException();
                     }
                  } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;

               case PipeElement::GENERATOR :
                  try {
                     $this->gLogger->debug( "Processing generator for " . $inURL );
                     $documentDOM = $component->run( $inVariableStack, $inURL, $inQueryString, new DomDocument() );
                     // Decide whether we invoke a view here. There should be a suitable query string parameter
                     // that gives us this information in $label.
                     $componentLabel = $component->getLabel();
                     $this->gLogger->debug( "[componentLabel:$componentLabel][fromLabel=$fromLabel]" );
                     if ( $fromLabel !== false and $fromLabel == $componentLabel ) {
                        // We have a match 
                        $this->gLogger->debug( ">>>>> Label match found in generator [from label=$fromLabel]" );
                        $views = Environment::$sitemapStack->peek()->getViews();
                        $view = $views->getView( $label );
                        $pipe = $view->getPipe();
                        // Must set output stream back to immediate (not internal)
                        $outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
                        Environment::$sitemapStack->peek()->setOutputStream( $outputStream );
                        $pipe->run( $inVariableStack, $inURL, $inQueryString, $documentDOM );
                        // Nothing more to run
                        $this->gLogger->debug( "Finished processing" );
                        throw new ExitException();
                     }
                  } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     $this->gLogger->debug( $e->getMessage() );
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;

               case PipeElement::SELECTOR :
                  try {
                     $this->gLogger->debug( "Processing " . get_class( $component ) . " for " . $inURL );
                     // Note that the $documentDOM may be NULL here in the selector is pretending to be
                     // a generator. To cover this we create a blank DOMDocument to satisfy the type
                     // constrants in the run method.
                     if ( $documentDOM == NULL ) {
                        $this->gLogger->debug( "Null document found" );
                        $documentDOM = new DomDocument();
                     }
                     $documentDOM = $component->run( $inVariableStack, $inURL, $inQueryString, $documentDOM );
                     // Decide whether we invoke a view here. There should be a suitable query string parameter
                     // that gives us this information in $label.
                     $componentLabel = $component->getLabel();
                     if ( $fromLabel !== false and $fromLabel == $componentLabel ) {
                        // We have a match 
                        $this->gLogger->debug( ">>>>> Label match found in selector [from label=$fromLabel]" );
                        $views = Environment::$sitemapStack->peek()->getViews();
                        $view = $views->getView( $label );
                        $pipe = $view->getPipe();
                        // Must set output stream back to immediate (not internal)
                        $outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
                        Environment::$sitemapStack->peek()->setOutputStream( $outputStream );
                        $pipe->run( $inVariableStack, $inURL, $inQueryString, $documentDOM );
                        //Nothing more to run
                        $this->gLogger->debug( "Finished processing" );
                        throw new ExitException();
                     }
                  } catch ( ExitException $e ) {
                     // Must have this in the selector as there may be an early exit here. If there
                     // is not we fall off the end of the method returning the DOM
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;

               case PipeElement::TRANSFORMER :
                  try {
                     $this->gLogger->debug( "Processing transformer for " . $inURL );
                     $documentDOM = $component->run( $inVariableStack, $inURL, $inQueryString, $documentDOM );
                     // Decide whether we invoke a view here. There should be a suitable query string parameter
                     // that gives us this information in $label.
                     $componentLabel = $component->getLabel();
                     if ( $fromLabel !== false and $fromLabel == $componentLabel ) {
                        // We have a match 
                        $this->gLogger->debug( ">>>>> Label match found in transformer [from label=$fromLabel]" );
                        $views = Environment::$sitemapStack->peek()->getViews();
                        $view = $views->getView( $label );
                        $pipe = $view->getPipe();
                        // Must set output stream back to immediate (not internal)
                        $outputStream = new OutputStream( OutputStream::STANDARD_OUTPUT );
                        Environment::$sitemapStack->peek()->setOutputStream( $outputStream );
                        $pipe->run( $inVariableStack, $inURL, $inQueryString, $documentDOM );
                        // Nothing more to run
                        $this->gLogger->debug( "Finished processing" );
                        throw new ExitException();
                     }
                  } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;

               case PipeElement::SERIALIZER :
                  $this->gLogger->debug( "Processing serializer for " . $inURL );
                  try {
                     $component->run( $inVariableStack, $inURL, $inQueryString, $documentDOM );
                     $this->gLogger->debug( "Finished running serializer for " . $inURL );
                  } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;
                  
               case PipeElement::READER :
                 $this->gLogger->debug( "Processing reader for " . $inURL );
                 try {
                     $documentDOM = $component->run( $inVariableStack, $inURL, $inQueryString, new DomDocument() );
                   } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;
                  
              case PipeElement::CALL :
                  try {
                     $component->run( $inVariableStack, $inURL, $inQueryString, ( $documentDOM == NULL ? new DOMDocument : $documentDOM ) );
                  } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;

              case PipeElement::ACTION :
                 //If the document is blank (at start of pipe) then set up a new document here.
                 if ( $documentDOM == NULL ) $documentDOM = new DOMDocument();
                  try {
                     $component->run( $inVariableStack, $inURL, $inQueryString, $documentDOM );
                  } catch ( ExitException $e ) {
                     throw new ExitException();
                  } catch ( UserException $e ) {
                     throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
                  } catch ( RunException $e ) {
                     throw new RunException( $e->getMessage(), $e->getCode() );
                  }
                  break;

             }
         }
         $this->gLogger->debug( "Finished running component " . $inURL );
         return $documentDOM;
      } else {
         $this->msg = "First pipe element must be a generator, but found component type " . $firstStage->getGenericTypeName();
         throw new RunException( $this->msg, PalooseException::SITEMAP_PIPE_ERROR );
      }
   }

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function toString()
   {
      $size = $this->getComponentListSize();
      $mess = "";
      if ( $size > 0 ) {
         for ( $i = 0; $i < $size; $i++ ) {
            $node = $this->gPipeComponentList[ $i ];
            $mess .= $node->toString();
         }
      }
      return $mess;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function getComponentListSize()
   {
      return $this->gPipeComponentListIndex;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

   public function isValid()
   {
   } 

}

?>
