<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage pipelines
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/Pipeline.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/pipelines/ComponentConfigurations.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/error-handling/HandleError.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Pipelines</i> maintains a list of the pipelines.
 * 
 * @package paloose
 * @subpackage pipelines
 */

class Pipelines {

   private $gDefault;
   
   /** Array of pipelines (each entry a class: Pipeline) and indexed by name */
   private $gPipelinesList;

   /** The component configuration mechanism */
   private $gComponentConfigurations;

   /** The error handler for this pipelines declaration (usually the default error handler for this sitemap) */
   private $gErrorHandler;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of <i>Pipelines</i>.
    *
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __ClASS__ );
      $this->gPipelinesList = array();
      $this->gComponentConfigurations = NULL;
      $this->gErrorHandler = NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the pipelines section of the sitemap.
    *
    * @param the node associated with the pipelines section.
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $pipelinesDom = new DOMDocument;
      $pipelinesDom->appendChild( $pipelinesDom->importNode( $inNode, 1 ) );

      $xpath = new domxpath( $pipelinesDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $nodeList = $xpath->query( "//*" );
      $idx = 0;
      foreach ( $nodeList as $node ) {
         if ( $node->localName == "pipeline" ) {
            $pipeline = new Pipeline();
            $this->gPipelinesList[ $idx++ ] = $pipeline;
            try {
               $pipeline->parse( $node );
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         } else if ( $node->localName == "component-configurations" ) {
            try {
               $this->gComponentConfigurations = new ComponentConfigurations();
               $this->gComponentConfigurations->parse( $node );
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         } else if ( $node->localName == "handle-errors" ) {
            try {
               $this->gErrorHandler = new HandleError();
               $this->gErrorHandler->parse( $node );
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Take the inputted URL and query string and offers it to the various
    * pipelines.
    * 
    * @param string $inURL the calling URL
    * @param string $inQueryString the query string associated with this URL
    * @param string $inInternalRequest true if the request comes from another Paloose sitemap
    * @throws ExitException when early success exit occurs
    * @throws UserException when lower level UserException is thrown
    * @throws RunException when lower level RunException is thrown
    * @retval false if there was no match in the pipelines, true if match succeeded.
    */

   public function run( $inURL, $inQueryString, $inInternalRequest )
   {
      // Each pipeline is offered the input to process and will return as soon
      // one has fired.
      foreach( $this->gPipelinesList as $node ) {
         try {
            // If returns true means then a match has been found and we can exit
            if ( $node->run( $inURL, $inQueryString, $inInternalRequest ) ) return true;
         } catch ( ExitException $e ) {
            $this->gLogger->debug( "Finished processing" );
            throw new ExitException();
         } catch ( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
         } catch ( RunException $e ) {
            throw new RunException( $e->getMessage(), $e->getCode(), NULL );
         }
      }
      return false;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the list of components for this pipelines declaration (globals/authorization etc). 
    *
    * @retval ComponentConfigurations The components for this pipelines declaration.
    */

   public function getComponentConfigurations()
   {
      return $this->gComponentConfigurations;
   } 

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get a string representation of this Class.
    *
    * @retval string the representation of the Class as a string
    *
    */

   public function toString()
   {
      $mess = " <pipelines>\n";
      if ( $this->gComponentConfigurations != NULL ) {
         $mess .= $this->gComponentConfigurations->toString();
      }
      foreach ( $this->gPipelinesList as $node ) {
         $mess .= $node->toString();
      }
      if ( $this->gErrorHandler != NULL ) {
         $mess .= $this->gErrorHandler->toString();
      }
      $mess .= " </pipelines>\n";
      return $mess;
   }

}

?>