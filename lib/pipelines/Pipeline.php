<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage pipelines
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/matching/Match.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>Pipeline</i> maintains a single pipeline of elements.
 *
 * They follow the following format
 *   
 * <pre>   &lt;map:pipeline xmlns:map="http://apache.org/cocoon/sitemap/1.0" internal-only="true/false">
 *       &lt;!-- List of match components -->
 *       &lt;!-- Error handler -->
 *    &lt;/map:pipeline></pre>
 *      
 * @package paloose
 * @subpackage pipelines
 */

class Pipeline {

   /** Array of matchers (class "SimpleMatch") */
   private $gMatchersList;

   /** Is this an internal pipeline? */
   private $gInternalPipeline;

   /** The error handler for this pipeline group */
   private $gErrorHandler;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make a new instance of <i>Pipelines</i>.
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gInternalPipeline = false;
      $this->gErrorHandler = NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse a single pipeline.
    *
    * @param DOMNode $inNode the pipeline node and its children
    * @throws UserException if there is a parse error
    */
    
   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $pipelineDom = new DOMDocument;
      $pipelineDom->appendChild( $pipelineDom->importNode( $inNode, 1 ) );
      
      $this->gInternalPipeline = Utilities::normalizeBoolean( $inNode->getAttribute( "internal-only" ) );

      $xpath = new domxpath( $pipelineDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      // Extract list of matchers/errorHandler (everything contained within the pipeline)
      $nodeList = $xpath->query( "//*" );
      $idx = 0;
      foreach ( $nodeList as $node ) {
         if ( $node->localName == "match" ) {
            $match = new SimpleMatch();
            try {
               $match->parse( $node );
               $this->gMatchersList[ $idx++ ] = $match;
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         } else if ( $node->localName == "handle-errors" ) {
            try {
               $this->gLogger->debug( "Found error handler in pipeline: " );
               $this->gErrorHandler = new HandleError();
               $this->gErrorHandler->parse( $node );
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Take the inputted URL and query string and offer it to the various
    * matchers in each pipeline.
    *
    * This is where we will handle errors for a particular
    * pipeline.
    * 
    * @param string $inURL the calling URL
    * @param string $inQueryString the query string associated with this URL
    * @param string $inInternalRequest true if the request comes from another Paloose sitemap
    * @throws ExitException when early success exit occurs
    * @throws UserException when lower level UserException is thrown
    * @throws RunException when lower level RunException is thrown
    * @retval false if there was no match in the pipelines, true if match succeeded.
    */

   public function run( $inURL, $inQueryString, $inInternalRequest )
   {
      // Ignore if the internal-only flags do not match
      if ( $inInternalRequest === false and $this->gInternalPipeline === true ) return false;
      // Offer to each matcher in turn
      foreach( $this->gMatchersList as $node ) {
         try {
            // Returning true indicated that request was successfully matched
            if ( $node->run( $inURL, $inQueryString ) ) return true;
         } catch ( ExitException $e ) {
            $this->gLogger->debug( "Exiting" );
            throw new ExitException();
         } catch ( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
         } catch ( RunException $e ) {
            // For a Run exception here we have to check on whether there is a relevant
            // handle-error.
            if ( $this->gErrorHandler == NULL ) {
               $this->gLogger->debug( "Using internal error reporting: " . $e->getMessage() );
               throw new RunException( $e->getMessage(), $e->getCode(), NULL );
            } else {
               $this->gLogger->debug( "Using sitemap error reporting: " . $e->getMessage() );
               $this->gErrorHandler->run( $inURL, $e->getMessage() );
               throw new ExitException();
           }
         }
      }
      return false;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Gets the Pipeline as a string. 
    *
    * @retval the string representation of this Pipeline.
    */

   public function toString()
   {
      $mess = "  <pipeline";
      if ( $this->gInternalPipeline ) $mess .= " internal-only='true'";
      $mess .= ">\n";
      foreach ( $this->gMatchersList as $node ) {
         $mess .= $node->toString();
      }
      $mess .= "  </pipeline>\n";
      return $mess;
   }



}

?>