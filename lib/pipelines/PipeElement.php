<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 *
 * Paloose is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * @package paloose
 * @subpackage pipelines
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/UserException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/InternalException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/exception-handling/RunException.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/caching/CacheFile.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Aggregator.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Mount.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/sitemap/Redirect.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/resources/Call.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * <i>PipeElement</i> is the base class for all the pipe elements.
 *
 * @package paloose
 * @subpackage pipelines
 */

class PipeElement
{

   // Define the various types of pipeline entities
   const UNKNOWN = -1;
   const GENERATOR = 0;
   const AGGREGATOR = 1;
   const MOUNT = 2;
   const TRANSFORMER = 3;
   const SERIALIZER = 4;
   const READER = 5;
   const SELECTOR = 6;
   const CALL = 7;
   const ACTION = 8;
   const REDIRECT = 8;

   /** Pipeline element type : generator, transformer etc) */
   protected $gGenericType;

   /** The type of this instance (from the name in the declaration) */
   protected $gType;

   /** Source of data to process, URL etc */
   protected $gSrc;

   /** The associated list of parameters (single value of Parameter class) */
   protected $gParameters;

   /** The input DOM from the previous stage (used by transformers and serializers) */
   protected $gInputDOM;

   /** The output DOM for firing into the next stage (use by generators and transformers) */
   protected $gTranslatedDOM;

   /** The label used for the views. */
   protected $gLabel;
   
    /** Associated component for the derived class ( "_className" ) */
   protected $gComponent;

    /** Is this component cachable (should it be using the cache) ? */
   protected $gIsCachable = 0;

   /** Cache file instance */
   protected $gCacheFile;

   /** Parameters cache file instance */
   protected $gParameterCacheFile;

    /** Logger instance for this class */
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a new instance of a <i>PipeElement</i>.
    *
    * @throws UserException If problem reading parameters
    */

   public function __construct( DOMDocument $inDOM, $inType = "", $inSrc = "", $inComponent = NULL )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );

      $this->gParameters = new Parameter();
      // Extract the label (in the inDOM document)
      $xpath = new domxpath( $inDOM );
      // Set the namespace for the sitemap.
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $this->gLabel = Utilities::getXPathListStringItem( 0, $xpath, "//@label" );

      $params = $xpath->query( "//m:parameter" );

      // Now we must sort out the associated parameters
      foreach ( $params as $node ) {
         $name = $node->getAttribute( "name" );
         $value = $node->getAttribute( "value" );
         if ( strlen( $name ) > 0 and strlen( $value ) > 0 ) {
            $this->gParameters->setParameter( $name, $value );
            $this->gLogger->debug( "Added parameter '$name' = '$value' : $inSrc" );
            // Put into globals module
            //$globalsModule = Environment::$modules[ 'global' ];
            //echo "[name: $name][value: $value]<br/>";
            //$globalsModule->add( $name, $value );
         } else {
            $dom = new DOMDocument;
            $dom->appendChild( $dom->importNode( $node, 1 ) );
            throw new UserException( "Parameters must have both 'name' and 'value' attributes",
                  PalooseException::SITEMAP_TRANSFORMER_RUN_ERROR,
                  $dom );
         }
      }
      
      // First see what cachable status is set at component level - check for null component which is for Mount etc
      if ( $inComponent == null ) {
         $this->gIsCachable = 0;
      } else {
         $this->gIsCachable = $inComponent->gIsCachable;
      }
      
      // Then override if necessary at pipeline instance level from attribute
      $localCachable = $this->gParameters->getParameter( CacheFile::CACHABLE_ATTRIBUTE_NAME );
      if ( $localCachable != NULL ) {
         $this->gLogger->debug( "localCachable = [$localCachable]" );
         $this->gIsCachable = $localCachable;  // May be a variable here - {request-param:cachable}
      } else {
         $this->gLogger->debug( "localCachable = [null]" );
         $this->gIsCachable = 0;
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Run this component in the pipeline.
    *
    * Always overridden.
    *
    * @param VariableStack $inVariableStack stack containing the arrays of the various regexp expansions.
    * @param string $inURL the URL that triggered this run.
    * @param string $inQueryString the associated query string.
    * @param DOMDocument $inDOM the pipeline DOM to transform.
    * @retval The document DOM representing the input.
    */

   public function run( VariableStack $inVariableStack, $inURL, $inQueryString, DOMDocument $inDOM )
   {
      $this->gLogger->debug( "Parameters: " . $this->gParameters->toString() );
      $this->gLogger->debug( "Number of Parameters: " . sizeof( $this->gParameters->getParameterList() ) );

      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $userReqParams = $this->gComponent->getUseRequestParametersFlag();
      $this->gLogger->debug( "Use request params: $userReqParams" );

      // If the use-request-params is set then add to the parameter list
      if ( $userReqParams ) {
         $this->gLogger->debug( "Using request parameters" );
         $requestParameterModule = Environment::$modules[ 'request-param' ];
         $params = $requestParameterModule->getParams();
         foreach ( $params as $name => $value ) {
            $this->gLogger->debug( "Setting '$name' parameter : $value" );
            $this->gParameters->setParameter( $name, $value );
         }
      }

      // Expand parameters
      if ( !empty( $this->gParameters->getParameterList() ) ) {
         $parameterList = $this->gParameters->getParameterList();
         foreach ( $parameterList as $name => $value ) {
            // Only translate if not a request parameter
            if ( strpos( $value, "{request-param:" ) === false ) {
                $expandedValue = StringResolver::expandString( $inVariableStack, $value );
                $expandedValue = StringResolver::expandPseudoProtocols( $expandedValue );
                $this->gParameters->setParameter( $name, $expandedValue );
                $this->gLogger->debug( "Set expanded parameter '$name' => '$expandedValue'" );
            }
         }
      }

     if ( !empty( $this->gParameters->getParameterList() ) ) {
         $parameterList = $this->gParameters->getParameterList();
         foreach ( $parameterList as $name => $value ) {
            $this->gLogger->debug( "Final parameter '$name' => '$value'" );
         }
      }

      return $inDOM;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   public function getType()
   {
      return $this->gType;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   public function getSrc()
   {
      return $this->gSrc;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   public function getGenericType()
   {
      return $this->gGenericType;
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   public function getGenericTypeName()
   {
      switch ( $this->gGenericType ) {
         case PipeElement::GENERATOR :
                 return "Generator";
         case PipeElement::AGGREGATOR :
                 return "Aggregator";
         case PipeElement::MOUNT :
                 return "MoOunt";
         case PipeElement::TRANSFORMER :
                 return "Transformer";
         case PipeElement::SERIALIZER :
                 return "Serializer";
         case PipeElement::READER :
                 return "Reader";
         case PipeElement::SELECTOR :
                 return "Selector";
         case PipeElement::CALL :
                 return "Call";
         case PipeElement::ACTION :
                 return "Action";
         case PipeElement::REDIRECT :
                 return "Redirect";
         default :
                 return "Unknown type";
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   public function getLabel()
   {
      return $this->gLabel;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a generator from the inputted node and returns the created generator.
    *
    * @param $inNode the node representing the generator.
    * @retval object the created generator
    * @throws UserException and InternalException
    */
 
   public static function createGenerator( $inNode )
   {
      $generators = Environment::$sitemapStack->root()->getGenerators();
      $matchDom = new DOMDocument;
      $matchDom->appendChild( $matchDom->importNode( $inNode, 1 ) );

      // Get the pattern that we must match the URL to
      $src = $inNode->getAttribute( "src" );
      if ( strlen( $src ) == 0 ) {
         $dom = new DOMDocument;
         $dom->appendChild( $dom->importNode( $inNode, 1 ) );
         throw new UserException( "Must have a src attribute present", PalooseException::SITEMAP_GENERATOR_PARSE_ERROR, $dom );
      }
      $type = $inNode->getAttribute( "type" );
      // if type not present we get the default type
      if ( strlen( $type ) == 0 ) {
         $type = $generators->getDefault();
      }

      $gIsCachable = Utilities::normalizeBoolean( $inNode->getAttribute( "cache" ) );
      // if cachable not set we assume true
      if ( strlen( $gIsCachable ) == 0 ) {
         $gIsCachable = true;
      }

      try {
         $generator = $generators->getGenerator( $type );
         $generatorPackageName = $generator->getPackageName();
         return new $generatorPackageName( $matchDom, $type, $src, $generator );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( InternalException $e ) {
         throw new InternalException( $e->getMessage(), $e->getCode() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Aggregators are built-in pipeline elements rather than declared components.
    * A bit like if..then..else statements that are built in (not that they behave like
    * conditionals). Typically they look like
    *
    * <map:aggregate element="root">
    *    <map:part src="context://content/menu.xml"/>
    *    <map:part src="context://content/locales.xml"/>
    *    <map:part src="context://content/{../0}"/>
    * </map:aggregate>
    *
    * and behave like generators (ie they start a pipeline off).
    *
    * @param $inNode the node representing the aggregator.
    * @retval Aggregator the created aggregator
    * @throws UserException if creation throws error
    */
    
   public static function createAggregator( $inNode )
   {
      $matchDom = new DOMDocument;
      $matchDom->appendChild( $matchDom->importNode( $inNode, 1 ) );

      try {
         return new Aggregator( $matchDom );
      } catch ( UserException $e ) {
         throw new UserException( "Error in aggregator :" . $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a transformer from the inputted node and returns the created transformer.
    *
    * @param $inNode the node representing the transformer.
    * @retval Transformer the created transformer
    * @throws UserException if creation throws exception
    * @throws InternalException if creation throws exception
    */
    
   public static function createTransformer( DOMNode $inNode )
   {
      $transformers = Environment::$sitemapStack->root()->getTransformers();
      $dom = new DOMDocument;
      $dom->appendChild( $dom->importNode( $inNode, 1 ) );
      
      $src = $inNode->getAttribute( "src" );
      $type = $inNode->getAttribute( "type" );
      //if type not present we get the default type. Might not have a src though (i18N etc)
      if ( strlen( $type ) == 0 ) {
         $type = $transformers->getDefault();
      }
      
      try {
         $transformer = $transformers->getTransformer( $type );
         $transformerPackageName = $transformer->getPackageName();
         return new $transformerPackageName( $dom, $type, $src, $transformer );
      } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( InternalException $e ) {
           throw new InternalException( $e->getMessage(), $e->getCode() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a serializer from the inputted node and returns the created serializer.
    *
    * @param $inNode the node representing the serializer.
    * @retval the created serializer
    * @throws UserException if creation throws exception
    * @throws InternalException if creation throws exception
    */

   public static function createSerializer( DOMNode $inNode )
   {
      $serializers = Environment::$sitemapStack->root()->getSerializers();
      $dom = new DOMDocument;
      $dom->appendChild( $dom->importNode( $inNode, 1 ) );
      
      //Get the pattern that we must match the URL to
      $attributes = $inNode->attributes;
      $type = $inNode->getAttribute( "type" );
      //if type not present we get the default type
      if ( strlen( $type ) == 0 ) {
         $type = $serializers->getDefault();
      }
   
      //Create a new serializers instance of the correct type.
      try {
         $serializer = $serializers->getSerializer( $type );
         $serializerPackageName = $serializer->getPackageName();
         return new $serializerPackageName( $dom, $type, "", $serializer );
      } catch ( InternalException $e ) {
           throw new InternalException( $e->getMessage(), $e->getCode() );
      }
   }


   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a reader from the inputted node and returns the created reader.
    *
    * @param $inNode the node representing the reader.
    * @retval the created reader
    * @throws UserException if creation throws exception
    * @throws InternalException if creation throws exception
    */

   public static function createReader( $inNode )
   {
      $readers = Environment::$sitemapStack->root()->getReaders();
      $dom = new DOMDocument;
      $dom->appendChild( $dom->importNode( $inNode, 1 ) );

      //Get the pattern that we must match the URL to
      $src = $inNode->getAttribute( "src" );
      if ( strlen( $src ) == 0 ) {
           throw new UserException( "Must have a src attribute present",
           PalooseException::SITEMAP_READER_PARSE_ERROR,
           $dom );
        }
      $type = $inNode->getAttribute( "type" );
      //if type not present we get the default type
      if ( strlen( $type ) == 0 ) {
         $type = $readers->getDefault();
      }

      //Create a new reader instance of the correct type.
      try {
         $reader = $readers->getReader( $type );
         $readerPackageName = $reader->getPackageName();
         return new $readerPackageName( $dom, $type, $src, $reader );
      } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( InternalException $e ) {
           throw new InternalException( $e->getMessage(), $e->getCode() );
      }
   }


   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a selector from the inputted node and returns the created selector.
    *
    * @param $inNode the node representing the selector.
    * @retval the created selector
    * @throws UserException if creation throws exception
    * @throws InternalException if creation throws exception
    */

    
   public static function createSelector( $inNode )
   {
      // Create a new selector instance. We have to look at the type first.
      $selectors = Environment::$sitemapStack->root()->getSelectors();
      $dom = new DOMDocument;
      $dom->appendChild( $dom->importNode( $inNode, 1 ) );

      $type = $inNode->getAttribute( "type" );
      // if type not present we get the default type
      if ( strlen( $type ) == 0 ) {
         $type = $selectors->getDefault();
      }

      try {
         $selector = $selectors->getSelector( $type );
         $selectorPackageName = $selector->getPackageName();
         return new $selectorPackageName( $dom, $type, "", $selector );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( InternalException $e ) {
         throw new InternalException( $e->getMessage(), $e->getCode() );
      }

   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create an action from the inputted node and returns the created action.
    *
    * @param $inNode the node representing the action.
    * @retval the created action
    * @throws UserException if creation throws exception
    * @throws InternalException if creation throws exception
    */
    
   public static function createAction( $inNode )
   {
      // Create a new action instance. We have to look at the type first.
      $actions = Environment::$sitemapStack->root()->getActions();
      $type = $inNode->getAttribute( "type" );
      // if type not present we get the default type
      if ( strlen( $type ) == 0 ) {
         $type = $actions->getDefault();
      }

      $dom = new DOMDocument;
      $dom->appendChild( $dom->importNode( $inNode, 1 ) );
      try {
         $action = $actions->getAction( $type );
         $actionPackageName = $action->getPackageName();
         return new $actionPackageName( $dom, $type, "", $action );
      } catch ( UserException $e ) {
         throw new UserException( $e->getMessage(), $e->getCode(), $dom );
      } catch ( InternalException $e ) {
         throw new InternalException( $e->getMessage(), $e->getCode() );
      }

   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a redirect-to from the inputted node and returns the created redirect.
    *
    * @param $inNode the node representing the redirect.
    * @retval Redirect the created redirect
    * @throws UserException if creation throws exception
    * @throws InternalException if creation throws exception
    */
    
   public static function createRedirect( $inNode )
   {
      // Create a new mount instance.
      try {
         $dom = new DOMDocument;
         $dom->appendChild( $dom->importNode( $inNode, 1 ) );
         return new Redirect( $dom, "", $inNode->getAttribute( "src" ) );
      } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( InternalException $e ) {
           throw new InternalException( $e->getMessage(), $e->getCode() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a mount from the inputted node and returns the created mount.
    *
    * @param $inNode DOMElement the node representing the mount.
    * @retval Mount the created mount
    * @throws UserException and InternalException
    */
    
   public static function createMount( $inNode )
   {
      // Create a new mount instance.
      try {
         $dom = new DOMDocument;
         $dom->appendChild( $dom->importNode( $inNode, 1 ) );
         return new Mount( $dom, "", $inNode->getAttribute( "src" ) );
      } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( InternalException $e ) {
           throw new InternalException( $e->getMessage(), $e->getCode() );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Create a call resouurce from the inputted node and returns the created call.
    *
    * @param $inNode the node representing the call.
    * @retval Call the created call
    * @throws UserException and InternalException
    */
    
   public static function createCall( $inNode )
   {
      // Create a new mount instance.
      try {
         $dom = new DOMDocument;
         $dom->appendChild( $dom->importNode( $inNode, 1 ) );
         return new Call( $dom, "", "", NULL );
      } catch ( UserException $e ) {
           throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
      } catch ( InternalException $e ) {
           throw new InternalException( $e->getMessage(), $e->getCode() );
      }
   }

  //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /**
    * Gets the class instance represented as a string.
    *
    * @retval string the string representation of this class instance.
    */
  
   public function toStringWithType( $inGenericType )
   {
      switch ( $inGenericType ) {
         case PipeElement::REDIRECT : $tagName = "redirect"; break;
         case PipeElement::GENERATOR : $tagName = "generate"; break;
         case PipeElement::AGGREGATOR : $tagName = "aggregate"; break;
         case PipeElement::MOUNT : $tagName = "mount"; break;
         case PipeElement::TRANSFORMER : $tagName = "transform"; break;
         case PipeElement::SERIALIZER : $tagName = "serialize"; break;
         case PipeElement::READER : $tagName = "read"; break;
         case PipeElement::SELECTOR : $tagName = "select"; break;
         case PipeElement::CALL : $tagName = "call"; break;
      }
   
      if ( !empty( $this->gParameters->getParameterList() ) ) {
         $parameterList = $this->gParameters->getParameterList();
         $mess = "     <$tagName type='{$this->gType}' package='" . get_class( $this ) . "'";
         if ( strlen( $this->gLabel ) ) {
            $mess .= " label='{$this->gLabel}'";
         }
         $mess .= ">\n";
         foreach ( $parameterList as $name => $value ) {
            $mess .= "       <parameter name='$name' value='$value'/>\n";
         }
         reset( $this->gParameters );
         $mess .= "     </$tagName>\n";
      } else {
         $mess = "     <$tagName type='{$this->gType}' package='" . get_class( $this ) . "'";
         if ( strlen( $this->gLabel ) ) {
            $mess .= " label='{$this->gLabel}'";
         }
         $mess .= "/>\n";
      }
      return $mess;
   }
   
}

?>
