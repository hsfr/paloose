<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage viewing
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/viewing/View.php" );

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>Views</i> class maintains a list of views.
 *
 * @package paloose
 * @subpackage viewing
 */

class Views {

   /** Array of views (each entry is a View class) and indexed by name */
   private $gViews;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>Views</i> class.
    * 
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gViews = array();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the view components.
    *
    * <pre>   &lt;map:views xmlns:map="http://apache.org/cocoon/sitemap/1.0" >
    *       &lt;map:view name="aggr" from-label="aggr-content">
    *          &lt;map:call name="xml2html"/>
    *       &lt;/map:view>
    *       ...
    *    &lt;/map:views></pre>
    *
    * @param DOMNode $inNode the DOM that contains the contents of this views element.
    * @throws UserException if duplicate View found
    * @throws UserException if View constructor throws UserException
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $viewsDom = new DOMDocument;
      $viewsDom->appendChild( $viewsDom->importNode( $inNode, 1 ) );

      // Set the namespace for the sitemap. Let us hope people use it.
      $xpath = new domxpath( $viewsDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      $view = $xpath->query( "//m:views/*" );
      // Scan around each view and set up the list in $gViews. Exception if problem.
      foreach ( $view as $node ) { 
         $name = $node->getAttribute( "name" );
         $fromLabel = $node->getAttribute( "from-label" );
         $fromPosition = $node->getAttribute( "from-position" );
         $this->gLogger->debug( "Found view [name:". $name . "] [from-label:" . $fromLabel . "] [from-position:" . $fromPosition . "]" );

         // We have something to add, so first of all check for duplicate names
         if ( $this->gViews != NULL and array_key_exists( $name, $this->gViews ) ) {
            $viewDom = new DOMDocument;
            $viewDom->appendChild( $viewDom->importNode( $node, 1 ) );
            throw new UserException( "Duplicate view name '" . $name . "' found", PalooseException::SITEMAP_VIEW_PARSE_ERROR, $viewDom );
         } else {
            try {
               $view = new View( $name, $fromLabel, $fromPosition, $node );
               $view->parse( $node );
               $this->gViews[ $name ] = $view;
               $this->gLogger->debug( "Adding view: '". $name . "' : " . count( $this->gViews) );
            } catch ( UserException $e ) {
               throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
            }
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the named view.
    *
     * @param string $inName the name of the node to fetch.
     * @retval View the view accessed by this name or NULL if view does not exist.
    */
    
   public function getView( $inName )
   {
      if ( array_key_exists( $inName, $this->gViews ) ) {
         return $this->gViews[ $inName ];
      }
      return NULL;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return a string representation of this class.
    *
    * @retval string the string representation.
   */
    
   public function toString()
   {
      $mess = "";
      if ( $this->gViews != null ) {
         $mess = "  <views>\n";
         foreach ( $this->gViews as $name => $view ) {
            $mess .= $view->toString();
         }
         $mess .= "  </views>\n";
      }
      return $mess;
   }

}

?>