<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
  * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage viewing
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * The <i>View</i> class maintains information on a single view. It stores the
 * label names as well as the enclosed pipe.
 *
 * @package paloose
 * @subpackage viewing
 */

class View {

    /** Logger instance for this class */   
   private $gLogger;

    /** Name of this view */   
   private $gName;

    /** from-label attribute */   
   private $gFromLabel;

    /** from-position attribute */
   private $gFromPosition;

    /** Pipeline associated with this view */
   private $gPipe;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct an instance of the <i>View</i> class.
    *
    * @param string $inName Name of this view.
    * @param string $inFromLabel Label from where the view is taken.
    * @param string $inFromPosition Position from where the view is taken.
    * @param DOMNode $inNode the DOM that contains the contents of this view.
    * @throws UserException if declaration does not have name and either from-label or from-position defined
    */

   public function __construct( $inName, $inFromLabel, $inFromPosition, DOMNode $inNode )
   {
       $this->gLogger = Logger::getLogger( __CLASS__ );
       $this->gName = $inName;
       $this->gFromLabel = $inFromLabel;
       $this->gFromPosition = $inFromPosition;
      if ( !( strlen( $inName ) > 0 and ( strlen( $inFromLabel ) > 0 or strlen( $inFromPosition ) > 0 ) ) ) {
           $viewDom = new DOMDocument;
           $viewDom->appendChild( $viewDom->importNode( $inNode, 1 ) );
           throw new UserException( "Sitemap view declaration must have name and either from-label or from-position defined",
                                     PalooseException::SITEMAP_VIEW_PARSE_ERROR, 
                                     $viewDom );
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the enclosed pipeline.
    *
    * Only a restricted set of pipeline components are allowed: transform, serialize,
    * and call.
    *
    * @param DOMNode $inNode the DOM that contains the view declaration.
    * @throws UserException if components other than restricted set appear in enclosed pipeline.
    * @throws UserException if exception from pipeline components.
    */
    
   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $viewDom = new DOMDocument;
      $viewDom->appendChild( $viewDom->importNode( $inNode, 1 ) );
      
      $xpath = new domxpath( $viewDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );

      // Scan the enclosed pipe
      $component = $xpath->query( "//m:view/*" );
      $this->gLogger->debug( "Making new View Pipe with " . count( $component ). " components" );
      $this->gPipe = new Pipe();
      foreach ( $component as $pipeNode ) {
         $name = $pipeNode->localName;
         $this->gLogger->debug( "pipe node: '". $name . "'" );
         // This is where we build a pipe. The validity of these pipes is carried out in that class (Pipe)
         switch ( $name ) {
            case "redirect-to" :
            case "mount" :
            case "aggregate" :
            case "generate" :
            case "read" :
                 throw new UserException( "View pipe cannot have generation/redirect components", PalooseException::SITEMAP_VIEW_PARSE_ERROR, $viewDom );

            case "transform" :
               try {
                  $transformer = PipeElement::createTransformer( $pipeNode );
                  $this->gLogger->debug( "Creating transformer: type='" . $transformer->getType() . "' src='" . $transformer->getSrc() . "' using component '" . get_class( $transformer ) . "'" );
                  // Put it into the pipe
                  $this->gPipe->addComponent( $transformer );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;

            case "serialize" :
               try {
                  $serializer = PipeElement::createSerializer( $pipeNode );
                  $this->gLogger->debug( "Creating serializer: type='" . $serializer->getType() . "' src='" . $serializer->getSrc() . "' using component '" . get_class( $serializer ) . "'" );
                  $this->gPipe->addComponent( $serializer );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
            
            
            case "call" :
               try {
                  $call = PipeElement::createCall( $pipeNode );
                  $this->gLogger->debug( "Creating call: name='" . $call->getResourceName() . "' using component '" . get_class( $call ) . "'" );
                  $this->gPipe->addComponent( $call );
               } catch ( UserException $e ) {
                  throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
               }
               break;
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the name of this view.
    *
    * @retval string the view name.
    */

   public function getName()
   {
      return $this->gName;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the "from-label" of this view.
    *
    * @retval string the view "from-label".
    */

   public function getFromLabel()
   {
      return $this->gFromLabel;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the "from-position" of this view.
    *
    * @retval string the view "from-position".
    */

   public function getFromPosition()
   {
      return $this->gFromPosition;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the enclosed pipeline of this view.
    *
    * @retval Pipe the enclosed pipe.
    */

   public function getPipe()
   {
      return $this->gPipe;
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return a string representation of this class.
    *
    * @retval string the string representation.
    */
    
   public function toString()
   {
      $mess = "   <view name='{$this->gName}'";
      if ( strlen( $this->gFromLabel ) > 0 )
         $mess .= " from-label='{$this->gFromLabel}'";
      if ( strlen( $this->gFromPosition ) > 0 )
         $mess .= " from-position='{$this->gFromPosition}'";
      $mess .= ">\n";
      $mess .= $this->gPipe->toString();
      $mess .= "  </view>\n";
      return $mess;
   }

}

?>