<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * <i>Continuations</i>
 *
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @copyright 2006 - 2011 Hugh Field-Richards
 */
 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
/**
 * Continuations
 *
 * This is the base class for all continuation scripts.
 * 
 * @package paloose
 * @subpackage flows
 */
 
class Continuations {

   /** Logger instance for this class */   
   private $gLogger;

   /** Continuation index */   
   protected $gContinuation;

   /** The form violations (keyed by the same keys as the data model). */
   private $gViolations;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Make an instance of the class.
    *
    * It sets the continuation session variable for this session.
    *
    * @param array $inFormModel reference to the data array for this flow.
    */
    
   function __construct( &$inFormModel )
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gLogger->debug( "Building script class" );
      
      $sessionModule = Environment::$modules[ 'session' ];
      $flowModule = Environment::$modules[ 'flow' ];
      $requestParameterModule = Environment::$modules[ 'request-param' ];
      $this->gLogger->debug( "Request parameters:\n" . $requestParameterModule->toString() );
      
      // First we must check for a session for this script. There must always be some form of session
      // running so that the session variables can store the data model.
      $sessionId = Environment::$sessionHandler->isValidSession();
      if ( $sessionId != '' ) {
          $this->gLogger->debug( "Valid session in progress" );
      } else {
         $this->gLogger->debug( "Creating Flow session" );
         $sessionModule = new SessionModule();
         Environment::$modules[ 'session' ] = $sessionModule;
      }
      
      $this->decodeFlowVariables( $inFormModel );

       // Reset session variable for this flow if first time. Set up flow module variable
       // here from session.
      if ( !Environment::$sessionHandler->isSessionVarSet( '__flowId' ) ) {
         $this->gContinuation = 0;
         $flowModule->add( '__flowId', time()  );
         $this->gLogger->debug( "Initialize flow, '__flowId': " . $flowModule->get( '__flowId' ) );
      } else {
         $flowModule->add( '__flowId', Environment::$sessionHandler->getVar( '__flowId' )  );
         $this->gLogger->debug( "Flow already running: " . Environment::$sessionHandler->getVar( '__flowId' ) );
      }

      // Are we going forward in the continuation ? 
      if ( strlen( $requestParameterModule->get( 'paloose-action-next' ) ) > 0 ) {
         // Yes - so let the continuation variable stand (points at next stage)
         $this->gLogger->debug( "Flow forward: " . $requestParameterModule->get( 'paloose-kont' ) );
         $this->gContinuation = $requestParameterModule->get( 'paloose-kont' );
      } else if ( strlen( $requestParameterModule->get( 'paloose-action-prev' ) ) > 0 ) {
         // We need to go backward - so update the continuation variable to point to previous stage
         // Remember that the variable points to the next stage here
         $this->gLogger->debug( "Flow backward: " );
         $requestParameterModule->add( 'paloose-kont', $requestParameterModule->get( 'paloose-kont' ) - 2 );
         $this->gContinuation = $requestParameterModule->get( 'paloose-kont' );
      } else {
         // Must be at start
         $this->gContinuation = 0;
      }
      
      $this->gLogger->debug( "continuation: " . $this->gContinuation );

      // Now take all the request parameters and put them into the flow module for this session.
      foreach ( $requestParameterModule->getParams() as $name => $value ) {
         $this->gLogger->debug( "  param->flow: '$name' = '$value'" );
         $flowModule->add( $name, $value );
      }
      // Now to transfer all the relevant parameters to the model (only those that are declared in the
      // model data structure. This is effectively what comes from the user form that has just been
      // filled in.
      foreach ( $flowModule->getParams() as $name => $value ) {
         if ( array_key_exists( $name, $inFormModel ) ) {
            $this->gLogger->debug( "  flow->session: '$name' = '$value'" );
            $inFormModel[ $name ] = $value;
         }
      }
      $this->encodeFlowVariables( $inFormModel );
   }
   
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Update the session variables for this flow.
    *
    * @param array $inFormModel reference to the data array for this flow.
    */

   private function encodeFlowVariables( &$inFormModel )
   {
      $sessionModule = Environment::$modules[ 'session' ];
      $flowModule = Environment::$modules[ 'flow' ];
        
      // Encode the model into a session variable
      $formData = "";
      foreach ( $inFormModel as $name => $value ) {
         $formData .= $name . ":=:" .$value . ";;";
      }
      $formData = trim( $formData, ";" );
      Environment::$sessionHandler->setVar( '__dataModel', $formData );
      
      // and the necessary flow variables (these are internal ones which the user does not know about)
      $flowData = "";
      foreach ( $flowModule->getParams() as $name => $value ) {
         $flowData .= $name . ":=:" .$value . ";;";
      }
      $flowData = trim( $flowData, ";" );
      Environment::$sessionHandler->setVar( '__flowData', $flowData );
      
      $this->gLogger->debug( "Flow module: " . $flowModule->toString() );
      $this->gLogger->debug( "Session module: " . $sessionModule->toString() );
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Decodes the session variables for this flow (__flowData and __dataModel).
    *
    * @param array $inFormModel reference to the data array for this flow.
    */

   private function decodeFlowVariables( &$inFormModel )
   {
      $sessionModule = Environment::$modules[ 'session' ];
      $flowModule = Environment::$modules[ 'flow' ];
        
      if ( Environment::$sessionHandler->isSessionVarSet( '__flowData' ) ) {
         $flowData = Environment::$sessionHandler->getVar( '__flowData' );
         $flowVarPairs = explode( ';;', $flowData );
         foreach ( $flowVarPairs as $pair ) {
            list( $name, $value ) = explode( ':=:', $pair );
            $flowModule->add( $name, $value );
         }
      }
        
      if ( Environment::$sessionHandler->isSessionVarSet( '__dataModel' ) ) {
         $dataModel = Environment::$sessionHandler->getVar( '__dataModel' );
         $dataVarPairs = explode( ';;', $dataModel );
         foreach ( $dataVarPairs as $pair ) {
            list( $name, $value ) = explode( ':=:', $pair );
            $inFormModel[ $name ] = $value;
         }
      }
      $this->gLogger->debug( "Flow module after decode:\n" . $flowModule->toString() );
      foreach ( $inFormModel as $name => $value ) {
         $this->gLogger->debug( "Data module: '$name':'$value'" );
      }
              
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Initialise continuations if not set.
    *
    * Sets up an initial (user chosen) value for the continuations if first time through this session.
    *
    * @param int $inValue the initial value.
    */

   protected function initContinuationIfNotSet( $inValue )
   {
      if ( !isset( $this->gContinuation ) or strlen( $this->gContinuation ) == 0 ) {
          $this->gContinuation = $inValue;
       }

   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Reset continuations to null.
    *
    * Sets up an initial (user chosen) value for the continuations if first time through this session.
    *
    * @param int $inValue the initial value.
    */

   protected function resetContinuation()
   {
       unset( $this->gContinuation );
    }


   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Send a page to the client. 
    *
    * A page is sent to the client and the information retrieved with appropriate
    * error checking. The big problem here is marking the violations. We have a list
    * in the parameter $inViolations which must be incorporated into the page
    * so that the form processor can display it.
    *
    * @param string $inPage the page to send to the client.
    * @param array $inModel the data model for the form information (pointer to it).
    * @param int $inContinuation the current sheet continuation (returned if error).
    * @param array $inViolations the list of violations.
    */

   protected function sendPage( $inPage, &$inFormModel, $inContinuation, $inViolations )
   {
      $this->gLogger->debug( "Sending page: " . $inPage );
      // Set the model into the "px" variable space.
      $flowModule = Environment::$modules[ "flow" ];
      foreach ( $inFormModel as $name => $value ) {
         $flowModule->add( $name, $value );
      }
      $flowModule->add( "continuation.id", $inContinuation );
      
      // We have to work a bit of magic here because we cannot pass XML strings to 
      // XSL as a parameter and get them parsed as an XML string. The solution is to pass
      // them a document node. However this does not seem to work so I pass it as an XML
      // string and let the XSL transformer sort it out - not nice but it works :-)
      $this->gLogger->debug( "Violations: '" . Utilities::arrayToXML( $inViolations, "violation-list" ) . "'" );
      $sessionModule = Environment::$modules[ "session" ];
      $sessionModule->add( "__violations", Utilities::arrayToXML( $inViolations, "violation-list" ) );
      $this->encodeFlowVariables( $inFormModel );

      // Resubmit to current sitemap
      $sitemap = Environment::$sitemapStack->peek();
      $sitemap->run( $inPage, "", true );
   }

}
?>
