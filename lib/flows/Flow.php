<?php

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * LICENSE:
 * 
 * Paloose is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program.  If not, see <http://www.gnu.org/licenses/> 
 *
 * @package paloose
 * @subpackage flows
 * @author Hugh Field-Richards <hsfr@hsfr.org.uk>
 * @version See {@link Paloose.php}
 * @license http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @copyright 2006 - 2011 Hugh Field-Richards
 */

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 */

require_once( PALOOSE_LIB_DIRECTORY . "/environment/Utilities.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/environment/Environment.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/resources/Resource.php" );
require_once( PALOOSE_LIB_DIRECTORY . "/flows/Script.php" );

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * Flow maintains a list of scripts.
 *
 * @package paloose
 * @subpackage flows
 */

class Flow {

   /** Array of scripts indexed by name */
   private $gScripts;

   /** The the language for this flow */
   private $gLanguage;

   /** Logger instance for this class */   
   private $gLogger;

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Construct a new instance of Flow.
    */

   public function __construct()
   {
      $this->gLogger = Logger::getLogger( __CLASS__ );
      $this->gScripts = array();
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Parse the contained script tags.
    *
    * @param DOMNode $inNode the sitemap node containing the scripts.
    * @throws UserException if cannot parse or duplicate script name.
    */

   public function parse( DOMNode $inNode )
   {
      // First thing is make node into a local DOM
      $scriptsDom = new DOMDocument;
      $scriptsDom->appendChild( $scriptsDom->importNode( $inNode, 1 ) );
      // Utilities::outputStringVerbatim( $scriptsDom->saveXML() );

      // Set the namespace for the sitemap. Let us hope people use it.
      $xpath = new domxpath( $scriptsDom );
      $xpath->registerNamespace( "m", Environment::$configuration[ 'sitemapNamespace' ] );
      $this->gLanguage = Utilities::getXPathListStringItem( 0, $xpath, "//@language" );
      if ( !isset( $this->gLanguage ) ) {
         $dom = new DOMDocument;
         $dom->appendChild( $dom->importNode( $inNode, 1 ) );
         throw new UserException( "Must be associated language attribute with flow", PalooseException::SITEMAP_FLOW_PARSE_ERROR, $dom );
      }
      $this->gLogger->debug( "Language: {$this->gLanguage}" );

      $scriptNodes = $xpath->query( "//m:script" );
      // Scan around each script and set up the list in $gScripts. Exception if problem.
      $idx = 0;
      $this->gLogger->debug( "Found {$scriptNodes->length} scripts" );
      foreach ( $scriptNodes as $node ) { 
         try {
            $script = new Script();
            $script->parse( $node );
            $this->gScripts[ $idx++ ] = $script;
         } catch ( UserException $e ) {
            throw new UserException( $e->getMessage(), $e->getCode(), $e->getDOMScrap() );
         }
      }
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Get the script associated with $inName.
    *
     * @param string $inName the name of the script to fetch.
     * @retval Script the script accessed by this name.
   */
    
   public function getScript( $inName )
   {
      return $this->gScripts[ $inName ];
   }

   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* 
   /**
    * Return a string representation of this class (used for debugging).
    *
    * @retval string the string representation.
    */
    
   public function toString()
   {
      $mess = "";
      if ( $this->gScripts != null ) {
         $mess = "  <flow>\n";
         foreach ( $this->gScripts as $name => $script ) {
            $mess .= $script->toString();
         }
         $mess .= "  </flow>\n";
      }
      return $mess;
   }

}

?>