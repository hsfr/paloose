# PALOOSE: COCOON WITHOUT JAVA

Paloose is a simplified version of Cocoon using PHP. It resulted from scratching a long standing personal
itch: that there are very few ISPs who will support Java/Tomcat for web sites, other than as a very 
expensive "professional" addition. Almost all will support PHP5 (sorry, Paloose does not use PHP4), so I 
decided to write my version of a simple, cut-down Cocoon in PHP5.
     
## Installation instructions

Installation is very simple it is just making sure that the Paloose directory is in the right place. There are two
files:

### paloose-latest.tgz

This is the full (including logging) system. You will require *log4php* (available from
[http://logging.apache.org/log4php/download.html](http://logging.apache.org/log4php/download.html). 
*log4php* can be put anywhere but 
generally I put it in the httpd documents folder, where the Paloose folders will end up.
It is important that in the configuration file of your site (`paloose-ini.xml`) that the
logging configuration variables are set: relative paths are from the Paloose server directory
and Paloose pseudo-protocols are forbidden here (see full documentation).

```
define( 'PALOOSE_LOG4PHP_DIRECTORY', '../log4php' );
define( 'PALOOSE_LOGGER_CONFIGURATION_FILE', '../pp/configs/Paloose.xml' );
```
Relative paths are from the Paloose server directory
and Paloose pseudo-protocols are forbidden here (see full documentation).

### paloose-compact-latest.tgz

This is the a stripped down system with no logging and no comments. It is done for
improving performance. You will not require log4php. However, if you are thinking of
developing for Paloose you will need a Ant plugin "PHPCompactor" which you can get from
BitBucket.
    
Once the downloads have finished loading to the folder of your choice, copy them to your 
Web documents folder (ROOT_DIR) and follow the commands below.

```
${ROOT_DIR}/paloose hsfr$ ls -l
total 200
-rw-r--r--   1 hsfr  wheel  101095 May 17 12:20 paloose-latest.tgz
${ROOT_DIR}/paloose hsfr$ tar zxvf paloose-latest.tgz
configs/
lib/
lib/environment/
...
${ROOT_DIR}/paloose hsfr$ rm paloose-latest.tgz
${ROOT_DIR}/paloose hsfr$ ls
configs        doc        log         lib        resources
${ROOT_DIR}/paloose hsfr$ 
```

Basically that is it. Read the documentation in the doc folder
(`palosseDocs.pdf`) which is an
automatic LaTeX translation of the main Paloose Web site.

The only thing that must be done to get Paloose to run is to make
 sure that your ISP is happy to run PHP5
with the correct XML parser. For example, when I built my PHP5 
on my local server the configure command was 
run with `--with-xsl`. The important thing is that you must 
not use `--with-xslt-sablot`. You need to turn 
off Sablotron support and use libxml.

Configuring Paloose is described in the PDF documentation 
(in `doc` folder) and requires Paloose to be told
where the various components are stored. However a standard 
install required very little changes.

There are still a few documentation holes to be filled which will be done as soon as I have the time.

Any questions, bug-reports or suggestions to me, 
Hugh Field-Richards <hsfr@hsfr.org.uk>. I have been using 
Paloose on several sites for a number of 
years and it is very stable. It is easily 
extensible (PHP and XML/XSL experience required) 
and any contributions would be welcome.

